//
//  PharmacyDetailsViewController.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 22/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PharmacyDetailsViewController.h"
#import "AppDelegate.h"
#import "Webservice.h"
#import "MenuList.h"
#import "LeftViewController.h"
#import "PopOverViewController.h"
#import "AddPharmacistVCTableViewCell.h"
#import "LeftViewController.h"
#import "NSDictionary+Dictionary.h"
#import "MBProgressHUD.h"
#import <SDWebImage/SDImageCache.h>
#import "UIImageView+WebCache.h"
@interface PharmacyDetailsViewController (){
    int totalPharmacistsCount;
    int addedPharmacistsCount;
    int pharmacistsdeniedInfoCount;
}
@property(nonatomic,assign)BOOL framesAreSet;
@property(nonatomic,assign)BOOL isFromNewPharmacist;
@property(nonatomic,assign)BOOL isFromBuildingPicSelection;
@property (nonatomic,strong) LeftViewController *leftVC;
@end

@implementation PharmacyDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTopButtons];
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    [self setDatePickerFields];
}

-(void)setDatePickerFields{
    LeftViewController *lVC = (LeftViewController *)self.delegate;
    CGRect frame = lVC.view.frame;
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.pharmacistScrollView.frame.size.width, 200)];
    UIDatePicker *datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40 , frame.size.width, 160)];
    datePicker.tag = 10;
    [datePicker setDatePickerMode:UIDatePickerModeTime];
    [inputView addSubview:datePicker];
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 70, 50)];
    doneButton.tag = 10;
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [inputView addSubview:doneButton];
    [doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.shiftTimingsFromTextField.inputView = inputView;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    UIView *inputView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.pharmacistScrollView.frame.size.width, 200)];
    UIDatePicker *datePicker1 = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40 , frame.size.width, 160)];
    [datePicker1 setDatePickerMode:UIDatePickerModeTime];
    datePicker1.tag = 20;
    [inputView1 addSubview:datePicker1];
    UIButton *doneButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 70, 50)];
    doneButton1.tag = 20;
    [doneButton1 setTitle:@"Done" forState:UIControlStateNormal];
    [inputView1 addSubview:doneButton1];
    [doneButton1 addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
    self.shiftTimingsToTextField.inputView = inputView1;
    [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
}
-(void)doneButton:(UIButton *)sender{
    if(sender.tag == 10){
        [self.shiftTimingsFromTextField resignFirstResponder];
    }
    else if(sender.tag == 20){
        [self.shiftTimingsToTextField resignFirstResponder];
    }
}
-(void)datePickerValueChanged:(id)sender{
    UIDatePicker *datePicker = (UIDatePicker *)sender;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:TIME_FORMATTER_STRING];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    if(datePicker.tag==10)
        self.shiftTimingsFromTextField.text = strDate;
    else if(datePicker.tag==20){
         self.shiftTimingsToTextField.text = strDate;
    }
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!self.framesAreSet) {
        self.framesAreSet = YES;
    }else{
        CGRect tmpFrame = self.scoreView.frame;
        self.scoreView.layer.cornerRadius = tmpFrame.size.height/2;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    //[MenuList networkChecking];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    if (!self.isFromBuildingPicSelection) {
        [self setUpPharmacyDetails];
        self.finalLocationDict = [[NSMutableDictionary alloc] init];
        self.currentLocationDict = [[NSMutableDictionary alloc] init];
        self.selectedLocationDict = [[NSMutableDictionary alloc] init];
        self.selectedPharmacist = [[NSDictionary alloc]init];
        self.mapView.delegate = self;
        self.mapView.userInteractionEnabled = YES;
        UILongPressGestureRecognizer *mapViewGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longpressToGetLocation:)];
        [self.mapView addGestureRecognizer:mapViewGesture];
        self.leftVC = (LeftViewController *)self.delegate;
    }else{
        self.isFromBuildingPicSelection = NO;
    }
    self.buildingPicImageView.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.XYDetailsView.backgroundColor = self.view.backgroundColor;
    self.submitBtn.layer.cornerRadius = 4.0;
    self.pharmacySubmitBtn.layer.cornerRadius = 4.0;
    self.pharmacistSubmitBtn.layer.cornerRadius = 4.0;
    self.topView.backgroundColor = [UIColor clearColor];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.pharmacistsTbl.backgroundColor = [UIColor clearColor];
    self.pharmacistsTbl.separatorStyle = UITableViewCellSeparatorStyleNone;
    UINib *cellNib = [UINib nibWithNibName:@"AddPharmacistVCTableViewCell" bundle:nil];
    [self.pharmacistsTbl registerNib:cellNib forCellReuseIdentifier:@"AddPharmacistVCTableViewCellIdentifier"];
    if ([self.passedPharmacyString isEqualToString:@"PHARMACY"]) {
        [self.pharmacyTopButton setBackgroundColor:[appDelegate colorWithHexString:YellowColor]];
        [self.pharmacistTopButton setBackgroundColor:[UIColor clearColor]];
        self.pharmacyTopButton.selected = YES;
        self.pharmacistTopButton.selected = NO;
        self.XYDetailsView.hidden = YES;
        self.pharmacyView.hidden = NO;
        self.pharmacistView.hidden = YES;
    }else if ([self.passedPharmacyString isEqualToString:@"PHARMACIST"]){
        [self.pharmacistTopButton setBackgroundColor:[appDelegate colorWithHexString:YellowColor]];
        [self.pharmacyTopButton setBackgroundColor:[UIColor clearColor]];
        self.pharmacyTopButton.selected = NO;
        self.pharmacistTopButton.selected = YES;
        self.XYDetailsView.hidden = NO;
        self.pharmacyView.hidden = YES;
        self.pharmacistView.hidden = YES;
        if(!self.XYDetailsView.superview){
            [self.view addSubview:self.XYDetailsView];
            [NSLayoutConstraint activateConstraints: @[
                                                       [self.XYDetailsView.topAnchor constraintEqualToAnchor:self.horizontalLineView.bottomAnchor],
                                                       [self.XYDetailsView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                       [self.XYDetailsView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                       [self.XYDetailsView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                       ]];
        }
        self.XYDetailsView.translatesAutoresizingMaskIntoConstraints = FALSE;
        [self reloadPharmacistsList];
    }
}

-(void)showProgressView{
    //offline
    [self.mbProgressView hideAnimated:YES];
    //offline
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = @"Loading...";
}

-(void)setUpPharmacyDetails{
    self.pharmacyNameTextField.text = [self.passedPharmacyDict stringForKey:@"PharmacyName"];
    self.address1TextField.text = [self.passedPharmacyDict stringForKey:@"Address1"];
    self.address2TextField.text = [self.passedPharmacyDict stringForKey:@"Address2"];
    self.cityTextField.text = [self.passedPharmacyDict stringForKey:@"City"];
    self.countryTextField.text = [self.passedPharmacyDict stringForKey:@"Country"];
    self.ownerNameTextField.text = [self.passedPharmacyDict stringForKey:@"OwnerName"];
    self.noOfPhysiciansTextField.text = [self.passedPharmacyDict stringForKey:@"NoOfPharmacists"];
    self.landlineNumberTextField.text = [self.passedPharmacyDict stringForKey:@"LandLineNumber1"];
    self.landlineNumberTextField2.text = [self.passedPharmacyDict stringForKey:@"LandLineNumber2"];
    self.landlineNumberTextField3.text = [self.passedPharmacyDict stringForKey:@"LandLineNumber3"];
    self.buildingPicImageView.image = nil;
    if (![[self.passedPharmacyDict stringForKey:@"BuildingPic"] isEqualToString:@""]) {
        //[self.buildingPicImageView sd_setImageWithURL:[NSURL URLWithString:[self.passedPharmacyDict stringForKey:@"BuildingPic"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
           UIImage *imagename = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self.passedPharmacyDict stringForKey:@"BuildingPic"]]]];
            // Then to set the image it must be done on the main thread
            dispatch_sync( dispatch_get_main_queue(), ^{
                if (imagename) {
                    [self.buildingPicImageView setImage: imagename];
                }else{
                    NSData *data = [[NSData alloc]initWithBase64EncodedString:[self.passedPharmacyDict stringForKey:@"BuildingPic"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                    if (data) {
                        self.buildingPicImageView.image = [UIImage imageWithData:data];
                    }
                }
            });
        });
    }else{
        self.buildingPicImageView.image = self.buildingPicImage;
    }
    float latitudeString = (float)[[self.passedPharmacyDict valueForKey:LATITUDE_STRING] floatValue];
    float longitudeString = (float)[[self.passedPharmacyDict valueForKey:LONGITUDE_STRING] floatValue];
    if (latitudeString && longitudeString) {
        [self.locationManager stopUpdatingLocation];
        self.mapView.delegate = self;
        float spanX = 0.1;
        float spanY = 0.1;
        [self.mapView removeAnnotations:self.mapView.annotations];
        CLLocation *locationAtual = [[CLLocation alloc] initWithLatitude:latitudeString longitude:longitudeString];
        MKCoordinateSpan span = self.mapView.region.span;
        span.latitudeDelta=spanX;
        span.longitudeDelta=spanY;
        MKCoordinateRegion region = MKCoordinateRegionMake(locationAtual.coordinate, span);
        self.mapView.region = region;
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitudeString, longitudeString);
        point.coordinate = coordinate;
        point.title = @"Where am I?";
        point.subtitle = @"selected loaction!";
        region = MKCoordinateRegionMakeWithDistance(coordinate, 400, 400);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
        [self.mapView removeAnnotations:self.mapView.annotations];
        [self.mapView addAnnotation:point];
    }else{
        self.mapView.delegate = self;
        self.mapView.showsUserLocation = YES;
        [self.locationManager startUpdatingLocation];
    }
}
-(void)addXYDetailsView{
    [self.leftVC showHideNavigationItems];
    [self.pharmacistTopButton setBackgroundColor:[appDelegate colorWithHexString:YellowColor]];
    [self.pharmacyTopButton setBackgroundColor:[UIColor clearColor]];
    self.pharmacyTopButton.selected = NO;
    self.pharmacistTopButton.selected = YES;
    self.XYDetailsView.hidden = NO;
    self.pharmacyView.hidden = YES;
    self.pharmacistView.hidden = YES;
    if(!self.XYDetailsView.superview){
        [self.view addSubview:self.XYDetailsView];
        [NSLayoutConstraint activateConstraints: @[
                                                   [self.XYDetailsView.topAnchor constraintEqualToAnchor:self.horizontalLineView.bottomAnchor],
                                                   [self.XYDetailsView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                   [self.XYDetailsView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                   [self.XYDetailsView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                   ]];
    }
    
    self.XYDetailsView.translatesAutoresizingMaskIntoConstraints = FALSE;
    [self reloadPharmacistsList];
}
- (IBAction)finalSubmitAction:(id)sender {
    //int deniedCount = [self.pharmacistDeniedInfoCountLbl.text intValue];
    if (addedPharmacistsCount == totalPharmacistsCount) {
        NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
        NSString *baseURLString = PHARMACY_FINAL_SUBMIT_NEW_MISSION;
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,baseURLString,mobileNumber,appDelegate.pharmacyId];
        [Webservice getWithUrlString:urlString success:^(id data) {
            if (data) {
                NSDictionary *dict = (NSDictionary *)data;
                if ([[dict stringForKey:@"Message"] isEqualToString:@"SUCCESS"]) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:@"Details submited successfully."preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    if ([self.delegate respondsToSelector:@selector(PHRSubmitSucessfull)]) {
                        [self.delegate PHRSubmitSucessfull];
                    }
                }
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        } failure:^(NSError *error) {
            //Offline
            /*
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
             UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
             [alert addAction:alertAction];
             [self presentViewController:alert animated:YES completion:nil];
             */
            
            NSLog(@"Error: %@",error.localizedDescription);
            if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
                if(!appDelegate.finalNewPharmacyArray){
                    appDelegate.finalNewPharmacyArray = [NSMutableArray new];
                }
                int i;
                for (i=0; i<appDelegate.finalNewPharmacyArray.count; ++i) {
                    NSString *strID = appDelegate.finalNewPharmacyArray[i];
                    if([strID isEqualToString:appDelegate.pharmacyId]){
                        break;
                    }
                }
                if(i==appDelegate.finalNewPharmacyArray.count){
                    [appDelegate.finalNewPharmacyArray addObject:[NSString stringWithString:appDelegate.pharmacyId]];
                }
                else{
                    [appDelegate.finalNewPharmacyArray replaceObjectAtIndex:i withObject:appDelegate.pharmacyId];
                }
                
                //remvove from current list as well.
                for(NSDictionary *dic in appDelegate.pharmacistNewMissionCurrentPharmacistList){
                    if([dic[@"PharmacyId"] isEqualToString:appDelegate.pharmacyId]){
                        [appDelegate.pharmacistNewMissionCurrentPharmacistList removeObject:dic];
                        break;
                    }
                }
                //remvove from current list as well.
                
                if ([self.delegate respondsToSelector:@selector(PHRSubmitSucessfull)]) {
                    [self.delegate PHRSubmitSucessfull];
                }
                NSLog(@"final submit pharmacy saved : %ld",appDelegate.finalNewPharmacyArray.count);
                
                
            }
            //Offline
            
        }];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:ENTER_ALL_PHARMACISTS_DETAILS_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
-(void)reloadPharmacistsList{
    [self showProgressView];
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = PHARMACY_ALL_PHARMACIST_LIST;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,baseURLString,mobileNumber,appDelegate.pharmacyId];
    [Webservice getWithUrlString:urlString success:^(id data) {
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.pharmacistsListArray = (NSMutableArray*)data;
            self.totalPharmacistsCountLbl.text = [NSString stringWithFormat:@"/%@",[self.passedPharmacyDict stringForKey:@"NoOfPharmacists"]];
            totalPharmacistsCount = [[self.passedPharmacyDict stringForKey:@"NoOfPharmacists"] intValue];
            self.addedPharmacistsCountLbl.text = [NSString stringWithFormat:@"%ld",self.pharmacistsListArray.count];
            addedPharmacistsCount = self.pharmacistsListArray.count;
            if ([self.pharmacistsListArray count] > 0) {
                NSDictionary *pharmacistDict = (NSDictionary*)[self.pharmacistsListArray firstObject];
                self.pharmacistDeniedInfoCountLbl.text = [pharmacistDict stringForKey:@"PharmacistDeniedCount"];
            }else{
                self.pharmacistDeniedInfoCountLbl.text = @"0";
            }
            self.pharmacyNameLbl.text = [self.passedPharmacyDict stringForKey:@"PharmacyName"];
            [self.pharmacistsTbl reloadData];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        [self.mbProgressView hideAnimated:YES];
        //Offline
        /*
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:alertAction];
         [self presentViewController:alert animated:YES completion:nil];
         */
        
        NSLog(@"Error: %@",error.localizedDescription);
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
            self.pharmacistsListArray =  [NSMutableArray arrayWithArray:self.passedPharmacyDict[@"PharmacistTicketMaster"]];
            // (NSMutableArray*)self.passedPharmacyDict[@"PharmacistTicketMaster"];
            //Add offline save pharamcist to  this list as well....
            for (NSDictionary *dicPharmacist in appDelegate.pharmacistNewMissionArray){
                if(!self.pharmacistsListArray){
                    self.pharmacistsListArray = [NSMutableArray new];
                }
                if([ dicPharmacist[@"PharmacyId"] isEqualToString : self.passedPharmacyDict[@"PharmacyId"]]){
                    
                    BOOL already = NO;
                    for (NSDictionary *dic in self.pharmacistsListArray){
                        if ([dicPharmacist[@"EmailID1"] isEqualToString:dic[@"EmailID1"]]){
                            already = YES;
                        }
                    }
                    if(!already){
                        [self.pharmacistsListArray addObject:dicPharmacist];
                        NSLog(@"count of list now : %ld",self.pharmacistsListArray.count);
                    }
                    
                }
            }
            //Add offline save pharamcist to  this list as well....
            
            self.totalPharmacistsCountLbl.text = [NSString stringWithFormat:@"/%@",[self.passedPharmacyDict stringForKey:@"NoOfPharmacists"]];
            totalPharmacistsCount = [[self.passedPharmacyDict stringForKey:@"NoOfPharmacists"] intValue];
            self.addedPharmacistsCountLbl.text = [NSString stringWithFormat:@"%ld",self.pharmacistsListArray.count];
            addedPharmacistsCount = self.pharmacistsListArray.count;
            if ([self.pharmacistsList count] > 0) {
                __unused NSDictionary *pharmacistDict = (NSDictionary*)[self.pharmacistsListArray firstObject];
                self.pharmacistDeniedInfoCountLbl.text = [pharmacistDict stringForKey:@"PharmacistDeniedCount"];
            }else{
                self.pharmacistDeniedInfoCountLbl.text = @"0";
            }
            self.pharmacyNameLbl.text = [self.passedPharmacyDict stringForKey:@"PharmacyName"];
            [self.pharmacistsTbl reloadData];
            
        }
        //Offline

    }];
}

-(NSDictionary *)getpharmacyPostData{
    [self.view endEditing:YES];
    NSData *imgData = self.BuildingPicData.length?self.BuildingPicData:self.serviceBuildingPicdata.length?self.serviceBuildingPicdata:nil;
    NSDictionary *postDataDictionary = @{
                               @"PharmacyId":appDelegate.pharmacyId,
                               @"PharmacyName":[self.passedPharmacyDict objectForKey:@"PharmacyName"],
                               @"Address1":self.address1TextField.text,
                               @"Address2":self.address2TextField.text,
                               @"City":self.cityTextField.text,
                               @"Country":self.countryTextField.text,
                               @"LandLineNumber1":self.landlineNumberTextField.text,
                               @"LandLineNumber2":self.landlineNumberTextField2.text?self.landlineNumberTextField2.text:@"",
                               @"LandLineNumber3":self.landlineNumberTextField3.text?self.landlineNumberTextField3.text:@"",
                               @"BuildingPic":[imgData base64EncodedStringWithOptions:NSUTF8StringEncoding]?[imgData base64EncodedStringWithOptions:NSUTF8StringEncoding]:@"",
                               @"OwnerName":self.ownerNameTextField.text?self.ownerNameTextField.text:@"",
                               @"NoOfPharmacists":self.noOfPhysiciansTextField.text,
                               @"Latitude":[self.finalLocationDict stringForKey:LATITUDE_STRING],
                               @"Longitude":[self.finalLocationDict stringForKey:LONGITUDE_STRING]
                               //offline
                               ,
                               @"PharmacistTicketMaster":[self.passedPharmacyDict objectForKey:@"PharmacistTicketMaster"],
                               @"CompletionDate":[self.passedPharmacyDict objectForKey:@"CompletionDate"],
                               @"MissionTakenDate":[self.passedPharmacyDict objectForKey:@"MissionTakenDate"],
                               @"MissionType":[self.passedPharmacyDict objectForKey:@"MissionType"]
                               //offline
                               };
    return postDataDictionary;
}

-(NSDictionary *)getpharmacistPostData{
    [self.view endEditing:YES];
    NSDictionary *dict = @{
                           @"PharmacistTransactionID":appDelegate.pharmacistTransactionID?:@"",
                           @"PharmacyId":appDelegate.pharmacyId,
                           @"FirstName":self.firstNameTextField.text?:@"",
                           @"LastName":self.lastNameTextField.text?:@"",
                           @"MobileNumber1":self.mobileNo1TextField.text?:@"",
                           @"MobileNumber2":self.mobileNo2TextField.text?:@"",
                           @"MobileNumber3":self.mobileNo3TextField.text?:@"",
                           @"EmailID1":self.emailNo1TextField.text?:@"",
                           @"EmailID2":self.emailNo2TextField.text?:@"",
                           @"EmailID3":self.emailNo3TextField.text?:@"",
                           @"YearOfGraduation":self.yearOfGragTextField.text?:@"",
                           @"ShiftTimingsFrom":self.shiftTimingsFromTextField.text?:@"",
                           @"ShiftTimingsTo":self.shiftTimingsToTextField.text?:@"",
                           @"Consent1Id":@"1",
                           @"Consent2Id":@"2",
                           @"Consent3Id":@"3"
                           };
    return dict;

}
- (IBAction)pharmacyDetailsSubmitAction:(id)sender {
    if ([self.passedPharmacyDict objectForKey:LATITUDE_STRING]){
        [self.finalLocationDict setObject:[self.passedPharmacyDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
    }if ([self.currentLocationDict objectForKey:LATITUDE_STRING]) {
        [self.finalLocationDict setObject:[self.currentLocationDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
    }if ([self.selectedLocationDict objectForKey:LATITUDE_STRING]){
        [self.finalLocationDict setObject:[self.selectedLocationDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
    }
    if([self.passedPharmacyDict objectForKey:LONGITUDE_STRING]){
        [self.finalLocationDict setObject:[self.passedPharmacyDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
    }
    if ([self.currentLocationDict objectForKey:LONGITUDE_STRING]) {
        [self.finalLocationDict setObject:[self.currentLocationDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
    }if([self.selectedLocationDict objectForKey:LONGITUDE_STRING]){
        [self.finalLocationDict setObject:[self.selectedLocationDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
    }

    self.BuildingPicData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.buildingPicImage, 0.1)];
    if (![[self.passedPharmacyDict stringForKey:@"BuildingPic"] isEqualToString:@""]) {
        self.serviceBuildingPicdata = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[self.passedPharmacyDict stringForKey:@"BuildingPic"]]];
    }
    if ([self isFinalValidationsOkToPostData]) {
        [self showProgressView];
        NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
        NSString *baseURLString = PHARMACY_ADD_PHARMACY_NEW_MISSION;
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
        //offline
        NSDictionary *dicPharmacistData = [self getpharmacyPostData];
        //offline
        [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
            //Success
            [self.mbProgressView hideAnimated:YES];
            if (data) {
                self.passedPharmacyDict = (NSDictionary*)data;
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:PHARMACY_DETAILS_SUBMIT_SUCCESS preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [self setUpPharmacyDetails];
                }];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            //Offline
            /*
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
             UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
             [alert addAction:alertAction];
             [self presentViewController:alert animated:YES completion:nil];
             */
            NSLog(@"Error: %@",error.localizedDescription);
            if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
                AppDelegate *delG =  appDelegate;
                if(!delG.pharmacyNewMissionArray){
                    delG.pharmacyNewMissionArray = [NSMutableArray new];
                }
                int i=0;
                for(i=0; i<delG.pharmacyNewMissionArray.count;++i){ //PharmacyId
                    NSDictionary *dic = (NSDictionary *)delG.pharmacyNewMissionArray[i];
                    if([dic[@"PharmacyId"] isEqualToString:self.passedPharmacyDict[@"PharmacyId"]]){
                        [delG.pharmacyNewMissionArray replaceObjectAtIndex:i withObject:dicPharmacistData];
                        break;
                    }
                    
                }
                if(i == delG.pharmacyNewMissionArray.count){
                    [delG.pharmacyNewMissionArray addObject:dicPharmacistData];
                }
                //replace in current list as well
                for(i=0; i<delG.pharmacistNewMissionCurrentPharmacistList.count;++i){ //PharmacyId
                    NSDictionary *dic = (NSDictionary *)delG.pharmacistNewMissionCurrentPharmacistList[i];
                    if([dic[@"PharmacyId"] isEqualToString:self.passedPharmacyDict[@"PharmacyId"]]){
                        [delG.pharmacistNewMissionCurrentPharmacistList replaceObjectAtIndex:i withObject:dicPharmacistData];
                        break;
                    }
                    
                }
                self.passedPharmacyDict =  [NSMutableDictionary dictionaryWithDictionary:dicPharmacistData] ;
                [self setUpPharmacyDetails];
                NSLog(@"pharmacy Saved : %ld",delG.pharmacyNewMissionArray.count);
                [self addXYDetailsView];
            }
            //Offline
        }];
    }
}

- (IBAction)submitBtnAction:(id)sender {
    [self.view endEditing:YES];
    if ([self isFinalValidationsOkToPostData]) {
        [self showProgressView];
        NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
        NSString *baseURLString = PHARMACY_ADD_PHARMACIST_NEW_MISSION;
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
        //offline
        NSDictionary *dicPharmacistData = [self getpharmacistPostData];
        //offline
        [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
            //Success
            [self.mbProgressView hideAnimated:YES];
            if (data) {
                if (![[data stringForKey:@"PharmacistTransactionID"] isEqualToString:@"0"] && ![[data stringForKey:@"PharmacistTransactionID"] isEqualToString:@""]) {
                    appDelegate.pharmacistTransactionID = [data stringForKey:@"PharmacistTransactionID"];
                }
                if (self.isFromNewPharmacist) {
                    NSMutableDictionary *pharmacistInfo = [NSMutableDictionary dictionaryWithDictionary:data];
                    if ([[data stringForKey:@"FirstName"] isEqualToString:@""] && [[data stringForKey:@"LastName"] isEqualToString:@""] && [[data stringForKey:@"EmailID1"] isEqualToString:@""]) {
                        [pharmacistInfo setObject:self.firstNameTextField.text forKey:@"FirstName"];
                        [pharmacistInfo setObject:self.lastNameTextField.text forKey:@"LastName"];
                        [pharmacistInfo setObject:self.emailNo1TextField.text forKey:@"EmailID1"];
                    }
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:PHARMACIST_DETAILS_SUBMIT_SUCCESS preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        if ([self.delegate respondsToSelector:@selector(PHRAddConsent:)]) {
                            [self.delegate PHRAddConsent:pharmacistInfo];
                        }
                    }];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:PHARMACIST_DETAILS_EDIT preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        [self addXYDetailsView];
                    }];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            //Offline
            /*
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
             UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
             [alert addAction:alertAction];
             [self presentViewController:alert animated:YES completion:nil];
             */
            NSLog(@"Error: %@",error.localizedDescription);
            if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
                AppDelegate *delG =  appDelegate;
                if(!delG.pharmacistNewMissionArray){
                    delG.pharmacistNewMissionArray = [NSMutableArray new];
                }
                //check for duplicacy here before adding.
                if(self.isFromNewPharmacist){
                    [delG.pharmacistNewMissionArray addObject:dicPharmacistData];
                }else{
                    //finding based on email id as pharmacist id still not generated.
                    BOOL isAdded = NO;
                    for(NSDictionary *dic in delG.pharmacistNewMissionArray){
                        if([dic[@"PharmacyId"] isEqualToString:dicPharmacistData[@"PharmacyId"]] &&
                           [dic[@"EmailID1"] isEqualToString:dicPharmacistData[@"EmailID1"]]){
                            NSUInteger index = [delG.pharmacistNewMissionArray indexOfObject:dic];
                            [delG.pharmacistNewMissionArray replaceObjectAtIndex:index withObject:dicPharmacistData];
                            isAdded = YES;
                            break;
                        }
                    }
                    if(!isAdded){
                        [delG.pharmacistNewMissionArray addObject:dicPharmacistData];
                        [delG.pharmacistNewMissionConsentArray addObject:[NSNumber numberWithBool:NO]];
                    }
                    //replace existing..
                }
                
                //Add this pharmacist in the current list of pharmacy.
                NSMutableDictionary *dicToBeReplaced;
                NSMutableArray *arrArrToBeReplaced;
                NSUInteger index = -1;
                for(NSDictionary *dic in delG.pharmacistNewMissionCurrentPharmacistList){
                    if([dic[@"PharmacyId"] isEqualToString:self.passedPharmacyDict[@"PharmacyId"]]){
                        index = [delG.pharmacistNewMissionCurrentPharmacistList indexOfObject:dic];
                        dicToBeReplaced = [NSMutableDictionary dictionaryWithDictionary:dic];
                        if(dic[@"PharmacistTicketMaster"]){
                            arrArrToBeReplaced = [NSMutableArray arrayWithArray:dic[@"PharmacistTicketMaster"]];
                            if(self.isFromNewPharmacist){
                                [arrArrToBeReplaced addObject:dicPharmacistData];
                            }else{
                                //finding based on email id as pharmacist id still not generated.
                                
                                for(NSDictionary *dic in arrArrToBeReplaced){
                                    if([dic[@"PharmacyId"] isEqualToString:dicPharmacistData[@"PharmacyId"]] &&
                                       [dic[@"EmailID1"] isEqualToString:dicPharmacistData[@"EmailID1"]]){
                                        NSUInteger index = [arrArrToBeReplaced indexOfObject:dic];
                                        [arrArrToBeReplaced replaceObjectAtIndex:index withObject:dicPharmacistData];
                                        break;
                                    }
                                }
                                //replace existing..
                            }
                            
                        }else{
                            arrArrToBeReplaced = [NSMutableArray arrayWithObject:dicPharmacistData];
                        }
                        dicToBeReplaced[@"PharmacistTicketMaster"] = arrArrToBeReplaced;
                        break;
                    }
                }
                if(index != -1){
                    [delG.pharmacistNewMissionCurrentPharmacistList replaceObjectAtIndex:index withObject:dicToBeReplaced];
                    self.passedPharmacyDict = dicToBeReplaced;
                }
                NSLog(@"pharmacist Saved");
                if (self.isFromNewPharmacist) {
                    if ([self.delegate respondsToSelector:@selector(PHRAddConsent:)]) {
                        [self.delegate PHRAddConsent:[NSMutableDictionary dictionaryWithDictionary:dicPharmacistData]];
                    }
                }else{
                    [self addXYDetailsView];
                }
            }
            //Offline
        }];
    }
}

- (IBAction)addNewPharmacistAction:(id)sender {
    if (addedPharmacistsCount < totalPharmacistsCount) {
        self.isFromNewPharmacist = YES;
        [self.leftVC addNavigationItem];
        self.pharmacyView.hidden = YES;
        self.XYDetailsView.hidden = YES;
        self.pharmacistView.hidden = NO;
        if(!self.pharmacistView.superview){
            [self.view addSubview:self.pharmacistView];
            [NSLayoutConstraint activateConstraints: @[
                                                       [self.pharmacistView.topAnchor constraintEqualToAnchor:/*self.horizontalLineView.bottomAnchor*/self.view.topAnchor],
                                                       [self.pharmacistView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                       [self.pharmacistView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                       [self.pharmacistView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                       ]];
        }
        self.pharmacistView.translatesAutoresizingMaskIntoConstraints = FALSE;
    [self setUpPharmacistDetails:nil];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:CHANGE_THE_NUMBER_OF_PHARMACISTS_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.pharmacistsListArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier =@"AddPharmacistVCTableViewCellIdentifier";
    AddPharmacistVCTableViewCell *cell =
    (AddPharmacistVCTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = self.view.backgroundColor;
    NSDictionary *pharmacistDict = [self.pharmacistsListArray objectAtIndex:indexPath.row];
    cell.pharmacistNameLbl.text = [NSString stringWithFormat:@"%@ %@",[pharmacistDict stringForKey:@"FirstName"],[pharmacistDict stringForKey:@"LastName"]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedPharmacist = [self.pharmacistsListArray objectAtIndex:indexPath.row];
    [self.leftVC addNavigationItem];
    self.pharmacyView.hidden = YES;
    self.XYDetailsView.hidden = YES;
    self.pharmacistView.hidden = NO;
    self.isFromNewPharmacist = NO;
    if(!self.pharmacistView.superview){
        [self.view addSubview:self.pharmacistView];
        [NSLayoutConstraint activateConstraints: @[
                                                   [self.pharmacistView.topAnchor constraintEqualToAnchor:/*self.horizontalLineView.bottomAnchor*/self.view.topAnchor],
                                                   [self.pharmacistView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                   [self.pharmacistView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                   [self.pharmacistView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                   ]];
    }
    self.pharmacistView.translatesAutoresizingMaskIntoConstraints = FALSE;
    [self setUpPharmacistDetails:self.selectedPharmacist];
}
-(void)setUpPharmacistDetails:(NSDictionary*)pharmacist{
    appDelegate.pharmacistTransactionID = [pharmacist stringForKey:@"PharmacistTransactionID"]?:@"";
    self.firstNameTextField.text = [pharmacist stringForKey:@"FirstName"];
    self.lastNameTextField.text = [pharmacist stringForKey:@"LastName"];
    self.mobileNo1TextField.text = [pharmacist stringForKey:@"MobileNumber1"];
    self.mobileNo2TextField.text = [pharmacist stringForKey:@"MobileNumber2"];
    self.mobileNo3TextField.text = [pharmacist stringForKey:@"MobileNumber3"];
    self.emailNo1TextField.text = [pharmacist stringForKey:@"EmailID1"];
    self.emailNo2TextField.text = [pharmacist stringForKey:@"EmailID2"];
    self.emailNo3TextField.text = [pharmacist stringForKey:@"EmailID3"];
    self.yearOfGragTextField.text = [pharmacist stringForKey:@"YearOfGraduation"];
    self.shiftTimingsFromTextField.text = [pharmacist stringForKey:@"ShiftTimingsFrom"];
    self.shiftTimingsToTextField.text = [pharmacist stringForKey:@"ShiftTimingsTo"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard Hide & Show Methods

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    UIScrollView *scrollView = nil;
    if(self.pharmacyTopButton.selected){
        return;
    }else if(self.pharmacistTopButton.selected){
        scrollView = self.pharmacistScrollView;
    }
    
    static NSString *str = @"first";

    if(str){
        self.initialContentSizePharmacistScrollView = self.pharmacistScrollView.contentSize;
    }
    str = nil;

    // keyboard frame is in window coordinates
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect keyboardInfoFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    // get the height of the keyboard by taking into account the orientation of the device too
    CGRect windowFrame = [self.view.window convertRect:self.view.frame fromView:self.view];
    CGRect keyboardFrame = CGRectIntersection (windowFrame, keyboardInfoFrame);
    CGRect coveredFrame = [self.view.window convertRect:keyboardFrame toView:self.view];
    // add the keyboard height to the content insets so that the scrollview can be scrolled
    UIEdgeInsets contentInsets = UIEdgeInsetsMake (0.0, 0.0, coveredFrame.size.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    // make sure the scrollview content size width and height are greater than 0
    [scrollView setContentSize:CGSizeMake (scrollView.frame.size.width, scrollView.contentSize.height+200)];
    // scroll to the text view
    [scrollView scrollRectToVisible:self.activeField.superview.frame animated:YES];
    
}
- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    UIScrollView *scrollView = nil;
    if(self.pharmacyTopButton.selected){
        return;
    }else if(self.pharmacistTopButton.selected){
        scrollView = self.pharmacistScrollView;
    }
    // scroll back..
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.pharmacistScrollView.contentInset = contentInsets;
    self.pharmacistScrollView.scrollIndicatorInsets = contentInsets;
    [self.pharmacistScrollView setContentSize:self.initialContentSizePharmacistScrollView];
}

#pragma mark - Top Buttons

-(void)setUpTopButtons{
   __unused AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.pharmacyTopButton setBackgroundColor:[appDelegate colorWithHexString:YellowColor]];
    self.automaticallyAdjustsScrollViewInsets = NO;
    /*
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    cancel.tintColor = [delegate colorWithHexString:@"25B4FF"];
    self.navigationItem.leftBarButtonItem = cancel;
    
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save)];
    save.tintColor = [delegate colorWithHexString:@"25B4FF"];
    self.navigationItem.rightBarButtonItem = save;
     */
}

- (IBAction)pharmacyOrPharmacistSelected:(id)sender;{
    if(self.activeField){
        [self.activeField resignFirstResponder];
    }
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case 10: //Pharmacy
            [self PharmacySelected];
            break;
        case 20: //Pharmacist
            [self PharmacistSelected];
            break;
        default:
            break;
    }
}
-(void)PharmacySelected{
    [self.leftVC addNavigationItem];
    [self.pharmacyTopButton setBackgroundColor:[appDelegate colorWithHexString:YellowColor]];
    [self.pharmacistTopButton setBackgroundColor:[UIColor clearColor]];
    self.pharmacyTopButton.selected = YES;
    self.pharmacistTopButton.selected = NO;
    self.pharmacistView.hidden = YES;
    self.XYDetailsView.hidden = YES;
    self.pharmacyView.hidden = NO;
    [self setUpPharmacyDetails];
}
-(void)PharmacistSelected{
    int noOfPharmacists = [[self.passedPharmacyDict stringForKey:@"NoOfPharmacists"] intValue];
    if (noOfPharmacists > 0) {
        [self.leftVC showHideNavigationItems];
        [self.pharmacistTopButton setBackgroundColor:[appDelegate colorWithHexString:YellowColor]];
        [self.pharmacyTopButton setBackgroundColor:[UIColor clearColor]];
        self.pharmacyTopButton.selected = NO;
        self.pharmacistTopButton.selected = YES;
        self.pharmacistView.hidden = YES;
        self.XYDetailsView.hidden = NO;
        self.pharmacyView.hidden = YES;
        self.pharmacistView.translatesAutoresizingMaskIntoConstraints = FALSE;
        
        if(!self.XYDetailsView.superview){
            [self.view addSubview:self.XYDetailsView];
            [NSLayoutConstraint activateConstraints: @[
                                                       [self.XYDetailsView.topAnchor constraintEqualToAnchor:self.horizontalLineView.bottomAnchor],
                                                       [self.XYDetailsView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                       [self.XYDetailsView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                       [self.XYDetailsView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                       ]];
        }
        self.XYDetailsView.translatesAutoresizingMaskIntoConstraints = FALSE;
        [self reloadPharmacistsList];
    }
}
#pragma mark - Date Picker

-(IBAction)datePickerSelected:(id)sender{
    UIButton *button = (UIButton*)sender;
    
    UIView *contentView = button.superview.superview;
    CGRect frame = [contentView convertRect:button.frame fromView:button.superview];
     UIDatePicker *picker1   = [[UIDatePicker alloc] initWithFrame:CGRectMake(contentView.frame.origin.x , frame.origin.y+button.bounds.size.height, contentView.frame.size.width, self.pharmacistScrollView.frame.size.height-frame.origin.y-frame.size.height)];
    picker1.backgroundColor = [UIColor clearColor];
    [picker1 setDatePickerMode:UIDatePickerModeTime];
    picker1.backgroundColor = [UIColor whiteColor];
    
 //   [contentView addSubview:picker1];
    if(button.tag == 10){ //From
        self.shiftTimingsFromTextField.inputView = picker1;
     //   [picker1 addTarget:self action:@selector(startDateSelected:) forControlEvents:UIControlEventValueChanged];
    }else if(button.tag == 20){ //To
        self.shiftTimingsToTextField.inputView = picker1;
    //    [picker1 addTarget:self action:@selector(endDateSelected:) forControlEvents:UIControlEventValueChanged];
    }

}
-(void)startDateSelected:(id)sender{
    UIDatePicker *datePicker = (UIDatePicker *)sender;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:TIME_FORMATTER_STRING];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    self.shiftTimingsFromTextField.text = strDate;
}
-(void)endDateSelected:(id)sender{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)countryDropDownSelected:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopOverViewController *popVC = (PopOverViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Pop"];
    popVC.arrOptions = self.countryNamesArray;
    popVC.modalPresentationStyle = UIModalPresentationPopover;
    popVC.countryField = self.countryTextField;
    popVC.countryCodeField = self.countryTextField;
    if ([self.countryNamesArray count] > 0) {
        [self presentViewController:popVC animated:YES completion:nil];
    }
    
    UIPopoverPresentationController *presentationController =[popVC popoverPresentationController];
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    presentationController.sourceView = self.countryTextField;
    presentationController.sourceRect = self.countryTextField.bounds;
}
- (IBAction)shiftTimingsDropDownSelected:(id)sender;{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopOverViewController *popVC = (PopOverViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Pop"];
    //Temp
    self.shiftTimingsArray = @[@"8 AM - 1 PM",@"2 PM - 6 PM",@"7 PM - 11 PM"];
    //Temp
    popVC.arrOptions = self.shiftTimingsArray;
    popVC.modalPresentationStyle = UIModalPresentationPopover;
    popVC.txtFieldToBeSet = self.shiftTimingsTextField;
   [self presentViewController:popVC animated:YES completion:nil];
    
    UIPopoverPresentationController *presentationController =[popVC popoverPresentationController];
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
    presentationController.sourceView = self.shiftTimingsTextField;
    presentationController.sourceRect = self.shiftTimingsTextField.bounds;
}

#pragma mark - Camera Methods

- (IBAction)cameraButtonPressed:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:CLICK_PHOTO_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self callImagePicker:UIImagePickerControllerSourceTypeCamera];
    }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:UPLOAD_IMAGE_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { [self callImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }];
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.buildingPicImageView;
    popPresenter.sourceRect = self.buildingPicImageView.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)clickPicture{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:NULL];
    }
    else{
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:OOPS_MESSAGE
                                    message:CAMERA_NOT_AVAILABLE_MESSAGE
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        LeftViewController *leftVC = (LeftViewController *)self.delegate;
        [leftVC presentViewController:alert animated:YES completion:nil];
    }
}

-(void)callImagePicker:(UIImagePickerControllerSourceType)type{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = type;
    LeftViewController *leftVC = (LeftViewController *)self.delegate;
    [leftVC presentViewController:imagePicker animated:YES completion:NULL];
}

#pragma mark - UIImagePickerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    UIImage *galleryImage = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
    self.buildingPicImageView.image = galleryImage;
    self.buildingPicImage = galleryImage;
    self.passedPharmacyString = @"PHARMACY";
    self.isFromBuildingPicSelection = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    self.passedPharmacyString = @"PHARMACY";
    self.isFromBuildingPicSelection = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeField = textField;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:ONLY_YEAR_FORMATTER_STRING];
    NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
    NSInteger currentYear = [strDate integerValue];
    NSInteger yearOfGraduationYear = [[NSString stringWithFormat:@"%@",self.yearOfGragTextField.text] integerValue];
    if (yearOfGraduationYear <= currentYear) {
        NSLog(@"Correct Data");
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:YEAR_OF_GRADUATION_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.firstNameTextField || textField == self.lastNameTextField || textField == self.cityTextField || textField == self.pharmacyNameTextField || textField == self.ownerNameTextField || textField == self.countryTextField )
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:ONLY_ALPHABETS_WITHSPACE_MESSAGE] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }else if (textField == self.emailNo1TextField || textField == self.emailNo2TextField || textField == self.emailNo3TextField) {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@.0123456789-_"] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }else if (textField == self.yearOfGragTextField || textField == self.noOfPhysiciansTextField ) {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:ONLY_NUMBERS] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }else if(textField == self.mobileNo1TextField || textField == self.mobileNo2TextField ||textField == self.mobileNo3TextField ||textField == self.landlineNumberTextField || textField == self.landlineNumberTextField2 || textField == self.landlineNumberTextField3 ){
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }
    return YES;
}

-(BOOL)isMandatoryFieldsEmptyForPharmacy{
    if ([self.pharmacyNameTextField.text  isEqualToString:@""] || [self.address1TextField.text  isEqualToString:@""]||[self.address2TextField.text  isEqualToString:@""]||[self.cityTextField.text  isEqualToString:@""]||[self.countryTextField.text  isEqualToString:@""]||[self.landlineNumberTextField.text  isEqualToString:@""] || ![self.finalLocationDict stringForKey:LATITUDE_STRING] || ![self.finalLocationDict stringForKey:LONGITUDE_STRING]){
        return YES;
    }
    return NO;
}

-(BOOL)isMandatoryFieldsEmptyForPharmacist{
    if ([self.firstNameTextField.text  isEqualToString:@""] ||[self.lastNameTextField.text  isEqualToString:@""] || [self.mobileNo1TextField.text  isEqualToString:@""]||[self.emailNo1TextField.text  isEqualToString:@""]){
        return YES;
    }
    return NO;
}


-(BOOL)isFinalValidationsOkToPostData{
    if(self.pharmacistTopButton.selected){ //pharmacist
        if ([self isMandatoryFieldsEmptyForPharmacist]){
            UIAlertController *alert =   [UIAlertController
                                          alertControllerWithTitle:OOPS_MESSAGE
                                          message:SOMETHING_MISSING_MESSAGE
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:ONLY_YEAR_FORMATTER_STRING];
        NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
        NSInteger currentYear = [strDate integerValue];
        NSInteger yearOfGraduationYear = [[NSString stringWithFormat:@"%@",self.yearOfGragTextField.text] integerValue];
        if (yearOfGraduationYear <= currentYear) {
            NSLog(@"Correct Data");
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:YEAR_OF_GRADUATION_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
        
        
        if (![self checkValidEmail])
        {
            UIAlertController *alert =   [UIAlertController
                                          alertControllerWithTitle:OOPS_MESSAGE
                                          message:VALID_EMAIL_ID_MESSAGE
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
    }else{//Pharmacy
        if ([self isMandatoryFieldsEmptyForPharmacy]){
            UIAlertController *alert =   [UIAlertController
                                          alertControllerWithTitle:OOPS_MESSAGE
                                          message:SOMETHING_MISSING_MESSAGE
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
    }
    return YES;
}

- (BOOL)checkValidEmail {
    BOOL validEmail = NO;
    if (self.emailNo1TextField.text != nil) {
        NSString *expression = EMAIL_VALIDATION_STRING;
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSTextCheckingResult* regexResult = [regex firstMatchInString:self.emailNo1TextField.text options:0 range:NSMakeRange(0, [self.emailNo1TextField.text length])];
        validEmail = regexResult != nil;
    }
    return validEmail;
}


#pragma mark-MapView Methods

#pragma mark - CLLocationManager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    float spanX = 0.1;
    float spanY = 0.1;
    CLLocation *location = [locations lastObject];
    MKCoordinateSpan span = self.mapView.region.span;
    span.latitudeDelta=spanX;
    span.longitudeDelta=spanY;
    MKCoordinateRegion region = MKCoordinateRegionMake(location.coordinate, span);
    self.mapView.region = region;
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"selected loaction!!!";
    region = MKCoordinateRegionMakeWithDistance(location.coordinate, 400, 400);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotation:point];
    NSString *latitude = [NSString stringWithFormat:@"%.7f", location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%.7f", location.coordinate.longitude];
    self.currentLocationDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitude,LATITUDE_STRING,longitude,LONGITUDE_STRING, nil];
    [self.locationManager stopUpdatingLocation];
}
- (void)longpressToGetLocation:(UILongPressGestureRecognizer *)gestureRecognizer{
    [self.view endEditing:YES];
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    self.mapView.showsUserLocation = NO;
    [self.mapView removeAnnotations:self.mapView.annotations];
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D location =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    float spanX = 0.1;
    float spanY = 0.1;
    CLLocation *locationAtual = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
    MKCoordinateSpan span = self.mapView.region.span;
    span.latitudeDelta=spanX;
    span.longitudeDelta=spanY;
    MKCoordinateRegion region = MKCoordinateRegionMake(locationAtual.coordinate, span);
    self.mapView.region = region;
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    region = MKCoordinateRegionMakeWithDistance(locationAtual.coordinate, 400, 400);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    [self.mapView addAnnotation:point];
    NSString *latitude = [NSString stringWithFormat:@"%.7f", location.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%.7f", location.longitude];
    self.selectedLocationDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitude,@"Latitude",longitude,@"Longitude", nil];
}

@end
