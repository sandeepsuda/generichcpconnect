//
//  AddPharmacistVCTableViewCell.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 25/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddPharmacistVCTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pharmacistNameLbl;

@end
