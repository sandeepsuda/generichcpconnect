//
//  MenuList.h
//  HealthConnect
//
//  Created by Abhatia on 06/01/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#define MenuTableTag 1000
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


/************ MESSAGES *******************/

#define YEAR_OF_GRADUATION_MESSAGE @"’Year of graduation’ has to be lesser than the current year."
#define FREQUENCY_OF_INTERNET_USAGE_PER_WEEK_MESSAGE @"Please enter less hours."
#define SOMETHING_MISSING_MESSAGE @"Something is missing! Please fill all the fields."
#define VALID_EMAIL_ID_MESSAGE @"Please enter a valid Email Address."
#define SOMETHING_WENT_WRONG_MESSAGE @"Server response is slow.Please try later."
#define VALID_MOBILE_NUMBER_MESSAGE @"Please enter a valid Mobile Number."
#define CHANGE_THE_NUMBER_OF_PHARMACISTS_MESSAGE @"Please change the number of pharmacists in ‘add pharmacy’ to add more pharmacists."
#define LOADING_MESSAGE @"Loading..."
#define ADDING_LANDLINE_NUMBERS_MESSAGE @"You can only add up to 3 landline numbers."
#define ONLY_ALPHABETS_WITHSPACE_MESSAGE @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
#define FOR_ENTER_EMAIL_ID_MESSAGE @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@.0123456789-"
#define LANDLINE_NUMBERS_WITH_SPECIAL_CHARACTERS @"+0123456789;-/\\"
#define ONLY_NUMBERS @"0123456789"
#define VERIFY_ALL_FIELDS_MESSAGE @"Please Verify All Fields."
#define OOPS_MESSAGE @"OOPS"
#define CAMERA_NOT_AVAILABLE_MESSAGE @"No Camera Available."
#define UPLOAD_IMAGE_MESSAGE @"Upload from image gallery"
#define CLICK_PHOTO_MESSAGE @"Click a photo now"
#define SELECT_MAXIMUM_NUMBER_OF_MISSIONS_MESSAGE @"Please select a maximum of 50 missions."
#define QUIZ_SUCCESS_MESSAGE @"Congrats, you have passed the Quiz. You will receive a call from Supervisor within 2 business days."
#define QUIZ_FAIL_MESSAGE @"Sorry, you have not achieved the required score. Please take the test after 24 hours."
#define QUIZ_AFTER_24HOURS_MESSAGE @"You have already taken the Quiz!! Please come back and take the Quiz after 24 Hours."
#define PHARMACIST_VERIFICATION_COMPLETE_MESSAGE @"Pharmacist verification complete."
#define PHARMACY_VERIFICATION_COMPLETE_MESSAGE @"Pharmacy verification complete."
#define CORRECT_AUTHENTICATION_CODE_MESSAGE @"That’s the right code, Thank You! Welcome to the PharmConnect world."
#define WRONG_AUTHENTICATION_CODE_MESSAGE @"Please enter correct Authentication code."
#define ENTER_ALL_PHARMACISTS_DETAILS_MESSAGE @"Please enter all the pharmacist details."
#define VERIFY_PHARMACY_PHARMACIST_DETAILS_MESSAGE @"Please verify all fields in both Pharmacy and Pharmacists."
#define PHARMACY_DETAILS_SUBMIT_SUCCESS @"Pharmacy details are saved successfully"
#define PHARMACIST_DETAILS_SUBMIT_SUCCESS @"Pharmacist details are added successfully"
#define PHARMACIST_DETAILS_EDIT @"Pharmacist details are saved successfully"

#define ONLY_ALPHABETS_WITHSPACE_MESSAGE @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
#define FOR_ENTER_EMAIL_ID_MESSAGE @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@.0123456789-"
#define LANDLINE_NUMBERS_WITH_SPECIAL_CHARACTERS @"+0123456789;-/\\"

/************ ARRAYS *******************/

#define AGE_GROUP_ARRAY @[@"20 - 29",@"30 - 39",@"40 - 49",@"50 - 59",@"60 and above"]
#define SPECIALITY_ARRAY @[@"General Physician",@"Internal Physician",@"Family Physician",@"Pharmacist"]
#define ONLINE_DIGITAL_MEDIUM_ARRAY @[@"Gathering information for professional needs (practice of medicine) - prescribing information, efficacy and safety information",@"Connect to other professionals through social media (facebook, twitter, etc)",@"Connect to other professionals through Blogs and discussion boards (read/ contribute)",@"Education / Continued Education Courses",@"Emails",@"Mobile Apps",@"Online patient tools",@"Shopping/ entertainment",@"Others"]
#define PREFERRED_DIGITAL_CHANNEL_ARRAY @[@"Emails",@"Internet websites (company/product websites)",@"e-detailing",@"Online blogs",@"Social media",@"Others"]
#define FREQUENCY_OF_INTERNET_USAGE_ARRAY @[@"Daily",@"4 to 6 days in a week",@"2 to 3 days in a week",@"Once a week",@"Once in 2 weeks",@"Less often",@"Don’t access internet for professional purpose"]
#define CONSENT_INTERESTED_ARRAY @[@"Latest scientific information",@"Medical publications",@"Latest clinical trials",@"Accredited education",@"Webinars & online symposia",@"Up coming meetings"]

/************ CONSENT TERMS & CONDITIONS *******************/

#define SOUTH_AFRICA_CONSENT_HEADING @"We know that your time is precious and we want to help you achieve the best outcomes for your patients. Sign up to receive short, timely and relevant email updates. Here are some of the services you can expect:"

#define SOUTH_AFRICA_CONSENT_BULLET_POINTS @"• Treatment information and therapy management \n•Attend live meetings from opinion leaders in Peer-to-Peer clinical insights \n• On-demand education \n• News on Pfizer products and services \n• Upcoming Pfizer-sponsored meetings and events"

#define SOUTH_AFRICA_CONSENT_DESCRIPTION @"Your data will be held on a database controlled by Eye Egypt Ltd. 6 Rashid street, Salah Eldin square, Heliopolis, Cairo, Egypt, and used for the marketing purpose described above.Only Pfizer (i.e., Pfizer Limited, Pfizer Inc. and other Pfizer Group companies (http://www.pfizer.com/), Pfizer Alliance Partners (i.e., other companies which co-promote medical products with Pfizer) and information and communication technology services providers working with Pfizer, will have access to your information to design, implement and control local, regional or global marketing campaigns on Pfizer promoted products and/or provide technical support to the database. The database and these recipients may be located within the AFME region. We will always employ appropriate technical security measures to protect your personal information and to ensure that it is not accessed by unauthorized persons. You may withdraw this consent by emailing us at DataProtectionSAR@Pfizer.com. Please use the reference “Pfizer e-permission programme” in any communication to us."

#define SOUTH_AFRICA_CONSENT_DESCRIPTION2 @"By completing the above, you consent to receiving communications from Pfizer South Africa and its affiliates regarding (a) Pfizer’s products and services; (b) Pfizer sponsored Programs, events and initiatives; (c) Customer feedback or (d) Educational information.Personal information will be collected, stored and used in accordance with Pfizer South Africa’s privacy policy for these purposes.If you do not want to receive further information from Pfizer South Africa or if you have any questions about Pfizer South Africa’s approach to privacy, please submit your queries to DataProtectionSAR@Pfizer.com, using “Pfizer e-permission programme” in any communication to us."

#define SOUTH_AFRICA_CONSENT_DESCRIPTION3 @"\n \n If you want to recieve e-mail/SMS from Pfizer South Africa \nplease complete and return via your Pfizer representative.";

#define SOUTH_AFRICA_CONSENT_COMPANYADDRESS @"Pfizer Laboratories (Pty) Ltd. 85 Bute Lane, Sandton, 2196, South Africa. Tel. No.: 0860 PFIZER (734937)."

#define SOUTH_AFRICA_CONSENT_PLEASEEMAILMELABEL @"Please email me the latest information."

#define ALGERIA_CONSENT_HEADING @"Nous savons que votre temps est précieux et nous voulons vous aider à atteindre les meilleurs résultats pour vos patients et votre pratique.\n \nEn vous inscrivant à notre base de données e-mail, vous recevrez des communications courtes et pertinentes qui vous permettront de:"

#define ALGERIA_CONSENT_BULLET_POINTS @"• Vous tenir au courant des formations et des programmes offerts par QuintilesIMS dans votre région \n• Accéder à un éventail de possibilités de formation médicale en ligne\n• Accéder directement à des ressources d'information clinique / scientifique et des ressources de soutien de patients pertinentes pour votre pratique"

#define ALGERIA_CONSENT_DESCRIPTION @"Nous veillerons à ce que le nombre d'e-mails / SMS que vous recevrez de nous soit réduit au minimum.Vous êtes libre de vous désabonner à tout moment. \nSi vous souhaitez recevoir des e-mail/SMS de QuintilesIMS  Algérie : \nVeuillez compléter le formulaire ci-dessous et retournez-le via votre représentant QuintilesIMS."

#define ALGERIA_CONSENT_DESCRIPTION2 @"En remplissant le formulaire ci-dessus, vous consentez à recevoir des communications de QuintilesIMS Algérie en ce qui concerne (a) les produits et services QuintilesIMS; (b) les programmes sponsorisés par QuintilesIMS, les événements et les initiatives; (c) L'avis des clients ou (d) l’information éducationnelle. Les informations personnelles seront recueillies, stockées et utilisées conformément à la politique de confidentialité QuintilesIMS Algérie. Si vous ne souhaitez pas recevoir de plus amples informations de QuintilesIMS Algérie ou si vous avez des questions au sujet de la politique de confidentialité de QuintilesIMS Algérie, contacter notre responsable conformité et qualité médicale à travers les coordonnées ci-dessous :"

#define ALGERIA_CONSENT_COMPANYADDRESS @"Omega Block, Embassy Tech Square Main Rd, Kaverappa Layout, Kadubeesanahalli, Bengaluru, Karnataka 560103"

#define ALGERIA_CONSENT_PLEASEEMAILMELABEL @"s'il vous plaît envoyez-moi les dernières informations"

#define ALGERIA_CONSENT_FIRSTNAMELABEL @"Prénom:";
#define ALGERIA_CONSENT_LASTNAMELABEL @"Nom:";
#define ALGERIA_CONSENT_SPECIALITYLABEL @"Spécialité:";
#define ALGERIA_CONSENT_EMAILIDLABEL @"Email:";
#define ALGERIA_CONSENT_MOBILELABEL @"Mobile:";
#define ALGERIA_CONSENT_WORKNUMBERLABEL @"Numéro de travail:";
#define ALGERIA_CONSENT_INTERESTEDLABEL @"Je suis très intéressé(e) de recevoir des informations sur :";

#define ALGERIA_CONSENT_SCIENTIFICLABEL @"Dernières informations scienti ques";
#define ALGERIA_CONSENT_MEDICALLABEL @"Publications médicales";
#define ALGERIA_CONSENT_CLINICALLABEL @"Dernières études cliniques";
#define ALGERIA_CONSENT_EDUCATIONLABEL @"Formations accréditées";
#define ALGERIA_CONSENT_WEBINARLABEL @"Webinars & symposiums en ligne";
#define ALGERIA_CONSENT_MEETINGLABEL @"Prochaines formations";



#define DUBAI_CONSENT_HEADING @"As part of Pfizer commitment to continuous medical education in the region,and provide the most updated scientific data,Pfizer Gulf & Levant would like to confirm your willingness to receive scientific and medical communication.\nYour time is valuable and we want to help you achieve the best outcomes for your patients and your practice.By signing-up to our e-mail database; you will receive short, timely and relevant communication that will allow you to:"

#define DUBAI_CONSENT_BULLET_POINTS @"• Keep up-to-date on meetings and programs being offered by Pfizer in your therapeutic area. \n• Access to a range of online medical information \"accrediated/non-accrediated\". \n• Clinical information on a board number of therapeutic areas relevant to your practice."

#define DUBAI_CONSENT_DESCRIPTION @"Pfizer respects the confidentiality of your personal information.Only Pfizer,or organizations acting on behalf of Pfizer,will have access to your inforamation.You will have,at all times,the right to review your information or request to have it altered.\nWe will ensure that the number of emails you receive from us will be kept to a minimum and you are free to unsubscribe anytime."

#define DUBAI_CONSENT_DESCRIPTION3 @"Pfizer repects the confidentiality of your personal information. Only Pfizer, or organisations acting on behalf of Pfizer, will have access to information. You will have, at all times, the right to review your information or request to have it altered. \nWe will ensure that the number of emails you receive from us will be kept to a minimum and you are free to unsubscribe any me.";

#define DUBAI_CONSENT_COMPANYADDRESS @"Pfizer International Corporation - Dubai Branch, Sheikh Zayed road, Al Moosa Tower 2, P.O.Box: 29553, Dubai, UAE, Tel: +971-4-3322286, Fax: +971-4-3320212, ®: Registered Trademark. TM: Trademark. ";

#define DUBAI_CONSENT_PLEASEEMAILMELABEL @"Please subscribe/unsubscribe me the latest information.";


#define EGYPT_CONSENT_HEADING @"Dear Doctor ,\n\nWe know your time is valuable and we want to help you achieve the best outcomes for your patients and your practice. You will receive from Pfizer Egypt short, timely and relevant communication that will allow you to:"

#define EGYPT_CONSENT_BULLET_POINTS @"• Keep up-to-date on meetings and programs being offered by Pfizer Egypt in your area; \n• Access to a range of online medical education opportunities; \n• A direct line to clinical/scientific information and patient support resources relevant to your practice."

#define EGYPT_CONSENT_DESCRIPTION @"We will ensure the number of emails you receive from us will be kept at a minimum."

#define EGYPT_CONSENT_COMPANYADDRESS @"Pfizer Egypt, 147 El Tahrir St., Dokki, Giza, Egypt P.O Box 2357 – Postal Code 12311 ™Trademark";

#define EGYPT_CONSENT_PLEASEEMAILMELABEL @"Please subscribe/unsubscribe me the latest information.";


#define KSA_CONSENT_HEADING @"One of QuintilesIMS's commitments to the continuous medical education process in the AFME region is providing the most updated scientific data to the Health Care Providers, hence QuintilesIMS is asking you to confirm your willingness to receive scientific and medical communication from our side.\nWe know that your time is valuable and we want to help you achieve the best outcomes for your patients and your practice.By signing-up to our e-mail database; you will receive short, timely and relevant communication that will allow you to:"

#define KSA_CONSENT_BULLET_POINTS @"• Keep up-to-date on meetings and programs being offered by QuintilesIMS in your area. \n• Access to a range of online medical education opportunities.\n• A direct line to clinical /scientific information and patient support resources relevant to your practice."

#define KSA_CONSENT_DESCRIPTION2 @"By completing the above you consent to receiving communications from QuintilesIMS and its affiliates regarding (a)QuintilesIMS’s products and services; (b)QuintilesIMS sponsored Programs, events and initiatives; (c) Customer feedback or (d) Educational information. Personal information will be collected, stored and used in accordance with QuintilesIMS privacy policy for these purposes. If you do not want to receive further information from QuintilesIMS or if you have any questions about QuintilesIMS approach to privacy, please contact our Medical Quality and Compliance Manager via the below address."

#define KSA_CONSENT_DESCRIPTION3 @"If you want to recieve e-mail/SMS from QuintilesIMS. \nPlease complete and return via your QuintilesIMS representative.";

#define KSA_CONSENT_COMPANYADDRESS @"Omega Block, Embassy Tech Square Main Rd, Kaverappa Layout, Kadubeesanahalli, Bengaluru, Karnataka 560103";

#define KSA_CONSENT_PLEASEEMAILMELABEL @"Please email me the latest information.";

/************ STRINGS *******************/

#define SUCCESS_STRING @"Success"
#define GO_BACK_STRING @"Go Back"
#define CANCEL_STRING @"Cancel"
#define LATITUDE_STRING @"Latitude"
#define LONGITUDE_STRING @"Longitude"
#define ALPHABETICAL_ASCENDING_STRING @"Alphabetical - Ascending"
#define ALPHABETICAL_DESCENDING_STRING @"Alphabetical - Descending"
#define LATEST_STRING @"Latest"
#define SORT_BY_STRING @"Sort By"
#define EMAIL_VALIDATION_STRING @"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
#define TRAINING_NOTE_STRING @"Please go through the following links to start using the app.All links are mandatory and to be completed in the same order."

/************ CONSENT CONSTANTS ******************/

// Country - Africa

#define CONSENT_TEXT_ONE_AFRICA @"We know that your time is precious and we want to help you achieve the best outcomes for your patients. Sign up to receive short, timely and relevant email updates. Here are some of the services you can expect:"


#define CONSENT_TEXT_TWO_AFRICA @"• Treatment information and therapy management\n• Attend live meetings from opinion leaders in Peer-to-Peer clinical insights\n• On-demand education\n• News on Pfizer products and services\n• Upcoming Pfizer-sponsored meetings and events"

#define CONSENT_TEXT_THREE_AFRICA @"Your data will be held on a database controlled by Eye Egypt Ltd. 6 Rashid street, Salah Eldin square, Heliopolis, Cairo, Egypt, and used for the marketing purpose described above.Only Pfizer(i.e., Pfizer Limited, Pfizer Inc. and other Pfizer Group companies (http://www.pfizer.com/), Pfizer Alliance artners(i.e.,other companies which co-promote medicalproducts with Pfizer) and information and communication technology services providers working with Pfizer, will have access to your information to design, implement and control local, regional or global marketing campaigns on Pfizer promoted products and/or provide technical support to the database. The database and these recipients may be located within the AFME region. We will always employ appropriate technical security measures to protect your personal information and to ensure that it is not accessed by unauthorized persons. You may withdraw this consent by emailing us at DataProtectionSAR@Pfizer.com. Please use the reference “Pfizer e-permission programme” in any communication to us."

#define CONSENT_TEXT_FOUR_AFRICA @"If you want to receive e-mail/SMS from Pfizer South Africa, please complete and return via your Pfizer representative"

#define CONSENT_TEXT_FIVE_AFRICA @"Please email me the latest information"



// Country - Algeria
#define CONSENT_TEXT_ONE_ALGERIA @"Nous savons que votre temps est précieux et nous voulons vous aider à atteindre les meilleurs résultats pour vos patients et votre pratique."


#define CONSENT_TEXT_TWO_ALGERIA @"En vous inscrivant à notre base de données e-mail, vous recevrez des communications courtes et pertinentes qui vous permettront de:"

#define CONSENT_TEXT_THREE_ALGERIA @"• Vous tenir au courant des formations et des programmes offerts par Pfizer dans votre région\n• Accéder à un éventail de possibilités de formation médicale en ligne\n• Accéder directement à des ressources d'information clinique/scientifique et des ressources de soutien de patients pertinentes pour votre pratique"

#define CONSENT_TEXT_FOUR_ALGERIA @"Nous veillerons à ce que le nombre d'e-mails / SMS que vous recevrez de nous soit réduit au minimum. Vous êtes libre de vous désabonner à tout moment"

#define CONSENT_TEXT_FIVE_ALGERIA @"Si vous souhaitez recevoir des e-mail/SMS de Pfizer Pharm Algérie :\nVeuillez compléter le formulaire ci-dessous et retournez-le via votre représentant Pfizer."

#define CONSENT_TEXT_SIX_ALGERIA @"s'il vous plaît envoyez-moi les dernières informations"


// Country - Dubai

#define CONSENT_TEXT_ONE_DUBAI @"As part of Pfizer commitment to continuous medical education in the region, and provide the most updated scientific data, Pfizer Gulf & Levant would like to confirm your willingness to receive scientific and medical communication"

#define CONSENT_TEXT_TWO_DUBAI @"Your time is valuable and we want to help you achieve the best outcomes for your patients and your practice. By signing-up to our e-mail database you will receive short, timely and relevant communication that will allow you to:"

#define CONSENT_TEXT_THREE_DUBAI @"• Keep up-to-date on meetings and programs being offered in your therapeutic area.\n• Access to a range of online medical information accredited/non-accredited.\n• Clinical information on a broad number of therapeutic areas relevant to your practice."

#define CONSENT_TEXT_FOUR_DUBAI @""

// Country - Egypt

// Country - KSA


/************ COLORS CONSTANTS *******************/

#define HexaDecimalColor @"3A4F5A"
#define BlueColor @"25B4FF"
#define RamGreenColor @"20C22F"
#define YellowColor @"FFCF32"
#define HCPBlueColor @"34A853"
#define HCPRamGreenColor @"FBBC05"
#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

/************ DATE FORMATTERS *******************/

#define ONLY_YEAR_FORMATTER_STRING @"yyyy"
#define DATE_TIME_YEAR_FORMATTER_STRING @"yyyy-MM-dd'T'HH:mm:ss"
#define TIME_FORMATTER_STRING @"hh:mm a"

/************ NSUSERDEFAULT KEYS *******************/

#define COUNTRY_DIALING_CODE_STRING @"CountryDialingCode"
#define MOBILE_NUMBER_STRING @"MobileNumber"
#define EMAIL_STRING @"Email"
#define WELCOME_STRING @"Welcome"
#define PHYCATEGORY @"PHYCATEGORY"

/************ Pfizer APIS *******************/

//#define KBASE_URL @"http://162.44.200.199/HCPConnect/" // Final Prod Chandra

#define KBASE_URL @"http://162.44.200.199/HCPConnectGeneric/"


//#define KBASE_URL @"https://native-platform-stg.imshealth.com/HCPConnectCR/" // DEV
//#ifdef DEBUG
//#define KBASE_URL @"https://native-platform-stg.imshealth.com/HCPConnectDEV/" //DEV
//#else
//#define KBASE_URL @"http://hcplink.imshealth.com/HCPConnect/" //PRE-PROD Chandra Given
//#define KBASE_URL  http://162.44.192.199/HCPConnect // PROD Malla Given
//#endif


//POST

#define EXPENSE_CLAIAM @"physician/claimexpense/"
//GET
#define ONE_TIME @"api/doctor/new/"

//GET
#define EXISTING_USER @"api/existuser/"
//https://native-platform-stg.imshealth.com/HCPConnectCR/api/existuser/RUppari@in.imshealth.com/29386D73

#define SINGLE_SIGN_ON @"api/user/"

//GET
#define HCP_GETCountryList @"api/country"

//POST
#define REGISTRATION @"api/registration"

//GET
#define HCP_Verify_CongratsPage @"api/validatepassword/"

//GET
#define HCP_GETQuiz @"api/quiz"

//POST
#define HCP_SubmitQuiz @"api/quiz/submit/"

//GET
#define HCP_Verify_WelcomePage @"api/quiz/validatedc/"

//GET
#define PHYSICIAN_TRACKER_LIST @"doctor/assign/mytracker/"

//GET
#define PHYSICIAN_SELECTION_PHYSICIAN_LIST @"doctor/mission/display/"

//https://native-platform-stg.imshealth.com/HCPConnectCR/doctor/display/newmission/VRMalla@in.imshealth.com/
//https://native-platform-stg.imshealth.com/HCPConnectCR/doctor/mission/display/VRMalla@in.imshealth.com/ASSIGNED

//POST
#define PHYSICIAN_SELECTION_SUBMIT @"doctor/mission/new/"

//POST
#define PHYSICIAN_SELECTION_SEARCH @"doctor/new/search/"

//GET
#define PHYSICIAN_CURRENT_LIST @"doctor/new/current/"

//GET
#define PHYSICIAN_HISTORICAL_LIST @"doctor/historic/"

//GET
#define LEADERBOARD_LIST @"doctor/leaderboard"

//POST
#define PHYSICIAN_CONSENT_FORM @"physician/saveconsent/"

//POST
#define ADD_PHYSICIAN @"physician/save/"
///physician/save/VRMalla@in.imshealth.com/ALL

//GET
#define PHARMACY_PHARMACIES_LIST_NEW_MISSION @"api/selectmission/new/"

//POST
#define PHARMACY_SEARCH_VERIFY @"api/selectmission/verify/pharmacysearch/"

//POST
#define PHARMACY_SELECT_SUBMIT_NEW_MISSION @"api/selectmission/new/"

//GET
#define PHARMACY_PHARMACIES_LIST_VERIFY_MISSION @"api/selectmission/verify/"

//GET
#define PHARMACY_SELECT_VERIFY @"api/selectmission/verify/"

//POST
#define PHARMACY_SELECT_SUBMIT_VERIFY_MISSION @"api/selectmission/verifysubmit/"

//POST
#define PHARMACY_SEARCH_NEW_MISSION @"api/selectmission/new/pharmacysearch/"

//GET
#define PHARMACY_CURRENT_PHARMACIES_NEW_MISSION @"api/missiontracker/new/current/"

//GET
#define PHARMACY_PHARMACY_DETAILS_NEW_MISSION @"api/pharmacy/getdetails/"

//GET
#define PHARMACY_PHARMACY_DETAILS_VERIFY @"api/pharmacy/verify/getdetails/"

//POST
#define PHARMACY_ADD_PHARMACY_NEW_MISSION @"api/pharmacy/new/"

//POST
#define PHARMACY_ADD_PHARMACY_VERIFY @"api/pharmacy/verifier/"

//POST
#define PHARMACY_ALL_PHARMACIST_LIST @"api/pharmacistlist/"

//POST
#define PHARMACY_ADD_PHARMACIST_NEW_MISSION @"api/pharmacist/new/"

//POST
#define PHARMACY_ADD_PHARMACIST_VERIFY_MISSION @"api/pharmacist/verifier/"

//GET
#define PHARMACY_HISTORICAL_NEW_MISSION @"api/missiontracker/new/historic/"

//GET
#define PHARMACY_CURRENT_VERIFY @"api/missiontracker/verify/current/"

//GET
#define PHARMACY_HISTORICAL_VERIFY @"api/missiontracker/verify/historic/"

//GET
#define PHARMACY_FINAL_SUBMIT_NEW_MISSION @"api/pharmacy/finalsubmit/"

//GET
#define PHARMACY_FINAL_SUBMIT_VERIFY @"api/pharmacy/verify/finalsubmit/"

//GET
#define PHARMACY_TRACKER @"api/mytracker/"

//POST
#define PHARMACY_MOBILE_NUMBERS @"api/pharmacy/verify/mobilenumbers/"

//GET
#define PHARMACY_LATEST_SEARCH_NEW_MISSION @"api/selectmission/new/latest/"

//GET
#define PHARMACY_LATEST_SEARCH_VERIFY @"api/selectmission/verify/latest/"

//POST
#define PHARMACY_CONSENT @"pharmacist/consent/"

//https://native-platform-stg.imshealth.com/HCPConnectDEV/api/pharmacy/getdetails/9876543210/PH1
//Production URL
//http://hcplink.imshealth.com/HCPConnect/

//DEv
//https://native-platform-stg.imshealth.com/HCPConnect/
@interface MenuList : NSObject

+ (MenuList *)sharedList;
@property(nonatomic)BOOL isListDisplayed;
+(void)networkChecking;
@end
