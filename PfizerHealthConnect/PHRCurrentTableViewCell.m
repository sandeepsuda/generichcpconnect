//
//  PHRCurrentTableViewCell.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 22/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PHRCurrentTableViewCell.h"

@implementation PHRCurrentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
