//
//  LeftViewController.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 07/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "LeftViewController.h"
#import "AppDelegate.h"
#import "MenuList.h"
#import "NSDictionary+Dictionary.h"

@interface LeftViewController (){
    BOOL isFromConsent;
    BOOL isFromConsentSubmitSuccess;
}

@end

@implementation LeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:WELCOME_STRING];
    [defaults setBool:YES forKey:@"Active"];
    self.placeHolderImg.layer.cornerRadius = self.placeHolderImg.frame.size.width/2;
    self.placeHolderImg.clipsToBounds = YES;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20]}];
    self.menuItems = @[@"Leaderboard",@"Physician",@"Pharmacy",@"Training Materials",@"About Us",@"Contact Us"];
    self.menuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //self.nameLabel.text = @"";
    self.selectedMenuIndex = 1;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - PharmacyTrackerViewControllerDelegate methods
-(void)addPharmacyMissionAction:(BOOL)verify{
    //if (!self.PHRSelectMissionVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
        self.PHRSelectMissionVC = [str instantiateViewControllerWithIdentifier:@"PharmacySelectionViewController"];
        //self.PHRSelectMissionVC.isVerifyMission = verify;
        self.PHRSelectMissionVC.delegate = self;
    //}
    if (self.rightView.subviews && self.rightView.subviews.count)
        [self.rightView.subviews[0] removeFromSuperview];
    self.PHRSelectMissionVC.view.frame = self.rightView.bounds;
    //self.PHRSelectMissionVC.isVerifyMission = verify;
    [self.rightView addSubview:self.PHRSelectMissionVC.view];
    [self setUpNavigationItems:@"PHRSelectMission"];
}
-(void)setPharmacyUserDetails:(NSDictionary*)userInfo{
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",[userInfo objectForKey:@"DCFirstName"],[userInfo objectForKey:@"DCLastName"]];
    self.gainedPointsLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[userInfo[@"DCPoints"] integerValue]];
}
-(void)showPharmacyDetails:(NSMutableDictionary*)pharmacyDict{
    appDelegate.pharmacyId = [pharmacyDict objectForKey:@"PharmacyId"];
    if (appDelegate.isVerify) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"PharmacyStoryboard" bundle:nil];
        self.verifyPharmacyDetailsVC = [str instantiateViewControllerWithIdentifier:@"VerifyPharmacyDetailsViewController"];
        self.verifyPharmacyDetailsVC.delegate = self;
        self.verifyPharmacyDetailsVC.passedPharmacyString = @"PHARMACY";
        self.verifyPharmacyDetailsVC.passedPharmacyDict = pharmacyDict;
        if (self.rightView.subviews && self.rightView.subviews.count)
            [self.rightView.subviews[0] removeFromSuperview];
        self.verifyPharmacyDetailsVC.view.frame = self.rightView.bounds;
        [self.rightView addSubview:self.verifyPharmacyDetailsVC.view];
        [self setUpNavigationItems:@"PHRPharmacyDetails"];
    }else{
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"PharmacyStoryboard" bundle:nil];
        self.PHRpharmacyDetailsVC = [str instantiateViewControllerWithIdentifier:@"PharmacyDetailsViewController"];
        self.PHRpharmacyDetailsVC.delegate = self;
        self.PHRpharmacyDetailsVC.passedPharmacyString = @"PHARMACY";
        self.PHRpharmacyDetailsVC.passedPharmacyDict = pharmacyDict;
        if (self.rightView.subviews && self.rightView.subviews.count)
            [self.rightView.subviews[0] removeFromSuperview];
        self.PHRpharmacyDetailsVC.view.frame = self.rightView.bounds;
        [self.rightView addSubview:self.PHRpharmacyDetailsVC.view];
        [self setUpNavigationItems:@"PHRPharmacyDetails"];
    }
}
-(void)showPharmacistDetails:(NSMutableDictionary*)pharmacistDict{
    appDelegate.pharmacyId = [pharmacistDict objectForKey:@"PharmacyId"];
    if(appDelegate.isVerify){
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"PharmacyStoryboard" bundle:nil];
        self.verifyPharmacyDetailsVC = [str instantiateViewControllerWithIdentifier:@"VerifyPharmacyDetailsViewController"];
        self.verifyPharmacyDetailsVC.delegate = self;
        self.verifyPharmacyDetailsVC.passedPharmacyString = @"PHARMACIST";
        self.verifyPharmacyDetailsVC.passedPharmacyDict = pharmacistDict;
        if (self.rightView.subviews && self.rightView.subviews.count)
            [self.rightView.subviews[0] removeFromSuperview];
        self.verifyPharmacyDetailsVC.view.frame = self.rightView.bounds;
        [self.rightView addSubview:self.verifyPharmacyDetailsVC.view];
        [self setUpNavigationItems:@"PHRPharmacistDetails"];
    }else{
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"PharmacyStoryboard" bundle:nil];
        self.PHRpharmacyDetailsVC = [str instantiateViewControllerWithIdentifier:@"PharmacyDetailsViewController"];
        self.PHRpharmacyDetailsVC.delegate = self;
        self.PHRpharmacyDetailsVC.passedPharmacyString = @"PHARMACIST";
        self.PHRpharmacyDetailsVC.passedPharmacyDict = pharmacistDict;
        if (self.rightView.subviews && self.rightView.subviews.count)
            [self.rightView.subviews[0] removeFromSuperview];
        self.PHRpharmacyDetailsVC.view.frame = self.rightView.bounds;
        [self.rightView addSubview:self.PHRpharmacyDetailsVC.view];
        [self setUpNavigationItems:@"PHRPharmacistDetails"];
    }
}
-(void)PHRSubmitSucessfull{
    [self PHRCancelAction:nil];
}
-(void)PHRAddConsent:(NSMutableDictionary*)pharmacistDict{
    isFromConsent = YES;
    UIStoryboard *str = [UIStoryboard storyboardWithName:@"PharmacyStoryboard" bundle:nil];
    self.phrConsentCon = [str instantiateViewControllerWithIdentifier:@"PharmacistConsentViewController"];
    self.phrConsentCon.passedPharmacistDict = [NSMutableDictionary dictionaryWithDictionary:pharmacistDict];
    self.phrConsentCon.consentDelegate = self;
    self.phrConsentCon.view.frame = self.rightView.bounds;
    [self.rightView addSubview:self.phrConsentCon.view];
}

#pragma mark - PhysicianTrackerViewControllerDelegate methods

-(void)setUserDetails:(NSDictionary*)userInfo{
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",[userInfo objectForKey:@"DCFirstName"],[userInfo objectForKey:@"DCLastName"]];
    self.gainedPointsLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[userInfo[@"DCPoints"] integerValue]];
    [self.placeHolderImg sd_setImageWithURL:[NSURL URLWithString:[userInfo stringForKey:@"DCImagePath"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

-(void)addMissionAction{
    if (!self.selectMissionVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.selectMissionVC = [str instantiateViewControllerWithIdentifier:@"SelectMissionViewController"];
        self.selectMissionVC.delegate = self;
    }
    if (self.rightView.subviews && self.rightView.subviews.count)
        [self.rightView.subviews[0] removeFromSuperview];
    self.selectMissionVC.view.frame = self.rightView.bounds;
    [self.rightView addSubview:self.selectMissionVC.view];
    [self setUpNavigationItems:@"SelectMission"];
}
-(void)showHideNavigationItems{
    self.navigationItem.rightBarButtonItem = nil;
}
-(void)addNavigationItem{
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Save"
                                style:UIBarButtonItemStyleDone
                                target:self
                                action:@selector(PHRPharmacyDetailsSaveAction:)];
    [saveBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
    self.navigationItem.rightBarButtonItem = saveBtn;
}
-(void)setUpNavigationItems:(NSString*)sender{
    if ([sender isEqualToString:@"SelectMission"]) {
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                       initWithTitle:@"Cancel"
                                       style:UIBarButtonItemStyleDone
                                       target:self
                                       action:@selector(cancelAction:)];
        [cancelBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.leftBarButtonItem = cancelBtn;
        UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc]
                                        initWithTitle:@"Save"
                                        style:UIBarButtonItemStyleDone
                                        target:self
                                        action:@selector(saveAction:)];
        [saveBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.rightBarButtonItem = saveBtn;
    }
    if ([sender isEqualToString:@"PHRSelectMission"]) {
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                      initWithTitle:@"Cancel"
                                      style:UIBarButtonItemStyleDone
                                      target:self
                                      action:@selector(PHRCancelAction:)];
        [cancelBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.leftBarButtonItem = cancelBtn;
        UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Save"
                                    style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(PHRSaveAction:)];
        [saveBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.rightBarButtonItem = saveBtn;
    }
    if ([sender isEqualToString:@"PHRPharmacyDetails"]) {
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                      initWithTitle:@"Cancel"
                                      style:UIBarButtonItemStyleDone
                                      target:self
                                      action:@selector(PHRCancelAction:)];
        [cancelBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.leftBarButtonItem = cancelBtn;
        UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Save"
                                    style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(PHRPharmacyDetailsSaveAction:)];
        [saveBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.rightBarButtonItem = saveBtn;
    }
    if ([sender isEqualToString:@"PHRPharmacistDetails"]) {
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                      initWithTitle:@"Cancel"
                                      style:UIBarButtonItemStyleDone
                                      target:self
                                      action:@selector(PHRCancelAction:)];
        [cancelBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.leftBarButtonItem = cancelBtn;
        self.navigationItem.rightBarButtonItem = nil;
    }
    if ([sender isEqualToString:@"AddPhysician"]) {
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                      initWithTitle:@"Cancel"
                                      style:UIBarButtonItemStyleDone
                                      target:self
                                      action:@selector(cancelAction:)];
        [cancelBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.leftBarButtonItem = cancelBtn;
        UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Save"
                                    style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(addPhycianSaveAction:)];
        [saveBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.rightBarButtonItem = saveBtn;
    }
    if ([sender isEqualToString:@"LeaderBoard"]) {
        UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Done"
                                    style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(cancelAction:)];
        [saveBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.rightBarButtonItem = saveBtn;
    }
    if ([sender isEqualToString:@"Consent"]) {
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                      initWithTitle:@"Cancel"
                                      style:UIBarButtonItemStyleDone
                                      target:self
                                      action:@selector(consentCancelAction:)];
        [cancelBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.leftBarButtonItem = cancelBtn;
//        //UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc]
//                                    initWithTitle:@""
//                                    style:UIBarButtonItemStyleDone
//                                    target:self
//                                    action:@selector(consentSaveAction:)];
//        //[saveBtn setTintColor:[UIColor colorWithRed:37.0/255.0 green:180.0/255.0 blue:255.0/255.0 alpha:1.0]];
        self.navigationItem.rightBarButtonItem = nil;
    }
}

-(void)consentCancelAction:(id)sender{
    //offline
    [self.consentCon removePhysicianIfSavedForOfflineMode];
    //offline
    if (!self.addPhysicianVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Pfizer" bundle:nil];
        self.addPhysicianVC = [str instantiateViewControllerWithIdentifier:@"PfizerAddPhysicianViewController"];
        //self.physicianVC.delegate = self;
    }
    if (self.rightView.subviews && self.rightView.subviews.count)
        [self.rightView.subviews[0] removeFromSuperview];
    self.addPhysicianVC.view.frame = self.rightView.bounds;
    [self.rightView addSubview:self.addPhysicianVC.view];
    [self setUpNavigationItems:@"AddPhysician"];
}

-(void)consentSuccessMethodAction{
    if (self.phrConsentCon) {
        isFromConsentSubmitSuccess = YES;
        [self PHRCancelAction:nil];
    }else{
        [self cancelAction:nil];
    }
}

-(void)consentSaveAction:(id)sender{
    
}

-(void)saveButtonAction:(NSMutableDictionary *)dict{
    //if (!self.consentVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
        self.consentCon = [str instantiateViewControllerWithIdentifier:@"ConsentViewController"];
   // }
//    if (self.rightView.subviews && self.rightView.subviews.count)
//        [self.rightView.subviews[0] removeFromSuperview];
   // self.consentVC.consentDelegate = self;
    self.consentCon.passedPhysicianDict = dict;
    self.consentCon.consentDelegate = self;
    self.consentCon.view.frame = self.rightView.bounds;
    [self.rightView addSubview:self.consentCon.view];
    [self setUpNavigationItems:@"Consent"];
}

-(void)addPhycianSaveAction:(id)sender{
    if (self.addPhysicianVC) {
        [self.addPhysicianVC saveAction:nil];
    }
//    //if (!self.consentVC) {
//        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
//        self.consentVC = [str instantiateViewControllerWithIdentifier:@"PHYConsentViewController"];
//        //self.physicianVC.delegate = self;
//    //}
//    if (self.rightView.subviews && self.rightView.subviews.count)
//        [self.rightView.subviews[0] removeFromSuperview];
//    self.consentVC.view.frame = self.rightView.bounds;
//    [self.rightView addSubview:self.consentVC.view];
//    [self setUpNavigationItems:@"Consent"];
}

- (void)saveAction:(id)sender{
    if (self.selectMissionVC) {
        [self.selectMissionVC.view endEditing:YES];
        [self.selectMissionVC submitSelectedPhysicians];
    }
}
- (void)PHRSaveAction:(id)sender{
    if (self.PHRSelectMissionVC) {
        [self.PHRSelectMissionVC.view endEditing:YES];
        [self.PHRSelectMissionVC submitSelectedPhysicians];
    }
}

- (void)cancelAction:(id)sender{
    if (self.selectMissionVC) {
        [self.selectMissionVC.view endEditing:YES];
    }
    [self.menuTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
    self.selectedMenuIndex = 1;
    [self setHighlightedTextLbl];
    if (!self.physicianVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
        self.physicianVC = [str instantiateViewControllerWithIdentifier:@"PhysicianTrackerViewController"];
        self.physicianVC.delegate = self;
    }
    if (self.rightView.subviews && self.rightView.subviews.count)
        [self.rightView.subviews[0] removeFromSuperview];
    self.physicianVC.view.frame = self.rightView.bounds;
    [self.rightView addSubview:self.physicianVC.view];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
}

-(void)PHRCancelAction:(id)sender{
    if (appDelegate.isVerify) {
        if(!self.verifyPharmacyDetailsVC.pharmacyView.hidden || !self.verifyPharmacyDetailsVC.XYDetailsView.hidden){
            [self.menuTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
            self.selectedMenuIndex = 2;
            [self setHighlightedTextLbl];
            if (!self.PHRpharmacyVC) {
                UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
                self.PHRpharmacyVC = [str instantiateViewControllerWithIdentifier:@"PhysicianTrackerViewController"];
                self.PHRpharmacyVC.delegate = self;
            }
            if (self.rightView.subviews && self.rightView.subviews.count)
                [self.rightView.subviews[0] removeFromSuperview];
            self.PHRpharmacyVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.PHRpharmacyVC.view];
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
        }else{
            if (self.phrConsentCon) {
                [self.phrConsentCon.view removeFromSuperview];
                self.phrConsentCon = nil;
                if (isFromConsent) {
                    isFromConsent = NO;
                    if (isFromConsentSubmitSuccess) {
                        isFromConsentSubmitSuccess = NO;
                        [self.verifyPharmacyDetailsVC addXYDetailsView];
                    }
                }
            }else{
                [self.verifyPharmacyDetailsVC.view endEditing:YES];
                [self.verifyPharmacyDetailsVC.pharmacistView removeFromSuperview];
                [self.verifyPharmacyDetailsVC addXYDetailsView];
            }
        }
    }else{
        if(!self.PHRpharmacyDetailsVC.pharmacyView.hidden || !self.PHRpharmacyDetailsVC.XYDetailsView.hidden){
            [self.PHRpharmacyDetailsVC.view endEditing:YES];
            [self.menuTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
            self.selectedMenuIndex = 2;
            [self setHighlightedTextLbl];
            if (!self.PHRpharmacyVC) {
                UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
                self.PHRpharmacyVC = [str instantiateViewControllerWithIdentifier:@"PhysicianTrackerViewController"];
                self.PHRpharmacyVC.delegate = self;
            }
            if (self.rightView.subviews && self.rightView.subviews.count)
                [self.rightView.subviews[0] removeFromSuperview];
            self.PHRpharmacyVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.PHRpharmacyVC.view];
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
        }else{
            if (self.phrConsentCon) {
                PharmacistConsentViewController *VC = self.phrConsentCon;
                [self.phrConsentCon.view removeFromSuperview];
                self.phrConsentCon = nil;
                if (isFromConsent) {
                    
                    isFromConsent = NO;
                    if (isFromConsentSubmitSuccess) {
                        isFromConsentSubmitSuccess = NO;
                        [self.PHRpharmacyDetailsVC addXYDetailsView];
                    }else{
                        [VC removePharmacistIfSavedForOfflineMode];
                        VC = nil;
                    }
                }
            }else{
                
                [self.PHRpharmacyDetailsVC.view endEditing:YES];
                [self.PHRpharmacyDetailsVC addXYDetailsView];
            }
        }
   }
}
-(void)PHRPharmacyDetailsSaveAction:(id)sender{
    if (appDelegate.isVerify) {
        if (!self.verifyPharmacyDetailsVC.pharmacyView.hidden) {
            [self.verifyPharmacyDetailsVC pharmacyDetailsSubmitAction:nil];
        }else if (!self.verifyPharmacyDetailsVC.pharmacistView.hidden){
            [self.verifyPharmacyDetailsVC submitBtnAction:nil];
        }else if (self.phrConsentCon) {
            [self.phrConsentCon submitAction:nil];
        }
    }else{
        if (!self.PHRpharmacyDetailsVC.pharmacyView.hidden) {
            [self.PHRpharmacyDetailsVC pharmacyDetailsSubmitAction:nil];
        }else if (!self.PHRpharmacyDetailsVC.pharmacistView.hidden){
            if (isFromConsent) {
                [self.phrConsentCon submitAction:nil];
            }else{
                [self.PHRpharmacyDetailsVC submitBtnAction:nil];
            }
        }
    }
}

-(void)addPhysicianAction:(NSMutableDictionary*)phyDict{
    [self setUpNavigationItems:@"AddPhysician"];
   // if (!self.addPhysicianVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Pfizer" bundle:nil];
        self.addPhysicianVC = [str instantiateViewControllerWithIdentifier:@"PfizerAddPhysicianViewController"];
        //self.addPhysicianVC.isNewPhysician = YES;
        self.addPhysicianVC.delegate = self;
    //}
    if (self.rightView.subviews && self.rightView.subviews.count)
        [self.rightView.subviews[0] removeFromSuperview];
    self.addPhysicianVC.cardData = phyDict;
    self.addPhysicianVC.view.frame = self.rightView.bounds;
    self.addPhysicianVC.isNewPhysician = YES;
    [self.rightView addSubview:self.addPhysicianVC.view];
}

-(void)physicianListSelection:(NSMutableDictionary *)phyDict{
    [self setUpNavigationItems:@"AddPhysician"];
    //if (!self.addPhysicianVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Pfizer" bundle:nil];
        self.addPhysicianVC = [str instantiateViewControllerWithIdentifier:@"PfizerAddPhysicianViewController"];
        [self.addPhysicianVC setPhysicianDict:phyDict];
        [self.addPhysicianVC setIsNewPhysician:NO];
        self.addPhysicianVC.delegate = self;
   // }
    if (self.rightView.subviews && self.rightView.subviews.count)
        [self.rightView.subviews[0] removeFromSuperview];
    self.addPhysicianVC.physicianDict = [NSMutableDictionary dictionaryWithDictionary:phyDict];
    [self.addPhysicianVC setIsNewPhysician:NO];
    self.addPhysicianVC.view.frame = self.rightView.bounds;
    [self.rightView addSubview:self.addPhysicianVC.view];
}

-(void)submitSucessfull{
    if (!self.physicianVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
        self.physicianVC = [str instantiateViewControllerWithIdentifier:@"PhysicianTrackerViewController"];
        self.physicianVC.delegate = self;
    }
    if (self.rightView.subviews && self.rightView.subviews.count)
        [self.rightView.subviews[0] removeFromSuperview];
    self.physicianVC.view.frame = self.rightView.bounds;
    [self.rightView addSubview:self.physicianVC.view];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
}

//-(void)consentSubmitMethodAction:(UIImage *)image{
//    self.consentVC.consentDelegate = self;
//    UIStoryboard *str = [UIStoryboard storyboardWithName:@"Pfizer" bundle:nil];
//    DummyScreenshotViewController *dumy = [str instantiateViewControllerWithIdentifier:@"DummyScreenshotViewController"];
//    [self.navigationController pushViewController:dumy animated:YES];
//}


/*

-(void)saveButtonAction:(NSMutableDictionary *)dict{
    if (!self.consentVC) {
        UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
        self.consentVC = [str instantiateViewControllerWithIdentifier:@"PHYConsentViewController"];
        [self.consentVC passedDictFromAddPhysician:dict];
    }
    if (self.rightView.subviews && self.rightView.subviews.count)
        [self.rightView.subviews[0] removeFromSuperview];
    self.physicianVC.view.frame = self.rightView.bounds;
    [self.rightView addSubview:self.physicianVC.view];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
}
 */

#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.menuItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.contentView.backgroundColor = [appDelegate colorWithHexString:@"8EAFBF"];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    [cell setSelectedBackgroundView:bgColorView];
    cell.textLabel.text = [self.menuItems objectAtIndex:indexPath.row];
    if(indexPath.row == self.selectedMenuIndex){
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:19.0];
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        if (!self.physicianVC) {
            UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
            self.physicianVC = [str instantiateViewControllerWithIdentifier:@"PhysicianTrackerViewController"];
            self.physicianVC.delegate = self;
        }
        if (self.rightView.subviews && self.rightView.subviews.count)
            [self.rightView.subviews[0] removeFromSuperview];
            self.physicianVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.physicianVC.view];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedMenuIndex = indexPath.row;
    [self setHighlightedTextLbl];
//    if (indexPath.row == 1) {
//        NSString *email = [[NSUserDefaults standardUserDefaults]stringForKey:@"DCEmail"];
//        [[NSUserDefaults standardUserDefaults] setObject:email forKey:MOBILE_NUMBER_STRING];
//        
//    }else if (indexPath.row == 2) {
//        NSString *mobile = [[NSUserDefaults standardUserDefaults]stringForKey:@"DCMobileNumber"];
//        [[NSUserDefaults standardUserDefaults] setObject:mobile forKey:MOBILE_NUMBER_STRING];
//    }
    switch (indexPath.row) {
        case 0:
            if (!self.leaderBoardVC) {
                UIStoryboard *str = [UIStoryboard storyboardWithName:@"Pfizer" bundle:nil];
                self.leaderBoardVC = [str instantiateViewControllerWithIdentifier:@"LeaderBoardViewController"];
                //self.physicianVC.delegate = self;
            }
            if (self.rightView.subviews && self.rightView.subviews.count)
                [self.rightView.subviews[0] removeFromSuperview];
            self.leaderBoardVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.leaderBoardVC.view];
            self.navigationItem.leftBarButtonItem = nil;
            [self setUpNavigationItems:@"LeaderBoard"];
            break;
        case 1:
            if (!self.physicianVC) {
                UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
                self.physicianVC = [str instantiateViewControllerWithIdentifier:@"PhysicianTrackerViewController"];
                self.physicianVC.delegate = self;
            }
            if (self.rightView.subviews && self.rightView.subviews.count)
                [self.rightView.subviews[0] removeFromSuperview];
            self.physicianVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.physicianVC.view];
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
            break;
        case 2:
            //PHRpharmacyVC
            if (!self.PHRpharmacyVC) {
                UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
                self.PHRpharmacyVC = [str instantiateViewControllerWithIdentifier:@"PharmacyTrackerViewController"];
                self.PHRpharmacyVC.delegate = self;
            }
            if (self.rightView.subviews && self.rightView.subviews.count)
                [self.rightView.subviews[0] removeFromSuperview];
            self.PHRpharmacyVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.PHRpharmacyVC.view];
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
            break;
        case 3:
            if (!self.trainingVC) {
                UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
                self.trainingVC = [str instantiateViewControllerWithIdentifier:@"TrainingMaterialsViewController"];
                //self.physicianVC.delegate = self;
            }
            if (self.rightView.subviews && self.rightView.subviews.count)
                [self.rightView.subviews[0] removeFromSuperview];
            self.trainingVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.trainingVC.view];
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
            break;
        case 4:
            if (!self.aboutUsVC) {
                UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
                self.aboutUsVC = [str instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
                //self.physicianVC.delegate = self;
            }
            if (self.rightView.subviews && self.rightView.subviews.count)
                [self.rightView.subviews[0] removeFromSuperview];
            self.aboutUsVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.aboutUsVC.view];
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
            break;
        case 5:
            if (!self.contactUsVC) {
                UIStoryboard *str = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
                self.contactUsVC = [str instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
                //self.physicianVC.delegate = self;
            }
            if (self.rightView.subviews && self.rightView.subviews.count)
                [self.rightView.subviews[0] removeFromSuperview];
            self.contactUsVC.view.frame = self.rightView.bounds;
            [self.rightView addSubview:self.contactUsVC.view];
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.rightBarButtonItem = nil;
            break;
            
        default:
            break;
    }
}

-(void)alertvc{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:@"Work is in Progress." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:alertAction];
    [self presentViewController:alert animated:YES completion:nil];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(indexPath.row == 0)
//        return 2;
    return 60.0;
}

-(void)setHighlightedTextLbl{
    NSInteger sections = self.menuTableView.numberOfSections;
    for (int section = 0; section < sections; section++) {
        NSInteger rows =  [self.menuTableView numberOfRowsInSection:section];
        for (int row = 0; row < rows; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            UITableViewCell *cell = [self.menuTableView cellForRowAtIndexPath:indexPath];
            if (indexPath.row == self.selectedMenuIndex) {
                cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:19.0];
            }else{
                cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:19.0];
            }
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
