//
//  PHRConsentViewController.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 02/12/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PHRConsentViewController.h"
#import "AppDelegate.h"
#import "NSDictionary+Dictionary.h"
#import "MenuList.h"
#import "Webservice.h"
#import "MenuList.h"

@interface PHRConsentViewController (){
    //AppDelegate *appdelegate;
    BOOL isScientificInformationCheck;
    BOOL isClinicalTrailsCheck;
    BOOL isWebinarsCheck;
    BOOL isMedicalCheck;
    BOOL isEducationCheck;
    BOOL isMeetingCheck;
}

@end

@implementation PHRConsentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[MenuList networkChecking];
    self.switchStateString = @"1";
    //appdelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.barTintColor = [appDelegate colorWithHexString:@"3A4F5A"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20]}];
    self.firstNameTxtField.text = [self.passedPharmacistDict stringForKey:@"FirstName"];
    self.lastNameTxtField.text = [self.passedPharmacistDict stringForKey:@"LastName"];
    //self.specialityTxtField.text = [self.passedPharmacistDict stringForKey:@"Speciality"];
    self.emailTxtField.text = [self.passedPharmacistDict stringForKey:@"EmailID1"];
    self.interestedArrayList = [[NSMutableArray alloc]init];
    self.submitBtn.layer.cornerRadius = 4.0;
    [self.mainScrollView setCanCancelContentTouches:NO];
    self.firstNameTxtField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.firstNameTxtField.layer.borderWidth = 1.0;
    self.firstNameTxtField.layer.cornerRadius = 4.5;
    self.lastNameTxtField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.lastNameTxtField.layer.borderWidth = 1.0;
    self.lastNameTxtField.layer.cornerRadius = 4.5;
    //self.specialityTxtField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //self.specialityTxtField.layer.borderWidth = 1.0;
    //self.specialityTxtField.layer.cornerRadius = 4.5;
    self.emailTxtField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.emailTxtField.layer.borderWidth = 1.0;
    self.emailTxtField.layer.cornerRadius = 4.5;
    self.signatureBG.layer.borderWidth = 1.0;
    self.signatureBG.layer.cornerRadius = 5.0;
    self.signatureBG.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [_mainScrollView setCanCancelContentTouches:NO];
}
-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = LOADING_MESSAGE;
}

- (IBAction)checkBtnAction:(id)sender {
    NSString *checkImg;
    switch ([(UIButton*)sender tag]) {
        case 100:
            if (isScientificInformationCheck) {
                isScientificInformationCheck = NO;
                checkImg = @"Consent_checkbox";
                [self updateInterestedList:1];
            }else{
                isScientificInformationCheck = YES;
                checkImg = @"Consent_checkbox_selected";
                [self.interestedArrayList addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"1",@"InterestedValue",[CONSENT_INTERESTED_ARRAY objectAtIndex:0] ,@"InterestedText",nil]];
            }
            break;
            
        case 101:
            if (isClinicalTrailsCheck) {
                isClinicalTrailsCheck = NO;
                checkImg = @"Consent_checkbox";
                [self updateInterestedList:3];
            }else{
                isClinicalTrailsCheck = YES;
                checkImg = @"Consent_checkbox_selected";
                [self.interestedArrayList addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"3",@"InterestedValue",[CONSENT_INTERESTED_ARRAY objectAtIndex:2] ,@"InterestedText",nil]];
            }
            break;
            
        case 102:
            if (isWebinarsCheck) {
                isWebinarsCheck = NO;
                checkImg = @"Consent_checkbox";
                [self updateInterestedList:5];
            }else{
                isWebinarsCheck = YES;
                checkImg = @"Consent_checkbox_selected";
                [self.interestedArrayList addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"5",@"InterestedValue",[CONSENT_INTERESTED_ARRAY objectAtIndex:4] ,@"InterestedText",nil]];
            }
            break;
            
        case 103:
            if (isMedicalCheck) {
                isMedicalCheck = NO;
                checkImg = @"Consent_checkbox";
                [self updateInterestedList:2];
            }else{
                isMedicalCheck = YES;
                checkImg = @"Consent_checkbox_selected";
                [self.interestedArrayList addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"2",@"InterestedValue",[CONSENT_INTERESTED_ARRAY objectAtIndex:1] ,@"InterestedText",nil]];
            }
            break;
            
        case 104:
            if (isEducationCheck) {
                isEducationCheck = NO;
                checkImg = @"Consent_checkbox";
                [self updateInterestedList:4];
                
            }else{
                isEducationCheck = YES;
                checkImg = @"Consent_checkbox_selected";
                [self.interestedArrayList addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"4",@"InterestedValue",[CONSENT_INTERESTED_ARRAY objectAtIndex:3] ,@"InterestedText",nil]];
            }
            break;
            
        case 105:
            if (isMeetingCheck) {
                isMeetingCheck = NO;
                checkImg = @"Consent_checkbox";
                [self updateInterestedList:6];
            }else{
                isMeetingCheck = YES;
                checkImg = @"Consent_checkbox_selected";
                [self.interestedArrayList addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"6",@"InterestedValue",[CONSENT_INTERESTED_ARRAY objectAtIndex:5] ,@"InterestedText",nil]];
            }
            break;
            
        default:
            break;
    }
    [(UIButton*)sender setImage:[UIImage imageNamed:checkImg] forState:UIControlStateNormal];
}
-(void)updateInterestedList:(int)sender{
    if (!([self.interestedArrayList count] > 0)) {
        return;
    }
    for (NSDictionary *dict in self.interestedArrayList) {
        int dictValue = [[dict objectForKey:@"InterestedValue"] intValue];
        if (dictValue == sender) {
            [self.interestedArrayList removeObject:dict];
            break;
        }
    }
}

-(IBAction)switchButtonAction:(UISwitch *)sender{
    if (sender.on) {
        NSLog(@"Switch is On");
        self.switchStateString = @"1";
        for (UIButton *checkButton in self.checkOptionsButton) {
            checkButton.userInteractionEnabled = YES;
            checkButton.alpha = 1.0;
        }
    }else{
        NSLog(@"Switch is OFF");
        self.switchStateString = @"0";
        for (UIButton *checkButton in self.checkOptionsButton) {
            NSLog(@"count:%lu",(unsigned long)self.checkOptionsButton.count);
            checkButton.userInteractionEnabled = NO;
            checkButton.alpha = 0.5;
        }
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect tmpFrame = self.signatureBG.frame;
    if (!signView) {
        signView = [[HCPSignatureView alloc]initWithFrame:CGRectMake(0, 0, tmpFrame.size.width, tmpFrame.size.height)];
        [self.signatureBG addSubview:signView];
    }
    CGRect Frame = self.mainScrollView.frame;
    // self.mainScrollView.frame = Frame;
    self.mainScrollView.contentInset = UIEdgeInsetsZero;
    self.mainScrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width,Frame.size.height + 745);
}

-(void)passedDictFromAddPhysician:(NSMutableDictionary *)dict{
    self.passedPharmacistDict = [NSMutableDictionary dictionaryWithDictionary:dict];
}

- (IBAction)submitAction:(id)sender {
    // CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGSize screenSize = CGSizeMake(self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height);
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctx = CGBitmapContextCreate(nil, screenSize.width, screenSize.height, 8, 4*(int)screenSize.width, colorSpaceRef, kCGImageAlphaPremultipliedLast);
    CGContextTranslateCTM(ctx, 0.0, screenSize.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    [(CALayer*)self.mainScrollView.layer renderInContext:ctx];
    CGImageRef cgImage = CGBitmapContextCreateImage(ctx);
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    self.screenShotData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(image, 0.1)];
    if ([self.switchStateString isEqualToString:@"1"]) {
        if ([signView getSignatureImage]) {
            self.signatureImage = [signView getSignatureImage];
            self.signatureData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.signatureImage, 0.1)];
            if (self.interestedArrayList.count > 0) {
                [self consentServiceCall:self.interestedArrayList];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please select atleast one option from interested receiving information from list." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:action];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Your Signature is missing." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else{
        if ([signView getSignatureImage]) {
            self.signatureImage = [signView getSignatureImage];
            self.signatureData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.signatureImage, 0.1)];
            [self consentServiceCall:self.interestedArrayList];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Your Signature is missing." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void)consentServiceCall:(NSMutableArray *)interestedArray{
    NSString *mobNum = [[NSUserDefaults standardUserDefaults]stringForKey:MOBILE_NUMBER_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",
                           KBASE_URL,PHARMACY_CONSENT,mobNum];
    [self showProgressView];
    //offline
    NSDictionary *dicConsentData = [self getConsentData:interestedArray];
    //offline
    [Webservice postWithURlString:urlString withParameters:dicConsentData sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            if ([data[@"Message"] isEqualToString:@"SUCCESS"]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:@"Details submitted successfully." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    if ([self.consentDelegate respondsToSelector:@selector(consentSuccessMethodAction)]) {
                        [self.consentDelegate consentSuccessMethodAction];
                    }
                }];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        //offline
        /*
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:alertAction];
         [self presentViewController:alert animated:YES completion:nil];
         */
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
            AppDelegate *delG =  appDelegate;
            if(!delG.pharmacistNewMissionConsentArray){
                delG.pharmacistNewMissionConsentArray = [NSMutableArray new];
            }
            NSLog(@"now Count : %ld",delG.pharmacistNewMissionConsentArray.count);
            NSLog(@"Consent Saved Pharmacist");
            
            [delG.pharmacistNewMissionConsentArray addObject:[NSMutableDictionary dictionaryWithDictionary:dicConsentData]];
            if ([self.consentDelegate respondsToSelector:@selector(consentSuccessMethodAction)]) {
                //remove this record from list of tracker
                [self.consentDelegate consentSuccessMethodAction];
            }
        }
        //offline
    }];
}

-(NSDictionary *)getConsentData:(NSMutableArray *)interestedArray{
    NSString *pharmacistID;
    if (![[self.passedPharmacistDict stringForKey:@"PharmacistTransactionID"] isEqualToString:@"0"] && ![[self.passedPharmacistDict stringForKey:@"PharmacistTransactionID"] isEqualToString:@""]) {
        pharmacistID = [self.passedPharmacistDict stringForKey:@"PharmacistTransactionID"];
    }else{
        pharmacistID = appDelegate.pharmacistTransactionID;
    }
    NSDictionary *physicianConsentData = [[NSDictionary alloc] initWithObjectsAndKeys:pharmacistID,@"PharmacistTransactionID",appDelegate.pharmacyId,@"PharmacyId",self.switchStateString,@"ConsentEmailAgreed",interestedArray.count>0?interestedArray:@"",@"InterestedListBO",[self.signatureData base64EncodedStringWithOptions:NSUTF8StringEncoding]?[self.signatureData base64EncodedStringWithOptions:NSUTF8StringEncoding]:@"",@"DigitalSignature",@"",@"ScreenShot",nil];
    return physicianConsentData;
}

//offline
-(void)removePharmacistIfSavedForOfflineMode;{
    NSDictionary *dicToBeRemoved = nil;
    for (NSDictionary *dicPharmacist in appDelegate.pharmacistNewMissionArray){
        if (dicPharmacist[@"PharmacistTransactionID"]){
            if([dicPharmacist[@"PharmacistTransactionID"] isEqualToString:self.passedPharmacistDict[@"PharmacistTransactionID"]]){
                dicToBeRemoved = dicPharmacist;
                break;
            }
        }else{
            if([dicPharmacist[@"EmailID1"] isEqualToString:self.passedPharmacistDict[@"EmailID1"]]){
                dicToBeRemoved = dicPharmacist;
                break;
            }
        }
       
    }
    if(dicToBeRemoved){
        [appDelegate.pharmacistNewMissionArray removeObject:dicToBeRemoved];
    }
    
    
    //Add this pharmacist in the current list of pharmacy.
    NSMutableDictionary *dicToBeReplaced;
    NSMutableArray *arrArrToBeReplaced;
    NSUInteger index = -1;
    for(NSDictionary *dic in appDelegate.pharmacistNewMissionCurrentPharmacistList){
        if([dic[@"PharmacyId"] isEqualToString:self.passedPharmacistDict[@"PharmacyId"]]){
            index = [appDelegate.pharmacistNewMissionCurrentPharmacistList indexOfObject:dic];
            dicToBeReplaced = [NSMutableDictionary dictionaryWithDictionary:dic];
            if(dic[@"PharmacistTicketMaster"]){
                arrArrToBeReplaced = [NSMutableArray arrayWithArray:dic[@"PharmacistTicketMaster"]];
                //finding based on email id as pharmacist id still not generated.
                    
                    for(NSDictionary *dic in arrArrToBeReplaced){
                        if([dic[@"PharmacyId"] isEqualToString:self.passedPharmacistDict[@"PharmacyId"]] &&
                           [dic[@"EmailID1"] isEqualToString:self.passedPharmacistDict[@"EmailID1"]]){
                            NSUInteger index = [arrArrToBeReplaced indexOfObject:dic];
                            [arrArrToBeReplaced removeObjectAtIndex:index];
                            break;
                        }
                    }
                    //replace existing..
                
                
            }
            dicToBeReplaced[@"PharmacistTicketMaster"] = arrArrToBeReplaced;
            break;
        }
    }
    if(index != -1){
        [appDelegate.pharmacistNewMissionCurrentPharmacistList replaceObjectAtIndex:index withObject:dicToBeReplaced];
        //self.passedPharmacistDict = dicToBeReplaced;
    }
    
}
//offline
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
