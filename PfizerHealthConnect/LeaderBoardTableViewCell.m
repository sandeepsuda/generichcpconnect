//
//  LeaderBoardTableViewCell.m
//  IPadApp
//
//  Created by RajaSekhar on 04/11/16.
//  Copyright © 2016 RajaSekhar. All rights reserved.
//

#import "LeaderBoardTableViewCell.h"

@implementation LeaderBoardTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.frame.size.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
