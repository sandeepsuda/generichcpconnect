//
//  AddPhysicianPopViewController.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 18/01/17.
//  Copyright © 2017 IMS Health. All rights reserved.
//

#import "AddPhysicianPopViewController.h"



@interface AddPhysicianPopViewController ()

@end

@implementation AddPhysicianPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)cameraButton:(id)sender{
    if ([self.delegate respondsToSelector:@selector(openCamera:)]) {
        [self.delegate openCamera:sender];
    }
}

-(IBAction)galleryButton:(id)sender{
    if ([self.delegate respondsToSelector:@selector(openGallery:)]) {
        [self.delegate openGallery:sender];
    }
}
- (IBAction)newPhysician:(id)sender {
    if ([self.delegate respondsToSelector:@selector(addNewPhysicianForm)]) {
        [self.delegate addNewPhysicianForm];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
