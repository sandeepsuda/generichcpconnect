//
//  VerifyPharmacyDetailsViewController.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 23/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PharmacyDetailsViewController.h"
@protocol VerifyPharmacyProtocol <NSObject>
@end

@interface VerifyPharmacyDetailsViewController : PharmacyDetailsViewController
@property (nonatomic,weak) id<VerifyPharmacyProtocol> verifyPharmacyDelegate;
@property (nonatomic,strong) NSMutableDictionary *passedPharmacistDict,*mobileNumbersDict;
@property (nonatomic,strong) NSData *buildingPicData;
/**Pharmacy**/
@property (weak, nonatomic) IBOutlet UIButton *verifyPharmacyName;
@property (weak, nonatomic) IBOutlet UIButton *verifyAddress1;
@property (weak, nonatomic) IBOutlet UIButton *verifyAddress2;
@property (weak, nonatomic) IBOutlet UIButton *verifyCity;
@property (weak, nonatomic) IBOutlet UIButton *verifyCountry;
@property (weak, nonatomic) IBOutlet UIButton *verifyOwnerName;
@property (weak, nonatomic) IBOutlet UIButton *verifyLandlineNumber;
@property (weak, nonatomic) IBOutlet UIButton *verifyLandlineNumber2;
@property (weak, nonatomic) IBOutlet UIButton *verifyLandlineNumber3;
@property (weak, nonatomic) IBOutlet UIButton *verifyNoPhysicians;
@property (weak, nonatomic) IBOutlet UIButton *verifyBuildingPic;
@property (weak, nonatomic) IBOutlet UIButton *verifyMap;
/**Pharmacy**/
@property (weak, nonatomic) IBOutlet UIButton *verifyPharmacySubmitBtn;


/**XYView**/
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *pharmacistDeniedInfoCountLbl;
@property (weak, nonatomic) IBOutlet UITableView *pharmacistsTbl;
@property (weak, nonatomic) IBOutlet UIView *topViewBtn;
@property (weak, nonatomic) IBOutlet UIView *viewBotton;
@property (weak, nonatomic) IBOutlet UILabel *pharmacyNameLbl;
@property (nonatomic,strong) NSMutableArray *pharmacistsList;
@property (weak, nonatomic) IBOutlet UIView *scoreView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scoreViewHightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *addedPharmacistsCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPharmacistsCountLbl;

-(void)reloadPharmacistsList;
-(void)addXYDetailsView;

/**Pharmacist**/
@property (weak, nonatomic) IBOutlet UIButton *verifyFirstName;
@property (weak, nonatomic) IBOutlet UIButton *verifyLastName;
@property (weak, nonatomic) IBOutlet UIButton *verifyMobNo1;
@property (weak, nonatomic) IBOutlet UIButton *verifyMobNo2;
@property (weak, nonatomic) IBOutlet UIButton *verifyMobNo3;
@property (weak, nonatomic) IBOutlet UIButton *verifyEmailID1;
@property (weak, nonatomic) IBOutlet UIButton *verifyEmailID2;
@property (weak, nonatomic) IBOutlet UIButton *verifyEmailID3;
@property (weak, nonatomic) IBOutlet UIButton *verifyYrOfGrad;
@property (weak, nonatomic) IBOutlet UIButton *verifyShiftTimings;
@property (weak, nonatomic) IBOutlet UIButton *verifyFromShiftTimings;
@property (weak, nonatomic) IBOutlet UIButton *verifyToShiftTimings;
@property (weak, nonatomic) IBOutlet UIButton *verifyPharmacistSubmitBtn;


/**Pharmacist**/
@end
