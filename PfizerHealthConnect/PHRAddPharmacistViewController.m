//
//  PHRNewAddPharmacistViewController.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 25/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PHRAddPharmacistViewController.h"
#import "MenuList.h"
#import "AppDelegate.h"
#import "AddPharmacistVCTableViewCell.h"

@interface PHRAddPharmacistViewController ()
@property(nonatomic,assign)BOOL framesAreSet;
@end

@implementation PHRAddPharmacistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.submitBtn.layer.cornerRadius = 4.0;
    self.pharmacistsList = [NSMutableArray arrayWithObjects:@"Bruce Wayne",@"Alfred",@"Bruce Wayne",@"Alfred", nil];
    self.topView.backgroundColor = [UIColor clearColor];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.pharmacistsTbl.backgroundColor = [UIColor clearColor];
    self.pharmacistsTbl.separatorStyle = UITableViewCellSeparatorStyleNone;
    UINib *cellNib = [UINib nibWithNibName:@"AddPharmacistVCTableViewCell" bundle:nil];
    [self.pharmacistsTbl registerNib:cellNib forCellReuseIdentifier:@"AddPharmacistVCTableViewCellIdentifier"];
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!self.framesAreSet) {
        self.framesAreSet = YES;
    }else{
        CGRect tmpFrame = self.scoreView.frame;
        self.scoreView.layer.cornerRadius = tmpFrame.size.height/2;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MenuList networkChecking];
    self.view.backgroundColor = appDelegate.isVerify?[appDelegate colorWithHexString:RamGreenColor]:[appDelegate colorWithHexString:BlueColor];
    [self.pharmacistsTbl reloadData];
}
- (IBAction)submitBtnAction:(id)sender {
    
}
- (IBAction)addNewPharmacistAction:(id)sender {
    
}
#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.pharmacistsList.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier =@"AddPharmacistVCTableViewCellIdentifier";
    AddPharmacistVCTableViewCell *cell =
    (AddPharmacistVCTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.contentView.backgroundColor = self.view.backgroundColor;
    cell.pharmacistNameLbl.text = [self.pharmacistsList objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
