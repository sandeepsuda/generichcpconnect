//
//  PharmacistConsentViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 09/01/17.
//  Copyright © 2017 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCPSignatureView.h"
#import "MBProgressHUD.h"

@protocol PHRConsentViewControllerDelegate <NSObject>

-(void)consentSubmitMethodAction:(UIImage *)image;
-(void)consentSuccessMethodAction;

@end

@interface PharmacistConsentViewController : UIViewController<UIAlertViewDelegate>{
    HCPSignatureView *signView;
    UIButton *closeBtn;
}
@property (nonatomic,weak) IBOutlet UILabel *consentHeadingLabel,*consentBulletListLabel,*consentDescription1Label;
@property (nonatomic,weak) IBOutlet UITextView *consentDescription2TextView;
@property (nonatomic,strong) NSData *signatureData,*screenShotData;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *checkOptionsButton;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (nonatomic,strong) NSString *signatureDataString;
@property (nonatomic,weak) id <PHRConsentViewControllerDelegate> consentDelegate;
@property (nonatomic,strong) NSMutableArray *interestedArrayList;
@property (nonatomic,strong) UIImage *signatureImage;
@property (nonatomic,strong) NSMutableDictionary *passedPharmacistDict,*signatureDict;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UIView *signatureBG;
@property (weak, nonatomic) IBOutlet UISwitch *emailCheckBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (nonatomic,strong) NSString *switchStateString;
@property (nonatomic,weak) IBOutlet UILabel *pleaseEmailMeLabel,*companyAddressLabel;
@property (nonatomic,weak) IBOutlet UILabel *firstNameLabel, *lastNameLabel, *specialityLabel, *emailIDLabel, *mobileLabel, *workNumberLabel, *interestedLabel, *scientificLabel, *medicalLabel, *clinicalLabel, *educationLabel, *webinarLabel, *meetingLabel;

-(void)passedDictFromAddPhysician:(NSMutableDictionary *)dict;
-(IBAction)switchButtonAction:(UISwitch *)sender;
- (IBAction)submitAction:(id)sender;
//offline
-(void)removePharmacistIfSavedForOfflineMode;
//offline

@property (weak, nonatomic) IBOutlet UIView *consentViewPartA;
@property (strong, nonatomic) IBOutlet UIView *consentViewPartB;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@end
