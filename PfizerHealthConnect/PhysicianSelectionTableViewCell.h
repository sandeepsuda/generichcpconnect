//
//  PhysicianSelectionTableViewCell.h
//  PfizerHealthConnect
//
//  Created by sandeep suda on 13/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhysicianSelectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *physicianNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *hospitalDetails;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@end
