//
//  VerificationViewController.m
//  PharmConnect
//
//  Created by Utpal  on 09/05/16.
//  Copyright © 2016 Kumar, Utpal (Bangalore). All rights reserved.
//

#import "VerificationViewController.h"
#import "TrainingPageViewController.h"
#import "MenuList.h"
@import SinchVerification;


@interface VerificationViewController ()


@end

@implementation VerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.containerView.layer.cornerRadius = 10.0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"OTPPAGE"];
    self.submitBtn.layer.cornerRadius = 4.0;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
}


#pragma mark - UIBarButtonItems Method

- (IBAction)clearButtonAction:(id)sender{
    self.code.text = @"";
}

- (IBAction)doneButtonAction:(id)sender{
    //[self checkVerifyCode];
}

#pragma mark - Custom Methods

-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
  ////  self.mbProgressView.label.text = LOADING_MESSAGE;
}

-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}

-(void)setUpBarButtonItems{
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done"
                                   style:UIBarButtonItemStyleDone
                                   target:self
                                   action:@selector(verifyCode:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Clear"
                                    style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(clearButtonAction:)];
    self.navigationItem.leftBarButtonItem = clearButton;
    
}

#pragma mark - UIButton Methods

- (IBAction)verifyCode:(id)sender{
    if ([_code.text isEqualToString:@""])
    {
        [_code resignFirstResponder];
        _status.text = @"You must enter a code";
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:OOPS_MESSAGE
                                    message:@"You must enter a password."
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        [_code resignFirstResponder];
        NSString  *mobileNumber = [[NSUserDefaults standardUserDefaults]stringForKey:EMAIL_STRING];
        NSString *accessCode = self.code.text;
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,HCP_Verify_CongratsPage,mobileNumber,accessCode];
        [self showProgressView];
        [Webservice getWithUrlString:urlString success:^(id data) {
            //Success
            [self.mbProgressView hideAnimated:YES];
            if (data) {
                if ([data[@"Message"] isEqualToString:@"SUCCESS"]) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        if ([self.delegate respondsToSelector:@selector(moveToTrainingPageVC)]) {
                            [self.delegate moveToTrainingPageVC];
                        } }];
                }else if([data[@"Message"] isEqualToString:@"FAIL"]){
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Invalid Password.Please enter correct password." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:error.description preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }];
        
    }
}

- (IBAction)resendVerificationCode:(id)sender {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    self.mobileNumberWithCountryCode = [defaults objectForKey:@"MobileNumberWithCountryCode"];
//    self.verification = [SINVerification SMSVerificationWithApplicationKey:@"5ed9928a-36c4-4d3b-a012-a4cc1d05cb79" phoneNumber:self.mobileNumberWithCountryCode];
//    [self.verification initiateWithCompletionHandler:^(BOOL success, NSError *error) {
//        if (success) {
//        } else {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *alertAction = [UIAlertAction  actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alert addAction:alertAction];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    }];

}

#pragma mark - UITextfield Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UIView Life Cycle Methods

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MenuList networkChecking];
 /*   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
  */
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Keyboard Hide & Show Methods

- (void)keyboardWasShown:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void)keyboardWillBeHidden:(NSNotification *)notification
{
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
