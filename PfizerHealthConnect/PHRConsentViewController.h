//
//  PHRConsentViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 02/12/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCPSignatureView.h"
#import "MBProgressHUD.h"
@protocol PHRConsentViewControllerDelegate <NSObject>

-(void)consentSubmitMethodAction:(UIImage *)image;
-(void)consentSuccessMethodAction;

@end
@interface PHRConsentViewController : UIViewController{
    HCPSignatureView *signView;
}
@property (nonatomic,strong) NSData *signatureData,*screenShotData;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *checkOptionsButton;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (nonatomic,strong) NSString *signatureDataString;
@property (nonatomic,weak) id <PHRConsentViewControllerDelegate> consentDelegate;
@property (nonatomic,strong) NSMutableArray *interestedArrayList;
@property (nonatomic,strong) UIImage *signatureImage;
@property (nonatomic,strong) NSMutableDictionary *passedPharmacistDict,*signatureDict;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxtField;
//@property (weak, nonatomic) IBOutlet UITextField *specialityTxtField;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UIView *signatureBG;
@property (weak, nonatomic) IBOutlet UISwitch *emailCheckBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (nonatomic,strong) NSString *switchStateString;
-(void)passedDictFromAddPhysician:(NSMutableDictionary *)dict;
-(IBAction)switchButtonAction:(UISwitch *)sender;
- (IBAction)submitAction:(id)sender;
//offline
-(void)removePharmacistIfSavedForOfflineMode;
//offline
@end
