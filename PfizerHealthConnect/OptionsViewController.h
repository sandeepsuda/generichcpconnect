//
//  OptionsViewController.h
//  PharmConnect
//
//  Created by Abhatia on 20/10/16.
//  Copyright © 2016 Kumar, Utpal (Bangalore). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) NSMutableArray *optionsArray;
@property (nonatomic)CGRect tableFrame;
@property (nonatomic,weak) UITextField *selectedTextfield;
@property (nonatomic,weak) UITextField *noOfPatientsTxtFld;
@property (nonatomic,weak)IBOutlet UITableView *tableView;
- (id)initWithTextfield:(UITextField *)textfield patientsTxtFld:(UITextField*)patientsFld andOptions:(NSArray *)optionsArr;
@end
