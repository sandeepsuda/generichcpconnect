//
//  MenuList.m
//  HealthConnect
//
//  Created by Abhatia on 06/01/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "MenuList.h"
#import <AFNetworking.h>

@implementation MenuList
static MenuList *sharedMenuList = nil;
+ (MenuList *)sharedList {
    if (sharedMenuList == nil) {
        sharedMenuList = [[super allocWithZone:NULL] init];
    }
    return sharedMenuList;
}

+(void)networkChecking{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if ([AFStringFromNetworkReachabilityStatus(status) isEqualToString:@"Not Reachable"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:@"You don't have Network Connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}



@end
