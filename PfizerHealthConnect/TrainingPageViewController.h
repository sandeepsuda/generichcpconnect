//
//  TrainingPageViewController.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 02/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "Webservice.h"

@interface TrainingPageViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property(weak,nonatomic)IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *almostLbl;
@property (weak, nonatomic) IBOutlet UILabel *topContentLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceConstraintOne;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceConstraintTwo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceConstraintThree;
@property (weak, nonatomic) IBOutlet UITextView *instructionsTxtLbl;
@property (weak, nonatomic) IBOutlet UILabel *thanksLbl;
@property (weak, nonatomic) IBOutlet UILabel *allTheBestLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightViewVerticalSpaceConstraintOne;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightViewVerticalConstraintTwo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightViewVerticalSpaceConstraintThree;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightViewVerticalSpaceConstraintFour;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *instructionTxtHightConstraint;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (weak,nonatomic) IBOutlet UITableView *questionsTable;
@property (nonatomic,weak)IBOutlet UIWebView *webTraining;
@property (weak,nonatomic)NSArray *arrMasterQuiz;
@property(nonatomic,strong)NSMutableDictionary *dicSelectedQuestionAnsIDs;
@property (nonatomic,weak)IBOutlet UITableView *leftPaneTableView;
@property (nonatomic,weak)IBOutlet UIView *rightPaneView;
@property (nonatomic,weak)IBOutlet UIButton *btnQuizSubmit;
@property (nonatomic,strong)NSArray *arrLeftPaneOptions;

- (IBAction)submitButton:(id)sender;
@property(nonatomic)BOOL isDowloadingQuiz;
@end
