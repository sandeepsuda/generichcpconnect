//
//  RegistrationViewController.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 25/10/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PHCPRegistrationViewController.h"
#import "PopOverViewController.h"
#import "MBProgressHUD.h"
#import "MenuList.h"
#import "Webservice.h"
#import "TrainingPageViewController.h"
#import "WelcomePageViewController.h"
#import "LeftViewController.h"

@interface PHCPRegistrationViewController (){
    NSString *emailIDAllows;
    PopOverViewController *popVC;
}
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@end

@implementation PHCPRegistrationViewController

- (IBAction)test:(UIButton *)sender {
    [self.view endEditing:YES];
    [MenuList networkChecking];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    popVC = (PopOverViewController *)[storyboard instantiateViewControllerWithIdentifier:@"Pop"];
    popVC.arrOptions = self.countryNamesArray;
    popVC.modalPresentationStyle = UIModalPresentationPopover;
    popVC.countryField = self.countryTextField;
    popVC.countryCodeField = self.countryCodeTextField;
    UIPopoverPresentationController *presentationController =[popVC popoverPresentationController];
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    presentationController.sourceView = self.countryTextField;
    presentationController.sourceRect = self.countryTextField.bounds;
    if ([self.countryNamesArray count] > 0) {
        [self presentViewController:popVC animated:YES completion:nil];
    }else{
        [self getCountryNamesList];
    }
}

#pragma mark-View LifeCycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.showPopUp) {
        [self showVerificationPopUp];
    }
    emailIDAllows = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_@.0123456789-";
    self.countryNamesArray = [[NSArray alloc]init];
    self.title = @"Registration";
    self.navigationController.navigationBar.barTintColor = [appDelegate colorWithHexString:@"3A4F5A"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20]}];
    self.signMeUpBtn.layer.cornerRadius = 4.0;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.countryCodeTextField.userInteractionEnabled = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
}
-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField == self.countryTextField) {
        [self test:nil];
        return NO;
    }
    if (textField == self.photoTextField) {
        [self cameraButtonPressed:nil];
        return NO;
    }
    if (textField == self.firstNameTextField || textField == self.lastNameTextField || textField == self.cityTextField) {
        textField.keyboardType = UIKeyboardTypeAlphabet;
    }
    if (textField == self.emailTextField) {
        textField.keyboardType = UIKeyboardTypeDefault;
    }
    if (textField == self.mobileTextField) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.firstNameTextField || textField == self.lastNameTextField || textField == self.cityTextField)
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:ONLY_ALPHABETS_WITHSPACE_MESSAGE] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }
//    else if (textField == self.emailTextField) {
//        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:emailIDAllows] invertedSet] invertedSet];
//        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
//        return [string isEqualToString:output];
//    }
    if(textField == self.mobileTextField){
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MenuList networkChecking];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = LOADING_MESSAGE;
}

#pragma mark - TextField Validation Methods

-(BOOL)isDataValid{
    if(self.firstNameTextField.text &&  self.lastNameTextField.text && self.cityTextField.text && ![self.firstNameTextField.text isEqualToString:@""]&& ![self.lastNameTextField.text isEqualToString:@""] && ![self.cityTextField.text isEqualToString:@""] && ![self.mobileTextField.text isEqualToString:@""] && ![self.countryTextField.text isEqualToString:@""] && ![self.countryCodeTextField.text isEqualToString:@""]){
        return YES;
    }
    return NO;
}

- (BOOL)checkValidEmail {
    BOOL validEmail = NO;
    if (self.emailTextField.text != nil) {
        NSString *expression = EMAIL_VALIDATION_STRING;
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSTextCheckingResult* regexResult = [regex firstMatchInString:self.emailTextField.text options:0 range:NSMakeRange(0, [self.emailTextField.text length])];
        validEmail = regexResult != nil;
    }
    return validEmail;
}

#pragma mark - Bar button Methods
- (IBAction)clearButtonAction:(UIBarButtonItem *)sender{
    [self.view endEditing:YES];
    self.countryTextField.text = nil;
    self.photoTextField.text = nil;
    self.countryCodeTextField.text = nil;
    self.firstNameTextField.text = nil;
    self.lastNameTextField.text = nil;
    self.mobileTextField.text = nil;
    self.emailTextField.text = nil;
    self.cityTextField.text = nil;
}
- (IBAction)backButtonAction:(UIBarButtonItem *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma VerificationDelegate methods
-(void)moveToTrainingPageVC{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TrainingPageViewController *trainingPageViewController =[storyboard instantiateViewControllerWithIdentifier:@"TrainingPageViewController"];
    [self.navigationController pushViewController:trainingPageViewController animated:YES];
}

-(IBAction)SignMeUp:(id)sender{
    [self.view endEditing:YES];
    if (![self isDataValid]) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:OOPS_MESSAGE
                                    message:SOMETHING_MISSING_MESSAGE
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if (![self checkValidEmail]){
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:OOPS_MESSAGE
                                    message:VALID_EMAIL_ID_MESSAGE
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if(!self.cameraImage){
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:OOPS_MESSAGE
                                    message:@"Please select image."
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:self.firstNameTextField.text forKey:@"FirstName"];
    [[NSUserDefaults standardUserDefaults] setObject:self.lastNameTextField.text forKey:@"LastName"];
    [[NSUserDefaults standardUserDefaults] setObject:self.emailTextField.text forKey:@"DCEmail"];
    [[NSUserDefaults standardUserDefaults] setObject:self.mobileTextField.text forKey:@"DCMobileNumber"];
    [[NSUserDefaults standardUserDefaults] setObject:self.countryTextField.text forKey:@"DCCountry"];
    [[NSUserDefaults standardUserDefaults] setObject:self.cityTextField.text forKey:@"DCCityOfResidence"];
    [[NSUserDefaults standardUserDefaults] setObject:self.photoTextField.text forKey:@"ImageData"];
    NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(_cameraImage, 0.1)];
    [[NSUserDefaults standardUserDefaults] setObject:imgData forKey:@"ImageData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:@"Please validate your details before submission" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self newUserRegistration];
    }];
    UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        recheck = YES;
    }];
    [alert addAction:alertActionCancel];
    [alert addAction:alertAction];
    if (recheck == NO) {
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self newUserRegistration];
    }
}

#pragma mark - Service Call Methods

-(void)getCountryNamesList{
    [self showProgressView];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",KBASE_URL,HCP_GETCountryList];
    [Webservice getWithUrlString:urlString success:^(id data) {
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.countryNamesArray = (NSArray*)data;
            popVC.arrOptions = self.countryNamesArray;
            [self presentViewController:popVC animated:YES completion:nil];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(NSDictionary *)getRegistrationData{
    NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.cameraImage, 0.1)];
    NSString *mobileNumber = self.mobileTextField.text;
    [[NSUserDefaults standardUserDefaults] setObject:mobileNumber forKey:MOBILE_NUMBER_STRING];
    [[NSUserDefaults standardUserDefaults] setObject:self.emailTextField.text forKey:EMAIL_STRING];
    NSDictionary *dict = @{
                           @"FirstName":self.firstNameTextField.text,
                           @"LastName":self.lastNameTextField.text,
                           @"DCCode":self.mobileTextField.text,
                           @"DCEmail":self.emailTextField.text,
                           @"DCMobileNumber":self.mobileTextField.text,
                           @"DCCountry":self.countryTextField.text,
                           @"DCCityOfResidence":self.cityTextField.text,
                           @"ImageData":[imgData base64EncodedStringWithOptions:NSUTF8StringEncoding]
                           };
    return dict;
}

-(void)newUserRegistration {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self showProgressView];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",KBASE_URL,REGISTRATION];
    [Webservice postWithURlString:urlString withParameters:[self getRegistrationData] sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            NSDictionary *userStatus = (NSDictionary*)data[@"DCStatusBO"];
            if([userStatus[@"Message"] isEqualToString:@"SUCCESS"] || [userStatus[@"Message"] isEqualToString:@"OTPPAGE"]){
                [self showVerificationPopUp];
            }
            if([userStatus[@"Message"] isEqualToString:@"QUIZPAGE"]){
                TrainingPageViewController *trainingPageVC = [storyboard instantiateViewControllerWithIdentifier:@"TrainingPageViewController"];
                [self.navigationController pushViewController:trainingPageVC animated:YES];
            }
            if([userStatus[@"Message"] isEqualToString:@"MYTRACKERPAGE"]){
                LeftViewController *tracker =[storyboard instantiateViewControllerWithIdentifier:@"LeftViewController"];
                [self.navigationController pushViewController:tracker animated:YES];
            }
            if ([userStatus[@"Message"] isEqualToString:@"WELCOMEPAGE"]){
                WelcomePageViewController *landingPageVC = [storyboard instantiateViewControllerWithIdentifier:@"WelcomePageViewController"];
                [self.navigationController pushViewController:landingPageVC animated:YES];
            }
        }else{
            [self.mbProgressView hideAnimated:YES];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
 }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:error.description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(void)showVerificationPopUp{
    VerificationViewController *verificationVC = [[VerificationViewController alloc] initWithNibName:@"VerificationView" bundle:nil];
    verificationVC.delegate = self;
    verificationVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:verificationVC animated:YES completion:nil];
}

#pragma mark - Camera Methods

- (IBAction)cameraButtonPressed:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:CLICK_PHOTO_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self callImagePicker:UIImagePickerControllerSourceTypeCamera];
    }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:UPLOAD_IMAGE_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { [self callImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }];
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.photoTextField;
    popPresenter.sourceRect = self.photoTextField.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)clickPicture{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:NULL];
    }
    else{
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:OOPS_MESSAGE
                                    message:CAMERA_NOT_AVAILABLE_MESSAGE
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)callImagePicker:(UIImagePickerControllerSourceType)type{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = type;
    [self presentViewController:imagePicker animated:YES completion:NULL];
}

#pragma mark - UIImagePickerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    UIImage *image_answer = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
    self.cameraImage = image_answer;
    self.photoTextField.text = @"You have successfully uploaded the image.";
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Keyboard Hide & Show Methods

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    UIScrollView *scrollView = (UIScrollView *)self.containerScrollView;
    self.initialContentSize = scrollView.contentSize;
    // keyboard frame is in window coordinates
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect keyboardInfoFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    // get the height of the keyboard by taking into account the orientation of the device too
    CGRect windowFrame = [self.view.window convertRect:self.view.frame fromView:self.view];
    CGRect keyboardFrame = CGRectIntersection (windowFrame, keyboardInfoFrame);
    CGRect coveredFrame = [self.view.window convertRect:keyboardFrame toView:self.view];
    // add the keyboard height to the content insets so that the scrollview can be scrolled
    UIEdgeInsets contentInsets = UIEdgeInsetsMake (0.0, 0.0, coveredFrame.size.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    // make sure the scrollview content size width and height are greater than 0
    [scrollView setContentSize:CGSizeMake (scrollView.frame.size.width, scrollView.contentSize.height+50)];
    // scroll to the text view
    [scrollView scrollRectToVisible:self.activeField.superview.frame animated:YES];
    
}
- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    UIScrollView *scrollView = (UIScrollView *)self.containerScrollView;
    // scroll back..
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    [scrollView setContentSize:self.containerScrollView.bounds.size];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
