//
//  NewMissionCurrentCell.h
//  PfizerHealthConnect
//
//  Created by sandeep suda on 06/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNCirclePercentView.h"

@interface NewMissionCurrentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *physicianNameLbl;
@property (weak, nonatomic) IBOutlet UIProgressView *physicianProgress;
@property (weak, nonatomic) IBOutlet KNCirclePercentView *circularProgress;
@property (weak, nonatomic) IBOutlet UILabel *hospitalNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *percentageLbl;
@end
