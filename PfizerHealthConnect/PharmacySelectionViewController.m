//
//  PharmacySelectionViewController.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 23/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PharmacySelectionViewController.h"
#import "MBProgressHUD.h"
#import "Webservice.h"
#import "MenuList.h"
#import "PhysicianSelectionTableViewCell.h"
#import "NSDictionary+Dictionary.h"
#import "AppDelegate.h"

@interface PharmacySelectionViewController ()
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (nonatomic,retain) NSMutableArray *tempPharmaciesArray,*pharmaciesArray,*seletedPharmacies;
@property (nonatomic,strong) NSDictionary *responseData;
@end

@implementation PharmacySelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tempPharmaciesArray=[NSMutableArray array];
    _pharmaciesArray=[NSMutableArray array];
    _seletedPharmacies=[NSMutableArray array];
    self.submitBtn.layer.cornerRadius = 4.0;
    self.searchBtn.layer.cornerRadius = 4.0;
    self.clearAllBtn.layer.cornerRadius = 4.0;
    [self.hospitalTextField addTarget:self
                               action:@selector(textFieldDidChange:)
                     forControlEvents:UIControlEventEditingChanged];
    [self.cityTextField addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    [self.bricTextField addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
}
- (IBAction)verifyMissionBtnAction:(id)sender {
    [self.view endEditing:YES];
    appDelegate.isVerify = YES;
    self.view.backgroundColor = [appDelegate colorWithHexString:RamGreenColor];
    self.missionVerifyBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.missionNewBtn.backgroundColor = self.view.backgroundColor;
    self.bricTextField.text = @"";
    self.cityTextField.text = @"";
    self.hospitalTextField.text = @"";
    [_seletedPharmacies removeAllObjects];
    [self.PharmacyListTable reloadData];
    [self showProgressView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self getPhysicianSelectionList];
    });
}
- (IBAction)newMissionBtnAction:(id)sender {
    [self.view endEditing:YES];
    appDelegate.isVerify = NO;
    self.view.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.missionNewBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.missionVerifyBtn.backgroundColor = self.view.backgroundColor;
    self.bricTextField.text = @"";
    self.cityTextField.text = @"";
    self.hospitalTextField.text = @"";
    [_seletedPharmacies removeAllObjects];
    [self.PharmacyListTable reloadData];
    [self showProgressView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self getPhysicianSelectionList];
    });
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MenuList networkChecking];
    self.PharmacyListTable.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = appDelegate.isVerify?[appDelegate colorWithHexString:RamGreenColor]:[appDelegate colorWithHexString:BlueColor];
    self.missionNewBtn.backgroundColor = appDelegate.isVerify?self.view.backgroundColor:[appDelegate colorWithHexString:YellowColor];
    self.missionVerifyBtn.backgroundColor = appDelegate.isVerify?[appDelegate colorWithHexString:YellowColor]:self.view.backgroundColor;
    self.bricTextField.text = @"";
    self.cityTextField.text = @"";
    self.hospitalTextField.text = @"";
    [_seletedPharmacies removeAllObjects];
    [self.PharmacyListTable reloadData];
    [self showProgressView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self getPhysicianSelectionList];
    });
}
-(void)getPhysicianSelectionList{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = appDelegate.isVerify?PHARMACY_SELECT_VERIFY:PHARMACY_SELECT_SUBMIT_NEW_MISSION;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.responseData = (NSDictionary*)data;
            self.pharmaciesArray = appDelegate.isVerify?[self.responseData objectForKey:@"pharmacyTicketMaster"]:[self.responseData objectForKey:@"PharmacyBase"];
            _tempPharmaciesArray=[NSMutableArray arrayWithArray:self.pharmaciesArray];
            //[self SearchWithAscending];
            [self.PharmacyListTable reloadData];
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.pharmaciesArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 81.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"SelectMissionCell";
    PhysicianSelectionTableViewCell *cell =
    (PhysicianSelectionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.contentView.backgroundColor = self.view.backgroundColor;
    NSDictionary *dict;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    dict =self.pharmaciesArray[indexPath.row];
    cell.physicianNameLbl.text = [dict stringForKey:@"PharmacyName"];
    cell.hospitalDetails.text= [NSString stringWithFormat:@"%@ %@",dict[@"Address1"],dict[@"Address2"]];
    cell.checkBtn.tag = 100;
    [cell.checkBtn setImage:[UIImage imageNamed:@"checkbox"] forState: UIControlStateNormal];
    [cell.checkBtn setImage:[UIImage imageNamed:@"checboxselected"] forState: UIControlStateSelected];
    [cell.checkBtn addTarget:self action:@selector(accessoryButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.checkBtn setSelected:NO];
    if ([_seletedPharmacies containsObject:dict]) {
        [cell.checkBtn setSelected:YES];
    }
    return cell;
}
-(void)accessoryButtonAction:(id)sender{
    UITableViewCell *cell =(UITableViewCell*)((UIButton *)sender).superview.superview;
    NSIndexPath *indexPath = [self.PharmacyListTable indexPathForCell:cell];
    NSDictionary *dict=[_pharmaciesArray objectAtIndex:indexPath.row];
    if (![self.seletedPharmacies containsObject:dict]) {
        [_seletedPharmacies addObject:dict];
    }else{
        [_seletedPharmacies removeObject:dict];
    }
    [self.PharmacyListTable beginUpdates];
    [self.PharmacyListTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.PharmacyListTable endUpdates];
}
- (IBAction)clearAction:(id)sender {
    [self.view endEditing:YES];
    [_seletedPharmacies removeAllObjects];
    [self.PharmacyListTable reloadData];
}
- (IBAction)submitAction:(id)sender {
    [self.view endEditing:YES];
    [self submitSelectedPhysicians];
}
-(void)submitSelectedPhysicians{
    self.missionsLimitNumber = [[self.responseData objectForKey:@"MissionsLimit"] integerValue];
    if(self.seletedPharmacies.count<=self.missionsLimitNumber){
        [self showProgressView];
        NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
        NSString *baseURLString = appDelegate.isVerify?PHARMACY_SELECT_SUBMIT_VERIFY_MISSION:PHARMACY_SELECT_SUBMIT_NEW_MISSION;
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
        [Webservice postWithURlString:urlString withParameters:[self getPostData] sucess:^(id data) {
            //Success
            [self.mbProgressView hideAnimated:YES];
            if (data) {
                NSString *message = [data valueForKey:@"Message"];
                if (message) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:message preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        if ([self.delegate respondsToSelector:@selector(PHRSubmitSucessfull)]) {
                            [self.delegate PHRSubmitSucessfull];
                        }
                    }];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }else{
                    if ([self.delegate respondsToSelector:@selector(PHRSubmitSucessfull)]) {
                        [self.delegate PHRSubmitSucessfull];
                    }
                }
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SELECT_MAXIMUM_NUMBER_OF_MISSIONS_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
-(NSMutableArray *)getPostData{
    // [{"DoctorId":"PH100"},{"DoctorId":"PH101"}]
    NSMutableArray *arry=[[NSMutableArray alloc] init];
    for (NSDictionary *pharmDict in _seletedPharmacies) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:[pharmDict objectForKey:@"PharmacyName"] forKey:@"PharmacyName"];
        [dict setObject:[pharmDict objectForKey:@"PharmacyId"] forKey:@"PharmacyId"];
        [arry addObject:dict];
    }
    return arry;
}

- (IBAction)searchPharmaciesList:(id)sender {
    [self.view endEditing:YES];
    NSData *requestData = [self getSearchPostData];
    NSString *string = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    //GetSearchCityPharmacyName,GetSearchVerifyPharmacyList
    NSString *baseURLString = appDelegate.isVerify?PHARMACY_SEARCH_VERIFY:PHARMACY_SEARCH_NEW_MISSION;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        if (error == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (data) {
                    self.pharmaciesArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    [self.PharmacyListTable reloadData];
                }
            });
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }] resume];
}
-(NSData *)getSearchPostData{
    NSString *strData = @"{";
    strData = [strData stringByAppendingString:[NSString stringWithFormat:@"\"searchString\" : \"%@\"",self.hospitalTextField.text]];
    strData = [strData stringByAppendingString:@","];
    strData = [strData stringByAppendingString:[NSString stringWithFormat:@"\"City\" : \"%@\"",self.cityTextField.text]];
    strData = [strData stringByAppendingString:@","];
    strData = [strData stringByAppendingString:[NSString stringWithFormat:@"\"Brick\" : \"%@\"",self.bricTextField.text]];
    strData = [strData stringByAppendingString:@"}"];
    NSData* data = [strData dataUsingEncoding:NSUTF8StringEncoding];
    return data;
}

-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = @"Loading...";
}

-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}
-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.cityTextField || textField == self.bricTextField || textField == self.hospitalTextField) {
        if ([textField.text isEqualToString:@""]){
            self.pharmaciesArray = [NSMutableArray arrayWithArray:self.tempPharmaciesArray];
            [self.PharmacyListTable reloadData];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.hospitalTextField || textField == self.cityTextField)
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:ONLY_ALPHABETS_WITHSPACE_MESSAGE] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
        return YES;
    }else if (textField == self.bricTextField){
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789"] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }
    else{
        return YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.sortTextFieldPharmacy) {
        [self sortingAction:nil];
        return NO;
    }
    return YES;
}
- (IBAction)sortingAction:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *latest = [UIAlertAction actionWithTitle:LATEST_STRING style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self SearchWithLatest];
                                                   }];
    UIAlertAction *ascending = [UIAlertAction actionWithTitle:ALPHABETICAL_ASCENDING_STRING style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self SearchWithAscending];
                                                      }];
    UIAlertAction *decending = [UIAlertAction actionWithTitle:ALPHABETICAL_DESCENDING_STRING style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self SearchWithDecending];
                                                      }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    [alert addAction:latest];
    [alert addAction:ascending];
    [alert addAction:decending];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.sortTextFieldPharmacy;
    popPresenter.sourceRect = self.sortTextFieldPharmacy.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Search Methods

-(void)SearchWithLatest{
    self.sortTextFieldPharmacy.text = LATEST_STRING;
    [self getLatestPharmaciesData];
}
-(void)getLatestPharmaciesData{
    [self showProgressView];
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = appDelegate.isVerify?PHARMACY_LATEST_SEARCH_VERIFY:PHARMACY_LATEST_SEARCH_NEW_MISSION;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.responseData = (NSDictionary*)data;
            self.pharmaciesArray = appDelegate.isVerify?[self.responseData objectForKey:@"pharmacyTicketMaster"]:[self.responseData objectForKey:@"PharmacyBase"];
            [self.PharmacyListTable reloadData];
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(void)SearchWithAscending{
    self.sortTextFieldPharmacy.text = ALPHABETICAL_ASCENDING_STRING;
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"PharmacyName" ascending:YES selector:@selector(localizedCompare:)];
    if (self.pharmaciesArray.count > 0) {
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.pharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self.PharmacyListTable reloadData];
    }else{
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.tempPharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]]; // At HOME
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.pharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self.PharmacyListTable reloadData];
    }
}
-(void)SearchWithDecending
{
    self.sortTextFieldPharmacy.text = ALPHABETICAL_DESCENDING_STRING;
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"PharmacyName" ascending:NO selector:@selector(localizedCompare:)];
    if (self.pharmaciesArray.count > 0) {
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.pharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self.PharmacyListTable reloadData];
    }else{
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.tempPharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.pharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self.PharmacyListTable reloadData];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
