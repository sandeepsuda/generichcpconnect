//
//  SelectMissionViewController.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 07/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectMissionViewControllerDelegate <NSObject>
@required
-(void)submitSucessfull;
-(void)filterAction:(UIButton *)sender;
@end


@interface SelectMissionViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) id<SelectMissionViewControllerDelegate> delegate;
@property (nonatomic, weak)IBOutlet UITextField *bricTextField;
@property (nonatomic, weak)IBOutlet UITextField *cityTextField;
@property (nonatomic, weak)IBOutlet UITextField *hospitalTextField;
@property (nonatomic, weak)IBOutlet UITextField *physicianTextField;
@property (weak, nonatomic) IBOutlet UITextField *sortTextFieldPharmacy;
@property (weak, nonatomic) IBOutlet UITableView *PharmacyListTable;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn,*filterButton;
@property (weak, nonatomic) IBOutlet UIButton *clearAllBtn;
@property (nonatomic) NSInteger missionsLimitNumber;
-(void)submitSelectedPhysicians;
@property (nonatomic) BOOL allPhysicianBool,assignedPhysicianBool,newPhysicianBool,unAssignedPhysicianBool;
@end
