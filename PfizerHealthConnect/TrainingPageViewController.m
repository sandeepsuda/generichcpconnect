//
//  TrainingPageViewController.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 02/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "TrainingPageViewController.h"
#import "MenuList.h"
#import "AppDelegate.h"
#import "WelcomePageViewController.h"

@interface TrainingPageViewController ()
@property (nonatomic,strong) NSString *message,*accessCode;
@end

@implementation TrainingPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"OTPPAGE"];
    [defaults setBool:YES forKey:@"QuizPage"];
    self.title = @"Introduction";
    self.navigationController.navigationBar.barTintColor = [appDelegate colorWithHexString:@"3A4F5A"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20]}];
    //self.lblTitle.text = @"Please go through the following links before we start using the app.\nAll the links are mandatory and are supposed to be followed in the same order.";
    self.instructionsTxtLbl.scrollEnabled = NO;
    [self.questionsTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.questionsTable.estimatedSectionHeaderHeight = 50;
    self.questionsTable.estimatedRowHeight = 50;
    [self.leftPaneTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell1"];
    self.leftPaneTableView.backgroundColor =  [appDelegate colorWithHexString:@"25B4FF"];
    self.leftPaneTableView.scrollEnabled = NO;
    self.leftPaneTableView.separatorInset = UIEdgeInsetsMake(0, -100, 0 , 0) ;
    self.arrLeftPaneOptions = @[@"1. Training\n    Instructions",@"2. Training\n    Materials",@"3. Training\n    Quiz"];
    self.webTraining.delegate = self;
    if(!appDelegate.arrMasterQuiz){
        [self downloadQuiz];
    }else{
        self.arrMasterQuiz = appDelegate.arrMasterQuiz;
    }
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self setUpConstraints];
}

-(void)setUpConstraints{
    CGFloat subViewsHeight = self.almostLbl.frame.size.height + self.topContentLbl.frame.size.height;
    CGFloat verticalSpacing = (self.topView.frame.size.height - subViewsHeight)/2.0;
    self.verticalSpaceConstraintOne.constant = 0;
    self.verticalSpaceConstraintTwo.constant = verticalSpacing;
    self.verticalSpaceConstraintThree.constant = verticalSpacing;
    
    subViewsHeight = self.instructionsTxtLbl.frame.size.height + self.thanksLbl.frame.size.height + self.allTheBestLbl.frame.size.height;
    verticalSpacing = (self.rightPaneView.frame.size.height - subViewsHeight);
    self.instructionTxtHightConstraint.constant =  320.0;
    //self.rightViewVerticalSpaceConstraintFour.constant = verticalSpacing * 0.73;
    //self.rightViewVerticalSpaceConstraintOne.constant = verticalSpacing * 0.1;
    //self.rightViewVerticalConstraintTwo.constant = verticalSpacing * 0.1;
    //self.rightViewVerticalSpaceConstraintThree.constant = verticalSpacing * 0.1;
    //[self.view layoutSubviews];
    [self.view layoutIfNeeded];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationItem.hidesBackButton = YES;
    [self.leftPaneTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
    [self leftPaneSelectedWithIndex:0];
    [self setHighlightedCellItem:[NSIndexPath indexPathForRow:0 inSection:0]];
}
#pragma mark - Tableview Datasource and Delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView == self.questionsTable){
        return self.arrMasterQuiz.count+1+1;
    }
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.questionsTable){
        if(section == self.arrMasterQuiz.count+1 || section == 0){
            return 0;
        }
        NSDictionary *dicQuiz = self.arrMasterQuiz[section-1];
        NSArray *arrOptions = dicQuiz[@"QuestionsOption"];
        return arrOptions.count;
    }
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.questionsTable){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        NSDictionary *dicQuiz = self.arrMasterQuiz[indexPath.section-1];
        NSArray *arrOptions = dicQuiz[@"QuestionsOption"];
        NSDictionary *dicOption = arrOptions[indexPath.row];
        cell.textLabel.text = dicOption[@"QuestionAnswer"];
        if(cell.tag!=1000){
            cell.tag = 1000;
            cell.textLabel.textColor =  [appDelegate colorWithHexString:@"25B4FF"];
            cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17];
            cell.textLabel.numberOfLines = 0;
            UIButton *accessoryButton= [[UIButton alloc] initWithFrame:CGRectMake(5, 0, 20, 20)];
            accessoryButton.tag = 100;
            UIImage *accImage = [UIImage imageNamed:@"checkbox_active"];
            [accessoryButton setImage:accImage forState: UIControlStateNormal];
            [accessoryButton setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateSelected];
            [accessoryButton addTarget:self action:@selector(accessoryButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [accessoryButton setSelected:NO];
            [cell setAccessoryView:accessoryButton];
        }
        
        NSDictionary *dicQuestion = self.arrMasterQuiz[indexPath.section-1];
        NSNumber *questionID = dicQuestion[@"QuestionId"];
        UIButton *accessoryButton = (UIButton *)cell.accessoryView;
        if (self.dicSelectedQuestionAnsIDs && self.dicSelectedQuestionAnsIDs[questionID] ){
            NSArray *dicOptions = dicQuestion[@"QuestionsOption"];
            NSDictionary *selectedOption = dicOptions[indexPath.row];
            NSNumber *optionID = selectedOption[@"QuestionOptionId"];
            if ([optionID isEqualToNumber:self.dicSelectedQuestionAnsIDs[questionID]])
                [accessoryButton setSelected:YES];
            else{
                [accessoryButton setSelected:NO];
            }
        }else{
            [accessoryButton setSelected:NO];
        }
        
        return cell;
    }
    
    if(tableView == self.leftPaneTableView){
        UITableViewCell *cell = [[UITableViewCell alloc] init];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = self.arrLeftPaneOptions[indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.accessoryView = [[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"Arrow_highlight"]];
        [cell.accessoryView setFrame:CGRectMake(0, 0, 15, 17)];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor =  [UIColor whiteColor];
        [cell setSelectedBackgroundView:bgColorView];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [appDelegate colorWithHexString:@"25B4FF"];
        cell.backgroundColor =  [appDelegate colorWithHexString:@"25B4FF"];
        cell.preservesSuperviewLayoutMargins = false;
        cell.separatorInset = UIEdgeInsetsZero;
        cell.layoutMargins = UIEdgeInsetsZero;
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.questionsTable){
        return UITableViewAutomaticDimension;
    }
    return 60;
}

/*
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView == self.questionsTable){
        if(section==self.arrMasterQuiz.count){
            return @"";
        }
        NSDictionary *dicQuiz = self.arrMasterQuiz[section];
        return dicQuiz[@"Question"];
    }
    return nil;
}

*/


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    // calculate height of UILabel
    /* UILabel *lblSectionName = [[UILabel alloc] init];
     NSDictionary *dicQuiz = self.arrMasterQuiz[section];
     lblSectionName.text =  dicQuiz[@"Question"];
     lblSectionName.numberOfLines = 0;
     lblSectionName.lineBreakMode = NSLineBreakByWordWrapping;
     
     [lblSectionName sizeToFit];
     
     return lblSectionName.frame.size.height;
     */
    if(tableView == self.questionsTable){
        if(section==self.arrMasterQuiz.count+1){
            return 90;
        }else if (section ==0){
            return 150;
        }
        return 70;
    }
    return 0.1;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == self.questionsTable){
        // calculate height of UILabel
        /*UILabel *lblSectionName = [[UILabel alloc] init];
         NSDictionary *dicQuiz = self.arrMasterQuiz[section];
         lblSectionName.text =   [NSString stringWithFormat:@"%ld %@",section+1,dicQuiz[@"Question"]] ;
         lblSectionName.numberOfLines = 0;
         lblSectionName.lineBreakMode = NSLineBreakByWordWrapping;
         [lblSectionName sizeToFit];
         [lblSectionName setBackgroundColor:[UIColor lightGrayColor]];
         return lblSectionName;
         */
        if(section==self.arrMasterQuiz.count+1){
            UIView *view = [[UIView alloc] init];
            UIButton *submitButton = [[UIButton alloc] init];
            [submitButton addTarget:self action:@selector(submitButton:) forControlEvents:UIControlEventTouchUpInside];
            submitButton.translatesAutoresizingMaskIntoConstraints = NO;
            [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
            [submitButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:19]];
            [submitButton setBackgroundColor:[appDelegate colorWithHexString:@"25B4FF"]];
            submitButton.layer.cornerRadius = 4.0;
            [view addSubview:submitButton];
            NSArray *arr = @[
                             [submitButton.widthAnchor constraintEqualToAnchor:view.widthAnchor multiplier:0.5],
                             [submitButton.heightAnchor constraintEqualToAnchor:view.heightAnchor multiplier:0.5],
                             [submitButton.centerXAnchor constraintEqualToAnchor:view.centerXAnchor],
                             [submitButton.centerYAnchor constraintEqualToAnchor:view.centerYAnchor]
                             ];
            [NSLayoutConstraint activateConstraints:arr];
            return view;
        }
        else if(section==0){
            UIView *view = [[UIView alloc] init];
            NSString *str=@"Let's Start!\nPlease answer the following questions:";
            NSString *strA = @"Let's Start!";
            NSString *strB = @"Please answer the following questions:\n";
            NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
            [hogan addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:@"HelveticaNeue-Thin" size:45]
                          range:[str rangeOfString:strA]];
            [hogan addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:@"HelveticaNeue-Light" size:20]
                          range:[str rangeOfString:strB]];
            UILabel *lbl = [[UILabel alloc] init];
            lbl.textColor = [appDelegate colorWithHexString:@"3A4F5A"];
            lbl.attributedText = hogan;
            lbl.numberOfLines = 0;
            lbl.translatesAutoresizingMaskIntoConstraints = NO;
            lbl.textAlignment = NSTextAlignmentCenter;
            [view addSubview:lbl];
            [NSLayoutConstraint activateConstraints:@[
                                                      [lbl.widthAnchor constraintEqualToAnchor:view.widthAnchor multiplier:1],
                                                      [lbl.heightAnchor constraintEqualToAnchor:view.heightAnchor multiplier:0.5],
                                                      [lbl.centerXAnchor constraintEqualToAnchor:view.centerXAnchor],
                                                      [lbl.centerYAnchor constraintEqualToAnchor:view.centerYAnchor constant:0],
                                                      ]];
            return view;
        }
        
        UIView *view = [[UIView alloc] init];
        UIView *separatorLine = [[UIView alloc]init];
        if (section != 1) {
            separatorLine.backgroundColor = [UIColor lightGrayColor];
        }else{
            separatorLine.backgroundColor = [UIColor clearColor];
        }
        
        separatorLine.translatesAutoresizingMaskIntoConstraints = NO;
        UILabel *txt = [[UILabel alloc] init];
        NSDictionary *dicQuiz = self.arrMasterQuiz[section-1];
        txt.text =   [NSString stringWithFormat:@"%ld %@",(long)section,dicQuiz[@"Question"]];
        txt.numberOfLines = 0;
        txt.translatesAutoresizingMaskIntoConstraints = NO;
        txt.textColor = [appDelegate colorWithHexString:@"3A4F5A"];
        txt.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        //txt.scrollEnabled = false;
        txt.userInteractionEnabled = false;
        [txt sizeToFit];
        txt.textAlignment = NSTextAlignmentLeft;
        [view addSubview:separatorLine];
        [view addSubview:txt];
        [NSLayoutConstraint activateConstraints:@[
                                                  [separatorLine.topAnchor constraintEqualToAnchor:view.topAnchor constant:0.0],
                                                  [separatorLine.leadingAnchor constraintEqualToAnchor:view.leadingAnchor constant:10.0],
                                                  [separatorLine.trailingAnchor constraintEqualToAnchor:view.trailingAnchor constant:-5.0],
                                                  [separatorLine.heightAnchor constraintEqualToConstant:1.0],
                                                  [txt.leadingAnchor constraintEqualToAnchor:view.leadingAnchor constant:10.0],
                                                  [txt.trailingAnchor constraintEqualToAnchor:view.trailingAnchor constant:-5.0],
                                                  [txt.heightAnchor constraintEqualToAnchor:view.heightAnchor multiplier:1.0],
                                                  [txt.centerXAnchor constraintEqualToAnchor:view.centerXAnchor],
                                                  [txt.centerYAnchor constraintEqualToAnchor:view.centerYAnchor]
                                                  ]];
        view.layer.zPosition = -1;
        return view;
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self setHighlightedCellItem:indexPath];
    [self leftPaneSelectedWithIndex:indexPath.row];
}

-(void)setHighlightedCellItem:(NSIndexPath*)selectedIndexPath{
    NSInteger sections = self.leftPaneTableView.numberOfSections;
    // NSMutableArray *cells = [[NSMutableArray alloc]  init];
    for (int section = 0; section < sections; section++) {
        NSInteger rows =  [self.leftPaneTableView numberOfRowsInSection:section];
        for (int row = 0; row < rows; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            UITableViewCell *cell = [self.leftPaneTableView cellForRowAtIndexPath:indexPath];
            if (indexPath.row == selectedIndexPath.row) {
                cell.accessoryView = [[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"Arrow"]];
            }else{
                cell.accessoryView = [[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"Arrow_highlight"]];
            }
        }
    }
}

#pragma mark - Custom Method

-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.rightPaneView animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = LOADING_MESSAGE;
}
-(void)leftPaneSelectedWithIndex:(NSInteger)index{
    NSString *strPath = nil;
    switch (index) {
        case 0:
            self.instructionsTxtLbl.hidden = NO;
            self.thanksLbl.hidden = NO;
            self.allTheBestLbl.hidden = NO;
            self.questionsTable.hidden = YES;
            self.webTraining.hidden = YES;
            //[self setUpConstraints];
            break;
        case 1:
            self.instructionsTxtLbl.hidden = YES;
            self.thanksLbl.hidden = YES;
            self.allTheBestLbl.hidden = YES;
            self.questionsTable.hidden = YES;
            self.webTraining.hidden = NO;
            if(self.webTraining.delegate){
            //self.webTraining.userInteractionEnabled = NO;
            self.webTraining.scalesPageToFit = YES;
            self.webTraining.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
            self.webTraining.delegate = self;
            strPath = [[NSBundle mainBundle]pathForResource:@"PharmConnect-HCPconnectTraining Deck - 23 Oct'16" ofType:@"pdf"];
            //[self.webTraining loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strPath]]];
            [self.webTraining loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[strPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]]]]];
            }
           
            break;
        case 2:
            strPath = nil;
            self.instructionsTxtLbl.hidden = YES;
            self.thanksLbl.hidden = YES;
            self.allTheBestLbl.hidden = YES;
            self.questionsTable.hidden = NO;
            self.webTraining.hidden = YES;
            if(!self.arrMasterQuiz){
                [self showProgressView];
                if(!self.isDowloadingQuiz){
                    [self performSelector:@selector(downloadQuiz) withObject:nil afterDelay:0];
                }
            }
            break;
        default:
            break;
    }
    self.btnQuizSubmit.hidden = self.questionsTable.hidden;
}
-(void)downloadQuiz{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",
                          KBASE_URL,HCP_GETQuiz];
    self.isDowloadingQuiz = YES;
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        if (data) {
            self.isDowloadingQuiz = NO;
            if(self.mbProgressView){
                [self.mbProgressView hideAnimated:YES];
                self.mbProgressView = nil;
            }

            appDelegate.arrMasterQuiz = [NSMutableArray arrayWithArray:(NSArray*)data];
            //Remove empty
            NSMutableArray *finalMaster = [NSMutableArray new];
            for (NSDictionary *dicQuestions in appDelegate.arrMasterQuiz){
                NSMutableDictionary *dicFinalQues = [[NSMutableDictionary alloc] initWithDictionary:dicQuestions];
                NSMutableArray *arrFinalAns = [[NSMutableArray alloc] initWithArray:dicQuestions[@"QuestionsOption"]];
                NSMutableArray *arrToBeRemoved;
                arrToBeRemoved = [NSMutableArray new];
                for (NSDictionary *dicOptions in arrFinalAns){
                    if([dicOptions[@"QuestionAnswer"] isEqualToString:@""]){
                        //[arrFinalAns removeObject:dicOptions];
                        [arrToBeRemoved addObject:dicOptions];
                    }
                }
                [arrFinalAns removeObjectsInArray:arrToBeRemoved];
                
                dicFinalQues[@"QuestionsOption"] = arrFinalAns;
                [finalMaster addObject:dicFinalQues];
            }
            appDelegate.arrMasterQuiz = finalMaster;
            //Remove empty
            self.arrMasterQuiz = appDelegate.arrMasterQuiz;
            [self.questionsTable reloadData];
        }
    } failure:^(NSError *error) {
        NSLog(@"Error");
        self.isDowloadingQuiz = NO;
        if(self.mbProgressView){
            [self.mbProgressView hideAnimated:YES];
            self.mbProgressView = nil;
        }
        if(!(self.questionsTable.hidden)){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:error.description preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }];
    
}

#pragma mark - UITableViewCell Button Method

-(IBAction)accessoryButtonAction:(UIButton *)sender{
    NSIndexPath *indexPath = [self indexPathForView:sender];
    sender.selected = !sender.selected;
    
    if(!self.dicSelectedQuestionAnsIDs){
        self.dicSelectedQuestionAnsIDs= [NSMutableDictionary new];
    }
    
    NSDictionary *dicQuestion = self.arrMasterQuiz[indexPath.section-1];
    NSNumber *questionID = dicQuestion[@"QuestionId"];
    
    NSArray *arrOptions = dicQuestion[@"QuestionsOption"];
    NSDictionary *selectedOption = arrOptions[indexPath.row];
    
    if (self.dicSelectedQuestionAnsIDs[questionID]){//already it is there
        NSNumber *selectedOptionID = selectedOption[@"QuestionOptionId"];
        if([selectedOptionID isEqualToNumber:self.dicSelectedQuestionAnsIDs[questionID]]){
            [self.dicSelectedQuestionAnsIDs removeObjectForKey:questionID];
        }
        else{
            NSNumber *previouslySelectedOptionID = self.dicSelectedQuestionAnsIDs[questionID];
            for (int i=0;i<arrOptions.count;++i){
                NSDictionary *dic = arrOptions[i];
                if([dic[@"QuestionOptionId"] isEqualToNumber:previouslySelectedOptionID]){
                    UITableViewCell *cell = [self.questionsTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
                    UIButton *accessoryButton = (UIButton *)cell.accessoryView;
                    [accessoryButton setSelected:NO];
                    break;
                }
            }
            self.dicSelectedQuestionAnsIDs[questionID] = selectedOption[@"QuestionOptionId"];
        }
    }else{ //not here
        self.dicSelectedQuestionAnsIDs[questionID] = selectedOption[@"QuestionOptionId"];
    }
    
    
}

-(NSIndexPath*)indexPathForView:(UIView*)view{
    while (view && ![view isKindOfClass:[UITableViewCell class]])
        view = view.superview;
    return [self.questionsTable indexPathForRowAtPoint:view.center];
}
#pragma mark - Webview Delegate methods
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    webView.delegate = nil;
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"Error!");
}

#pragma mark - Service Call Method

-(NSMutableArray *)getQuizData{
   
    if (self.dicSelectedQuestionAnsIDs.allKeys.count == self.arrMasterQuiz .count) {
        NSMutableArray *selected=[[NSMutableArray alloc] init];
        for(NSNumber *keyQuestion in self.dicSelectedQuestionAnsIDs.allKeys){
            NSDictionary *dic = @{@"QuestionId":keyQuestion,@"QuestionOptionId":self.dicSelectedQuestionAnsIDs[keyQuestion]};
            [selected addObject:dic];
            NSLog(@"Selected : %@",selected);
        }
        return selected;
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:[NSString stringWithFormat:@"Please attempt all the %lu Questions.",(unsigned long)self.arrMasterQuiz.count] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];

    }
    return nil;
}

#pragma mark - UIButton Action Methods

- (IBAction)submitButton:(id)sender
{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults]stringForKey:EMAIL_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,HCP_SubmitQuiz,mobileNumber];
    NSMutableArray *array = [self getQuizData];
    if(!array)
        return;
    [self showProgressView];
    [Webservice postWithURlString:urlString withParameters:array sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.message = [data objectForKey:@"Message"];
            self.accessCode = [data objectForKey:@"AccessCode"];
            if ([self.message isEqualToString:@"SUCCESS"]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:QUIZ_SUCCESS_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    WelcomePageViewController *trainingPageViewController =[storyboard instantiateViewControllerWithIdentifier:@"WelcomePageViewController"];
                    [self.navigationController pushViewController:trainingPageViewController animated:YES];
                }];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }if([self.message isEqualToString:@"FAIL"]){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:QUIZ_FAIL_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }if ([self.message isEqualToString:QUIZ_AFTER_24HOURS_MESSAGE]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"You have already taken the Quiz. Please come back and re-take the Quiz after completing 24  hours." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:error.description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
