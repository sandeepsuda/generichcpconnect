//
//  PhysicianTrackerViewController.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 04/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PhysicianTrackerViewController.h"
#import "MBProgressHUD.h"
#import "Webservice.h"
#import "NewMissionCurrentCell.h"
#import "NewMissionHistoricalCell.h"
#import "AppDelegate.h"
#import "MenuList.h"
#import "NSDictionary+Dictionary.h"
#import "LeftViewController.h"
#import "NSString+Base64.h"

static NSString* MyApplicationID = @"GenericHCPApp";
static NSString* MyPassword = @"km6LlLBbGjd4JAfWF+gwDGCB";


@interface PhysicianTrackerViewController ()<AddPhysicianPopViewControllerDelegate>{
     AddPhysicianPopViewController *popVC;
     UIImagePickerController *imgPicker;
}
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property(nonatomic,assign)BOOL framesAreSet;
@property(nonatomic)BOOL oneTimeCall;
@property (nonatomic,strong) LeftViewController *leftVC;
@property (nonatomic, strong) NSXMLParser *xmlParser;
@property (nonatomic, strong) NSMutableDictionary *dictTempDataStorage;
@property (nonatomic, strong) NSMutableString *foundValue;
@property (nonatomic, strong) NSString *currentElement;
@property (nonatomic, strong) NSString *element;
@end

@implementation PhysicianTrackerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //self.scoreView.layer.cornerRadius = self.scoreView.frame.size.width/2;
    self.addMissionBtn.layer.cornerRadius = self.addMissionBtn.frame.size.width/2;
    self.physiciansList = [[NSMutableArray alloc] init];
    self.physicianListTbl.backgroundColor = [UIColor clearColor];
    self.physicianListTbl.separatorStyle = UITableViewCellSeparatorStyleNone;
    UINib *cellNib = [UINib nibWithNibName:@"NewMissionCurrentCell" bundle:nil];
    [self.physicianListTbl registerNib:cellNib forCellReuseIdentifier:@"NewMissionCurrentCellIdentifier"];
    UINib *cellNib2 = [UINib nibWithNibName:@"NewMissionHistoricalCell" bundle:nil];
    [self.physicianListTbl registerNib:cellNib2 forCellReuseIdentifier:@"NewMissionHistoricalCellIdentifier"];
    //self.physicianListTbl.canCancelContentTouches = YES;
    self.pointHeaderLbl.hidden = YES;
    [self.searchTxtField addTarget:self
                               action:@selector(textFieldDidChange:)
                     forControlEvents:UIControlEventEditingChanged];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:NO];
    
    //offline
   // [self addObserver:self forKeyPath:@"physiciansList" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    //offline
}
-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}
- (void)viewWillAppear:(BOOL)animated{
    //[MenuList networkChecking];
    self.dateOfCompletionLabel.text = @"Date of Completion";
    [super viewWillAppear:animated];
    self.leftVC = (LeftViewController *)self.delegate;
    [self.mbProgressView hideAnimated:YES];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"Editable"];
    self.filterButton.layer.cornerRadius = 5;
    [self.filterButton setTitle:@"Assigned" forState:UIControlStateNormal];
    isCurrentData = YES;
    self.pointHeaderLbl.hidden = YES;
    self.addPhysicianLbl.hidden = NO;
    self.addPhysicianBtn.hidden = NO;
    self.view.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.missionNewBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.verifyMissionBtn.backgroundColor = self.view.backgroundColor;
    self.currentBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.historicalBtn.backgroundColor = self.view.backgroundColor;
    searchActive = NO;
    self.searchTxtField.text = nil;
    NSUserDefaults *defaluts =[NSUserDefaults standardUserDefaults];
    BOOL oneTimeCall = [defaluts boolForKey:@"ONETIME"];
    if (oneTimeCall == NO) {
        [self oneTimeCallService];
        [defaluts setBool:YES forKey:@"ONETIME"];
    }
    
    if(!self.wasPresentingPopUp){
        [self showProgressView];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [self getMainTrackerDetails];
        });
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[NSUserDefaults standardUserDefaults] setObject:@"ASSIGNED" forKey:PHYCATEGORY];
            [self filterSeriveCallMethod:@"ASSIGNED"];
        });
    }
    self.wasPresentingPopUp = NO;
}

//offline
-(void)removeSavedRecordsFromListOfCurrnetMissions{
    AppDelegate *delG =  appDelegate;
    if(delG.physicianNewMissionArray && delG.physicianNewMissionArray.count){
        NSLog(@"physArrCount : %ld",delG.physicianNewMissionArray.count);
        NSMutableArray *arrToBeRemoved = [NSMutableArray new];
        for(NSDictionary *dicPhysician in delG.physicianNewMissionArray){
            
            NSString *docID = dicPhysician[@"DoctorId"];
            
            //compare with every dic in list
            for(NSDictionary *dicInList in self.physiciansList){
                NSString *docID1 = dicInList[@"DoctorId"];
                if([docID isEqualToString:docID1]){
                    [arrToBeRemoved addObject:dicInList];
                }
            }
        }
        [self.physiciansList removeObjectsInArray:arrToBeRemoved];
        NSLog(@"Reoved : %ld  Remaining : %ld",arrToBeRemoved.count,self.physiciansList.count);
    }
}

-(void)sendSavedRecordsForPhysicians{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showProgressView];
    });
    AppDelegate *delG =  appDelegate;
    NSString *mobNum = [[NSUserDefaults standardUserDefaults]stringForKey:EMAIL_STRING];
    NSMutableDictionary *dicPharmacistData =(NSMutableDictionary*)delG.physicianNewMissionArray[0];
    NSString *category = [dicPharmacistData stringForKey:@"RepType"];
    [dicPharmacistData removeObjectForKey:@"RepType"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,ADD_PHYSICIAN,mobNum,category];
    [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
        //Success
        if (data) {
            if(delG.physicianNewMissionArray.count){
              [delG.physicianNewMissionArray removeObjectAtIndex:0];
            }
            NSLog(@"Record Sent");
            if (delG.physicianNewMissionConsentArray.count) {
              [self sendSavedRecordsForPhysiciansconsents:data];
            }
        }else{
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        if(delG.physicianNewMissionArray.count)
            [delG.physicianNewMissionArray removeObjectAtIndex:0];
        
    }];
    
}
-(void)sendSavedRecordsForPhysiciansconsents:(NSDictionary *)data{
    AppDelegate *delG =  appDelegate;
    NSMutableDictionary *dicSavedPhysicianConsentData = delG.physicianNewMissionConsentArray[0];
    dicSavedPhysicianConsentData[@"DoctorTransactionID"] = data[@"DoctorTransactionID"];
    dicSavedPhysicianConsentData[@"DoctorId"] = data[@"DoctorId"];
    NSString *category = [dicSavedPhysicianConsentData stringForKey:@"RepType"];
    [dicSavedPhysicianConsentData removeObjectForKey:@"RepType"];
    NSString *mobNum = [[NSUserDefaults standardUserDefaults]stringForKey:EMAIL_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",
                           KBASE_URL,PHYSICIAN_CONSENT_FORM,mobNum,category];
    NSDictionary *dicConsentData = dicSavedPhysicianConsentData;
    [Webservice postWithURlString:urlString withParameters:dicConsentData sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            NSLog(@"Consent Sent");
            if(delG.physicianNewMissionConsentArray.count)
                [delG.physicianNewMissionConsentArray removeObjectAtIndex:0];
            
            if(delG.physicianNewMissionArray.count){
                [self sendSavedRecordsForPhysicians];
            }
            else{
                //in the last consent, reload table
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [self getMainTrackerDetails];
                });
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [[NSUserDefaults standardUserDefaults] setObject:self.filterButton.titleLabel.text forKey:PHYCATEGORY];
                    [self filterSeriveCallMethod:self.filterButton.titleLabel.text];
                });
                //in the last consent, reload table
                [self.mbProgressView hideAnimated:YES];
            }
            
            
        }else{
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        if(delG.physicianNewMissionConsentArray.count)
            [delG.physicianNewMissionConsentArray removeObjectAtIndex:0];
        
    }];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"physiciansList"]) {
        AppDelegate *delG =  appDelegate;
        delG.physicianNewMissionCurrentPhysicianList = change[@"new"];
        
    }
}
//offline

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!self.framesAreSet) {
        self.framesAreSet = YES;
    }else{
        CGRect tmpFrame = self.scoreView.frame;
        self.scoreView.layer.cornerRadius = tmpFrame.size.height/2;
    }
} 

-(void)filterSeriveCallMethod:(NSString *)filterString{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults]objectForKey:EMAIL_STRING];
    NSString *urlString;
    if (isCurrentData == YES) {
       urlString  = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,PHYSICIAN_CURRENT_LIST,mobileNumber,filterString];
    }else{
        urlString  = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,PHYSICIAN_HISTORICAL_LIST,mobileNumber,filterString];
    }
    [Webservice getWithUrlString:urlString success:^(id data) {
        [self.mbProgressView hideAnimated:YES];
        //Success
        if (!self.mbProgressView.finished) {
            [self.mbProgressView hideAnimated:YES];
        }
        if (data) {
            if ([filterString isEqualToString:@"ALL"] && [data isKindOfClass:[NSDictionary class]]) {
                NSMutableArray *allPhysiciansList = [[NSMutableArray alloc]init];
                [allPhysiciansList addObjectsFromArray:data[@"repNewDoctorDetails"]];
                [allPhysiciansList addObjectsFromArray:data[@"repAssignDoctorDetails"]];
                [allPhysiciansList addObjectsFromArray:data[@"repUnAssignDoctorDetails"]];
                self.physiciansList = allPhysiciansList;
                self.tempArray = self.physiciansList;
            }else{
                self.physiciansList = [NSMutableArray arrayWithArray:(NSArray*)data];
                self.tempArray = self.physiciansList;
            }
            //offline
            if (isCurrentData == YES){
               appDelegate.physicianNewMissionCurrentPhysicianList = self.physiciansList;
            }
            //offline
            [self.physicianListTbl reloadData];
        }else{
            if ([self.physiciansList count]) {
                [self.physiciansList removeAllObjects];
            }
        }
        [self.physicianListTbl reloadData];
        //offline
        if(appDelegate.physicianNewMissionArray && appDelegate.physicianNewMissionArray.count){
            [self sendSavedRecordsForPhysicians];
            
        }
        //offline
    } failure:^(NSError *error) {
        [self.mbProgressView hideAnimated:YES];
        //error
        if (!self.mbProgressView.finished) {
            [self.mbProgressView hideAnimated:YES];
        }
        //offline
        /*
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:alertAction];
         [self presentViewController:alert animated:YES completion:nil];
         */if(isCurrentData){
             if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
                 AppDelegate *delG =  appDelegate;
                 self.physiciansList = delG.physicianNewMissionCurrentPhysicianList;
                 [self removeSavedRecordsFromListOfCurrnetMissions];
                 [self.physicianListTbl reloadData];
             }
         }
        //offline
    }];
}

-(void)oneTimeCallService{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:EMAIL_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,ONE_TIME,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            NSLog(@"Data Loaded:");
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
        }
    }];
}

-(void)getMainTrackerDetails{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:EMAIL_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,PHYSICIAN_TRACKER_LIST,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.mainTracketData = (NSDictionary*)data;
            self.availableCountLbl.numberOfLines = 0;
            self.availableCountLbl.text = [NSString stringWithFormat:@"%@/%@",[self.mainTracketData stringForKey:@"UnAssignAvailableCount"],[self.mainTracketData stringForKey:@"UnAssignTotalCount"]];
            self.takenCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTracketData stringForKey:@"TotalTakenCount"]];
            self.submitCountLbl.text = [NSString stringWithFormat:@"/%@",[self.mainTracketData stringForKey:@"TotalSubmitCount"]];
            if ([self.delegate respondsToSelector:@selector(setUserDetails:)]) {
                [self.delegate setUserDetails:self.mainTracketData];
            }
            appDelegate.myTrackerDetails = [NSMutableDictionary dictionaryWithDictionary:self.mainTracketData];
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
            AppDelegate *delG =  appDelegate;
            self.mainTracketData = delG.myTrackerDetails;
            self.availableCountLbl.numberOfLines = 0;
            self.availableCountLbl.text = [NSString stringWithFormat:@"%@/%@",[self.mainTracketData stringForKey:@"UnAssignAvailableCount"],[self.mainTracketData stringForKey:@"UnAssignTotalCount"]];
            self.takenCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTracketData stringForKey:@"TotalTakenCount"]];
            self.submitCountLbl.text = [NSString stringWithFormat:@"/%@",[self.mainTracketData stringForKey:@"TotalSubmitCount"]];
            if ([self.delegate respondsToSelector:@selector(setUserDetails:)]) {
                [self.delegate setUserDetails:self.mainTracketData];
            }
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:error.description preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
        }
        
    }];
}
-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = @"Loading...";
}
#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchActive) {
        return self.searchArray.count;
    }else{
        return self.physiciansList.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier =@"NewMissionCurrentCellIdentifier";
    static NSString *cellIdentifier2 = @"NewMissionHistoricalCellIdentifier";
    if (isCurrentData == YES) {
        NewMissionCurrentCell *cell =
        (NewMissionCurrentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [appDelegate colorWithHexString:@"29B9FF"];
        NSDictionary *dict = [[NSDictionary alloc] init];
        if (searchActive) {
            dict = [self.searchArray objectAtIndex:indexPath.row];
        }else{
            dict = [self.physiciansList objectAtIndex:indexPath.row];
        }
        cell.physicianNameLbl.text = [dict stringForKey:@"HospitalClinicName"];
        NSString *str=[NSString stringWithFormat:@"%@, %@ \n\n%@ %@\n",[dict stringForKey:@"HospitalAddress1"],[dict stringForKey:@"HospitalAddress2"],[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
        NSString *strA = [NSString stringWithFormat:@"%@, %@",[dict stringForKey:@"HospitalAddress1"],[dict stringForKey:@"HospitalAddress2"]];
        NSString *strB = [NSString stringWithFormat:@"%@ %@",[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
        NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
        [hogan addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"HelveticaNeue-Light" size:15]
                      range:[str rangeOfString:strA]];
        [hogan addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]
                      range:[str rangeOfString:strB]];
        cell.hospitalNameLbl.attributedText = hogan;
        NSString *completionDateString = [dict stringForKey:@"CompletionDate"];
        NSInteger completionDays = [self getNumberOfDaysFromDate:completionDateString];
        if (completionDays == 0) {
            cell.userInteractionEnabled = NO;
        }else {
            cell.userInteractionEnabled = YES;
        }
        // Convert string to date object
//        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
//        NSDate *completionDate = [dateFormat dateFromString:completionDateString];
//        NSDate *notificationDate = [completionDate dateByAddingTimeInterval:-7*24*60*60];
//        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//        localNotification.fireDate = notificationDate;
//        localNotification.timeZone = [NSTimeZone defaultTimeZone];
//        localNotification.alertAction = @"OK";
//        localNotification.alertTitle = @"HCPConnect";
//        localNotification.soundName = UILocalNotificationDefaultSoundName;
//        localNotification.applicationIconBadgeNumber = 1;
//        localNotification.alertBody = [NSString stringWithFormat:@"You have only 7 days to complete this %@  physician",cell.physicianNameLbl.text];
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        NSUInteger pharmacyMaximumValue = [[dict valueForKey:@"TotalFields"] integerValue];
        NSUInteger pharmacySubmittedValue = [[dict valueForKey:@"SubmittedFields"] integerValue];
        float totalValueFloat = (float)pharmacyMaximumValue;
        float submitedValueFloat = (float) pharmacySubmittedValue;
        cell.physicianProgress.progress = submitedValueFloat/totalValueFloat;
        cell.circularProgress.maxValue=30;
        cell.circularProgress.percentLabel.font = [UIFont systemFontOfSize:15];
        cell.circularProgress.backgroundColor = [appDelegate colorWithHexString:@"25B4FF"];
        [cell.circularProgress startAnimation];
        if (completionDays >= 0 && completionDays <= 10 ) {
            [cell.circularProgress drawCircleWithPercent:((float)100/30)*completionDays
                                                duration:0.5
                                               lineWidth:2
                                               clockwise:YES
                                                 lineCap:kCALineCapRound
                                               fillColor:[UIColor clearColor]
                                             strokeColor:[UIColor redColor]
                                          animatedColors:nil];
            
        }
        else if (completionDays > 11 && completionDays <= 20){
            [cell.circularProgress drawCircleWithPercent:((float)100/30)*completionDays
                                                duration:0.5
                                               lineWidth:2
                                               clockwise:YES
                                                 lineCap:kCALineCapRound
                                               fillColor:[UIColor clearColor]
                                             strokeColor:[UIColor orangeColor]
                                          animatedColors:nil];
        }
        else if (completionDays > 21 && completionDays <= 30){
            [cell.circularProgress drawCircleWithPercent:((float)100/30)*completionDays
                                                duration:0.5
                                               lineWidth:2
                                               clockwise:YES
                                                 lineCap:kCALineCapRound
                                               fillColor:[UIColor clearColor]
                                             strokeColor:[UIColor whiteColor]
                                          animatedColors:nil];
        }
        return cell;
    }else if (isCurrentData == NO){
        NewMissionHistoricalCell *cell2 =
        (NewMissionHistoricalCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        cell2.userInteractionEnabled = YES;
        NSDictionary *dict = [[NSDictionary alloc] init];
        if (searchActive) {
            dict = [self.searchArray objectAtIndex:indexPath.row];
        }else{
            dict = [self.physiciansList objectAtIndex:indexPath.row];
        }
        cell2.physicianNameLbl.text = [dict stringForKey:@"HospitalClinicName"];
        NSString *str=[NSString stringWithFormat:@"%@, %@ \n\n%@ %@\n",[dict stringForKey:@"HospitalAddress1"],[dict stringForKey:@"HospitalAddress2"],[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
        NSString *strA = [NSString stringWithFormat:@"%@, %@",[dict stringForKey:@"HospitalAddress1"],[dict stringForKey:@"HospitalAddress2"]];
        NSString *strB = [NSString stringWithFormat:@"%@ %@",[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
        NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
        [hogan addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"HelveticaNeue-Light" size:15]
                      range:[str rangeOfString:strA]];
        [hogan addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]
                      range:[str rangeOfString:strB]];
        cell2.hospitalNameLbl.attributedText = hogan;
        NSInteger points = [[dict objectForKey:@"Points"] integerValue];
        if (points == 0) {
            cell2.pointsLbl.text = [NSString stringWithFormat:@"%ld",(long)points];
            cell2.pointsLbl.textColor = [UIColor whiteColor];
        }else {
            cell2.pointsLbl.text = [NSString stringWithFormat:@"%ld",(long)points];
            cell2.pointsLbl.textColor = [UIColor whiteColor];
        }
        cell2.physicianProgress.progressTintColor = [UIColor greenColor];
        NSString *missiontakenDateString = [dict stringForKey:@"CompletionDate"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        NSDate *missiontakenDate = [formatter dateFromString:missiontakenDateString];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday;
        NSDateComponents *components = [calendar components:units fromDate:missiontakenDate];
        NSInteger year = [components year];
        NSInteger day = [components day];
        NSInteger month = [components month];
        NSDateFormatter *weekDay = [[NSDateFormatter alloc] init];
        [weekDay setDateFormat:@"EEE"];
        NSDateFormatter *calMonth = [[NSDateFormatter alloc] init];
        [calMonth setDateFormat:@"MMMM"];
        cell2.backgroundColor = [appDelegate colorWithHexString:@"25B4FF"];
        cell2.completionDateLbl.text = [NSString stringWithFormat:@"%ld/%ld/%ld",(long)month,(long)day,(long)year];
        return cell2;
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    NSMutableDictionary *phyDict = [self.physiciansList objectAtIndex:indexPath.row];
    if ([self.delegate respondsToSelector:@selector(physicianListSelection:)]) {
        [self.delegate physicianListSelection:phyDict];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

#pragma mark - NoOfDays Method

- (NSInteger)getNumberOfDaysFromDate:(NSString *)completionDateString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *completionDate = [dateFormatter dateFromString:completionDateString];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:[NSDate date]
                                                          toDate:completionDate
                                                         options:NSCalendarWrapComponents];
    return components.day;
}
- (IBAction)newMissionBtnAction:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)verifyMissionBtnAction:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:@"Verification process is not applicable." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:alertAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)currentBtnAction:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"Editable"];
    [self.view endEditing:YES];
    isCurrentData = YES;
    self.dateOfCompletionLabel.text = @"Date of Completion";
    self.pointHeaderLbl.hidden = YES;
    self.addPhysicianLbl.hidden = NO;
    self.addPhysicianBtn.hidden = NO;
    self.currentBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.historicalBtn.backgroundColor = self.view.backgroundColor;
    [self showProgressView];
    [[NSUserDefaults standardUserDefaults] setObject:[self.filterButton.titleLabel.text uppercaseString] forKey:PHYCATEGORY];
    [self filterSeriveCallMethod:[self.filterButton.titleLabel.text uppercaseString]];
}
- (IBAction)historicalBtnAction:(id)sender {
    [self.view endEditing:YES];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:NO forKey:@"Editable"];
    self.dateOfCompletionLabel.text = @"Date of Submission";
    isCurrentData = NO;
    self.pointHeaderLbl.hidden = NO;
    self.addPhysicianBtn.hidden = YES;
    self.addPhysicianLbl.hidden = YES;
    self.historicalBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.currentBtn.backgroundColor = self.view.backgroundColor;
    [self showProgressView];
    [[NSUserDefaults standardUserDefaults] setObject:[self.filterButton.titleLabel.text uppercaseString] forKey:PHYCATEGORY];
    [self filterSeriveCallMethod:[self.filterButton.titleLabel.text uppercaseString]];
    
}
- (IBAction)addMissionBtnAction:(id)sender {
    [self.view endEditing:YES];
    if ([self.delegate respondsToSelector:@selector(addMissionAction)]) {
        [self.delegate addMissionAction];
    }
}
- (IBAction)addPhysicianAction:(id)sender {
    [self.view endEditing:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Tracker" bundle:nil];
    popVC = (AddPhysicianPopViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AddPhysicianPopViewController"];
    popVC.delegate = self;
    popVC.modalPresentationStyle = UIModalPresentationPopover;
    popVC.preferredContentSize = CGSizeMake(300, 120);
    UIPopoverPresentationController *presentationController =[popVC popoverPresentationController];
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionRight;
    presentationController.sourceView = self.addPhysicianBtn;
    presentationController.sourceRect = self.addPhysicianBtn.bounds;
    [self presentViewController:popVC animated:YES completion:nil];
}
-(void)callImagePicker:(UIImagePickerControllerSourceType)type{
    [popVC dismissViewControllerAnimated:YES completion:nil];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = type;
    self.wasPresentingPopUp = YES;
    self.leftVC = (LeftViewController *)self.delegate;
    [self.leftVC presentViewController:imagePicker animated:YES completion:nil];
}
#pragma mark - UIImagePickerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    self.cardImage = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self performSelector:@selector(closeImagePicker) withObject:nil afterDelay:0.1];
}
- (void)closeImagePicker{
    [self showProgressView];
    [self cardProcessing];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)cardProcessing{
    Client *client = [[Client alloc] initWithApplicationID:MyApplicationID password:MyPassword];
    client.delegate = self;
    ProcessingParams* params = [[ProcessingParams alloc] init];
    [client processImage:self.cardImage withParams:params];
}
-(void)openCamera:(id)sender{
    [self callImagePicker:UIImagePickerControllerSourceTypeCamera];
}
-(void)openGallery:(id)sender{
    [self callImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
}
-(void)addNewPhysicianForm{
    [popVC dismissViewControllerAnimated:YES completion:nil];
    if ([self.delegate respondsToSelector:@selector(addPhysicianAction:)]) {
     self.dictTempDataStorage = [[NSMutableDictionary alloc] init];
     [self.delegate addPhysicianAction:self.dictTempDataStorage];
    }
}

#pragma mark - ClientDelegate implementation

//- (void)clientDidFinishUpload:(Client *)sender{
//    
//}
//
//- (void)clientDidFinishProcessing:(Client *)sender{
//    
//}
- (void)client:(Client *)sender didFinishDownloadData:(NSData *)downloadedData{
    [self.mbProgressView hideAnimated:YES];
    NSString* result = [[NSString alloc] initWithData:downloadedData encoding:NSUTF8StringEncoding];
    //NSLog(@"result: %@",result);
    self.xmlParser = [[NSXMLParser alloc] initWithData:downloadedData];
    self.xmlParser.delegate = self;
    // Initialize the mutable string that we'll use during parsing.
    self.foundValue = [[NSMutableString alloc] init];
    // Start parsing.
    [self.xmlParser parse];
}
- (void)client:(Client *)sender didFailedWithError:(NSError *)error{
    [self.mbProgressView hideAnimated:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:alertAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)parserDidStartDocument:(NSXMLParser *)parser{
    // Initialize the neighbours data array.
    self.dictTempDataStorage = [[NSMutableDictionary alloc] init];
}
-(void)parserDidEndDocument:(NSXMLParser *)parser{
    // When the parsing has been finished then simply reload the table view.
   // NSLog(@"data: %@",self.dictTempDataStorage);
    [self.mbProgressView hideAnimated:YES];
    if ([self.delegate respondsToSelector:@selector(addPhysicianAction:)]) {
        [self.delegate addPhysicianAction:self.dictTempDataStorage];
    }
}
-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    NSLog(@"%@", [parseError localizedDescription]);
}
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    self.element = elementName;
    if ([elementName isEqualToString:@"field"]) {
        self.currentElement = [attributeDict stringForKey:@"type"];
    }
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
   self.element = @"";
   if ([elementName isEqualToString:@"field"]) {
       if(self.dictTempDataStorage.allKeys.count > 0){
           for (NSString *key in self.dictTempDataStorage.allKeys) {
               if ([key isEqualToString:self.currentElement]) {
                   self.currentElement = [NSString stringWithFormat:@"%@2",self.currentElement];
               }
           }
       }
       [self.dictTempDataStorage setValue:self.foundValue forKey:self.currentElement];
    }
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    // Store the found characters if only we're interested in the current element.
    if ([self.element isEqualToString:@"value"]) {
        self.foundValue = [NSMutableString stringWithString:string];
    }
}
- (NSString*)authString{
    NSString *encodedCredentials = [[NSString stringWithFormat:@"%@:%@", MyApplicationID, MyPassword] base64EncodedString];
    
    return [NSString stringWithFormat:@"Basic %@", encodedCredentials];
}

-(IBAction)filterButtonAction:(id)sender{
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *all = [UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action) {
                                                    [self.filterButton setTitle:@"All" forState:UIControlStateNormal];
                                                    [self showProgressView];
                                                    [[NSUserDefaults standardUserDefaults] setObject:@"ALL" forKey:PHYCATEGORY];
                                                    [self filterSeriveCallMethod:@"ALL"];
                                                }];
    UIAlertAction *assigned = [UIAlertAction actionWithTitle:@"Assigned" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                    [self.filterButton setTitle:@"Assigned" forState:UIControlStateNormal];
                                                    [self showProgressView];
                                                    [[NSUserDefaults standardUserDefaults] setObject:@"ASSIGNED" forKey:PHYCATEGORY];
                                                         [self filterSeriveCallMethod:@"ASSIGNED"];
                                                     }];
    UIAlertAction *unAssigned = [UIAlertAction actionWithTitle:@"Open/Unassigned" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [self.filterButton setTitle:@"Unassigned" forState:UIControlStateNormal];
                                                           [self showProgressView];
                                                           [[NSUserDefaults standardUserDefaults] setObject:@"UNASSIGNED" forKey:PHYCATEGORY];
                                                           [self filterSeriveCallMethod:@"UNASSIGNED"];
                                                       }];
    UIAlertAction *newPhysician = [UIAlertAction actionWithTitle:@"New Physician" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [self.filterButton setTitle:@"New" forState:UIControlStateNormal];
                                                             [self showProgressView];
                                                             [[NSUserDefaults standardUserDefaults] setObject:@"NEW" forKey:PHYCATEGORY];
                                                             [self filterSeriveCallMethod:@"NEW"];
                                                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    [alert addAction:all];
    [alert addAction:assigned];
    [alert addAction:unAssigned];
    [alert addAction:newPhysician];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.filterButton;
    popPresenter.sourceRect = self.filterButton.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)filterContentForSearchText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"Brick contains[c] %@", searchText];
    self.searchArray  = [self.physiciansList filteredArrayUsingPredicate:resultPredicate];
    //[self.view endEditing:YES];
    [self.physicianListTbl reloadData];
}
-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.searchTxtField) {
        if ([textField.text isEqualToString:@""]){
            searchActive = NO;
            self.physiciansList = self.tempArray;
            //[self.view endEditing:YES];
            [self.physicianListTbl reloadData];
        }else{
            searchActive = YES;
            [self filterContentForSearchText:textField.text];
        }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
