//
//  AppDelegate.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 25/10/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuList.h"
#import "PHCPHelloScreenViewController.h"
#import "PHCPRegistrationViewController.h"
#import "VerificationViewController.h"
#import "TrainingPageViewController.h"
#import "LeftViewController.h"
#import "WelcomePageViewController.h"
#import "AppUpdater.h"

//Offline
#define PhysicianNewMissionFile @"PhysicianNewMissionFile"
#define PhysicianNewMissionCurrentPhysicianListFile @"PhysicianNewMissionCurrentPhysicianListFile"
#define MyTrackerDetailsFile @"MyTrackerDetailsFile"
#define PhysicianNewMissionConsentFile @"PhysicianNewMissionConsentFile"

#define PharmacistNewMissionCurrentPharmacistListFile @"PharmacistNewMissionCurrentPharmacistListFile" //this is actually pharmacynewmissioncurrent list
#define PharmacyNewMissionFile @"PharmacyNewMissionFile"
#define PharmacistNewMissionFile @"PharmacistNewMissionFile"
#define PharmacyNewMissionConsentFile @"PharmacyNewMissionConsentFile"

#define PharmacistVerifyMissionCurrentPharmacistListFile @"PharmacistVerifyMissionCurrentPharmacistListFile"

#define finalNewPharmacyArrayFile @"finalNewPharmacyArrayFile"
#define finalVerifyPharmacyArrayFile @"finalVerifyPharmacyArrayFile"
//Offline

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[[Crashlytics class]]];
    [Crashlytics startWithAPIKey:@"22b411d2af670c77c89ea1b74d86585d9b8aad60"];
    
    [[AppUpdater sharedUpdater] showUpdateWithConfirmation];
    
    UIUserNotificationType types = UIUserNotificationTypeBadge |
    UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    UIUserNotificationSettings *notificationSettings =
    [UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    
    UILocalNotification *localNotification =
    [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotification) {
        [application cancelLocalNotification:localNotification];
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"HCPConnect" message:localNotification.alertBody delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
         application.applicationIconBadgeNumber = localNotification.applicationIconBadgeNumber;
    }
   

    [[UINavigationBar appearance] setBarTintColor:[self colorWithHexString:@"3A4F5A"]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    UINavigationController *navigationController = [[UINavigationController alloc] init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:@"OTPPAGE"] == YES){
        PHCPRegistrationViewController *registrationVC = [storyboard instantiateViewControllerWithIdentifier:@"PHCPRegistrationViewController"];
        registrationVC.showPopUp = YES;
        navigationController = [[UINavigationController alloc] initWithRootViewController:registrationVC];
        self.window.rootViewController = navigationController;
    }else if ([defaults boolForKey:@"QuizPage"] == YES){
        TrainingPageViewController *trainingPageVC = [storyboard instantiateViewControllerWithIdentifier:@"TrainingPageViewController"];
        navigationController = [[UINavigationController alloc] initWithRootViewController:trainingPageVC];
        self.window.rootViewController = navigationController;
    }else if ([defaults boolForKey:WELCOME_STRING] == YES){
        WelcomePageViewController *landingPageVC = [storyboard instantiateViewControllerWithIdentifier:@"WelcomePageViewController"];
        navigationController = [[UINavigationController alloc] initWithRootViewController:landingPageVC];
        self.window.rootViewController = navigationController;
    }else if ([defaults boolForKey:@"Active"] == YES) {
        LeftViewController *homeVC = [storyboard instantiateViewControllerWithIdentifier:@"LeftViewController"];
        navigationController = [[UINavigationController alloc] initWithRootViewController:homeVC];
        self.window.rootViewController = navigationController;
    }else{
        PHCPHelloScreenViewController *launchVC = [storyboard instantiateViewControllerWithIdentifier:@"PHCPHelloScreenViewController"];
        navigationController = [[UINavigationController alloc] initWithRootViewController:launchVC];
        self.window.rootViewController = navigationController;
    }
    self.window.backgroundColor = [UIColor whiteColor];
    
    //offline
    [self initialiseFromSavedFiles];
    //offline
    
    return YES;
}
//offline
-(void)initialiseFromSavedFiles{
    
    //physician traker current list
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *strFilePhysicianNewMissionCurrentList = [self getFilePathForPhysicianNewMissionCurrentList];
    if([fileManager fileExistsAtPath:strFilePhysicianNewMissionCurrentList]){
        NSArray *arrPhysicianNewMissionCurrentListFile = [NSKeyedUnarchiver unarchiveObjectWithFile:strFilePhysicianNewMissionCurrentList];
        self.physicianNewMissionCurrentPhysicianList = [[NSMutableArray alloc] initWithArray:arrPhysicianNewMissionCurrentListFile];
    }
    //MyTracker Details
    NSString *strFileMyTrackerDetails = [self getFilePathForMyTrackerDetails];
    if([fileManager fileExistsAtPath:strFileMyTrackerDetails]){
        NSDictionary *dictMyTrackerDetailsFile = [NSKeyedUnarchiver unarchiveObjectWithFile:strFileMyTrackerDetails];
        self.myTrackerDetails = [[NSMutableDictionary alloc] initWithDictionary:dictMyTrackerDetailsFile];
    }
    //physician new missions
    NSString *strFilePhysicianNewMissions = [self getFilePathForPhysicianNewMission];
    if([fileManager fileExistsAtPath:strFilePhysicianNewMissions]){
        NSArray *arrPhysicianNewMissions = [NSKeyedUnarchiver unarchiveObjectWithFile:strFilePhysicianNewMissions];
        self.physicianNewMissionArray = [[NSMutableArray alloc] initWithArray:arrPhysicianNewMissions];
    }
    
    
    //physician new missions consents
    NSString *strFilePhysicianNewMissionConsents = [self getFilePathForPhysicianNewMissionConsents];
    if([fileManager fileExistsAtPath:strFilePhysicianNewMissionConsents]){
        NSArray *arrPhysicianNewMissionConsents = [NSKeyedUnarchiver unarchiveObjectWithFile:strFilePhysicianNewMissionConsents];
        self.physicianNewMissionConsentArray = [[NSMutableArray alloc] initWithArray:arrPhysicianNewMissionConsents];
    }
    
    
    //pharmacist traker current list
    NSString *strFilePharmacistNewMissionCurrentList = [self getFilePathForPharmacistNewMissionCurrentList];
    if([fileManager fileExistsAtPath:strFilePharmacistNewMissionCurrentList]){
        NSArray *arrPharmacistNewMissionCurrentListFile = [NSKeyedUnarchiver unarchiveObjectWithFile:strFilePharmacistNewMissionCurrentList];
        self.pharmacistNewMissionCurrentPharmacistList = [[NSMutableArray alloc] initWithArray:arrPharmacistNewMissionCurrentListFile];
    }
    
    //pharmacy new missions
    NSString *strFilePharmacyNewMissions = [self getFilePathForPharmacyNewMission];
    if([fileManager fileExistsAtPath:strFilePharmacyNewMissions]){
        NSArray *arrPharmacyNewMissions = [NSKeyedUnarchiver unarchiveObjectWithFile:strFilePharmacyNewMissions];
        self.pharmacyNewMissionArray = [[NSMutableArray alloc] initWithArray:arrPharmacyNewMissions];
    }
    
    //pharmacist new missions
    NSString *strFilePharmacistNewMissions = [self getFilePathForPharmacistNewMission];
    if([fileManager fileExistsAtPath:strFilePharmacistNewMissions]){
        NSArray *arrPharmacistNewMissions = [NSKeyedUnarchiver unarchiveObjectWithFile:strFilePharmacistNewMissions];
        self.pharmacistNewMissionArray = [[NSMutableArray alloc] initWithArray:arrPharmacistNewMissions];
    }
    
    //pharmacist new missions consents
    NSString *strFilePharmacistNewMissionConsents = [self getFilePathForPharmacistNewMissionConsents];
    if([fileManager fileExistsAtPath:strFilePharmacistNewMissionConsents]){
        NSArray *arrPharmacistNewMissionConsents = [NSKeyedUnarchiver unarchiveObjectWithFile:strFilePharmacistNewMissionConsents];
        self.pharmacistNewMissionConsentArray = [[NSMutableArray alloc] initWithArray:arrPharmacistNewMissionConsents];
    }
    
    //verify pharmacist traker current list
    NSString *strFilePharmacistVerifyMissionCurrentList = [self getFilePathForPharmacistVerifyMissionCurrentList];
    if([fileManager fileExistsAtPath:strFilePharmacistVerifyMissionCurrentList]){
        NSArray *arrPharmacistVerifyMissionCurrentListFile = [NSKeyedUnarchiver unarchiveObjectWithFile:strFilePharmacistVerifyMissionCurrentList];
        self.pharmacistVerifyMissionCurrentPharmacistList = [[NSMutableArray alloc] initWithArray:arrPharmacistVerifyMissionCurrentListFile];
    }
    
    //final new mission submit
    NSString *strFileFinalNewSubmit = [self getFilePathForFinalNewMission];
    if([fileManager fileExistsAtPath:strFileFinalNewSubmit]){
        NSArray *arrFileFinalNewSubmit = [NSKeyedUnarchiver unarchiveObjectWithFile:strFileFinalNewSubmit];
        self.finalNewPharmacyArray = [[NSMutableArray alloc] initWithArray:arrFileFinalNewSubmit];
    }
    
    //final verify mission submit
    NSString *strFileFinalVerifySubmit = [self getFilePathForFinalVerifyMission];
    if([fileManager fileExistsAtPath:strFileFinalVerifySubmit]){
        NSArray *arrFileFinalVerifySubmit = [NSKeyedUnarchiver unarchiveObjectWithFile:strFileFinalVerifySubmit];
        self.finalVerifyPharmacyArray = [[NSMutableArray alloc] initWithArray:arrFileFinalVerifySubmit];
    }
    
    
}

-(NSString *)getFilePathForFinalNewMission{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:finalNewPharmacyArrayFile]];
    return databasePath;
}
-(NSString *)getFilePathForFinalVerifyMission{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:finalVerifyPharmacyArrayFile]];
    return databasePath;
}

-(NSString *)getFilePathForPhysicianNewMission{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:PhysicianNewMissionFile]];
    return databasePath;
}

-(NSString *)getFilePathForPhysicianNewMissionCurrentList{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:PhysicianNewMissionCurrentPhysicianListFile]];
    return databasePath;
}
-(NSString *)getFilePathForMyTrackerDetails{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:MyTrackerDetailsFile]];
    return databasePath;
}
-(NSString *)getFilePathForPhysicianNewMissionConsents{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:PhysicianNewMissionConsentFile]];
    return databasePath;
}

-(NSString *)getFilePathForPharmacistNewMissionCurrentList{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:PharmacistNewMissionCurrentPharmacistListFile]];
    return databasePath;
}

-(NSString *)getFilePathForPharmacyNewMission{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:PharmacyNewMissionFile]];
    return databasePath;
}

-(NSString *)getFilePathForPharmacistNewMission{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:PharmacistNewMissionFile]];
    return databasePath;
}

-(NSString *)getFilePathForPharmacistNewMissionConsents{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:PharmacyNewMissionConsentFile]];
    return databasePath;
}


-(NSString *)getFilePathForPharmacistVerifyMissionCurrentList{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:PharmacistVerifyMissionCurrentPharmacistListFile]];
    return databasePath;
}
//offline

-(UIColor*)colorWithHexString:(NSString *)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    //OFFLINE
    NSString *strFilePhysicianNewMission = [self getFilePathForPhysicianNewMission];
    NSData *data =   [NSKeyedArchiver archivedDataWithRootObject:self.physicianNewMissionArray];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFilePhysicianNewMission atomically:YES];
        
    }
    
    NSString *strFilePhysicianNewMissionCurrentList = [self getFilePathForPhysicianNewMissionCurrentList];
    data =   [NSKeyedArchiver archivedDataWithRootObject:self.physicianNewMissionCurrentPhysicianList];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFilePhysicianNewMissionCurrentList atomically:YES];
        
    }
    
    NSString *strFileMyTrackerDetails = [self getFilePathForMyTrackerDetails];
    data =   [NSKeyedArchiver archivedDataWithRootObject:self.myTrackerDetails];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFileMyTrackerDetails atomically:YES];
        
    }
    
    NSString *strFilePhysicianNewMissionConsents = [self getFilePathForPhysicianNewMissionConsents];
    data =   [NSKeyedArchiver archivedDataWithRootObject:self.physicianNewMissionConsentArray];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFilePhysicianNewMissionConsents atomically:YES];
        
    }
    
    
    
    NSString *strFilePharmacistNewMissionCurrentList = [self getFilePathForPharmacistNewMissionCurrentList];
    data =   [NSKeyedArchiver archivedDataWithRootObject:self.pharmacistNewMissionCurrentPharmacistList];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFilePharmacistNewMissionCurrentList atomically:YES];
        
    }
    
    //pharmacy new missions
    NSString *strFilePharmacyNewMission = [self getFilePathForPharmacyNewMission];
    data =   [NSKeyedArchiver archivedDataWithRootObject:self.pharmacyNewMissionArray];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFilePharmacyNewMission atomically:YES];
        
    }
    
    //pharmacist new missions
    NSString *strFilePharmacistNewMission = [self getFilePathForPharmacistNewMission];
    data =   [NSKeyedArchiver archivedDataWithRootObject:self.pharmacistNewMissionArray];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFilePharmacistNewMission atomically:YES];
        
    }
    
    //pharmacist new missions consents
    NSString *strFilePharmacistNewMissionConsents = [self getFilePathForPharmacistNewMissionConsents];
    data =   [NSKeyedArchiver archivedDataWithRootObject:self.pharmacistNewMissionConsentArray];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFilePharmacistNewMissionConsents atomically:YES];
        
    }
    
    NSString *strFilePharmacistVerifyMissionCurrentList = [self getFilePathForPharmacistVerifyMissionCurrentList];
    data =   [NSKeyedArchiver archivedDataWithRootObject:self.pharmacistVerifyMissionCurrentPharmacistList];
    if(data){
        __unused  BOOL isSaved1= [data writeToFile:strFilePharmacistVerifyMissionCurrentList atomically:YES];
        
    }
    
    //OFFLINE
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self initialiseFromSavedFiles];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive ) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"HCPConnect Local Notification." message:notification.alertBody preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
        //The application received a notification in the active state, so you can display an alert view or do something appropriate.
    }
    [application cancelLocalNotification:notification];
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = notification.applicationIconBadgeNumber;
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.imshealth.in.PfizerHealthConnect" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"PfizerHealthConnect" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"PfizerHealthConnect.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
