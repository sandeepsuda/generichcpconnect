//
//  LeftViewController.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 07/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectMissionViewController.h"
#import "PhysicianTrackerViewController.h"
#import "PharmacyTrackerViewController.h"
#import "PharmacySelectionViewController.h"
#import "AboutUsViewController.h"
#import "ContactUsViewController.h"
#import "TrainingMaterialsViewController.h"
#import "LeaderBoardViewController.h"
#import "PfizerAddPhysicianViewController.h"
#import "PHYConsentViewController.h"
#import "PHRConsentViewController.h"
#import "PHRAddPharmacistViewController.h"
#import "PharmacyDetailsViewController.h"
#import "VerifyPharmacyDetailsViewController.h"
#import "ConsentViewController.h"
#import "PharmacistConsentViewController.h"

@interface LeftViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,PhysicianTrackerViewControllerDelegate,SelectMissionViewControllerDelegate,UITextFieldDelegate,PfizerAddPhysicianProtocol,PhysicianConsentProtocol,UIImagePickerControllerDelegate, UINavigationControllerDelegate,MKAnnotation,UIPopoverPresentationControllerDelegate,PharmacyTrackerViewControllerDelegate,PharmacySelectionViewControllerDelegate,PharmacyDetailsViewControllerDelegate,PHRConsentViewControllerDelegate>
@property (nonatomic,weak)IBOutlet UIView *leftView;
@property (nonatomic,weak)IBOutlet UIView *rightView;
@property(nonatomic,weak)IBOutlet UILabel *nameLabel;
@property(nonatomic,weak)IBOutlet UILabel *gainedPointsLabel;
@property(nonatomic,weak)IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UIImageView *placeHolderImg;
@property(nonatomic,strong)NSArray *menuItems;
@property(nonatomic)NSInteger selectedMenuIndex;
@property(nonatomic,strong)PharmacySelectionViewController *PHRSelectMissionVC;
@property(nonatomic,strong)PharmacyTrackerViewController *PHRpharmacyVC;
@property(nonatomic,strong)PHRAddPharmacistViewController *PHRaddPharmacistVC;
@property(nonatomic,strong)SelectMissionViewController *selectMissionVC;
@property(nonatomic,strong)LeaderBoardViewController *leaderBoardVC;
@property(nonatomic,strong)PfizerAddPhysicianViewController *addPhysicianVC;
@property(nonatomic,strong)PhysicianTrackerViewController *physicianVC;
@property(nonatomic,strong)AboutUsViewController *aboutUsVC;
@property(nonatomic,strong)ContactUsViewController *contactUsVC;
@property(nonatomic,strong)TrainingMaterialsViewController *trainingVC;
//@property(nonatomic,strong)PHYConsentViewController *consentVC;
//@property(nonatomic,strong)PHRConsentViewController *PHRConsentVC;
@property(nonatomic,strong)ConsentViewController *consentCon;
@property(nonatomic,strong)PharmacistConsentViewController *phrConsentCon;
@property(nonatomic,strong)PharmacyDetailsViewController *PHRpharmacyDetailsVC;
@property (nonatomic,strong) VerifyPharmacyDetailsViewController *verifyPharmacyDetailsVC;
@property (nonatomic,weak) UIImage *buildingPicImage;
@property (nonatomic,weak) UIImageView *buildingPicImageView;

@property(nonatomic)BOOL wasPresenting;
-(void)showHideNavigationItems;
-(void)addNavigationItem;
@end
