//
//  PHRNewAddPharmacistViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 25/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHRAddPharmacistViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *pharmacistDeniedInfoCountLbl;
@property (weak, nonatomic) IBOutlet UITableView *pharmacistsTbl;
@property (weak, nonatomic) IBOutlet UIView *topViewBtn;
@property (weak, nonatomic) IBOutlet UIView *viewBotton;
@property (weak, nonatomic) IBOutlet UILabel *pharmacyNameLbl;
@property (nonatomic,strong) NSMutableArray *pharmacistsList;
@property (weak, nonatomic) IBOutlet UIView *scoreView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scoreViewHightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *addedPharmacistsCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPharmacistsCountLbl;
@end
