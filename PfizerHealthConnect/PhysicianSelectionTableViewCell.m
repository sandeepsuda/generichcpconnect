//
//  PhysicianSelectionTableViewCell.m
//  PfizerHealthConnect
//
//  Created by sandeep suda on 13/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PhysicianSelectionTableViewCell.h"

@implementation PhysicianSelectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
