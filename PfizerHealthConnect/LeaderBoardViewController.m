//
//  LeaderBoardViewController.m
//  IPadApp
//
//  Created by RajaSekhar on 04/11/16.
//  Copyright © 2016 RajaSekhar. All rights reserved.
//

#import "LeaderBoardViewController.h"
#import "NSDictionary+Dictionary.h"
#import "Webservice.h"
#import "MenuList.h"
#import "AppDelegate.h"

@interface LeaderBoardViewController ()
@property (nonatomic) BOOL report1Bool,report2Bool,report3Bool,report4Bool;
@end

@implementation LeaderBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:NO];
}
-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.jsonDict = [[NSMutableDictionary alloc] init];
    self.jsonArray = [[NSMutableArray alloc] init];
    [self.serachField addTarget:self
                            action:@selector(textFieldDidChange:)
                  forControlEvents:UIControlEventEditingChanged];
    self.report1Bool = YES;
    searchActive = NO;
    [self getLeaderBoardDetails:@"Report1"];
    self.reportsSubView.backgroundColor = [UIColor whiteColor];
    self.recordsCompletedLabel.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report1Button.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.report2Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report3Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report4Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.tableView.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"LeaderBoardTableViewCell" bundle:nil] forCellReuseIdentifier:@"LeaderBoardTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LBAssignedTableViewCell" bundle:nil] forCellReuseIdentifier:@"LBAssignedTableViewCell"];
}
-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.serachField) {
        if ([textField.text isEqualToString:@""]){
            searchActive = NO;
            self.jsonArray = self.tempArray;
            //[self.view endEditing:YES];
            [self.tableView reloadData];
        }else{
            searchActive = YES;
            [self filterContentForSearchText:textField.text];
        }
    }
}
- (void)filterContentForSearchText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"FirstName contains[c] %@", searchText];
    self.searchArray  = [self.jsonArray filteredArrayUsingPredicate:resultPredicate];
    //[self.view endEditing:YES];
    [self.tableView reloadData];
}

#pragma mark - Custom Methods

-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = @"Loading...";
}

#pragma mark - UITableView Delegate & DataSource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchActive) {
        return self.searchArray.count;
    }else{
        return self.jsonArray.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LeaderBoardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeaderBoardTableViewCell"];
    LBAssignedTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"LBAssignedTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell1.selectionStyle = UITableViewCellSelectionStyleNone;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = [[NSDictionary alloc] init];
    if (searchActive) {
        dict = [self.searchArray objectAtIndex:indexPath.row];
    }else{
        dict = [self.jsonArray objectAtIndex:indexPath.row];
    }
    if (self.report1Bool) {
        cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
        cell.pharmacyPointsLabel.text = [NSString stringWithFormat:@"%@",[dict stringForKey:@"DCPharmacyPoints"]];
        cell.physicianPointsLabel.text = [NSString stringWithFormat:@"%@",[dict stringForKey:@"DCPhysicianTotalPoints"]];
        cell.gainedPointsLabel.text = [dict stringForKey:@"DCTotalPoints"];
        [cell.userProfileImageView sd_setImageWithURL:[NSURL URLWithString:[dict stringForKey:@"DCImagePath"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        cell.userProfileImageView.clipsToBounds = YES;
        cell.userProfileImageView.layer.cornerRadius = cell.userProfileImageView.frame.size.width/2;
        cell.userProfileImageView.backgroundColor = [UIColor lightGrayColor];
        if (![[dict stringForKey:@"DCTotalPoints"] isEqualToString:@"0"]) {
            if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
                cell.badgeImageView.image = [UIImage imageNamed:@"Badge-Gold"];
            } else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5){
                cell.badgeImageView.image = [UIImage imageNamed:@"Badge-Silver"];
            }else if (indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9){
                cell.badgeImageView.image = [UIImage imageNamed:@"Badge-Bronze"];
            }else{
                cell.badgeImageView.image = [UIImage imageNamed:@""];
            }
        }else{
            cell.badgeImageView.image = nil;
        }
        if (indexPath.row != 0) {
            cell.pointsGainedLbl.hidden = YES;
        }else{
            cell.pointsGainedLbl.hidden = NO;
        }
        return cell;
    }else if (self.report2Bool){
        cell1.assignedNameLabel.hidden = NO;
        cell1.assignedNameLabel.text = @"Assigned";
        cell1.assignedPointsLabel.hidden = NO;
        cell1.completedNameLabel.hidden = NO;
        cell1.completedPointsLabel.hidden = NO;
        [cell1.profileImageView sd_setImageWithURL:[NSURL URLWithString:[dict stringForKey:@"DCImagePath"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        cell1.dataCollectorNameLabel.text = [NSString stringWithFormat:@"%@ %@",[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
        cell1.completedPointsLabel.text = [dict stringForKey:@"Completed"];
        cell1.assignedPointsLabel.text = [dict stringForKey:@"Assigned"];
        cell1.profileImageView.clipsToBounds = YES;
        cell1.profileImageView.layer.cornerRadius = cell.userProfileImageView.frame.size.width/2;
        cell1.profileImageView.backgroundColor = [UIColor lightGrayColor];
        if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
            cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Gold"];
        } else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5){
            cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Silver"];
        }else if (indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9){
            cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Bronze"];
        }else{
            cell1.badgeImageView.image = [UIImage imageNamed:@""];
        }
        if (indexPath.row != 0) {
            cell1.assignedLbl.hidden = YES;
            cell1.completedLbl.hidden = YES;
        }else{
            cell1.assignedLbl.hidden = NO;
            cell1.completedLbl.hidden = NO;
        }
        return cell1;
    }else if (self.report3Bool){
        cell1.assignedNameLabel.hidden = NO;
        cell1.assignedNameLabel.text = @"Assigned";
        cell1.assignedPointsLabel.hidden = NO;
        cell1.completedNameLabel.hidden = NO;
        cell1.completedPointsLabel.hidden = NO;
        cell1.dataCollectorNameLabel.text = [NSString stringWithFormat:@"%@ %@",[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
        [cell1.profileImageView sd_setImageWithURL:[NSURL URLWithString:[dict stringForKey:@"DCImagePath"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        cell1.completedPointsLabel.text = [dict stringForKey:@"Completed"];
        cell1.assignedPointsLabel.text = [dict stringForKey:@"Assigned"];
        cell1.profileImageView.clipsToBounds = YES;
        cell1.profileImageView.layer.cornerRadius = cell.userProfileImageView.frame.size.width/2;
        cell1.profileImageView.backgroundColor = [UIColor lightGrayColor];
        if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
            cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Gold"];
        } else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5){
            cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Silver"];
        }else if (indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9){
            cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Bronze"];
        }else{
            cell1.badgeImageView.image = [UIImage imageNamed:@""];
        }
        if (indexPath.row != 0) {
            cell1.assignedLbl.hidden = YES;
            cell1.completedLbl.hidden = YES;
        }else{
            cell1.assignedLbl.hidden = NO;
            cell1.completedLbl.hidden = NO;
        }
        return cell1;
    }else{
        cell1.assignedNameLabel.hidden = YES;
        cell1.assignedPointsLabel.hidden = YES;
        cell1.completedNameLabel.text = @"Completed";
        cell1.completedNameLabel.hidden = NO;
        cell1.completedPointsLabel.hidden = NO;
        cell1.dataCollectorNameLabel.text = [NSString stringWithFormat:@"%@ %@",[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
        cell1.completedPointsLabel.text = [dict stringForKey:@"Completed"];
        [cell1.profileImageView sd_setImageWithURL:[NSURL URLWithString:[dict stringForKey:@"DCImagePath"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        cell1.profileImageView.clipsToBounds = YES;
        cell1.profileImageView.layer.cornerRadius = cell.userProfileImageView.frame.size.width/2;
        cell1.profileImageView.backgroundColor = [UIColor lightGrayColor];
        if (![[dict stringForKey:@"Completed"] isEqualToString:@"0"]) {
            if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
                cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Gold"];
            } else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5){
                cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Silver"];
            }else if (indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9){
                cell1.badgeImageView.image = [UIImage imageNamed:@"Badge-Bronze"];
            }else{
                cell1.badgeImageView.image = [UIImage imageNamed:@""];
            }
        }else{
            cell1.badgeImageView.image = nil;
        }
        if (indexPath.row != 0) {
            cell1.completedNameLabel.hidden = YES;
        }else{
            cell1.completedNameLabel.hidden = NO;
        }
        return cell1;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

#pragma mark - UIButton Action Methods

-(IBAction)report1ButtonAction:(id)sender{
    searchActive = NO;
    self.serachField.text = nil;
    self.report1Bool = YES;
    self.report2Bool = NO;
    self.report3Bool = NO;
    self.report4Bool = NO;
    [self getLeaderBoardDetails:@"Report1"];
    self.report1Button.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.report2Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report3Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report4Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
}

-(IBAction)report2ButtonAction:(id)sender{
    searchActive = NO;
    self.serachField.text = nil;
    self.report1Bool = NO;
    self.report2Bool = YES;
    self.report3Bool = NO;
    self.report4Bool = NO;
    [self getLeaderBoardDetails:@"Report2"];
    self.report1Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report2Button.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.report3Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report4Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
}

-(IBAction)report3ButtonAction:(id)sender{
    searchActive = NO;
    self.serachField.text = nil;
    self.report1Bool = NO;
    self.report2Bool = NO;
    self.report3Bool = YES;
    self.report4Bool = NO;
    [self getLeaderBoardDetails:@"Report3"];
    self.report1Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report2Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report3Button.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.report4Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
}

-(IBAction)report4ButtonAction:(id)sender{
    searchActive = NO;
    self.serachField.text = nil;
    self.report1Bool = NO;
    self.report2Bool = NO;
    self.report3Bool = NO;
    self.report4Bool = YES;
    [self getLeaderBoardDetails:@"Report4"];
    self.report1Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report2Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report3Button.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.report4Button.backgroundColor = [appDelegate colorWithHexString:YellowColor];
}


#pragma mark - ServiceCall Method

-(void)getLeaderBoardDetails:(NSString *)reportString{
    [self showProgressView];
    //DCCountry
    NSString *country = [[NSUserDefaults standardUserDefaults] objectForKey:@"DCCountry"];
    NSString *urlString = [NSString stringWithFormat:@"%@%@/%@/%@",KBASE_URL,LEADERBOARD_LIST,reportString,[country stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.jsonDict = (NSMutableDictionary *)data;
            if (self.report1Bool) {
                self.jsonArray = [self.jsonDict objectForKey:@"leaderBoardResponse"];
                NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"DCTotalPoints" ascending:NO selector:@selector(compare:)];
                self.jsonArray=[NSMutableArray arrayWithArray:[self.jsonArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
            }else if(self.report4Bool){
                self.jsonArray = [self.jsonDict objectForKey:@"leaderBoardDoctorBase"];
                NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"Completed" ascending:NO selector:@selector(compare:)];
                self.jsonArray=[NSMutableArray arrayWithArray:[self.jsonArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
            }else{
                self.jsonArray = [self.jsonDict objectForKey:@"leaderBoardDoctorBase"];
            }
            self.tempArray = self.jsonArray;
            if (self.report1Bool) {
                self.completedMissionsString = [self.jsonDict stringForKey:@"CompletePhysiciandMissionCount"];
                self.totalMissionsString = [self.jsonDict stringForKey:@"TotalPhysicianMissionCount"];
                self.completedPharmacyString = [self.jsonDict stringForKey:@"CompletedMissionCount"];
                self.totalPharmacystring = [self.jsonDict stringForKey:@"TotalMissionCount"];
                NSString *str=[NSString stringWithFormat:@"%@%@/%@   %@ %@/%@",@"Records Completed:  Physician ",self.completedMissionsString,self.totalMissionsString,@"Pharmacy",self.completedPharmacyString,self.totalPharmacystring];
                NSString *strA = @"Records Completed:";
                NSString *strG = @"Physician";
                NSString *strB = [NSString stringWithFormat:@"%@/%@",self.completedMissionsString,self.totalMissionsString];
                //NSString *strC = self.totalMissionsString;
                NSString *strD = @"Pharmacy";
                NSString *strE = [NSString stringWithFormat:@"%@/%@",self.completedPharmacyString,self.totalPharmacystring];
                //NSString *strF = self.totalPharmacystring;
                NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strA]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strB]];
                //                [hogan addAttribute:NSFontAttributeName
                //                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                //                              range:[str rangeOfString:strC]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Light" size:19]
                              range:[str rangeOfString:strD]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Light" size:19]
                              range:[str rangeOfString:strG]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strE]];
                //                [hogan addAttribute:NSFontAttributeName
                //                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                //                              range:[str rangeOfString:strF]];
                self.recordsCompletedLabel.attributedText = hogan;
            }else{
                self.completedMissionsString = [self.jsonDict stringForKey:@"CompletedMissionCount"];
                self.totalMissionsString = [self.jsonDict stringForKey:@"TotalMissionCount"];
            }
            
            if (self.report2Bool){
                NSString *str=[NSString stringWithFormat:@"%@%@/%@",@"Total Assigned List:  ",self.completedMissionsString,self.totalMissionsString];
                NSString *strA = @"Total Assigned List:";
                NSString *strB = [NSString stringWithFormat:@"%@/",self.completedMissionsString];
                NSString *strC = self.totalMissionsString;
                
                NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strA]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strB]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strC]];
                self.recordsCompletedLabel.attributedText = hogan;
            }else if (self.report3Bool){
                NSString *str=[NSString stringWithFormat:@"%@%@/%@",@"Total Open List:  ",self.completedMissionsString,self.totalMissionsString];
                NSString *strA = @"Total Open List:";
                NSString *strB = [NSString stringWithFormat:@"%@/",self.completedMissionsString];
                NSString *strC = self.totalMissionsString;
                NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strA]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strB]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strC]];
                self.recordsCompletedLabel.attributedText = hogan;
            }else if(self.report4Bool){
                NSString *str=[NSString stringWithFormat:@"%@%@/%@",@"New Physician Records Completed:  ",self.completedMissionsString,self.totalMissionsString];
                NSString *strA = @"New Physician Records Completed:";
                NSString *strB = [NSString stringWithFormat:@"%@/%@",self.completedMissionsString,self.totalMissionsString];
                // NSString *strC = self.totalMissionsString;
                NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strA]];
                [hogan addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                              range:[str rangeOfString:strB]];
                //                [hogan addAttribute:NSFontAttributeName
                //                              value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:19]
                //                              range:[str rangeOfString:strC]];
                self.recordsCompletedLabel.attributedText = hogan;
            }
            [self.tableView reloadData];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
