//
//  PhysicianTrackerViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 04/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNCirclePercentView.h"
#import <SDWebImage/SDImageCache.h>
#import "UIImageView+WebCache.h"
#import "Client.h"
#import "AddPhysicianPopViewController.h"

@protocol PhysicianTrackerViewControllerDelegate <NSObject>
@required
-(void)addMissionAction;
-(void)physicianListSelection:(NSDictionary*)phyDict;
-(void)addPhysicianAction:(NSMutableDictionary*)phyDict;
-(void)setUserDetails:(NSDictionary*)userInfo;
-(void)filterAction:(UIButton *)sender;
@end

@interface PhysicianTrackerViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate,ClientDelegate,NSXMLParserDelegate>{
    BOOL isCurrentData;
    BOOL searchActive;
}
@property (weak,nonatomic) id<PhysicianTrackerViewControllerDelegate> delegate;
@property (nonatomic, strong) UIImage *cardImage;
@property (weak, nonatomic) IBOutlet UIButton *verifyMissionBtn;
@property (weak, nonatomic) IBOutlet UIView *scoreView;
@property (weak, nonatomic) IBOutlet UIButton *currentBtn;
@property (weak, nonatomic) IBOutlet UIButton *historicalBtn;
@property (weak, nonatomic) IBOutlet UIButton *addMissionBtn;
@property (weak, nonatomic) IBOutlet UIButton *missionNewBtn,*filterButton;
@property (weak, nonatomic) IBOutlet UILabel *takenCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *submitCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *availableCountLbl;
@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;
@property (nonatomic, strong) NSDictionary *mainTracketData;
@property (nonatomic, strong) NSMutableArray *physiciansList,*tempArray;
@property (nonatomic, strong) NSArray *searchList;
@property (nonatomic,strong) NSArray *searchArray;
@property (weak, nonatomic) IBOutlet UITableView *physicianListTbl;
@property (nonatomic) NSInteger dateOfcompletionValue;
@property (weak, nonatomic) IBOutlet UILabel *addPhysicianLbl,*dateOfCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointHeaderLbl;
@property (weak, nonatomic) IBOutlet UIButton *addPhysicianBtn;
@property(nonatomic)BOOL wasPresentingPopUp;
@end
