//
//  Webservice.h
//  PharmConnect
//
//  Created by Sandeep Suda on 24/08/16.
//  Copyright © 2016 Kumar, Utpal (Bangalore). All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^HttpSuccess)(id data);
typedef void (^HttpFailure)(NSError *error);


@interface Webservice : NSObject

+  (void)getWithUrlString:(NSString *)strURL success:(HttpSuccess)success failure:(HttpFailure)failure;

+  (void)postWithURlString:(NSString*)strURL withParameters:(id)params sucess:(HttpSuccess)success failure:(HttpFailure)failure;

@end
