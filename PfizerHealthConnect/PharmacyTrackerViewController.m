//
//  PharmacyTrackerViewController.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 21/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PharmacyTrackerViewController.h"
#import "MenuList.h"
#import "AppDelegate.h"
#import "PHRCurrentTableViewCell.h"
#import "PHRHistoricalTableViewCell.h"
#import "MBProgressHUD.h"
#import "Webservice.h"
#import "NSDictionary+Dictionary.h"
#import "AFNetworkReachabilityManager.h"

@interface PharmacyTrackerViewController ()
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property(nonatomic,strong) NSDictionary *selectedPharmacyInfoDict;
@property(nonatomic,assign)BOOL framesAreSet;
@end

@implementation PharmacyTrackerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.pharmacyList = [[NSMutableArray alloc] init];
    self.pharmacyTblView.backgroundColor = [UIColor clearColor];
    self.pharmacyTblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UINib *cellNib = [UINib nibWithNibName:@"PHRCurrentTableViewCell" bundle:nil];
    [self.pharmacyTblView registerNib:cellNib forCellReuseIdentifier:@"PHRCurrentTableViewCellIdentifier"];
    UINib *cellNib2 = [UINib nibWithNibName:@"PHRHistoricalTableViewCell" bundle:nil];
    [self.pharmacyTblView registerNib:cellNib2 forCellReuseIdentifier:@"PHRHistoricalTableViewCellIdentifier"];
    [self.searchTxtField addTarget:self
                            action:@selector(textFieldDidChange:)
                  forControlEvents:UIControlEventEditingChanged];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:NO];
    
    //offline
  //  [self addObserver:self forKeyPath:@"pharmacyList" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    //offline
    
}
-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}
- (void)filterContentForSearchText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"Brick contains[c] %@", searchText];
    self.searchArray  = [self.pharmacyList filteredArrayUsingPredicate:resultPredicate];
    //[self.view endEditing:YES];
    [self.pharmacyTblView reloadData];
}
-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.searchTxtField) {
        if ([textField.text isEqualToString:@""]){
            searchActive = NO;
            self.pharmacyList = self.tempArray;
            //[self.view endEditing:YES];
            [self.pharmacyTblView reloadData];
        }else{
            searchActive = YES;
            [self filterContentForSearchText:textField.text];
        }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.dateLbl.text = @"Date of Completion";
    //[MenuList networkChecking];
    self.pharmacyTblView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = appDelegate.isVerify?[appDelegate colorWithHexString:RamGreenColor]:[appDelegate colorWithHexString:BlueColor];
    isCurrentData = YES;
    self.pointsGainedLbl.hidden = YES;
    self.missionNewBtn.backgroundColor = appDelegate.isVerify?self.view.backgroundColor:[appDelegate colorWithHexString:YellowColor];
    self.verifyMissionBtn.backgroundColor = appDelegate.isVerify?[appDelegate colorWithHexString:YellowColor]:self.view.backgroundColor;
    self.currentBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.historicalBtn.backgroundColor = self.view.backgroundColor;
    searchActive = NO;
    self.searchTxtField.text = nil;
    [self showProgressView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self getMainTrackerDetails];
    });
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        appDelegate.isVerify?[self getVerifyMissionCurrentDetails]:[self getNewMissionCurrentDetails];
    });
}
-(void)showProgressView{
    //offline
    [self.mbProgressView hideAnimated:YES];
    //offline
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = @"Loading...";
}

//offline
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"pharmacyList"]) {
        AppDelegate *delG =  appDelegate;
        if (delG.isVerify) {
            delG.pharmacistVerifyMissionCurrentPharmacistList = change[@"new"];
        }else{
            delG.pharmacistNewMissionCurrentPharmacistList = change[@"new"];
        }
    }
}
//offline
-(void)getMainTrackerDetails{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",KBASE_URL,PHARMACY_TRACKER,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.mainTrackerData = (NSDictionary*)data;
            if (appDelegate.isVerify) {
                self.availableCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTrackerData objectForKey:@"VerifyMissionAvailableCount"]];
                self.takenCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTrackerData objectForKey:@"VerifyMissionTakenCount"]];
                self.submitCountLbl.text = [NSString stringWithFormat:@"/%@",[self.mainTrackerData objectForKey:@"VerifyMissionSubmitCount"]];
            }else{
                self.availableCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTrackerData objectForKey:@"NewMissionAvailableCount"]];
                self.takenCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTrackerData objectForKey:@"NewMissionTakenCount"]];
                self.submitCountLbl.text = [NSString stringWithFormat:@"/%@",[self.mainTrackerData objectForKey:@"NewMissionSubmitCount"]];
            }
            if ([self.delegate respondsToSelector:@selector(setPharmacyUserDetails:)]) {
                [self.delegate setPharmacyUserDetails:self.mainTrackerData];
            }
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:error.description preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}
-(void)getNewMissionCurrentDetails{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults]objectForKey:MOBILE_NUMBER_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,PHARMACY_CURRENT_PHARMACIES_NEW_MISSION,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        if (!self.mbProgressView.finished) {
            [self.mbProgressView hideAnimated:YES];
        }
        if (data) {
            self.pharmacyList = [NSMutableArray arrayWithArray:(NSArray*)data];
            self.tempArray = self.pharmacyList;
            //offline
            appDelegate.pharmacistNewMissionCurrentPharmacistList = self.pharmacyList;
            //offline
        }else{
            if ([self.pharmacyList count]) {
                [self.pharmacyList removeAllObjects];
            }
        }
        [self.pharmacyTblView reloadData];
        //offline
        if((appDelegate.pharmacyNewMissionArray && appDelegate.pharmacyNewMissionArray.count)|| (appDelegate.pharmacistNewMissionArray && appDelegate.pharmacistNewMissionArray.count)){
            [self sendSavedRecordsForPharmacy];
            
        }else if(appDelegate.finalNewPharmacyArray && appDelegate.finalNewPharmacyArray.count){
            [self sendFinalSubmitRecordsForPharmacy];
        }
    } failure:^(NSError *error) {
        //error
        if (!self.mbProgressView.finished) {
            [self.mbProgressView hideAnimated:YES];
        }
        //offline
        /*
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:error.description preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:alertAction];
         [self presentViewController:alert animated:YES completion:nil];
         */
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
            AppDelegate *delG =  appDelegate;
            self.pharmacyList = delG.pharmacistNewMissionCurrentPharmacistList;
            self.tempArray = self.pharmacyList;
            [self.pharmacyTblView reloadData];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        //offline
    }];
}
-(void)getVerifyMissionCurrentDetails{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults]objectForKey:MOBILE_NUMBER_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,PHARMACY_CURRENT_VERIFY,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        if (!self.mbProgressView.finished) {
            [self.mbProgressView hideAnimated:YES];
        }
        if (data) {
            self.pharmacyList = [NSMutableArray arrayWithArray:(NSArray*)data];
            self.tempArray = self.pharmacyList;
            //offline
            appDelegate.pharmacistVerifyMissionCurrentPharmacistList = self.pharmacyList;
            //offlone
        }else{
            if ([self.pharmacyList count]) {
                [self.pharmacyList removeAllObjects];
            }
        }
        [self.pharmacyTblView reloadData];
        //offline
        if((appDelegate.pharmacyVerifyNewMissionArray && appDelegate.pharmacyVerifyNewMissionArray.count)|| (appDelegate.pharmacistverifyMissionArray && appDelegate.pharmacistverifyMissionArray.count) ){
            [self sendSavedRecordsForPharmacyVerify];
        }
        //offine
    } failure:^(NSError *error) {
        //error
        if (!self.mbProgressView.finished) {
            [self.mbProgressView hideAnimated:YES];
        }
        //offline
        /*
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:error.description preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:alertAction];
         [self presentViewController:alert animated:YES completion:nil];
         */
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
            AppDelegate *delG =  appDelegate;
            self.pharmacyList = delG.pharmacistVerifyMissionCurrentPharmacistList;
            self.tempArray = self.pharmacyList;
            [self.pharmacyTblView reloadData];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        //offline
    }];
}
-(void)getNewMissionHistoricalDetials{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,PHARMACY_HISTORICAL_NEW_MISSION,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.pharmacyList = [NSMutableArray arrayWithArray:(NSArray*)data];
            self.tempArray = self.pharmacyList;
        }else{
            if ([self.pharmacyList count]) {
                [self.pharmacyList removeAllObjects];
            }
        }
        [self.pharmacyTblView reloadData];
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:error.description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
-(void)getVerifyMissionHistoricalDetials{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,PHARMACY_HISTORICAL_VERIFY,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        if (!self.mbProgressView.finished){
            [self.mbProgressView hideAnimated:YES];
        }
        if (data) {
            self.pharmacyList = [NSMutableArray arrayWithArray:(NSArray*)data];
            self.tempArray = self.pharmacyList;
        }else{
            if ([self.pharmacyList count]) {
                [self.pharmacyList removeAllObjects];
            }
        }
        [self.pharmacyTblView reloadData];
    } failure:^(NSError *error) {
        //error
        if (!self.mbProgressView.finished){
            [self.mbProgressView hideAnimated:YES];
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:error.description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

//offline
-(void)sendFinalSubmitRecordsForPharmacy{
    NSString *strPharmacyID = appDelegate.finalNewPharmacyArray[0];
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = PHARMACY_FINAL_SUBMIT_NEW_MISSION;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,baseURLString,mobileNumber,strPharmacyID];
    [Webservice getWithUrlString:urlString success:^(id data) {
        if (data) {
            NSLog(@"Final Submit sent");
            if(appDelegate.finalNewPharmacyArray.count){
                [appDelegate.finalNewPharmacyArray removeObjectAtIndex:0];
            }
            
            if(appDelegate.finalNewPharmacyArray.count){
                [self sendFinalSubmitRecordsForPharmacy];
            }else{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [self getMainTrackerDetails];
                });
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    appDelegate.isVerify?[self getVerifyMissionCurrentDetails]:[self getNewMissionCurrentDetails];
                });
            }

        }else{
           
        }
    } failure:^(NSError *error) {
       
    }];
}

-(void)sendFinalSubmitVerifyRecordsForPharmacy{
    NSString *strPharmacyID = appDelegate.finalVerifyPharmacyArray[0];
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = PHARMACY_FINAL_SUBMIT_NEW_MISSION;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,baseURLString,mobileNumber,strPharmacyID];
    [Webservice getWithUrlString:urlString success:^(id data) {
        if (data) {
            NSLog(@"verify Final Submit sent : %@",data);
            if(appDelegate.finalVerifyPharmacyArray.count){
                [appDelegate.finalVerifyPharmacyArray removeObjectAtIndex:0];
            }
            
            if(appDelegate.finalVerifyPharmacyArray.count){
                [self sendFinalSubmitVerifyRecordsForPharmacy];
            }else{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    [self getMainTrackerDetails];
                });
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    appDelegate.isVerify?[self getVerifyMissionCurrentDetails]:[self getNewMissionCurrentDetails];
                });
            }
            
        }else{
            
        }
    } failure:^(NSError *error) {
        
    }];
}
-(void)sendSavedRecordsForPharmacy{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showProgressView];
    });
    
    AppDelegate *delG =  appDelegate;
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = PHARMACY_ADD_PHARMACY_NEW_MISSION;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
    if(delG.pharmacyNewMissionArray.count){
        NSDictionary *dicPharmacistData = delG.pharmacyNewMissionArray[0];
        [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
            //Success
            [self.mbProgressView hideAnimated:YES];
            if (data) {
                if(delG.pharmacyNewMissionArray.count){
                    [delG.pharmacyNewMissionArray removeObjectAtIndex:0];
                }
                NSLog(@"pharmacy Record Sent");
                //send next if any
                if(delG.pharmacyNewMissionArray.count){
                    [self sendSavedRecordsForPharmacy];
                }else if(delG.pharmacyNewMissionArray && delG.pharmacyNewMissionArray.count){
                        [self sendSavedRecordsForPharmacists];
                }else if(appDelegate.finalNewPharmacyArray && appDelegate.finalNewPharmacyArray.count){
                    [self sendFinalSubmitRecordsForPharmacy];
                }else{
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        [self getMainTrackerDetails];
                    });
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        appDelegate.isVerify?[self getVerifyMissionCurrentDetails]:[self getNewMissionCurrentDetails];
                    });

                }
                
                //[self sendSavedRecordsForPhysiciansconsents:data];
            }else{//pharmacy done start with pharmacists
                
            }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            /*if(delG.physicianNewMissionArray.count)
             [delG.physicianNewMissionArray removeObjectAtIndex:0];
             if(!([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound)){
             if(delG.pharmacyNewMissionArray.count){
             [self sendSavedRecordsForPharmacy];
             }else{//pharmacy done start with pharmacists
             
             }
             }
             */
        }];
    }else if(delG.pharmacistNewMissionArray.count){
        [self sendSavedRecordsForPharmacists];
    }
    
}

-(void)sendSavedRecordsForPharmacists{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showProgressView];
    });
    
    AppDelegate *delG =  appDelegate;
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = PHARMACY_ADD_PHARMACIST_NEW_MISSION;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
    NSDictionary *dicPharmacistData = delG.pharmacistNewMissionArray[0];
    [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            if(delG.pharmacistNewMissionArray.count)
                [delG.pharmacistNewMissionArray removeObjectAtIndex:0];
            NSLog(@"Pharmacist Record Sent");
            [self sendSavedRecordsForPharmacistconsents:/*[(NSArray*)data lastObject]*/data];
        }else{
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        /*if(delG.pharmacistNewMissionArray.count)
         [delG.pharmacistNewMissionArray removeObjectAtIndex:0];
         */
        
    }];
    
}

-(void)sendSavedRecordsForPharmacistconsents:(NSDictionary *)data{
    AppDelegate *delG =  appDelegate;
    if([delG.pharmacistNewMissionConsentArray[0] isKindOfClass:[NSNumber class]]){
        if(delG.pharmacistNewMissionArray.count){
            [self sendSavedRecordsForPharmacists];
        }else if(appDelegate.finalNewPharmacyArray && appDelegate.finalNewPharmacyArray.count){
            [self sendFinalSubmitRecordsForPharmacy];
        }else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [self getMainTrackerDetails];
            });
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                appDelegate.isVerify?[self getVerifyMissionCurrentDetails]:[self getNewMissionCurrentDetails];
            });

        }
        return;
    }
    NSMutableDictionary *dicSavedPhysicianConsentData = delG.pharmacistNewMissionConsentArray[0];
    dicSavedPhysicianConsentData[@"PharmacistTransactionID"] = data[@"PharmacistTransactionID"];
    
    NSString *mobNum = [[NSUserDefaults standardUserDefaults]stringForKey:MOBILE_NUMBER_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",
                           KBASE_URL,@"pharmacist/consent/",mobNum];
    NSDictionary *dicConsentData = dicSavedPhysicianConsentData;
    [Webservice postWithURlString:urlString withParameters:dicConsentData sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            NSLog(@"Pharmacist Consent Sent");
            if(delG.pharmacistNewMissionConsentArray.count)
                [delG.pharmacistNewMissionConsentArray removeObjectAtIndex:0];
            
            if(delG.pharmacistNewMissionArray.count){
                [self sendSavedRecordsForPharmacists];
            }else if(appDelegate.finalNewPharmacyArray && appDelegate.finalNewPharmacyArray.count){
                [self sendFinalSubmitRecordsForPharmacy];
            }
            else{
                [self.mbProgressView hideAnimated:YES];
                
                //in the last consent, reload table
                [self getNewMissionCurrentDetails];
                //now start the process for verify records, if any
            }
            
        }else{
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        if(delG.physicianNewMissionConsentArray.count)
            [delG.physicianNewMissionConsentArray removeObjectAtIndex:0];
        
    }];
}


//Verify...
-(void)sendSavedRecordsForPharmacyVerify{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showProgressView];
    });
    
    AppDelegate *delG =  appDelegate;
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = PHARMACY_ADD_PHARMACY_VERIFY;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
    if(delG.pharmacyVerifyNewMissionArray.count){
        NSDictionary *dicPharmacistData = delG.pharmacyVerifyNewMissionArray[0];
        [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
            //Success
            [self.mbProgressView hideAnimated:YES];
            if (data) {
                if(delG.pharmacyVerifyNewMissionArray.count){
                    [delG.pharmacyVerifyNewMissionArray removeObjectAtIndex:0];
                }
                NSLog(@"pharmacy verify Record Sent");
                //send next if any
                if(delG.pharmacyVerifyNewMissionArray.count){
                    [self sendSavedRecordsForPharmacyVerify];
                }else if(delG.pharmacistverifyMissionArray && delG.pharmacistverifyMissionArray.count){
                    [ self sendSavedRecordsForPharmacistsVerify];
                }else if(delG.finalVerifyPharmacyArray && delG.finalVerifyPharmacyArray.count){
                    [self sendFinalSubmitVerifyRecordsForPharmacy];
                }
                
            }else{//pharmacy done start with pharmacists
                
            }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            /*if(delG.physicianNewMissionArray.count)
             [delG.physicianNewMissionArray removeObjectAtIndex:0];
             if(!([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound)){
             if(delG.pharmacyNewMissionArray.count){
             [self sendSavedRecordsForPharmacy];
             }else{//pharmacy done start with pharmacists
             
             }
             }
             */
        }];
    }else if(delG.pharmacistverifyMissionArray &&  delG.pharmacistverifyMissionArray.count){
        [ self sendSavedRecordsForPharmacistsVerify];
    }else if(delG.finalVerifyPharmacyArray && delG.finalVerifyPharmacyArray.count){
        [self sendFinalSubmitVerifyRecordsForPharmacy];
    }
    
}

-(void)sendSavedRecordsForPharmacistsVerify{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showProgressView];
    });
    
    AppDelegate *delG =  appDelegate;
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = PHARMACY_ADD_PHARMACIST_VERIFY_MISSION;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
    NSDictionary *dicPharmacistData = delG.pharmacistverifyMissionArray[0];
    [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            if(delG.pharmacistverifyMissionArray.count)
                [delG.pharmacistverifyMissionArray removeObjectAtIndex:0];
            NSLog(@"Pharmacist verify Record Sent");
            if(delG.pharmacistverifyMissionArray.count){
                [self sendSavedRecordsForPharmacistsVerify];
            }else if(delG.finalVerifyPharmacyArray && delG.finalVerifyPharmacyArray.count){
                [self sendFinalSubmitVerifyRecordsForPharmacy];
            }
        }else{
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        /*if(delG.pharmacistNewMissionArray.count)
         [delG.pharmacistNewMissionArray removeObjectAtIndex:0];
         */
        
    }];
    
}
//offline

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!self.framesAreSet) {
        self.framesAreSet = YES;
    }else{
        CGRect tmpFrame = self.scoreView.frame;
        self.scoreView.layer.cornerRadius = tmpFrame.size.height/2;
    }
}
- (IBAction)newMissionBtnAction:(id)sender {
    [self showProgressView];
    appDelegate.isVerify = NO;
    isCurrentData = YES;
    searchActive = NO;
    self.searchTxtField.text = nil;
    self.pointsGainedLbl.hidden = YES;
    self.view.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    self.missionNewBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.verifyMissionBtn.backgroundColor = self.view.backgroundColor;
    self.currentBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.historicalBtn.backgroundColor = self.view.backgroundColor;
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (![AFStringFromNetworkReachabilityStatus(status) isEqualToString:@"Not Reachable"]) {
            self.availableCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTrackerData objectForKey:@"NewMissionAvailableCount"]];
            self.takenCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTrackerData objectForKey:@"NewMissionTakenCount"]];
            self.submitCountLbl.text = [NSString stringWithFormat:@"/%@",[self.mainTrackerData objectForKey:@"NewMissionSubmitCount"]];
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self getNewMissionCurrentDetails];
    });
}
- (IBAction)verifyMissionBtnAction:(id)sender {
    [self showProgressView];
    appDelegate.isVerify = YES;
    isCurrentData = YES;
    searchActive = NO;
    self.searchTxtField.text = nil;
    self.pointsGainedLbl.hidden = YES;
    self.view.backgroundColor = [appDelegate colorWithHexString:RamGreenColor];
    self.verifyMissionBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.missionNewBtn.backgroundColor = self.view.backgroundColor;
    self.currentBtn.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.historicalBtn.backgroundColor = self.view.backgroundColor;
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (![AFStringFromNetworkReachabilityStatus(status) isEqualToString:@"Not Reachable"]) {
            self.availableCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTrackerData objectForKey:@"VerifyMissionAvailableCount"]];
            self.takenCountLbl.text = [NSString stringWithFormat:@"%@",[self.mainTrackerData objectForKey:@"VerifyMissionTakenCount"]];
            self.submitCountLbl.text = [NSString stringWithFormat:@"/%@",[self.mainTrackerData objectForKey:@"VerifyMissionSubmitCount"]];
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self getVerifyMissionCurrentDetails];
    });
}
- (IBAction)addMissionBtnAction:(id)sender {
    [self.view endEditing:YES];
    if ([self.delegate respondsToSelector:@selector(addPharmacyMissionAction:)]) {
        [self.delegate addPharmacyMissionAction:appDelegate.isVerify];
    }
}
- (IBAction)currentBtnAction:(id)sender {
    [self.view endEditing:YES];
    self.dateLbl.text = @"Date of Completion";
    isCurrentData = YES;
    self.pointsGainedLbl.hidden = YES;
    self.currentBtn.backgroundColor = [appDelegate colorWithHexString:@"FFCF32"];
    self.historicalBtn.backgroundColor = self.view.backgroundColor;
    [self showProgressView];
    if (appDelegate.isVerify) {
        [self getVerifyMissionCurrentDetails];
    }else{
        [self getNewMissionCurrentDetails];
    }
    
}
- (IBAction)historicalBtnAction:(id)sender {
    [self.view endEditing:YES];
    self.dateLbl.text = @"Date of Submission";
    isCurrentData = NO;
    self.pointsGainedLbl.hidden = NO;
    self.historicalBtn.backgroundColor = [appDelegate colorWithHexString:@"FFCF32"];
    self.currentBtn.backgroundColor = self.view.backgroundColor;
    [self showProgressView];
    if (appDelegate.isVerify) {
        [self getVerifyMissionHistoricalDetials];
    }else{
        [self getNewMissionHistoricalDetials];
    }
}

#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchActive) {
        return self.searchArray.count;
    }else{
        return self.pharmacyList.count;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier =@"PHRCurrentTableViewCellIdentifier";
    static NSString *cellIdentifier2 = @"PHRHistoricalTableViewCellIdentifier";
    if (isCurrentData == YES) {
        PHRCurrentTableViewCell *cell =
        (PHRCurrentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.contentView.backgroundColor = self.view.backgroundColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary *dict = [[NSDictionary alloc] init];
        if (searchActive) {
            dict = [self.searchArray objectAtIndex:indexPath.row];
        }else{
            dict = [self.pharmacyList objectAtIndex:indexPath.row];
        }
        int numberOfPharmacists = [[dict stringForKey:@"NoOfPharmacists"] intValue];
        cell.pharmacyNameLbl.text = [dict objectForKey:@"PharmacyName"];
        cell.addressLbl.text = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Address1"],[dict objectForKey:@"Address2"]];
        NSUInteger pharmacyMaximumValue = [[dict valueForKey:@"TotalFields"] integerValue];
        NSUInteger pharmacySubmittedValue = [[dict valueForKey:@"SubmittedFields"] integerValue];
        NSString *completionDateString = [dict objectForKey:@"CompletionDate"];
        NSInteger completionDays = [self getNumberOfDaysFromDate:completionDateString];
        if (completionDays == 0) {
            cell.userInteractionEnabled = NO;
        }else {
            cell.userInteractionEnabled = YES;
        }
        cell.pharmacistDetailsBtn.alpha = numberOfPharmacists > 0?1.0:0.5;
        cell.pharmacistDetailsBtn.userInteractionEnabled = numberOfPharmacists > 0?YES:NO;
        cell.pharmacyDetailsBtn.tag = indexPath.row;
        cell.pharmacistDetailsBtn.tag = indexPath.row;
        [cell.pharmacyDetailsBtn addTarget:self action:@selector(pharmacyDetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.pharmacistDetailsBtn addTarget:self action:@selector(pharmacistDetailsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        float totalAddedPharmacistProgressFloat = (float)[[dict objectForKey:@"PharmacistTicketMaster"] count];
        float noofPharmacistFloat = (float)[[dict valueForKey:@"NoOfPharmacists"] integerValue];
        cell.pharmacyProgressView.progress = (float)pharmacySubmittedValue/pharmacyMaximumValue;
        if (!totalAddedPharmacistProgressFloat) {
            cell.pharmacistProgressView.progress = 0;
        }else{
            cell.pharmacistProgressView.progress = (float)totalAddedPharmacistProgressFloat/noofPharmacistFloat;
        }
        cell.completionDateView.maxValue=22;
        cell.completionDateView.percentLabel.font = [UIFont systemFontOfSize:15];
        cell.completionDateView.backgroundColor = self.view.backgroundColor;
        [cell.completionDateView startAnimation];
        if (completionDays >= 0 && completionDays <= 6 ) {
            [cell.completionDateView drawCircleWithPercent:((float)100/22)*completionDays
                                                  duration:0.5
                                                 lineWidth:2
                                                 clockwise:YES
                                                   lineCap:kCALineCapRound
                                                 fillColor:[UIColor clearColor]
                                               strokeColor:[UIColor redColor]
                                            animatedColors:nil];
            
        }
        else if (completionDays >6 && completionDays <=11){
            [cell.completionDateView drawCircleWithPercent:((float)100/22)*completionDays
                                                  duration:0.5
                                                 lineWidth:2
                                                 clockwise:YES
                                                   lineCap:kCALineCapRound
                                                 fillColor:[UIColor clearColor]
                                               strokeColor:[UIColor orangeColor]
                                            animatedColors:nil];
        }
        else if (completionDays >11 && completionDays <= 22){
            [cell.completionDateView drawCircleWithPercent:((float)100/22)*completionDays
                                                  duration:0.5
                                                 lineWidth:2
                                                 clockwise:YES
                                                   lineCap:kCALineCapRound
                                                 fillColor:[UIColor clearColor]
                                               strokeColor:[UIColor whiteColor]
                                            animatedColors:nil];
            
        }
        return cell;
    }else if (isCurrentData == NO){
        PHRHistoricalTableViewCell *cell2 =
        (PHRHistoricalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
        cell2.userInteractionEnabled = NO;
        cell2.contentView.backgroundColor = self.view.backgroundColor;
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary *dict = [[NSDictionary alloc] init];
        if (searchActive) {
            dict = [self.searchArray objectAtIndex:indexPath.row];
        }else{
            dict = [self.pharmacyList objectAtIndex:indexPath.row];
        }
        cell2.pharmacyNameLbl.text = [dict objectForKey:@"PharmacyName"];
        cell2.addressLbl.text = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Address1"],[dict objectForKey:@"Address2"]];
        NSInteger points = [[dict objectForKey:@"Points"] integerValue];
        if (points == 0) {
            cell2.pointsLbl.text = [NSString stringWithFormat:@"%ld points",(long)points];
            cell2.pointsLbl.textColor = [UIColor whiteColor];
        }else {
            cell2.pointsLbl.text = [NSString stringWithFormat:@"%ld points",(long)points];
            cell2.pointsLbl.textColor = [UIColor whiteColor];
        }
        NSUInteger pharmacyMaximumValue = [[dict valueForKey:@"TotalFields"] integerValue];
        NSUInteger pharmacySubmittedValue = [[dict valueForKey:@"SubmittedFields"] integerValue];
        float totalAddedPharmacistProgressFloat = (float)[[dict objectForKey:@"PharmacistTicketMaster"] count];
        float noofPharmacistFloat = (float)[[dict valueForKey:@"NoOfPharmacists"] integerValue];
        cell2.pharmacyProgressBar.progress = (float)pharmacySubmittedValue/pharmacyMaximumValue;
        if (!totalAddedPharmacistProgressFloat) {
            cell2.pharmacistProgressBar.progress = 0;
        }else{
            cell2.pharmacistProgressBar.progress = (float)totalAddedPharmacistProgressFloat/noofPharmacistFloat;
        }
        NSString *missiontakenDateString = [dict objectForKey:@"CompletionDate"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:DATE_TIME_YEAR_FORMATTER_STRING];
        NSDate *missiontakenDate = [formatter dateFromString:missiontakenDateString];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday;
        NSDateComponents *components = [calendar components:units fromDate:missiontakenDate];
        NSInteger year = [components year];
        NSInteger day = [components day];
        NSInteger month = [components month];
        NSDateFormatter *weekDay = [[NSDateFormatter alloc] init];
        [weekDay setDateFormat:@"EEE"];
        NSDateFormatter *calMonth = [[NSDateFormatter alloc] init];
        [calMonth setDateFormat:@"MMMM"];
        cell2.backgroundColor = [appDelegate colorWithHexString:BlueColor];
        cell2.completionDateLbl.text = [NSString stringWithFormat:@"%ld/%ld/%ld",(long)month,(long)day,(long)year];
        return cell2;
    }
    return nil;
}
-(void)pharmacyDetailsButtonClicked:(id)sender{
    self.selectedPharmacyInfoDict = searchActive?[self.searchArray objectAtIndex:((UIButton*)sender).tag]:[self.pharmacyList objectAtIndex:((UIButton*)sender).tag];
    if ([self.delegate respondsToSelector:@selector(showPharmacyDetails:)]) {
        [self.delegate showPharmacyDetails:[NSMutableDictionary dictionaryWithDictionary:self.selectedPharmacyInfoDict]];
    }
}
-(void)pharmacistDetailsButtonClicked:(id)sender{
    self.selectedPharmacyInfoDict = searchActive?[self.searchArray objectAtIndex:((UIButton*)sender).tag]:[self.pharmacyList objectAtIndex:((UIButton*)sender).tag];
    if ([self.delegate respondsToSelector:@selector(showPharmacistDetails:)]) {
        [self.delegate showPharmacistDetails:[NSMutableDictionary dictionaryWithDictionary:self.selectedPharmacyInfoDict]];
    }
}

- (NSInteger)getNumberOfDaysFromDate:(NSString *)completionDateString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_TIME_YEAR_FORMATTER_STRING];
    NSDate *completionDate = [dateFormatter dateFromString:completionDateString];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:[NSDate date]
                                                          toDate:completionDate
                                                         options:NSCalendarWrapComponents];
    return components.day;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
