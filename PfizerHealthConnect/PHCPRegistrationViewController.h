//
//  RegistrationViewController.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 25/10/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SinchVerification/SinchVerification.h>
#import "VerificationViewController.h"
#import "AppDelegate.h"

@protocol SINVerification,VerificationDelegate;
@class SINUITextFieldPhoneNumberFormatter;


@interface PHCPRegistrationViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,VerificationDelegate>{
    BOOL recheck;
}
@property (nonatomic, assign) BOOL showPopUp;
@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (strong, nonatomic) SINUITextFieldPhoneNumberFormatter *formatter;
@property (strong, nonatomic) id<SINVerification> verification;
@property (weak, nonatomic) IBOutlet UITextField *countryTextField,*firstNameTextField,*lastNameTextField,*mobileTextField,*emailTextField,*cityTextField,*photoTextField,*countryCodeTextField,*activeField;
@property (strong,nonatomic) UIImage *cameraImage;
@property (nonatomic,strong) NSArray *countryNamesArray;
@property(nonatomic)CGSize initialContentSize;
@property (weak, nonatomic) IBOutlet UIButton *signMeUpBtn;

- (IBAction)SignMeUp:(id)sender;
- (IBAction)cameraButtonPressed:(id)sender;
- (IBAction)clearButtonAction:(UIBarButtonItem *)sender;
- (void)showVerificationPopUp;
@end
