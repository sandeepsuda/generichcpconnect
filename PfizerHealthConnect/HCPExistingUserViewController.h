//
//  HCPExistingUserViewController.h
//  PfizerHealthConnect
//
//  Created by RajaSekhar on 06/01/17.
//  Copyright © 2017 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "Webservice.h"
#import "MenuList.h"
#import "AppDelegate.h"

@protocol ExistingUserProtocol <NSObject>
-(void)navigatingToPageVC:(NSDictionary *)existingUserDict;
@end


@interface HCPExistingUserViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong,nonatomic) UIImage *cameraImage;
@property (nonatomic, assign) BOOL SingleSignOnCheck;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (weak, nonatomic) id<ExistingUserProtocol> delegate;
@property (nonatomic,weak) IBOutlet UIView *subView;
@property (nonatomic,weak) IBOutlet UITextField *userNameTextField,*passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *pictureTextField;
@property (weak, nonatomic) IBOutlet UIImageView *cameraIcon;
@property (nonatomic,weak) IBOutlet UIButton *loginButton;
-(IBAction)loginButtonAction:(id)sender;
-(IBAction)cancelButtonAction:(id)sender;
@end
