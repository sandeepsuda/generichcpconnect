//
//  AppUpdater.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 22/12/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "AppUpdater.h"
#import "AFNetworkReachabilityManager.h"
#import "MenuList.h"
#import "AppDelegate.h"

@implementation AppUpdater

#pragma mark - Init


+ (id)sharedUpdater
{
    static AppUpdater *sharedUpdater;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUpdater = [[AppUpdater alloc] init];
    });
    return sharedUpdater;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.alertTitle = @"New Version";
        self.alertMessage = @"Version %@ is available.";
        self.alertUpdateButtonTitle = @"Update";
        self.alertCancelButtonTitle = @"Not Now";
    }
    return self;
}

#pragma mark - Instance Methods


- (void)showUpdateWithForce
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (![AFStringFromNetworkReachabilityStatus(status) isEqualToString:@"Not Reachable"]) {
            [self checkNewAppVersion:^(BOOL newVersion, NSString *version) {
                if (newVersion) {
                    [self alertUpdateForVersion:version withForce:YES];
                }
            }];
        }else{
            return;
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}
- (void)showUpdateWithConfirmation
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (![AFStringFromNetworkReachabilityStatus(status) isEqualToString:@"Not Reachable"]) {
            [self checkNewAppVersion:^(BOOL newVersion, NSString *version) {
                if (newVersion) {
                    [self alertUpdateForVersion:version withForce:NO];
                }
            }];
        }else{
            return;
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

- (void)forceOpenNewAppVersion:(BOOL)force
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (![AFStringFromNetworkReachabilityStatus(status) isEqualToString:@"Not Reachable"]) {
            [self checkNewAppVersion:^(BOOL newVersion, NSString *version) {
                if (newVersion) {
                    [self alertUpdateForVersion:version withForce:force];
                }
            }];
        }else{
            return;
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

NSString *appStoreURL = nil;

- (void)checkNewAppVersion:(void(^)(BOOL newVersion, NSString *version))completion
{
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    NSString *bundleID = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleIdentifierKey];
    
    NSURL *lookupURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://pollenapps.com/IMAD/api/AppsAPI/CanUpgrade/?bundleID=%@&version=%@", bundleID, version]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void) {
        NSData *lookupResults = [NSData dataWithContentsOfURL:lookupURL];
        if (!lookupResults) {
            completion(NO, nil);
            return;
        }
        NSDictionary *jsonResults = [NSJSONSerialization JSONObjectWithData:lookupResults options:0 error:nil];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSNumber *canUpdate = [jsonResults objectForKey:@"CanUpgrade"];
            if ([canUpdate boolValue])
            {
                NSString *updateVersion = [[jsonResults objectForKey:@"AppVersion"] copy];
                appStoreURL = [[jsonResults objectForKey:@"URL"] copy];
                completion(YES, updateVersion);
                
            }else{
                completion(NO, nil);
            }
        });
    });
}

- (void)alertUpdateForVersion:(NSString *)version withForce:(BOOL)force
{
    NSString *msg = [NSString stringWithFormat:self.alertMessage, version];
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:self.alertTitle message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:force ? self.alertUpdateButtonTitle:self.alertCancelButtonTitle style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:force ? nil:self.alertUpdateButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        NSURL *appUrl = [NSURL URLWithString:appStoreURL];
        if ([[UIApplication sharedApplication] canOpenURL:appUrl]) {
            [[UIApplication sharedApplication] openURL:appUrl];
        } else {
            UIAlertView *cantOpenUrlAlert = [[UIAlertView alloc] initWithTitle:@"Not Available" message:@"Could not open the Store, please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [cantOpenUrlAlert show];
        }
    }];
    [alertView addAction:alertActionCancel];
    [alertView addAction:alertAction];
    [appDelegate.window.rootViewController presentViewController:alertView animated:YES completion:nil];
}

@end
