//
//  ViewController.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 25/10/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PHCPHelloScreenViewController.h"
#import "AppDelegate.h"
#import "PHCPRegistrationViewController.h"
#import "LeftViewController.h"
#import "MenuList.h"
#import "TrainingPageViewController.h"
#import "WelcomePageViewController.h"

@interface PHCPHelloScreenViewController ()
@end

@implementation PHCPHelloScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"HCP Connect";
    self.navigationController.navigationBar.barTintColor = [appDelegate colorWithHexString:@"3A4F5A"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20]}];
    self.btnRegister.layer.cornerRadius = 4.0;
    self.btnSignin.layer.cornerRadius = 4.0;
    self.helloLbl = [[UILabel alloc]init];
    self.helloLbl.translatesAutoresizingMaskIntoConstraints = NO;
    self.helloLbl.text = @"Hello!";
    self.helloLbl.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:70.0];
    self.helloLbl.textColor = [appDelegate colorWithHexString:@"3A4F5A"];
    [self.contentView addSubview:self.helloLbl];

    self.welcomeTxtLbl = [[UILabel alloc]init];
    self.welcomeTxtLbl.translatesAutoresizingMaskIntoConstraints = NO;
    self.welcomeTxtLbl.textAlignment = NSTextAlignmentCenter;
    self.welcomeTxtLbl.text = @"Welcome to HCPConnect.\nPlease tap the register button below to get started.";
    self.welcomeTxtLbl.textColor = [appDelegate colorWithHexString:@"3A4F5A"];
    self.welcomeTxtLbl.numberOfLines = 0;
    self.welcomeTxtLbl.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
    [self.contentView addSubview:self.welcomeTxtLbl];
    [self setUpConstraints];
}
-(void)setUpConstraints{
    [self.contentView addConstraints:@[
                                       [NSLayoutConstraint constraintWithItem:self.helloLbl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0f constant:5.0],
                                       [NSLayoutConstraint constraintWithItem:self.helloLbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f],
                                       [NSLayoutConstraint constraintWithItem:self.helloLbl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.welcomeTxtLbl attribute:NSLayoutAttributeTop multiplier:1.0f constant:-5.0],
                                       [NSLayoutConstraint constraintWithItem:self.welcomeTxtLbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f],
                                       ]];
}

#pragma mark - Protocol Methods

-(void)navigatingToPageVC{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LeftViewController *tracker =[storyboard instantiateViewControllerWithIdentifier:@"LeftViewController"];
    [self.navigationController pushViewController:tracker animated:YES];
}

-(void)navigatingToPageVC:(NSDictionary *)existingUserDict{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if(_SingleSignOnCheck){
        LeftViewController *tracker =[storyboard instantiateViewControllerWithIdentifier:@"LeftViewController"];
        [self.navigationController pushViewController:tracker animated:YES];
    }else{
        if([existingUserDict[@"Message"] isEqualToString:@"SUCCESS"] || [existingUserDict[@"Message"] isEqualToString:@"OTPPAGE"]){
            [self showVerificationPopUp];
        }
        if([existingUserDict[@"Message"] isEqualToString:@"QUIZPAGE"]){
            TrainingPageViewController *trainingPageVC = [storyboard instantiateViewControllerWithIdentifier:@"TrainingPageViewController"];
            [self.navigationController pushViewController:trainingPageVC animated:YES];
        }
        if([existingUserDict[@"Message"] isEqualToString:@"MYTRACKERPAGE"]){
            LeftViewController *tracker =[storyboard instantiateViewControllerWithIdentifier:@"LeftViewController"];
            [self.navigationController pushViewController:tracker animated:YES];
        }
        if ([existingUserDict[@"Message"] isEqualToString:@"WELCOMEPAGE"]){
            WelcomePageViewController *landingPageVC = [storyboard instantiateViewControllerWithIdentifier:@"WelcomePageViewController"];
            [self.navigationController pushViewController:landingPageVC animated:YES];
        }
    }
}

-(void)showVerificationPopUp{
    VerificationViewController *verificationVC = [[VerificationViewController alloc] initWithNibName:@"VerificationView" bundle:nil];
    verificationVC.delegate = self;
    verificationVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:verificationVC animated:YES completion:nil];
}

#pragma mark - UIButton Action Methods

- (IBAction)registerBtnAction:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PHCPRegistrationViewController *registrationVC = [storyBoard instantiateViewControllerWithIdentifier:@"PHCPRegistrationViewController"];
    [self.navigationController pushViewController:registrationVC animated:YES];
}

-(IBAction)signInCheckingButtonAction:(UIButton *)sender{
    if (sender.selected) {
        _SingleSignOnCheck = NO;
        [self.checkButton setSelected:NO];
        [self.checkButton setImage:[UIImage imageNamed:@"ExistingUser"] forState: UIControlStateNormal];
    }else{
        _SingleSignOnCheck = YES;
        [self.checkButton setSelected:YES];
        [self.checkButton setImage:[UIImage imageNamed:@"ExistingUserCheck"] forState:UIControlStateSelected];
    }
}

-(IBAction)existingUserButtonAction:(id)sender{
    HCPExistingUserViewController *existingUserVC = [[HCPExistingUserViewController alloc] initWithNibName:@"HCPExistingUserViewController" bundle:nil];
    existingUserVC.SingleSignOnCheck = _SingleSignOnCheck;
    existingUserVC.delegate = self;
    existingUserVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:existingUserVC animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
