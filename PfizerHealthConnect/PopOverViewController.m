//
//  PopOverViewController.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 26/10/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PopOverViewController.h"

@interface PopOverViewController ()

@end

@implementation PopOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    // self.view.bounds = CGRectMake(0, 0, 300, self.arrOptions.count * 44 +10);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrOptions.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if(!self.txtFieldToBeSet){
        cell.textLabel.text = [self.arrOptions[indexPath.row] objectForKey:@"CountryName"];
    }else {
        cell.textLabel.text = self.arrOptions[indexPath.row];
    }
    return cell;
}
- (void)viewDidLayoutSubviews {
    self.preferredContentSize = CGSizeMake(320, self.tableView.contentSize.height);
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!self.txtFieldToBeSet){
        self.countryField.text = [self.arrOptions[indexPath.row] objectForKey:@"CountryName"];
        self.countryCodeField.text = [self.arrOptions[indexPath.row] objectForKey:@"CountryDialingCode"];
    }
    
    if(self.txtFieldToBeSet){
        self.txtFieldToBeSet.text = self.arrOptions[indexPath.row];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
