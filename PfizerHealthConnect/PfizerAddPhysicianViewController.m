//
//  PfizerAddPhysicianViewController.m
//  PfizerHealthConnect
//
//  Created by RajaSekhar on 12/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PfizerAddPhysicianViewController.h"
#import "MenuList.h"
#import "NSDictionary+Dictionary.h"
#import "PopOverViewController.h"
#import <SDWebImage/SDImageCache.h>
#import "UIImageView+WebCache.h"
#import "Webservice.h"
#import "OptionsViewController.h"
#import "LeftViewController.h"
#import "AppDelegate.h"
#import "AFNetworkReachabilityManager.h"
@interface PfizerAddPhysicianViewController (){
    NSArray *listOptions;
}
@property (nonatomic,strong) LeftViewController *leftVC;
@end

@implementation PfizerAddPhysicianViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[MenuList networkChecking];
    
    listOptions = [[NSArray alloc]init];
    [self.hospitalNameTextField addTarget:self
                            action:@selector(textFieldDidChange:)
                  forControlEvents:UIControlEventEditingChanged];
    self.hosListView.hidden =YES;
    self.hosListView.layer.cornerRadius = 4.0;
    self.leftVC = (LeftViewController *)self.delegate;
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.scrollView.frame.size.height+ 1000);
    geocoder = [[CLGeocoder alloc] init];
    locationManager = [[CLLocationManager alloc]init];
    self.selectedLocationDict = [[NSMutableDictionary alloc] init];
    self.finalLocationDict = [[NSMutableDictionary alloc] init];
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestAlwaysAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    UILongPressGestureRecognizer *mapViewGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longpressToGetLocation:)];
    [self.mapView addGestureRecognizer:mapViewGesture];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageGesture:)];
    [self.scrollView setCanCancelContentTouches:YES];
    self.buildingPicImageView.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    [self.buildingPicImageView setUserInteractionEnabled:YES];
    [self.buildingPicImageView addGestureRecognizer:tapGesture];
    if (self.isNewPhysician) {
        [[NSUserDefaults standardUserDefaults] setObject:@"NEW" forKey:PHYCATEGORY];
        if (self.cardData.allKeys.count > 0 && !self.wasPresentingPopUp) {
           NSString *personName = [self.cardData stringForKey:@"Name"];
           NSArray* stringComponents = [personName componentsSeparatedByString:@" "];
           if (stringComponents.count > 1) {
               for (int i = 0; i < stringComponents.count; i++) {
                   if (i != 0) {
                       self.lastNameTextField.text = [NSString stringWithFormat:@"%@ %@",self.lastNameTextField.text,[stringComponents objectAtIndex:i]];
                   }
                   self.firstNameTextField.text = [stringComponents objectAtIndex:0];
               }
               
           }else{
               self.firstNameTextField.text = [self.cardData stringForKey:@"Name"];
           }
           self.mobileNumber1TextField.text = [self.cardData stringForKey:@"Mobile"];
           self.mobileNumber2TextField.text = [self.cardData stringForKey:@"Mobile2"];
           self.workLandLineNumber1TextField.text = [self.cardData stringForKey:@"Phone"];
           self.workLandLineNumber2TextField.text = [self.cardData stringForKey:@"Phone2"];
           self.emailID1TextField.text = [self.cardData stringForKey:@"Email"];
           self.emailID2TextField.text = [self.cardData stringForKey:@"Email2"];
           self.hospitalNameTextField.text = [NSString stringWithFormat:@"%@ %@",[self.cardData stringForKey:@"Company"],[self.cardData stringForKey:@"Company2"]];
           self.hospitalAddress1TextField.text = [self.cardData stringForKey:@"Address"];
        }
    }
    if(!self.wasPresentingPopUp){
        if (self.physicianDict.allKeys.count > 0){
            [self initialSetup];
        }else{
            NSLog(@"NO Data");
        }
    }
    self.wasPresentingPopUp = NO;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL editable = [userDefaults boolForKey:@"Editable"];
    if (editable == NO) {
        [self.leftVC showHideNavigationItems];
        self.firstNameTextField.userInteractionEnabled = NO;
        self.lastNameTextField.userInteractionEnabled = NO;
        self.cityTextField.userInteractionEnabled = NO;
        self.hospitalAddress1TextField.userInteractionEnabled = NO;
        self.hospitalAddress2TextField.userInteractionEnabled = NO;
        self.hospitalNameTextField.userInteractionEnabled = NO;
        self.hospitalLandLineNumber1TextField.userInteractionEnabled = NO;
        self.hospitalLandLineNumber2TextField.userInteractionEnabled = NO;
        self.hospitalLandLineNumber3TextField.userInteractionEnabled = NO;
        self.specialtyTextField.userInteractionEnabled = NO;
        self.mobileNumber1TextField.userInteractionEnabled = NO;
        self.mobileNumber2TextField.userInteractionEnabled = NO;
        self.mobileNumber3TextField.userInteractionEnabled = NO;
        self.emailID1TextField.userInteractionEnabled = NO;
        self.emailID2TextField.userInteractionEnabled = NO;
        self.emailID3TextField.userInteractionEnabled = NO;
        self.mapView.userInteractionEnabled = NO;
        self.buildingPicImageView.userInteractionEnabled = NO;
        self.yearOfGraduationTextField.userInteractionEnabled = NO;
        self.internetPerWeekTextField.userInteractionEnabled = NO;
        self.ageGroupTextField.userInteractionEnabled = NO;
        self.onlineDigitalMediumOption1TextField.userInteractionEnabled = NO;
        self.onlineDigitalMediumOption2TextField.userInteractionEnabled = NO;
        self.onlineDigitalMediumOption3TextField.userInteractionEnabled = NO;
        self.frequencyOfInternetOption1TextField.userInteractionEnabled = NO;
        self.numberOfPatientsPerWeekTextField.userInteractionEnabled = NO;
        self.workLandLineNumber1TextField.userInteractionEnabled = NO;
        self.workLandLineNumber2TextField.userInteractionEnabled = NO;
        self.workLandLineNumber3TextField.userInteractionEnabled = NO;
        self.receivingInformationOption1TextField.userInteractionEnabled = NO;
        self.receivingInformationOption2TextField.userInteractionEnabled = NO;
        self.receivingInformationOption3TextField.userInteractionEnabled = NO;
    }
    
}

//-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    [self.view endEditing:YES];
//    [self.hosListView setHidden:YES];
//}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)imageGesture:(UITapGestureRecognizer *)gestureRecognizer{
    [self imageView];
}


#pragma mark - Custom Methods

-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = LOADING_MESSAGE;
}

#pragma UITableView Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listOptions.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *hospInfo = [listOptions objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@, %@, %@",[hospInfo stringForKey:@"HospitalClinicName"],[hospInfo stringForKey:@"HospitalAddress1"],[hospInfo objectForKey:@"HospitalAddress2"]];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:17];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *hospInfo = [listOptions objectAtIndex:indexPath.row];
    self.hospitalNameTextField.text = hospInfo[@"HospitalClinicName"];
    self.hospitalAddress1TextField.text = hospInfo[@"HospitalAddress1"];
    self.hospitalAddress2TextField.text = hospInfo[@"HospitalAddress2"];
    self.hosListView.hidden = YES;
}

-(void)initialSetup{
    self.currentLocationDict = [[NSMutableDictionary alloc] init];
    self.firstNameTextField.text = [self.physicianDict stringForKey:@"FirstName"];
    self.lastNameTextField.text = [self.physicianDict stringForKey:@"LastName"];
    self.specialtyTextField.text = [self.physicianDict stringForKey:@"Speciality"];
    if ([self.specialtyTextField.text isEqualToString:@"Pharmacist"]) {
        self.numberOfPatientsPerWeekTextField.userInteractionEnabled = NO;
        self.numberOfPatientsPerWeekTextField.backgroundColor = [UIColor lightGrayColor];
        self.numberOfPatientsPerWeekTextField.text = @"0";
    }else{
        self.numberOfPatientsPerWeekTextField.userInteractionEnabled = YES;
        self.numberOfPatientsPerWeekTextField.backgroundColor = [UIColor whiteColor];
    }
    self.cityTextField.text = [self.physicianDict stringForKey:@"CityOfPractice"];
    self.hospitalNameTextField.text = [self.physicianDict stringForKey:@"HospitalClinicName"];
    self.hospitalAddress1TextField.text = [self.physicianDict stringForKey:@"HospitalAddress1"];
    self.hospitalAddress2TextField.text = [self.physicianDict stringForKey:@"HospitalAddress2"];
    self.mobileNumber1TextField.text = [self.physicianDict stringForKey:@"MobileNumber1"];
    self.mobileNumber2TextField.text = [self.physicianDict stringForKey:@"MobileNumber2"];
    self.mobileNumber3TextField.text = [self.physicianDict stringForKey:@"MobileNumber3"];
    self.emailID1TextField.text = [self.physicianDict stringForKey:@"EmailID1"];
    self.emailID2TextField.text = [self.physicianDict stringForKey:@"EmailID2"];
    self.emailID3TextField.text = [self.physicianDict stringForKey:@"EmailID3"];
    self.workLandLineNumber1TextField.text = [self.physicianDict stringForKey:@"WorkLandlineNumber1"];
    self.workLandLineNumber2TextField.text = [self.physicianDict stringForKey:@"WorkLandlineNumber2"];
    self.workLandLineNumber3TextField.text = [self.physicianDict stringForKey:@"WorkLandlineNumber3"];
    self.yearOfGraduationTextField.text = [self.physicianDict stringForKey:@"YearOfGraduation"];
    self.numberOfPatientsPerWeekTextField.text = [self.physicianDict stringForKey:@"NoOfPatientsSeenInWeek"];
    self.internetPerWeekTextField.text = [self.physicianDict stringForKey:@"InternetUsageHoursPerWeek"];    
    if ([self.physicianDict objectForKey:@"PreferredDigitalChannelBO"] != [NSNull null]) {
        NSArray *preferredDigitalChannelArray = [self.physicianDict objectForKey:@"PreferredDigitalChannelBO"];
        if (preferredDigitalChannelArray.count == 1) {
            NSDictionary *dict = [preferredDigitalChannelArray firstObject];
            NSInteger preferredDigitalChannelInteger = (NSInteger)[dict valueForKey:@"ChannelValue"];
            if (preferredDigitalChannelInteger == 6) {
                self.receivingInformationOption1TextField.text = [dict stringForKey:@"ChannelText"];
            }else{
                NSInteger inte = [[dict valueForKey:@"ChannelValue"] integerValue];
                self.receivingInformationOption1TextField.text = [PREFERRED_DIGITAL_CHANNEL_ARRAY objectAtIndex:inte-1];
            }
        }else if(preferredDigitalChannelArray.count == 2) {
            NSDictionary *dict = [preferredDigitalChannelArray firstObject];
            NSInteger preferredDigitalChannelInteger = (NSInteger)[dict valueForKey:@"ChannelValue"];
            if (preferredDigitalChannelInteger == 6) {
                self.receivingInformationOption1TextField.text = [dict stringForKey:@"ChannelText"];
            }else{
                NSInteger inte = [[dict valueForKey:@"ChannelValue"] integerValue];
                self.receivingInformationOption1TextField.text = [PREFERRED_DIGITAL_CHANNEL_ARRAY objectAtIndex:inte-1];
            }
            NSDictionary *secondDict = [preferredDigitalChannelArray objectAtIndex:1];
            NSInteger preferredDigitalChannelInteger1 = (NSInteger)[secondDict valueForKey:@"ChannelValue"];
            if (preferredDigitalChannelInteger1 == 6) {
                self.receivingInformationOption2TextField.text = [secondDict stringForKey:@"ChannelText"];
            }else{
                NSInteger inte = [[secondDict valueForKey:@"ChannelValue"] integerValue];
                self.receivingInformationOption2TextField.text = [PREFERRED_DIGITAL_CHANNEL_ARRAY objectAtIndex:inte-1];
            }
        }else if(preferredDigitalChannelArray.count == 3) {
            NSDictionary *dict = [preferredDigitalChannelArray firstObject];
            NSInteger preferredDigitalChannelInteger = (NSInteger)[dict valueForKey:@"ChannelValue"];
            if (preferredDigitalChannelInteger == 6) {
                self.receivingInformationOption1TextField.text = [dict stringForKey:@"ChannelText"];
            }else{
                NSInteger inte = [[dict valueForKey:@"ChannelValue"] integerValue];
                self.receivingInformationOption1TextField.text = [PREFERRED_DIGITAL_CHANNEL_ARRAY objectAtIndex:inte-1];
            }
            NSDictionary *secondDict = [preferredDigitalChannelArray objectAtIndex:1];
            NSInteger preferredDigitalChannelInteger1 = (NSInteger)[secondDict valueForKey:@"ChannelValue"];
            if (preferredDigitalChannelInteger1 == 6) {
                self.receivingInformationOption2TextField.text = [secondDict stringForKey:@"ChannelText"];
            }else{
                NSInteger inte = [[secondDict valueForKey:@"ChannelValue"] integerValue];
                self.receivingInformationOption2TextField.text = [PREFERRED_DIGITAL_CHANNEL_ARRAY objectAtIndex:inte-1];
            }
            NSDictionary *thirdDict = [preferredDigitalChannelArray objectAtIndex:2];
            NSInteger preferredDigitalChannelInteger2 = (NSInteger)[thirdDict valueForKey:@"ChannelValue"];
            if (preferredDigitalChannelInteger2 == 6) {
                self.receivingInformationOption3TextField.text = [thirdDict stringForKey:@"ChannelText"];
            }else{
                NSInteger inte = [[thirdDict valueForKey:@"ChannelValue"] integerValue];
                self.receivingInformationOption3TextField.text = [PREFERRED_DIGITAL_CHANNEL_ARRAY objectAtIndex:inte-1];
            }
        }
    }
    
    if ([self.physicianDict objectForKey:@"OnlineDigitalMediumBO"] != [NSNull null]) {
        NSArray *onlineDigitalMediumArray = [self.physicianDict objectForKey:@"OnlineDigitalMediumBO"];
        if (onlineDigitalMediumArray.count) {
            if (onlineDigitalMediumArray.count == 1) {
                NSDictionary *dict = [onlineDigitalMediumArray firstObject];
                NSInteger onlineDigitalMediumInterger = (NSInteger)[dict valueForKey:@"DigitalMediumValue"];
                if (onlineDigitalMediumInterger == 9) {
                    self.onlineDigitalMediumOption1TextField.text = [dict stringForKey:@"DigitalMediumText"];
                }else{
                    NSInteger inte = [[dict valueForKey:@"DigitalMediumValue"] integerValue];
                    self.onlineDigitalMediumOption1TextField.text = [ONLINE_DIGITAL_MEDIUM_ARRAY objectAtIndex:inte-1];
                }
            }else if (onlineDigitalMediumArray.count == 2){
                NSDictionary *dict = [onlineDigitalMediumArray firstObject];
                NSInteger onlineDigitalMediumInterger = (NSInteger)[dict valueForKey:@"DigitalMediumValue"];
                if (onlineDigitalMediumInterger == 9) {
                    self.onlineDigitalMediumOption1TextField.text = [dict stringForKey:@"DigitalMediumText"];
                }else{
                    NSInteger inte = [[dict valueForKey:@"DigitalMediumValue"] integerValue];
                    self.onlineDigitalMediumOption1TextField.text = [ONLINE_DIGITAL_MEDIUM_ARRAY objectAtIndex:inte-1];
                }
                NSDictionary *secondDict = [onlineDigitalMediumArray objectAtIndex:1];
                NSInteger onlineDigitalMediumInterger1 = (NSInteger)[secondDict valueForKey:@"DigitalMediumValue"];
                if (onlineDigitalMediumInterger1 == 9) {
                    self.onlineDigitalMediumOption2TextField.text = [secondDict stringForKey:@"DigitalMediumText"];
                }else{
                    NSInteger inte = [[secondDict valueForKey:@"DigitalMediumValue"] integerValue];
                    self.onlineDigitalMediumOption2TextField.text = [ONLINE_DIGITAL_MEDIUM_ARRAY objectAtIndex:inte-1];
                }
            }else{
                NSDictionary *dict = [onlineDigitalMediumArray firstObject];
                NSInteger onlineDigitalMediumInterger = (NSInteger)[dict valueForKey:@"DigitalMediumValue"];
                if (onlineDigitalMediumInterger == 9) {
                    self.onlineDigitalMediumOption1TextField.text = [dict stringForKey:@"DigitalMediumText"];
                }else{
                    NSInteger inte = [[dict valueForKey:@"DigitalMediumValue"] integerValue];
                    self.onlineDigitalMediumOption1TextField.text = [ONLINE_DIGITAL_MEDIUM_ARRAY objectAtIndex:inte-1];
                }
                NSDictionary *secondDict = [onlineDigitalMediumArray objectAtIndex:1];
                NSInteger onlineDigitalMediumInterger1 = (NSInteger)[secondDict valueForKey:@"DigitalMediumValue"];
                if (onlineDigitalMediumInterger1 == 9) {
                    self.onlineDigitalMediumOption2TextField.text = [secondDict stringForKey:@"DigitalMediumText"];
                }else{
                    NSInteger inte = [[secondDict valueForKey:@"DigitalMediumValue"] integerValue];
                    self.onlineDigitalMediumOption2TextField.text = [ONLINE_DIGITAL_MEDIUM_ARRAY objectAtIndex:inte-1];
                }
                NSDictionary *thirdDict = [onlineDigitalMediumArray objectAtIndex:2];
                NSInteger onlineDigitalMediumInterger2 = (NSInteger)[thirdDict valueForKey:@"DigitalMediumValue"];
                if (onlineDigitalMediumInterger2 == 9) {
                    self.onlineDigitalMediumOption3TextField.text = [thirdDict stringForKey:@"DigitalMediumText"];
                }else{
                    NSInteger inte = [[thirdDict valueForKey:@"DigitalMediumValue"] integerValue];
                    self.onlineDigitalMediumOption3TextField.text = [ONLINE_DIGITAL_MEDIUM_ARRAY objectAtIndex:inte-1];
                }
            }
    }
    }
    
    int ageGroupValue = [[self.physicianDict stringForKey:@"AgeGroup"] intValue];
    self.ageGroupTextField.text = ageGroupValue > 0?[AGE_GROUP_ARRAY objectAtIndex:(ageGroupValue-1)]:@"";
    int usageValue = [[self.physicianDict stringForKey:@"InternetUsageFrequency"] intValue];
    self.frequencyOfInternetOption1TextField.text = usageValue > 0?[FREQUENCY_OF_INTERNET_USAGE_ARRAY objectAtIndex:(usageValue-1)]:@"";
    
    if (![[self.physicianDict stringForKey:@"BuildingPicPath"] isEqualToString:@"0"]) {
        if (![[self.physicianDict stringForKey:@"BuildingPicPath"] isEqualToString:@""]) {
            [self.buildingPicImageView sd_setImageWithURL:[NSURL URLWithString:[[self.physicianDict stringForKey:@"BuildingPicPath"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""]];
        }else{
            self.buildingPicImageView.image = self.buildingPicImage;
        }
    }

    //float latitudeString = (float)[[self.physicianDict valueForKey:LATITUDE_STRING] floatValue];
    //float longitudeString = (float)[[self.physicianDict valueForKey:LONGITUDE_STRING] floatValue];
    
    float latitudeString = [self.physicianDict stringForKey:LATITUDE_STRING]?[[self.physicianDict stringForKey:LATITUDE_STRING]floatValue]:0;
    float longitudeString = [self.physicianDict stringForKey:LONGITUDE_STRING]?[[self.physicianDict stringForKey:LONGITUDE_STRING]floatValue]:0;
    
    if (latitudeString && longitudeString) {
        [locationManager stopUpdatingLocation];
        self.mapView.delegate = self;
        float spanX = 0.1;
        float spanY = 0.1;
        [self.mapView removeAnnotations:self.mapView.annotations];
        CLLocation *locationAtual = [[CLLocation alloc] initWithLatitude:latitudeString longitude:longitudeString];
        MKCoordinateSpan span = self.mapView.region.span;
        span.latitudeDelta=spanX;
        span.longitudeDelta=spanY;
        MKCoordinateRegion region = MKCoordinateRegionMake(locationAtual.coordinate, span);
        self.mapView.region = region;
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitudeString, longitudeString);
        point.coordinate = coordinate;
        point.title = @"Where am I?";
        point.subtitle = @"selected loaction!";
        region = MKCoordinateRegionMakeWithDistance(coordinate, 400, 400);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
        [self.mapView removeAnnotations:self.mapView.annotations];
        [self.mapView addAnnotation:point];
    }else{
        self.mapView.delegate = self;
        self.mapView.showsUserLocation = YES;
        [locationManager startUpdatingLocation];
    }
}
- (BOOL)checkValidEmail {
    BOOL validEmail = NO;
    if (self.emailID1TextField.text != nil) {
        NSString *expression = EMAIL_VALIDATION_STRING;
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSTextCheckingResult* regexResult = [regex firstMatchInString:self.emailID1TextField.text options:0 range:NSMakeRange(0, [self.emailID1TextField.text length])];
        validEmail = regexResult != nil;
    }
    return validEmail;
}

#pragma mark - Camera Methods

-(void)imageView {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *camera = [UIAlertAction actionWithTitle:CLICK_PHOTO_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self callImagePicker:UIImagePickerControllerSourceTypeCamera];
    }];
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:UPLOAD_IMAGE_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { [self callImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }];
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.buildingPicImageView;
    popPresenter.sourceRect = self.buildingPicImageView.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)clickPicture{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        self.leftVC = (LeftViewController *)self.delegate;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.wasPresentingPopUp = YES;
        [self.leftVC presentViewController:imagePicker animated:YES completion:nil];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:CAMERA_NOT_AVAILABLE_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
    }
}

-(void)callImagePicker:(UIImagePickerControllerSourceType)type{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = type;
    self.leftVC = (LeftViewController *)self.delegate;
    self.wasPresentingPopUp = YES;
    [self.leftVC presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    UIImage *galleryImage = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
    self.buildingPicImage = galleryImage;
    self.buildingPicImageView.image = galleryImage;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Custom Methods

-(void)validationFieldsChecking{
    
}


-(NSMutableDictionary *)getPharmacistData{
    NSString *ageGroupValue = [NSString stringWithFormat:@"%lu",(unsigned long)([AGE_GROUP_ARRAY indexOfObject:self.ageGroupTextField.text]+1)];
    NSString *usageFreqValue = [NSString stringWithFormat:@"%u",([FREQUENCY_OF_INTERNET_USAGE_ARRAY indexOfObject:self.frequencyOfInternetOption1TextField.text]+1)];
    NSMutableArray *preDigitalChannelArray = [[NSMutableArray alloc]init];
    if (self.receivingInformationOption1TextField.text.length > 0) {
        if ([PREFERRED_DIGITAL_CHANNEL_ARRAY containsObject:self.receivingInformationOption1TextField.text]) {
            [preDigitalChannelArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%u",([PREFERRED_DIGITAL_CHANNEL_ARRAY indexOfObject:self.receivingInformationOption1TextField.text]+1)],@"ChannelValue",self.receivingInformationOption1TextField.text,@"ChannelText", nil]];
        }else{
            [preDigitalChannelArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:@"6",@"ChannelValue",self.receivingInformationOption1TextField.text,@"ChannelText", nil]];
        }
    }
    if (self.receivingInformationOption2TextField.text.length > 0) {
        if ([PREFERRED_DIGITAL_CHANNEL_ARRAY containsObject:self.receivingInformationOption2TextField.text]) {
            [preDigitalChannelArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%u",([PREFERRED_DIGITAL_CHANNEL_ARRAY indexOfObject:self.receivingInformationOption2TextField.text]+1)],@"ChannelValue",self.receivingInformationOption2TextField.text,@"ChannelText", nil]];
        }else{
            [preDigitalChannelArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:@"6",@"ChannelValue",self.receivingInformationOption2TextField.text,@"ChannelText", nil]];
        }
    }
    if (self.receivingInformationOption3TextField.text.length > 0) {
        if ([PREFERRED_DIGITAL_CHANNEL_ARRAY containsObject:self.receivingInformationOption3TextField.text]) {
            [preDigitalChannelArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%u",([PREFERRED_DIGITAL_CHANNEL_ARRAY indexOfObject:self.receivingInformationOption3TextField.text]+1)],@"ChannelValue",self.receivingInformationOption3TextField.text,@"ChannelText", nil]];
        }else{
            [preDigitalChannelArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:@"6",@"ChannelValue",self.receivingInformationOption3TextField.text,@"ChannelText", nil]];
        }
    }
    NSMutableArray *digitalMediumArray = [[NSMutableArray alloc]init];
    if (self.onlineDigitalMediumOption1TextField.text.length > 0) {
        if ([ONLINE_DIGITAL_MEDIUM_ARRAY containsObject:self.onlineDigitalMediumOption1TextField.text]) {
            [digitalMediumArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%u",([ONLINE_DIGITAL_MEDIUM_ARRAY indexOfObject:self.onlineDigitalMediumOption1TextField.text]+1)],@"DigitalMediumValue",self.onlineDigitalMediumOption1TextField.text,@"DigitalMediumText", nil]];
        }else{
            [digitalMediumArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:@"9",@"DigitalMediumValue",self.onlineDigitalMediumOption1TextField.text.length?self.onlineDigitalMediumOption1TextField.text:@"",@"DigitalMediumText", nil]];
        }
    }
    if (self.onlineDigitalMediumOption2TextField.text.length > 0) {
        if ([ONLINE_DIGITAL_MEDIUM_ARRAY containsObject:self.onlineDigitalMediumOption2TextField.text]) {
            [digitalMediumArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%u",([ONLINE_DIGITAL_MEDIUM_ARRAY indexOfObject:self.onlineDigitalMediumOption2TextField.text]+1)],@"DigitalMediumValue",self.onlineDigitalMediumOption2TextField.text,@"DigitalMediumText", nil]];
        }else{
            [digitalMediumArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:@"9",@"DigitalMediumValue",self.onlineDigitalMediumOption2TextField.text,@"DigitalMediumText", nil]];
        }
    }
    if (self.onlineDigitalMediumOption3TextField.text.length > 0) {
        if ([ONLINE_DIGITAL_MEDIUM_ARRAY containsObject:self.onlineDigitalMediumOption3TextField.text]) {
            [digitalMediumArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%u",([ONLINE_DIGITAL_MEDIUM_ARRAY indexOfObject:self.onlineDigitalMediumOption3TextField.text]+1)],@"DigitalMediumValue",self.onlineDigitalMediumOption3TextField.text,@"DigitalMediumText", nil]];
        }else{
            [digitalMediumArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys:@"9",@"DigitalMediumValue",self.onlineDigitalMediumOption3TextField.text,@"DigitalMediumText", nil]];
        }
    }

//    NSLog(@"funal Dict:%@",self.finalLocationDict);
//    NSLog(@"latitude:%@",[self.finalLocationDict stringForKey:LATITUDE_STRING]);
//    NSLog(@"longitude:%@",[self.finalLocationDict stringForKey:LONGITUDE_STRING]);
    NSMutableDictionary *physicianData;
    if ([self.specialtyTextField.text isEqualToString:@"Pharmacist"]) {
        physicianData = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[self.physicianDict stringForKey:@"DoctorTransactionID"]?[self.physicianDict stringForKey:@"DoctorTransactionID"]:@"0",@"DoctorTransactionID",[self.physicianDict stringForKey:@"DoctorId"]?[self.physicianDict stringForKey:@"DoctorId"]:@"",@"DoctorId",@"",@"PhysicianId",@"",@"NoOfPatientsSeenInWeek",self.firstNameTextField.text,@"FirstName",self.lastNameTextField.text,@"LastName",self.specialtyTextField.text,@"Speciality",self.cityTextField.text,@"CityOfPractice",self.mobileNumber1TextField.text,@"MobileNumber1",self.mobileNumber2TextField.text,@"MobileNumber2",self.mobileNumber3TextField.text,@"MobileNumber3",self.workLandLineNumber1TextField.text,@"WorkLandlineNumber1",self.workLandLineNumber2TextField.text,@"WorkLandlineNumber2",self.workLandLineNumber3TextField.text,@"WorkLandlineNumber3",self.hospitalNameTextField.text,@"HospitalClinicName",self.hospitalAddress1TextField.text,@"HospitalAddress1",self.hospitalAddress2TextField.text,@"HospitalAddress2",self.emailID1TextField.text,@"EmailID1",self.emailID2TextField.text,@"EmailID2",self.emailID3TextField.text,@"EmailID3",self.yearOfGraduationTextField.text,@"YearOfGraduation",ageGroupValue,@"AgeGroup",usageFreqValue,@"InternetUsageFrequency",self.internetPerWeekTextField.text,@"InternetUsageHoursPerWeek",preDigitalChannelArray,@"PreferredDigitalChannelBO",digitalMediumArray,@"OnlineDigitalMediumBO",[self.finalLocationDict stringForKey:LATITUDE_STRING],@"Latitude", [self.finalLocationDict stringForKey:LONGITUDE_STRING],@"Longitude",[self.buidlingPicData base64EncodedStringWithOptions:NSUTF8StringEncoding]?[self.buidlingPicData base64EncodedStringWithOptions:NSUTF8StringEncoding]:@"",@"BuildingPic",nil];
    }else{
        physicianData = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[self.physicianDict stringForKey:@"DoctorTransactionID"]?[self.physicianDict stringForKey:@"DoctorTransactionID"]:@"0",@"DoctorTransactionID",[self.physicianDict stringForKey:@"DoctorId"]?[self.physicianDict stringForKey:@"DoctorId"]:@"",@"DoctorId",@"",@"PhysicianId",self.numberOfPatientsPerWeekTextField.text,@"NoOfPatientsSeenInWeek",self.firstNameTextField.text,@"FirstName",self.lastNameTextField.text,@"LastName",self.specialtyTextField.text,@"Speciality",self.cityTextField.text,@"CityOfPractice",self.mobileNumber1TextField.text,@"MobileNumber1",self.mobileNumber2TextField.text,@"MobileNumber2",self.mobileNumber3TextField.text,@"MobileNumber3",self.workLandLineNumber1TextField.text,@"WorkLandlineNumber1",self.workLandLineNumber2TextField.text,@"WorkLandlineNumber2",self.workLandLineNumber3TextField.text,@"WorkLandlineNumber3",self.hospitalNameTextField.text,@"HospitalClinicName",self.hospitalAddress1TextField.text,@"HospitalAddress1",self.hospitalAddress2TextField.text,@"HospitalAddress2",self.emailID1TextField.text,@"EmailID1",self.emailID2TextField.text,@"EmailID2",self.emailID3TextField.text,@"EmailID3",self.yearOfGraduationTextField.text,@"YearOfGraduation",ageGroupValue,@"AgeGroup",usageFreqValue,@"InternetUsageFrequency",self.internetPerWeekTextField.text,@"InternetUsageHoursPerWeek",preDigitalChannelArray,@"PreferredDigitalChannelBO",digitalMediumArray,@"OnlineDigitalMediumBO",[self.finalLocationDict stringForKey:LATITUDE_STRING],@"Latitude", [self.finalLocationDict stringForKey:LONGITUDE_STRING],@"Longitude",[self.buidlingPicData base64EncodedStringWithOptions:NSUTF8StringEncoding]?[self.buidlingPicData base64EncodedStringWithOptions:NSUTF8StringEncoding]:@"",@"BuildingPic",nil];
    }
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if ([AFStringFromNetworkReachabilityStatus(status) isEqualToString:@"Not Reachable"]) {
            if (self.isNewPhysician) {
                [physicianData setObject:@"NEW" forKey:@"RepType"];
            }else{
                [physicianData setObject:[self.physicianDict stringForKey:@"RepType"] forKey:@"RepType"];
            }
        }
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    return physicianData;
}

#pragma mark - UIButtonAction Methods

-(IBAction)specialityButtonAction:(id)sender{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL editable = [userDefaults boolForKey:@"Editable"];
    if (editable == NO) {
        return;
    }
    [self.view endEditing:YES];
    OptionsViewController *optionsVC = [[OptionsViewController alloc] initWithTextfield:self.specialtyTextField patientsTxtFld:self.numberOfPatientsPerWeekTextField andOptions:SPECIALITY_ARRAY];
    
    optionsVC.modalPresentationStyle =  UIModalPresentationOverFullScreen;
    [self presentViewController:optionsVC animated:YES completion:nil];
}

-(IBAction)ageGroupButtonAction:(id)sender{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL editable = [userDefaults boolForKey:@"Editable"];
    if (editable == NO) {
        return;
    }
    [self.view endEditing:YES];
    OptionsViewController *optionsVC = [[OptionsViewController alloc] initWithTextfield:self.ageGroupTextField patientsTxtFld:nil andOptions:AGE_GROUP_ARRAY];
    optionsVC.modalPresentationStyle =  UIModalPresentationOverFullScreen;
    LeftViewController *leftVC = (LeftViewController *)self.delegate;
    [/*self*/leftVC presentViewController:optionsVC animated:YES completion:nil];
}

-(IBAction)frequencyOfInternetButtonAction:(id)sender{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL editable = [userDefaults boolForKey:@"Editable"];
    if (editable == NO) {
        return;
    }
    [self.view endEditing:YES];
    OptionsViewController *optionsVC = [[OptionsViewController alloc] initWithTextfield:self.frequencyOfInternetOption1TextField patientsTxtFld:nil andOptions:FREQUENCY_OF_INTERNET_USAGE_ARRAY];
    optionsVC.modalPresentationStyle =  UIModalPresentationOverFullScreen;
    [self presentViewController:optionsVC animated:YES completion:nil];
    
}

-(IBAction)receivingInformationButtonAction:(id)sender{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL editable = [userDefaults boolForKey:@"Editable"];
    if (editable == NO) {
        return;
    }
    [self.view endEditing:YES];
    OptionsViewController *optionsVC = [[OptionsViewController alloc] initWithTextfield:nil patientsTxtFld:nil andOptions:PREFERRED_DIGITAL_CHANNEL_ARRAY];
    optionsVC.modalPresentationStyle =  UIModalPresentationOverFullScreen;
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case 10:
            [self.receivingInformationOption1TextField setUserInteractionEnabled:false];
            optionsVC.selectedTextfield = self.receivingInformationOption1TextField;
            [self presentViewController:optionsVC animated:YES completion:nil];
            break;
        case 20:
            [self.receivingInformationOption2TextField setUserInteractionEnabled:false];
            optionsVC.selectedTextfield = self.receivingInformationOption2TextField;
            [self presentViewController:optionsVC animated:YES completion:nil];
            break;
        case 30:
            if ([self.receivingInformationOption1TextField.text isEqualToString:(self.receivingInformationOption2TextField.text)]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please select other options" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];                                                                                    }else{
                [self.receivingInformationOption3TextField setUserInteractionEnabled:false];
                optionsVC.selectedTextfield = self.receivingInformationOption3TextField;
                [self presentViewController:optionsVC animated:YES completion:nil];
                break;
            }
        default:
            break;
    }
}


-(IBAction)onlineDigitalMediumButtonAction:(id)sender{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL editable = [userDefaults boolForKey:@"Editable"];
    if (editable == NO) {
        return;
    }
    [self.view endEditing:YES];
    OptionsViewController *optionsVC = [[OptionsViewController alloc] initWithTextfield:nil patientsTxtFld:nil andOptions:ONLINE_DIGITAL_MEDIUM_ARRAY];
    optionsVC.modalPresentationStyle =  UIModalPresentationOverFullScreen;
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case 10:
            if (![(self.receivingInformationOption2TextField.text) isEqualToString:@""] && ![(self.receivingInformationOption3TextField.text) isEqualToString:@""]) {
                if ([(self.receivingInformationOption1TextField.text) isEqualToString:(self.receivingInformationOption3TextField.text)] || [(self.receivingInformationOption2TextField.text) isEqualToString:(self.receivingInformationOption3TextField.text)]) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please select other options" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    [self.onlineDigitalMediumOption1TextField setUserInteractionEnabled:false];
                    optionsVC.selectedTextfield = self.onlineDigitalMediumOption1TextField;
                    [self presentViewController:optionsVC animated:YES completion:nil];
                }
            }else{
                [self.onlineDigitalMediumOption1TextField setUserInteractionEnabled:false];
                optionsVC.selectedTextfield = self.onlineDigitalMediumOption1TextField;
                [self presentViewController:optionsVC animated:YES completion:nil];
            }
            break;
        case 20:
            [self.onlineDigitalMediumOption2TextField setUserInteractionEnabled:false];
            optionsVC.selectedTextfield = self.onlineDigitalMediumOption2TextField;
            [self presentViewController:optionsVC animated:YES completion:nil];
            break;
        case 30:
            if ([(self.onlineDigitalMediumOption1TextField.text) isEqualToString:(self.onlineDigitalMediumOption2TextField.text)]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please select other options" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                [self.onlineDigitalMediumOption3TextField setUserInteractionEnabled:false];
                optionsVC.selectedTextfield = self.onlineDigitalMediumOption3TextField;
                [self presentViewController:optionsVC animated:YES completion:nil];
            }
            break;
        default:
            break;
    }
}

-(IBAction)saveAction:(id)sender{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL editable = [userDefaults boolForKey:@"Editable"];
    if (editable == YES) {
        [self.view endEditing:YES];
        if (self.buildingPicImage) {
            self.buidlingPicData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.buildingPicImage, 0.1)];
        }
        if ([self.physicianDict objectForKey:LATITUDE_STRING]){
            [self.finalLocationDict setObject:[self.physicianDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
        }
        if ([self.currentLocationDict objectForKey:LATITUDE_STRING]) {
            [self.finalLocationDict setObject:[self.currentLocationDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
        }
        if ([self.selectedLocationDict objectForKey:LATITUDE_STRING]){
            [self.finalLocationDict setObject:[self.selectedLocationDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
        }
        if([self.physicianDict objectForKey:LONGITUDE_STRING]){
            [self.finalLocationDict setObject:[self.physicianDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
        }
        if ([self.currentLocationDict objectForKey:LONGITUDE_STRING]) {
            [self.finalLocationDict setObject:[self.currentLocationDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
        }
        if([self.selectedLocationDict objectForKey:LONGITUDE_STRING]){
            [self.finalLocationDict setObject:[self.selectedLocationDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
        }
        if ([self.specialtyTextField.text isEqualToString:@"Pharmacist"]) {
            if (![self.firstNameTextField.text  isEqualToString:@""]) {
                if (![self.lastNameTextField.text isEqualToString:@""]) {
                    if (![self.specialtyTextField.text isEqualToString:@""]) {
                        if (![self.cityTextField.text isEqualToString:@""]) {
                            if (![self.hospitalNameTextField.text isEqualToString:@""]) {
                                if (![self.hospitalAddress1TextField.text isEqualToString:@""]) {
                                    if (![self.hospitalAddress2TextField.text isEqualToString:@""]) {
                                        if (![self.mobileNumber1TextField.text isEqualToString:@""]) {
                                            if (![self.emailID1TextField.text isEqualToString:@""]) {
                                                if (![self.workLandLineNumber1TextField.text isEqualToString:@""]) {
                                                    //                                                    if ([self.numberOfPatientsPerWeekTextField.text isEqualToString:@""]) {
                                                    if (![self.yearOfGraduationTextField.text isEqualToString:@""]) {
                                                        if (![self.ageGroupTextField.text isEqualToString:@""]) {
                                                            if (![self.internetPerWeekTextField.text isEqualToString:@""]) {
                                                                if (![self.frequencyOfInternetOption1TextField.text isEqualToString:@""]) {
                                                                    if (![self.receivingInformationOption1TextField.text isEqualToString:@""]) {
                                                                        if (![self.onlineDigitalMediumOption1TextField.text isEqualToString:@""]) {
                                                                            if (![[self.finalLocationDict stringForKey:LATITUDE_STRING] isEqualToString:@""] && ![[self.finalLocationDict stringForKey:LONGITUDE_STRING] isEqualToString:@""]) {
                                                                                NSLog(@"data is there");
                                                                                
                                                                                //[self validationFieldsChecking];
                                                                                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                                                                [dateFormatter setDateFormat:ONLY_YEAR_FORMATTER_STRING];
                                                                                NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
                                                                                NSInteger currentYear = [strDate integerValue];
                                                                                NSInteger frequencyOfInternetPerWeekInteger = [[NSString stringWithFormat:@"%@",self.internetPerWeekTextField.text] integerValue];
                                                                                if (frequencyOfInternetPerWeekInteger <= 168) {
                                                                                    NSInteger yearOfGraduationYear = [[NSString stringWithFormat:@"%@",self.yearOfGraduationTextField.text] integerValue];
                                                                                    if (yearOfGraduationYear <= currentYear) {
                                                                                        if (![self checkValidEmail])
                                                                                        {
                                                                                            UIAlertController *alert =   [UIAlertController
                                                                                                                          alertControllerWithTitle:OOPS_MESSAGE
                                                                                                                          message:VALID_EMAIL_ID_MESSAGE
                                                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                                                                                            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                                                            }];
                                                                                            [alert addAction:okAction];
                                                                                            [self presentViewController:alert animated:YES completion:nil];
                                                                                            return;
                                                                                        }else{
                                                                                            [self addPhysicianService];
                                                                                        }
                                                                                    }else{
                                                                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:YEAR_OF_GRADUATION_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                                                                                        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                                                                                        [alert addAction:action];
                                                                                        [self presentViewController:alert animated:YES completion:nil];
                                                                                    }
                                                                                }else{
                                                                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:FREQUENCY_OF_INTERNET_USAGE_PER_WEEK_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                                                                                    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                                                                                    [alert addAction:action];
                                                                                    [self presentViewController:alert animated:YES completion:nil];
                                                                                }
                                                                            }else{
                                                                                [self showAlert:@"Please Select Location."];
                                                                            }
                                                                        }else{
                                                                            [self showAlert:@"Please Select most preferred usage of any online digital medium."];
                                                                        }
                                                                    }else{
                                                                        [self showAlert:@"Please Select most preferred digital channel for receiving information from pharmaceutical companies."];
                                                                    }
                                                                }else{
                                                                    [self showAlert:@"Please Select the internet usage for professional purpose."];
                                                                }
                                                            }else{
                                                                [self showAlert:@"Please Enter how many hours do you spend to access internet for professional purpose."];
                                                            }
                                                        }else{
                                                            [self showAlert:@"Please Select Age Group."];
                                                        }
                                                    }else{
                                                        [self showAlert:@"Please Enter Year of Graduation."];
                                                    }
                                                    //                                                    }else{
                                                    //                                                        [self showAlert:@"Please enter  Number of Patients."];
                                                    //                                                    }
                                                }else{
                                                    [self showAlert:@"Please Enter Work Landline Number1."];
                                                }
                                            }else{
                                                [self showAlert:@"Please Enter EmailID1."];
                                            }
                                        }else{
                                            [self showAlert:@"Please Enter Mobile Number1"];
                                        }
                                    }else{
                                        [self showAlert:@"Please Enter Hospital Address2."];
                                    }
                                }else{
                                    [self showAlert:@"Please Enter Hospital Address1."];
                                }
                            }else{
                                [self showAlert:@"Please Enter Hospital Name."];
                            }
                        }else{
                            [self showAlert:@"Please Enter City of Practice."];
                        }
                    }else{
                        [self showAlert:@"Please Select Speciality."];
                    }
                }else{
                    [self showAlert:@"Please Enter Last Name."];
                }
            }else{
                [self showAlert:@"Please Enter First Name."];
            }
        }else{
            if (![self.firstNameTextField.text  isEqualToString:@""]) {
                if (![self.lastNameTextField.text isEqualToString:@""]) {
                    if (![self.specialtyTextField.text isEqualToString:@""]) {
                        if (![self.cityTextField.text isEqualToString:@""]) {
                            if (![self.hospitalNameTextField.text isEqualToString:@""]) {
                                if (![self.hospitalAddress1TextField.text isEqualToString:@""]) {
                                    if (![self.hospitalAddress2TextField.text isEqualToString:@""]) {
                                        if (![self.mobileNumber1TextField.text isEqualToString:@""]) {
                                            if (![self.emailID1TextField.text isEqualToString:@""]) {
                                                if (![self.workLandLineNumber1TextField.text isEqualToString:@""]) {
                                                    if (![self.numberOfPatientsPerWeekTextField.text isEqualToString:@""]) {
                                                        if (![self.yearOfGraduationTextField.text isEqualToString:@""]) {
                                                            if (![self.ageGroupTextField.text isEqualToString:@""]) {
                                                                if (![self.internetPerWeekTextField.text isEqualToString:@""]) {
                                                                    if (![self.frequencyOfInternetOption1TextField.text isEqualToString:@""]) {
                                                                        if (![self.receivingInformationOption1TextField.text isEqualToString:@""]) {
                                                                            if (![self.onlineDigitalMediumOption1TextField.text isEqualToString:@""]) {
                                                                                if (![[self.finalLocationDict stringForKey:LATITUDE_STRING] isEqualToString:@""] && ![[self.finalLocationDict stringForKey:LONGITUDE_STRING] isEqualToString:@""]) {
                                                                                    NSLog(@"data is there");
                                                                                    
                                                                                    //[self validationFieldsChecking];
                                                                                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                                                                    [dateFormatter setDateFormat:ONLY_YEAR_FORMATTER_STRING];
                                                                                    NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
                                                                                    NSInteger currentYear = [strDate integerValue];
                                                                                    NSInteger frequencyOfInternetPerWeekInteger = [[NSString stringWithFormat:@"%@",self.internetPerWeekTextField.text] integerValue];
                                                                                    if (frequencyOfInternetPerWeekInteger <= 168) {
                                                                                        NSInteger yearOfGraduationYear = [[NSString stringWithFormat:@"%@",self.yearOfGraduationTextField.text] integerValue];
                                                                                        if (yearOfGraduationYear <= currentYear) {
                                                                                            if (![self checkValidEmail])
                                                                                            {
                                                                                                UIAlertController *alert =   [UIAlertController
                                                                                                                              alertControllerWithTitle:OOPS_MESSAGE
                                                                                                                              message:VALID_EMAIL_ID_MESSAGE
                                                                                                                              preferredStyle:UIAlertControllerStyleAlert];
                                                                                                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                                                                }];
                                                                                                [alert addAction:okAction];
                                                                                                [self presentViewController:alert animated:YES completion:nil];
                                                                                                return;
                                                                                            }else{
                                                                                                [self addPhysicianService];
                                                                                            }
                                                                                        }else{
                                                                                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:YEAR_OF_GRADUATION_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                                                                                            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                                                                                            [alert addAction:action];
                                                                                            [self presentViewController:alert animated:YES completion:nil];
                                                                                        }
                                                                                    }else{
                                                                                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:FREQUENCY_OF_INTERNET_USAGE_PER_WEEK_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                                                                                        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                                                                                        [alert addAction:action];
                                                                                        [self presentViewController:alert animated:YES completion:nil];
                                                                                    }
                                                                                }else{
                                                                                    [self showAlert:@"Please Select Location."];
                                                                                }
                                                                            }else{
                                                                                [self showAlert:@"Please Select most preferred usage of any online digital medium."];
                                                                            }
                                                                        }else{
                                                                            [self showAlert:@"Please Select most preferred digital channel for receiving information from pharmaceutical companies."];
                                                                        }
                                                                    }else{
                                                                        [self showAlert:@"Please Select the internet usage for professional purpose."];
                                                                    }
                                                                }else{
                                                                    [self showAlert:@"Please Enter how many hours do you spend to access internet for professional purpose."];
                                                                }
                                                            }else{
                                                                [self showAlert:@"Please Select Age Group."];
                                                            }
                                                        }else{
                                                            [self showAlert:@"Please Enter Year of Graduation."];
                                                        }
                                                    }else{
                                                        [self showAlert:@"Please Enter Number of Patients seen in a week."];
                                                    }
                                                }else{
                                                    [self showAlert:@"Please Enter Work Landline Number1."];
                                                }
                                            }else{
                                                [self showAlert:@"Please Enter EmailID1."];
                                            }
                                        }else{
                                            [self showAlert:@"Please Enter Mobile Number1"];
                                        }
                                    }else{
                                        [self showAlert:@"Please Enter Hospital Address2."];
                                    }
                                }else{
                                    [self showAlert:@"Please Enter Hospital Address1."];
                                }
                            }else{
                                [self showAlert:@"Please Enter Hospital Name."];
                            }
                        }else{
                            [self showAlert:@"Please Enter City of Practice."];
                        }
                    }else{
                        [self showAlert:@"Please Select Speciality."];
                    }
                }else{
                    [self showAlert:@"Please Enter Last Name."];
                }
            }else{
                [self showAlert:@"Please Enter First Name."];
            }
        }
        
        
    }
}

#pragma mark - Service Call Method

-(void)addPhysicianService{
    [self showProgressView];
    NSString *mobNum = [[NSUserDefaults standardUserDefaults]stringForKey:EMAIL_STRING];
    NSString *category = [self.physicianDict stringForKey:@"RepType"];
    if (self.isNewPhysician) {
       category = @"NEW";
       [[NSUserDefaults standardUserDefaults] setObject:@"NEW" forKey:PHYCATEGORY];
    }else{
       [[NSUserDefaults standardUserDefaults] setObject:category forKey:PHYCATEGORY];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,ADD_PHYSICIAN,mobNum,category];
    //offline
    NSMutableDictionary *dicPharmacistData = [self getPharmacistData];
    //offline
    [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.physicianDict = (NSMutableDictionary *)data;
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:@"Details saved successfully." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                //do something when click button
                if ([self.delegate respondsToSelector:@selector(saveButtonAction:)]) {
                    [self.delegate saveButtonAction:self.physicianDict];
                }
            }];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        [self.mbProgressView hideAnimated:YES];
        //Offline
        NSLog(@"Error: %@",error.localizedDescription);
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
            AppDelegate *delG =  appDelegate;
            if(!delG.physicianNewMissionArray){
                delG.physicianNewMissionArray = [NSMutableArray new];
            }
            [delG.physicianNewMissionArray addObject:dicPharmacistData];
            NSLog(@"Physician Saved");
            if ([self.delegate respondsToSelector:@selector(saveButtonAction:)]) {
                [self.delegate saveButtonAction:/*self.physicianDict*/[NSMutableDictionary dictionaryWithDictionary:dicPharmacistData]];
            }
        }
        //Offline
    }];
}

#pragma mark - UITextFieldDelegate Methods

-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.hospitalNameTextField) {
        if ([textField.text isEqualToString:@""]){
            self.hosListView.hidden = YES;
        }else{
            [self getOptionsList:textField.text];
        }
    }
}
-(void)getOptionsList:(NSString*)inputStr{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",KBASE_URL,PHYSICIAN_SELECTION_SEARCH,[inputStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [Webservice getWithUrlString:urlString success:^(id data) {
        if (data) {
            listOptions = (NSArray*)data;
            if (listOptions.count > 0) {
                self.hosListView.hidden = NO;
                [self.hosListView reloadData];
            }else{
                self.hosListView.hidden = YES;
            }
        }else{
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alert addAction:alertAction];
//            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField{
    if ([self.specialtyTextField.text isEqualToString:@"Pharmacist"]) {
        self.numberOfPatientsPerWeekTextField.userInteractionEnabled = NO;
        self.numberOfPatientsPerWeekTextField.text = @"0";
    }else{
        self.numberOfPatientsPerWeekTextField.userInteractionEnabled = YES;
    }
    if (textField == self.yearOfGraduationTextField) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:ONLY_YEAR_FORMATTER_STRING];
        NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
        NSInteger currentYear = [strDate integerValue];
        NSInteger yearOfGraduationYear = [[NSString stringWithFormat:@"%@",self.yearOfGraduationTextField.text] integerValue];
        if (yearOfGraduationYear <= currentYear) {
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:YEAR_OF_GRADUATION_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }if (textField == self.internetPerWeekTextField) {
        NSInteger frequencyOfInternetPerWeekInteger = [[NSString stringWithFormat:@"%@",self.internetPerWeekTextField.text] integerValue];
        if (frequencyOfInternetPerWeekInteger >= 168) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:FREQUENCY_OF_INTERNET_USAGE_PER_WEEK_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([self.specialtyTextField.text isEqualToString:@"Pharmacist"]) {
        self.numberOfPatientsPerWeekTextField.backgroundColor = [UIColor lightGrayColor];
        self.numberOfPatientsPerWeekTextField.userInteractionEnabled = NO;
        self.numberOfPatientsPerWeekTextField.text = @"0";
    }else{
        self.numberOfPatientsPerWeekTextField.backgroundColor = [UIColor whiteColor];
        self.numberOfPatientsPerWeekTextField.userInteractionEnabled = YES;
    }
    if (textField == self.firstNameTextField || textField == self.lastNameTextField || textField == self.cityTextField || textField == self.hospitalNameTextField || textField == self.hospitalAddress1TextField || textField == self.hospitalAddress2TextField) {
        textField.keyboardType = UIKeyboardTypeDefault;
    }
    if (textField == self.emailID1TextField || textField == self.emailID2TextField || textField == self.emailID3TextField) {
        textField.keyboardType = UIKeyboardTypeEmailAddress;
    }
    if (textField == self.mobileNumber1TextField || textField == self.mobileNumber2TextField || textField == self.mobileNumber3TextField || textField == self.yearOfGraduationTextField || textField == self.numberOfPatientsPerWeekTextField || textField == self.internetPerWeekTextField || textField == self.workLandLineNumber1TextField || textField == self.workLandLineNumber2TextField ||textField == self.workLandLineNumber3TextField ) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if (textField == self.hospitalNameTextField) {
//        if ([textField.text isEqualToString:@""]){
//            self.hosListView.hidden = YES;
//        }
//    }
    if(textField == self.firstNameTextField || textField == self.lastNameTextField || textField == self.cityTextField || textField == self.hospitalNameTextField)
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:ONLY_ALPHABETS_WITHSPACE_MESSAGE] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }else if (textField == self.emailID1TextField || textField == self.emailID2TextField || textField == self.emailID3TextField) {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@.0123456789-_"] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }else if (textField == self.yearOfGraduationTextField || textField == self.numberOfPatientsPerWeekTextField || textField == self.internetPerWeekTextField) {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:ONLY_NUMBERS] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }else if(textField == self.mobileNumber1TextField || textField == self.mobileNumber2TextField ||textField == self.mobileNumber3TextField ||textField == self.hospitalLandLineNumber1TextField || textField == self.hospitalLandLineNumber2TextField || textField == self.workLandLineNumber1TextField || textField == self.workLandLineNumber2TextField || textField == self.workLandLineNumber3TextField ){
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }else if(textField == self.receivingInformationOption1TextField || textField == self.receivingInformationOption2TextField ||textField == self.receivingInformationOption3TextField ||textField == self.onlineDigitalMediumOption1TextField || textField == self.onlineDigitalMediumOption2TextField || textField == self.onlineDigitalMediumOption3TextField){
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:ONLY_ALPHABETS_WITHSPACE_MESSAGE] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }else
        return YES;
}

#pragma mark - CLLocationManager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    float spanX = 0.1;
    float spanY = 0.1;
    CLLocation *location = [locations lastObject];
    MKCoordinateSpan span = self.mapView.region.span;
    span.latitudeDelta=spanX;
    span.longitudeDelta=spanY;
    MKCoordinateRegion region = MKCoordinateRegionMake(location.coordinate, span);
    self.mapView.region = region;
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"selected loaction!!!";
    region = MKCoordinateRegionMakeWithDistance(location.coordinate, 400, 400);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotation:point];
    NSString *latitude = [NSString stringWithFormat:@"%.7f", location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%.7f", location.coordinate.longitude];
    self.currentLocationDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitude,LATITUDE_STRING,longitude,LONGITUDE_STRING, nil];
    [locationManager stopUpdatingLocation];
}


- (void)longpressToGetLocation:(UILongPressGestureRecognizer *)gestureRecognizer{
    [self.view endEditing:YES];
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    self.mapView.showsUserLocation = NO;
    [self.mapView removeAnnotations:self.mapView.annotations];
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D location =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    float spanX = 0.1;
    float spanY = 0.1;
    CLLocation *locationAtual = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
    MKCoordinateSpan span = self.mapView.region.span;
    span.latitudeDelta=spanX;
    span.longitudeDelta=spanY;
    MKCoordinateRegion region = MKCoordinateRegionMake(locationAtual.coordinate, span);
    self.mapView.region = region;
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    region = MKCoordinateRegionMakeWithDistance(locationAtual.coordinate, 400, 400);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    [self.mapView addAnnotation:point];
    NSString *latitude = [NSString stringWithFormat:@"%.7f", location.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%.7f", location.longitude];
    self.selectedLocationDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitude,@"Latitude",longitude,@"Longitude", nil];
}


-(void)showAlert:(NSString *)message{
    UIAlertController *alert =   [UIAlertController
                                  alertControllerWithTitle:OOPS_MESSAGE
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //do something when click button
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
