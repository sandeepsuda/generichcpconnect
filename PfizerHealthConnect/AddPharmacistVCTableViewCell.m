//
//  AddPharmacistVCTableViewCell.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 25/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "AddPharmacistVCTableViewCell.h"

@implementation AddPharmacistVCTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
