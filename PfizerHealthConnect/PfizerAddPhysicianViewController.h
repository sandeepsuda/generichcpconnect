//
//  PfizerAddPhysicianViewController.h
//  PfizerHealthConnect
//
//  Created by RajaSekhar on 12/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "MBProgressHUD.h"
@protocol PfizerAddPhysicianProtocol <NSObject>

-(void)saveButtonAction:(NSMutableDictionary *)dict;
-(void)buildingPicSelection;

@end

@interface PfizerAddPhysicianViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,MKAnnotation,UIPopoverPresentationControllerDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}
@property (nonatomic) BOOL isNewPhysician;
@property (nonatomic,weak) id<PfizerAddPhysicianProtocol> delegate;
@property (nonatomic,strong) UIImage *buildingPicImage;
@property (nonatomic,weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (nonatomic,weak) IBOutlet UITextField *firstNameTextField,*lastNameTextField,*hospitalNameTextField,*specialtyTextField,*cityTextField,*mobileNumber1TextField,*mobileNumber2TextField,*mobileNumber3TextField,*hospitalLandLineNumber1TextField,*hospitalLandLineNumber2TextField,*hospitalLandLineNumber3TextField,*emailID1TextField,*emailID2TextField,*emailID3TextField,*hospitalAddress1TextField,*hospitalAddress2TextField,*internetPerWeekTextField,*frequencyOfInternetOption1TextField,*receivingInformationOption1TextField,*receivingInformationOption2TextField,*receivingInformationOption3TextField,*numberOfPatientsPerWeekTextField,*ageGroupTextField,*onlineDigitalMediumOption1TextField,*onlineDigitalMediumOption2TextField,*onlineDigitalMediumOption3TextField,*yearOfGraduationTextField,*workLandLineNumber1TextField,*workLandLineNumber2TextField,*workLandLineNumber3TextField;
@property (weak, nonatomic) IBOutlet UITableView *hosListView;
@property (nonatomic,weak) IBOutlet UIButton *specialityButton,*ageGroupButton,*frequencyOfInternetButton,*receivingInformationButton,*onlineDigitalMediumButton;
@property (nonatomic,weak) IBOutlet UIImageView *buildingPicImageView;
@property (nonatomic,weak) IBOutlet MKMapView *mapView;
@property (nonatomic,strong) NSMutableDictionary *physicianDict,*currentLocationDict,*selectedLocationDict,*finalLocationDict,*cardData;
@property (nonatomic,strong) NSData *buidlingPicData;
-(IBAction)specialityButtonAction:(id)sender;
-(IBAction)ageGroupButtonAction:(id)sender;
-(IBAction)frequencyOfInternetButtonAction:(id)sender;
-(IBAction)receivingInformationButtonAction:(id)sender;
-(IBAction)onlineDigitalMediumButtonAction:(id)sender;
-(IBAction)saveAction:(id)sender;

@property(nonatomic)BOOL wasPresentingPopUp;
@end
