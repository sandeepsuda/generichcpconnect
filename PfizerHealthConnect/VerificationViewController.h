//
//  VerificationViewController.h
//  PharmConnect
//
//  Created by Utpal  on 09/05/16.
//  Copyright © 2016 Kumar, Utpal (Bangalore). All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SinchVerification/SinchVerification.h>
#import "PHCPRegistrationViewController.h"
#import "MBProgressHUD.h"
#import "Webservice.h"

@protocol VerificationDelegate <NSObject>
-(void)moveToTrainingPageVC;
@end

@import UIKit;

@protocol SINVerification;
@class SINUITextFieldPhoneNumberFormatter;

@interface VerificationViewController : UIViewController<UITextFieldDelegate>
{
    NSString *phoneNo;
    NSString *countryTextField;
    NSString  *firstNameTextField;
    NSString *lastNameTextField;
    //NSString *mobileTextField;
    NSString  *emailTextField;
    NSString *cityTextField;
    NSString  *photoTextField;
}
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) id<VerificationDelegate> delegate;
@property (strong, nonatomic) NSString *isoCountryCode;
@property (strong, nonatomic) SINUITextFieldPhoneNumberFormatter *formatter;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
//@property (weak, nonatomic) IBOutlet UITextView /**congratsText,*detailsText,*/*otpMessageText;
@property(weak,nonatomic)IBOutlet UILabel *congratsText, *detailsText, *lblOTP,*otpMessageText;
//@property id<SINVerification> verification;
@property (strong, nonatomic) id<SINVerification> verification;
@property (weak, nonatomic) IBOutlet UIButton *verifyCodeButton,*resendVerificationCodeButton;
@property (nonatomic,strong) NSString *mobileNumberWithCountryCode;
//@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UITextField *code;
@property (weak, nonatomic) IBOutlet UILabel *status,*phoneNoLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
- (IBAction)resendVerificationCode:(id)sender;
- (IBAction)verifyCode:(id)sender;

@property(nonatomic,weak) IBOutlet UIView *containerView;
@end
