//
//  LBAssignedTableViewCell.h
//  PfizerHealthConnect
//
//  Created by RajaSekhar on 22/12/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LBAssignedTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIImageView *profileImageView,*badgeImageView;
@property (nonatomic,weak) IBOutlet UILabel *dataCollectorNameLabel,*assignedNameLabel,*assignedPointsLabel,*completedNameLabel,*completedPointsLabel;
@property (nonatomic,weak) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UILabel *assignedLbl;
@property (weak, nonatomic) IBOutlet UILabel *completedLbl;
@end
