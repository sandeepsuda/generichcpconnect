//
//  LeaderBoardPopUpViewController.h
//  PfizerHealthConnect
//
//  Created by RajaSekhar on 14/12/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaderBoardPopUpViewController : UIViewController

@property (nonatomic,weak) IBOutlet UIView *subView;
@property (nonatomic,weak) IBOutlet UIImageView *badgeImageView;
@end
