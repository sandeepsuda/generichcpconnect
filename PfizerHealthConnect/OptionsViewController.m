//
//  OptionsViewController.m
//  PharmConnect
//
//  Created by Abhatia on 20/10/16.
//  Copyright © 2016 Kumar, Utpal (Bangalore). All rights reserved.
//

#import "OptionsViewController.h"

@interface OptionsViewController ()

@end

@implementation OptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = 10.0;
    self.tableView.estimatedRowHeight = 200;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithTextfield:(UITextField *)textfield patientsTxtFld:(UITextField*)patientsFld andOptions:(NSArray *)optionsArr;{
    if (self = [super init]) {
        self.selectedTextfield = textfield;
        self.noOfPatientsTxtFld = patientsFld;
        self.optionsArray = [NSMutableArray arrayWithArray:optionsArr];
         [self.optionsArray addObject:@"Cancel"];
    }
    return self;
}


#pragma mark - TableView Deleate and Datasource methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  60;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.optionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"%ld_%ld",(long)indexPath.section,(long)indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.numberOfLines = 0;
        [cell.textLabel sizeToFit];
    }
    if(indexPath.row == self.optionsArray.count-1){//Cancel
        cell.textLabel.textColor = [UIColor redColor];
    }else {
        cell.textLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = [self.optionsArray objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self.optionsArray[indexPath.row] compare:@"others" options:NSCaseInsensitiveSearch] == NSOrderedSame){
        [self dismissViewControllerAnimated:YES completion:nil];
        self.selectedTextfield.text = @"";
        [self.selectedTextfield setUserInteractionEnabled:true];
        [self.selectedTextfield becomeFirstResponder];
        return;
    }
    if(indexPath.row!=self.optionsArray.count-1){
        self.selectedTextfield.text = self.optionsArray[indexPath.row];
        if ([self.selectedTextfield.text isEqualToString:@"Pharmacist"] && self.noOfPatientsTxtFld != nil) {
            self.noOfPatientsTxtFld.userInteractionEnabled = NO;
            self.noOfPatientsTxtFld.backgroundColor = [UIColor lightGrayColor];
            self.noOfPatientsTxtFld.text = @"0";
        }else{
            self.noOfPatientsTxtFld.userInteractionEnabled = YES;
            self.noOfPatientsTxtFld.backgroundColor = [UIColor whiteColor];
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
