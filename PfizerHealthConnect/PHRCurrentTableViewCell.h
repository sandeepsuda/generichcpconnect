//
//  PHRCurrentTableViewCell.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 22/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNCirclePercentView.h"

@interface PHRCurrentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pharmacyNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet KNCirclePercentView *completionDateView;
@property (weak, nonatomic) IBOutlet UIProgressView *pharmacyProgressView;
@property (weak, nonatomic) IBOutlet UIProgressView *pharmacistProgressView;
@property (weak, nonatomic) IBOutlet UIButton *pharmacyDetailsBtn;
@property (weak, nonatomic) IBOutlet UIButton *pharmacistDetailsBtn;
@end
