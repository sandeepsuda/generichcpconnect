//
//  LeaderBoardViewController.h
//  IPadApp
//
//  Created by RajaSekhar on 04/11/16.
//  Copyright © 2016 RajaSekhar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeaderBoardTableViewCell.h"
#import <SDWebImage/SDImageCache.h>
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "LBAssignedTableViewCell.h"

@interface LeaderBoardViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
     BOOL searchActive;
}
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableDictionary *jsonDict;
@property (nonatomic,strong) NSMutableArray *jsonArray,*tempArray;
@property (nonatomic, strong) NSArray *searchList;
@property (nonatomic,strong) NSArray *searchArray;
@property (nonatomic,weak) IBOutlet UIButton *report1Button,*report2Button,*report3Button,*report4Button;
@property (nonatomic,weak) IBOutlet UIView *reportsSubView,*recordsCompletedSubView;
@property (nonatomic,weak) IBOutlet UILabel *recordsCompletedLabel;
@property (nonatomic,strong) NSString *completedMissionsString,*totalMissionsString,*completedPharmacyString,*totalPharmacystring;
-(IBAction)report1ButtonAction:(id)sender;
-(IBAction)report2ButtonAction:(id)sender;
-(IBAction)report3ButtonAction:(id)sender;
-(IBAction)report4ButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *serachField;
@end
