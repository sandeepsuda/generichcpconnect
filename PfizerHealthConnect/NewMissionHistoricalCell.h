//
//  NewMissionHistoricalCell.h
//  PfizerHealthConnect
//
//  Created by sandeep suda on 06/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMissionHistoricalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *completionDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *pointsLbl;
@property (weak, nonatomic) IBOutlet UIProgressView *physicianProgress;
@property (weak, nonatomic) IBOutlet UILabel *physicianNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *hospitalNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *percentageLbl;
@end
