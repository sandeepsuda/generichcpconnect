//
//  PharmacySelectionViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 23/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PharmacySelectionViewControllerDelegate <NSObject>
@required
-(void)PHRSubmitSucessfull;
@end

@interface PharmacySelectionViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) id<PharmacySelectionViewControllerDelegate> delegate;
@property (nonatomic, weak)IBOutlet UITextField *bricTextField;
@property (nonatomic, weak)IBOutlet UITextField *cityTextField;
@property (nonatomic, weak)IBOutlet UITextField *hospitalTextField;
@property (weak, nonatomic) IBOutlet UITextField *sortTextFieldPharmacy;
@property (weak, nonatomic) IBOutlet UITableView *PharmacyListTable;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIButton *clearAllBtn;
@property (weak, nonatomic) IBOutlet UIButton *missionNewBtn;
@property (nonatomic) NSInteger missionsLimitNumber;
@property (weak, nonatomic) IBOutlet UIButton *missionVerifyBtn;

-(void)submitSelectedPhysicians;
@end
