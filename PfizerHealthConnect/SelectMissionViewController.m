//
//  SelectMissionViewController.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 07/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "SelectMissionViewController.h"
#import "PhysicianSelectionTableViewCell.h"
#import "MBProgressHUD.h"
#import "Webservice.h"
#import "MenuList.h"
#import "NSDictionary+Dictionary.h"
#import "AppDelegate.h"
#define PhysicianTag 10
#define HospitalTag 20
#define AddressTag 30
#define ImageViewTag 40

@interface SelectMissionViewController ()<UIActionSheetDelegate,UIAlertViewDelegate>
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (nonatomic,retain) NSMutableArray *tempPharmaciesArray,*pharmaciesArray,*seletedPharmacies;
@property (nonatomic,strong) NSDictionary *responseData;
@property (nonatomic) BOOL assigned;
@end

@implementation SelectMissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tempPharmaciesArray=[NSMutableArray array];
    _pharmaciesArray=[NSMutableArray array];
    _seletedPharmacies=[NSMutableArray array];
    self.submitBtn.layer.cornerRadius = 4.0;
    self.filterButton.layer.cornerRadius = 4.0;
    self.searchBtn.layer.cornerRadius = 4.0;
    self.clearAllBtn.layer.cornerRadius = 4.0;
    [self.hospitalTextField addTarget:self
                               action:@selector(textFieldDidChange:)
                     forControlEvents:UIControlEventEditingChanged];
    [self.cityTextField addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    [self.bricTextField addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    [self.physicianTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.PharmacyListTable.estimatedRowHeight = 200;
    self.PharmacyListTable.rowHeight = UITableViewAutomaticDimension;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
}
-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SearchAssigned"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SearchUnAssigned"];
    [self.filterButton setTitle:@"Unassigned" forState:UIControlStateNormal];
    self.clearAllBtn.hidden = NO;
    self.submitBtn.hidden = NO;
    self.filterButton.layer.cornerRadius = 5;
    self.bricTextField.text = @"";
    self.cityTextField.text = @"";
    self.hospitalTextField.text = @"";
    self.physicianTextField.text = @"";
    [_seletedPharmacies removeAllObjects];
    [self.PharmacyListTable reloadData];
    [self showProgressView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self serviceCall:@"UNASSIGNED"];
        self.clearAllBtn.hidden = NO;
        self.allPhysicianBool = YES;
        self.assignedPhysicianBool = NO;
        self.unAssignedPhysicianBool = YES;
    });
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.seletedPharmacies = [[NSMutableArray alloc] init];
}
-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = @"Loading...";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.pharmaciesArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"SelectMissionCell";
    PhysicianSelectionTableViewCell *cell =
    (PhysicianSelectionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    NSDictionary *dict;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    dict =self.pharmaciesArray[indexPath.row];
    //cell.physicianNameLbl.text = [NSString stringWithFormat:@"%@ %@",[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
    cell.physicianNameLbl.text = [dict stringForKey:@"HospitalClinicName"];
    NSString *str=[NSString stringWithFormat:@"\n%@, %@ \n\n%@ %@",[dict stringForKey:@"HospitalAddress1"],[dict stringForKey:@"HospitalAddress2"],[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
    NSString *strA = [NSString stringWithFormat:@"%@, %@",[dict stringForKey:@"HospitalAddress1"],[dict stringForKey:@"HospitalAddress2"]];
    NSString *strB = [NSString stringWithFormat:@"%@ %@",[dict stringForKey:@"FirstName"],[dict stringForKey:@"LastName"]];
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"HelveticaNeue-Light" size:15]
                  range:[str rangeOfString:strA]];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]
                  range:[str rangeOfString:strB]];
    cell.hospitalDetails.attributedText = hogan;
//    cell.hospitalDetails.text = [NSString stringWithFormat:@"%@ %@ \n%@ %@",[dict stringForKey:@"HospitalAddress1"],[dict stringForKey:@"HospitalAddress2"],];
    if (self.allPhysicianBool || self.unAssignedPhysicianBool) {
        cell.checkBtn.hidden = NO;
    }else{
        cell.checkBtn.hidden = YES;
    }
    if ([[dict stringForKey:@"Flag"] isEqualToString:@"2"]) {
        cell.contentView.backgroundColor = [UIColor lightGrayColor];
    }else{
        cell.contentView.backgroundColor = [appDelegate colorWithHexString:BlueColor];
    }
    if (![[dict stringForKey:@"DoctorId"] isEqualToString:@""]) {
        NSString *doctorIdString = [dict stringForKey:@"DoctorId"];
        NSString *firstLetterDoctorIdString = [doctorIdString substringToIndex:1];
        if ([firstLetterDoctorIdString isEqualToString:@"U"]) {
            cell.checkBtn.hidden = NO;
        }else{
            cell.checkBtn.hidden = YES;
        }
    }else{
        cell.checkBtn.hidden = YES;
    }
    
    cell.checkBtn.tag = 100;
    [cell.checkBtn setImage:[UIImage imageNamed:@"checkbox"] forState: UIControlStateNormal];
    [cell.checkBtn setImage:[UIImage imageNamed:@"checboxselected"] forState: UIControlStateSelected];
    [cell.checkBtn addTarget:self action:@selector(accessoryButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.checkBtn setSelected:NO];
    if ([_seletedPharmacies containsObject:dict]) {
        [cell.checkBtn setSelected:YES];
    }
    return cell;
}
-(void)accessoryButtonAction:(id)sender{
    UITableViewCell *cell =(UITableViewCell*)((UIButton *)sender).superview.superview;
    NSIndexPath *indexPath = [self.PharmacyListTable indexPathForCell:cell];
    NSDictionary *dict=[_pharmaciesArray objectAtIndex:indexPath.row];
    if (![self.seletedPharmacies containsObject:dict]) {
        [_seletedPharmacies addObject:dict];
    }else{
        [_seletedPharmacies removeObject:dict];
    }
    [self.PharmacyListTable beginUpdates];
    [self.PharmacyListTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.PharmacyListTable endUpdates];
}

-(IBAction)selectMissionFilterButtonAction:(id)sender{
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *all = [UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self.filterButton setTitle:@"All" forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SearchAll"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SearchUnAssigned"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SearchAssigned"];
        self.clearAllBtn.hidden = NO;
        self.submitBtn.hidden = NO;
        self.allPhysicianBool = YES;
        self.assignedPhysicianBool = NO;
        self.unAssignedPhysicianBool = YES;
        [self showProgressView];
        [self serviceCall:@"ALL"];
    }];
    UIAlertAction *assigned = [UIAlertAction actionWithTitle:@"Assigned" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SearchAssigned"];
                                                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SearchUnAssigned"];
                                                         self.assigned = YES;
                                                         [self.filterButton setTitle:@"Assigned" forState:UIControlStateNormal];
                                                         self.clearAllBtn.hidden = YES;
                                                         self.submitBtn.hidden = YES;
                                                         self.allPhysicianBool = NO;
                                                         self.assignedPhysicianBool = YES;
                                                         self.unAssignedPhysicianBool = NO;
                                                         [self showProgressView];
                                                         [self serviceCall:@"ASSIGNED"];
                                                     }];
    UIAlertAction *unAssigned = [UIAlertAction actionWithTitle:@"Open/Unassigned" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SearchUnAssigned"];
                                                           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SearchAssigned"];
                                                           [self.filterButton setTitle:@"Unassigned" forState:UIControlStateNormal];
                                                           self.clearAllBtn.hidden = NO;
                                                           self.submitBtn.hidden = NO;
                                                           self.allPhysicianBool = YES;
                                                           self.assignedPhysicianBool = NO;
                                                           self.unAssignedPhysicianBool = YES;
                                                           [self showProgressView];
                                                           [self serviceCall:@"UNASSIGNED"];
                                                       }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    [alert addAction:all];
    [alert addAction:assigned];
    [alert addAction:unAssigned];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.filterButton;
    popPresenter.sourceRect = self.filterButton.bounds;
    [self presentViewController:alert animated:YES completion:nil];
//    NSLog(@"button clicked:");
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Filter by Physician" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Cancel"otherButtonTitles:@"ALL",@"Assigned",@"Open/UnAssigned",@"New Physician",nil];
//    [actionSheet showFromRect:[(UIButton *)sender frame] inView:self.view animated:YES];
}
- (IBAction)clearAction:(id)sender {
    [self.view endEditing:YES];
    [_seletedPharmacies removeAllObjects];
    [self.PharmacyListTable reloadData];
}
- (IBAction)searchPharmaciesList:(id)sender {
    [self.view endEditing:YES];
    NSData *requestData = [self getSearchPostData];
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:EMAIL_STRING];
    BOOL assignedBool = [[NSUserDefaults standardUserDefaults] boolForKey:@"SearchAssigned"];
    BOOL unAssignedBool = [[NSUserDefaults standardUserDefaults] boolForKey:@"SearchUnAssigned"];
    NSString *urlString;
    if (assignedBool == YES) {
        urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,PHYSICIAN_SELECTION_SEARCH,mobileNumber,@"ASSIGNED"];
    }else if (unAssignedBool == YES){
        urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,PHYSICIAN_SELECTION_SEARCH,mobileNumber,@"UNASSIGNED"];
    }else{
        urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,PHYSICIAN_SELECTION_SEARCH,mobileNumber,@"ALL"];
    }
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        if (error == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (data) {
                    self.pharmaciesArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    [self.PharmacyListTable reloadData];
                    [self.PharmacyListTable beginUpdates];
                    [self.PharmacyListTable setContentOffset:CGPointZero animated:NO];
                    [self.PharmacyListTable endUpdates];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Search Count" message:[NSString stringWithFormat:@"Physicians search count : %d",(int)self.pharmaciesArray.count] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            });
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }] resume];
}

-(NSData *)getSearchPostData{
    //{"hospitalClinicSearchString":"ADN","City":"", "Brick":"", "cityOfPractice":""}
    NSString *strData = @"{";
    strData = [strData stringByAppendingString:[NSString stringWithFormat:@"\"hospitalClinicSearchString\" : \"%@\"",self.hospitalTextField.text]];
    strData = [strData stringByAppendingString:@","];
    strData = [strData stringByAppendingString:[NSString stringWithFormat:@"\"City\" : \"%@\"",self.cityTextField.text]];
    strData = [strData stringByAppendingString:@","];
    strData = [strData stringByAppendingString:[NSString stringWithFormat:@"\"Brick\" : \"%@\"",self.bricTextField.text]];
    strData = [strData stringByAppendingString:@","];
    strData = [strData stringByAppendingString:[NSString stringWithFormat:@"\"doctorSearchString\" : \"%@\"",self.physicianTextField.text]];
    strData = [strData stringByAppendingString:@"}"];
    NSData* data = [strData dataUsingEncoding:NSUTF8StringEncoding];
    return data;
}


- (IBAction)submitAction:(id)sender {
    [self.view endEditing:YES];
    [self submitSelectedPhysicians];
}

-(void)submitSelectedPhysicians{
        [self showProgressView];
        NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:EMAIL_STRING];
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,PHYSICIAN_SELECTION_SUBMIT,mobileNumber];
        [Webservice postWithURlString:urlString withParameters:[self getPostData] sucess:^(id data) {
            //Success
            [self.mbProgressView hideAnimated:YES];
            if (data) {
                NSString *message = [data valueForKey:@"Message"];
                if (message) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:message preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                        if ([self.delegate respondsToSelector:@selector(submitSucessfull)]) {
                            [self.delegate submitSucessfull];
                        }
                    }];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }else{
                    if ([self.delegate respondsToSelector:@selector(submitSucessfull)]) {
                        [self.delegate submitSucessfull];
                    }
                }
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }];
//    }else{
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SELECT_MAXIMUM_NUMBER_OF_MISSIONS_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//        [alert addAction:okAction];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
}

-(NSMutableArray *)getPostData{
    // [{"DoctorId":"PH100"},{"DoctorId":"PH101"}]
    NSMutableArray *arry=[[NSMutableArray alloc] init];
    for (NSDictionary *pharmDict in _seletedPharmacies) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        [dict setObject:[pharmDict objectForKey:@"DoctorId"] forKey:@"DoctorId"];
        [arry addObject:dict];
    }
    return arry;
}

- (IBAction)sortingAction:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
//    UIAlertAction *latest = [UIAlertAction actionWithTitle:LATEST_STRING style:UIAlertActionStyleDefault
//                                                   handler:^(UIAlertAction * action) {
//                                                       [self SearchWithLatest];
//                                                   }];
    UIAlertAction *ascending = [UIAlertAction actionWithTitle:ALPHABETICAL_ASCENDING_STRING style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self SearchWithAscending];
                                                      }];
    UIAlertAction *decending = [UIAlertAction actionWithTitle:ALPHABETICAL_DESCENDING_STRING style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          [self SearchWithDecending];
                                                      }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                   }];
    //[alert addAction:latest];
    [alert addAction:ascending];
    [alert addAction:decending];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.sortTextFieldPharmacy;
    popPresenter.sourceRect = self.sortTextFieldPharmacy.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Search Methods

-(void)SearchWithLatest{
    self.sortTextFieldPharmacy.text = LATEST_STRING;
    [self getLatestPharmaciesData];
}
-(void)getLatestPharmaciesData{
    [self showProgressView];
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:EMAIL_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",
                          KBASE_URL,PHYSICIAN_SELECTION_SEARCH,mobileNumber];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            //self.responseData = (NSDictionary*)data;
            //self.pharmaciesArray = [self.responseData objectForKey:@"DoctorBase"];
            self.pharmaciesArray = (NSMutableArray *)data;
            [self.PharmacyListTable reloadData];
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(void)SearchWithAscending{
    self.sortTextFieldPharmacy.text = ALPHABETICAL_ASCENDING_STRING;
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"HospitalClinicName" ascending:YES selector:@selector(localizedCompare:)];
    if (self.pharmaciesArray.count > 0) {
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.pharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self.PharmacyListTable reloadData];
    }else{
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.tempPharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]]; // At HOME
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.pharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self.PharmacyListTable reloadData];
    }
}
-(void)SearchWithDecending{
    self.sortTextFieldPharmacy.text = ALPHABETICAL_DESCENDING_STRING;
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"HospitalClinicName" ascending:NO selector:@selector(localizedCompare:)];
    if (self.pharmaciesArray.count > 0) {
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.pharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self.PharmacyListTable reloadData];
    }else{
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.tempPharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        self.pharmaciesArray=[NSMutableArray arrayWithArray:[self.pharmaciesArray sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self.PharmacyListTable reloadData];
    }
}

-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.cityTextField || textField == self.bricTextField || textField == self.hospitalTextField || textField == self.physicianTextField) {
        if ([textField.text isEqualToString:@""]){
            self.pharmaciesArray = [NSMutableArray arrayWithArray:self.tempPharmaciesArray];
            [self.PharmacyListTable reloadData];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.hospitalTextField || textField == self.cityTextField || textField == self.physicianTextField)
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:ONLY_ALPHABETS_WITHSPACE_MESSAGE] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
        return YES;
    }else if (textField == self.bricTextField){
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789"] invertedSet] invertedSet];
        NSString *output = [string stringByTrimmingCharactersInSet:[invalidCharSet invertedSet]];
        return [string isEqualToString:output];
    }
    else{
        return YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.sortTextFieldPharmacy) {
        [self sortingAction:nil];
        return NO;
    }
    return YES;
}
#pragma mark - Service Call Methods

-(void)serviceCall:(NSString *)filterString{
    NSString *emailID = [[NSUserDefaults standardUserDefaults] objectForKey:EMAIL_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,PHYSICIAN_SELECTION_PHYSICIAN_LIST,emailID,filterString];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            self.pharmaciesArray = (NSMutableArray *)data;
            _tempPharmaciesArray=[NSMutableArray arrayWithArray:self.pharmaciesArray];
            if ([filterString isEqualToString:@"UNASSIGNED"] && self.pharmaciesArray.count == 0) {
                self.submitBtn.hidden = YES;
                self.clearAllBtn.hidden = YES;
            }
            [self.PharmacyListTable reloadData];
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
@end
