//
//  AppDelegate.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 25/10/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic,strong) NSString *pharmacyId;
@property (nonatomic,strong) NSString *pharmacistTransactionID;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic,strong)NSMutableArray *arrMasterQuiz;
@property(nonatomic,assign)BOOL isVerify;

//Offline
@property(nonatomic,strong)NSMutableArray *physicianNewMissionArray;
@property(nonatomic,strong)NSMutableArray *physicianNewMissionConsentArray;
@property(nonatomic,strong)NSMutableArray *physicianNewMissionCurrentPhysicianList;
@property(nonatomic,strong)NSMutableDictionary *myTrackerDetails;
-(NSString *)getFilePathForPhysicianNewMissionCurrentList;

@property(nonatomic,strong)NSMutableArray *pharmacyNewMissionArray;
@property(nonatomic,strong)NSMutableArray *pharmacyVerifyNewMissionArray;
@property(nonatomic,strong)NSMutableArray *pharmacistNewMissionCurrentPharmacistList;
@property(nonatomic,strong)NSMutableArray *pharmacistVerifyMissionCurrentPharmacistList;

@property(nonatomic,strong)NSMutableArray *pharmacistNewMissionArray;
@property(nonatomic,strong)NSMutableArray *pharmacistverifyMissionArray;
@property(nonatomic,strong)NSMutableArray *pharmacistNewMissionConsentArray;
@property(nonatomic,strong)NSMutableArray *finalNewPharmacyArray;
@property(nonatomic,strong)NSMutableArray *finalVerifyPharmacyArray;
-(NSString *)getFilePathForPharmacistNewMissionCurrentList;
-(NSString *)getFilePathForPharmacistVerifyMissionCurrentList;
//Offline

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(UIColor*)colorWithHexString:(NSString*)hex;
@end

