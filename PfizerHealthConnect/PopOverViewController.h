//
//  PopOverViewController.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 26/10/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopOverViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray *arrOptions;
@property (nonatomic,weak)IBOutlet UITableView *tableView;
@property (nonatomic,weak) UITextField *countryField;
@property (nonatomic,weak) UITextField *countryCodeField;

@property(nonatomic,weak)UITextField *txtFieldToBeSet;
@end
