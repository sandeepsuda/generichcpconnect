//
//  TrainingMaterialsViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 04/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrainingMaterialsViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webContentView;
@property (nonatomic,strong) NSString *fileName;
@end
