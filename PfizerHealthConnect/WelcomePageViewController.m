//
//  WelcomePageViewController.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 02/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "WelcomePageViewController.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import "MenuList.h"
#import "LeftViewController.h"

@interface WelcomePageViewController ()

@end

@implementation WelcomePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:WELCOME_STRING];
    [defaults removeObjectForKey:@"QuizPage"];
    self.navigationItem.title = @"Welcome";
    self.navigationController.navigationBar.barTintColor = [appDelegate colorWithHexString:@"3A4F5A"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:20]}];
    self.submitButton.layer.cornerRadius = 4.0;
    NSString *str=@"Your details and score has been shared with Supervisor for review.\n\nWithin 2 business days, Supervisor will have a call with you and accordingly share the Authentication Code to access  the next set of screens of HCPConnect\n\nIf you are not receiving any call within 2 business days, please write to\nDLE-PhmConnect@in.imshealth.com";
    NSString *strA = @"Your details and score has been shared with Supervisor for review.\n\nWithin 2 business days, Supervisor will have a call with you and accordingly share the Authentication Code to access  the next set of screens of HCPConnect\n\nIf you are not receiving any call within 2 business days, please write to";
    NSString *strB = @"DLE-PhmConnect@in.imshealth.com";
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:str];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"HelveticaNeue-Light" size:20]
                  range:[str rangeOfString:strA]];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"Helvetica Neue" size:20]
                  range:[str rangeOfString:strB]];
    self.contentTxtLbl.textColor = [appDelegate colorWithHexString:@"3A4F5A"];
    self.contentTxtLbl.attributedText = hogan;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
}
-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clearText:(id)sender {
    self.accessCodeTextField.text = nil;
}
- (IBAction)submitAction:(id)sender {
    [self.view endEditing:YES];
    NSString  *mobileNumber = [[NSUserDefaults standardUserDefaults]stringForKey:EMAIL_STRING];
    NSString *accessCode = self.accessCodeTextField.text;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",
                          KBASE_URL,HCP_Verify_WelcomePage,mobileNumber,accessCode];
    [self showProgressView];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            if ([data[@"Message"] isEqualToString:@"SUCCESS"]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Thank you for providing the right Code. Welcome to HCPConnect." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                              {
                                                  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                  LeftViewController *tracker =[storyboard instantiateViewControllerWithIdentifier:@"LeftViewController"];
                                                  [self.navigationController pushViewController:tracker animated:YES];
                                              }];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:@"Please enter the right authentication code otherwise Contact Supervisor." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
           }
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OOPS" message:error.description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

#pragma mark - ProgressView Method

-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = @"Loading...";
}


#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - Keyboard Hide & Show Methods

- (void)keyboardDidShow:(NSNotification *)notification
{
   // [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    self.view.backgroundColor = [UIColor whiteColor];
    //[self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
