//
//  PharmacyTrackerViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 21/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNCirclePercentView.h"
#define TakenCountLblH 57
#define SubmitLalH 29

@protocol PharmacyTrackerViewControllerDelegate <NSObject>
@required
-(void)addPharmacyMissionAction:(BOOL)verify;
-(void)setPharmacyUserDetails:(NSDictionary*)userInfo;
-(void)showPharmacyDetails:(NSMutableDictionary*)pharmacyDict;
-(void)showPharmacistDetails:(NSMutableDictionary*)pharmacistDict;
@end

@interface PharmacyTrackerViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    BOOL isCurrentData;
    BOOL searchActive;
}
@property(weak,nonatomic) id<PharmacyTrackerViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *missionNewBtn;
@property (weak, nonatomic) IBOutlet UIButton *verifyMissionBtn;
@property (weak, nonatomic) IBOutlet UIButton *currentBtn;
@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;
@property (nonatomic, strong) NSMutableArray *pharmacyList,*tempArray;
@property (nonatomic,strong) NSArray *searchArray;
@property (weak, nonatomic) IBOutlet UITableView *pharmacyTblView;
@property (weak, nonatomic) IBOutlet UILabel *availableCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *takenCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *submitCountLbl;
@property (weak, nonatomic) IBOutlet UIView *scoreView;
@property (weak, nonatomic) IBOutlet UIButton *historicalBtn;
@property (weak, nonatomic) IBOutlet UILabel *pointsGainedLbl;
@property (nonatomic, strong) NSDictionary *mainTrackerData;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@end
