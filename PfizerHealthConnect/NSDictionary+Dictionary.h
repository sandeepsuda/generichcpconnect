//
//  NSDictionary+Dictionary.h
//  PharmConnect
//
//  Created by Local user on 18/06/16.
//  Copyright © 2016 Kumar, Utpal (Bangalore). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Dictionary)

-(NSString*)stringForKey:(NSString*)key;


@end
