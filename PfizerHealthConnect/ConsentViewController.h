//
//  ConsentViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 04/01/17.
//  Copyright © 2017 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MenuList.h"
#import "HCPSignatureView.h"
#import "MBProgressHUD.h"

@protocol PhysicianConsentProtocol <NSObject>

-(void)consentSubmitMethodAction:(UIImage *)image;
-(void)consentSuccessMethodAction;

@end

@interface ConsentViewController : UIViewController{
    HCPSignatureView *signView;
    UIButton *closeBtn;
}

@property (nonatomic,weak) IBOutlet UILabel *consentHeadingLabel,*consentBulletListLabel,*consentDescription1Label;
@property (nonatomic,weak) IBOutlet UITextView *consentDescription2TextView;
@property (nonatomic,strong) NSData *signatureData,*screenShotData;
@property (weak, nonatomic) IBOutlet UIView *consentViewPartA;
@property (strong, nonatomic) IBOutlet UIView *consentViewPartB;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSMutableArray *checkOptionsButton;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (nonatomic,strong) NSString *signatureDataString;
@property (nonatomic,weak) id <PhysicianConsentProtocol> consentDelegate;
@property (nonatomic,strong) NSMutableArray *interestedArrayList;
@property (nonatomic,strong) UIImage *signatureImage;
@property (nonatomic,strong) NSMutableDictionary *passedPhysicianDict,*signatureDict;
@property(nonatomic, strong) UIView *consentViewPartOne;
@property(nonatomic, strong) UIView *consentViewPartTWo;
@property(nonatomic, strong) NSString *switchStateString;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *specialityTxtField;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UIView *signatureBG;
@property (weak, nonatomic) IBOutlet UISwitch *emailCheckBtn;
@property (weak, nonatomic) IBOutlet UITextField *mobileTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *workNumberTxtFld;
@property (nonatomic,weak) IBOutlet UILabel *pleaseEmailMeLabel,*companyAddressLabel;
@property (nonatomic,weak) IBOutlet UILabel *firstNameLabel, *lastNameLabel, *specialityLabel, *emailIDLabel, *mobileLabel, *workNumberLabel, *interestedLabel, *scientificLabel, *medicalLabel, *clinicalLabel, *educationLabel, *webinarLabel, *meetingLabel;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
-(void)passedDictFromAddPhysician:(NSMutableDictionary *)dict;
-(IBAction)switchButtonAction:(UISwitch *)sender;
//offline
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
-(void)removePhysicianIfSavedForOfflineMode;
@end
