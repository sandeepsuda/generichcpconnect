//
//  WelcomePageViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 02/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface WelcomePageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *accessCodeTextField;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITextView *detailsTextView;
@property (weak, nonatomic) IBOutlet UILabel *contentTxtLbl;
@end
