//
//  PHRHistoricalTableViewCell.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 22/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "PHRHistoricalTableViewCell.h"

@implementation PHRHistoricalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
