//
//  PHRHistoricalTableViewCell.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 22/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHRHistoricalTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pharmacyNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *pointsLbl;
@property (weak, nonatomic) IBOutlet UILabel *completionDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIProgressView *pharmacyProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *pharmacistProgressBar;

@end
