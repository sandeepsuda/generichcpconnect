//
//  ViewController.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 25/10/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCPExistingUserViewController.h"
#import "VerificationViewController.h"

@interface PHCPHelloScreenViewController : UIViewController<ExistingUserProtocol,VerificationDelegate>{
    
}
@property (nonatomic, assign) BOOL SingleSignOnCheck;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property(nonatomic, strong) UILabel *helloLbl;
@property(nonatomic, strong) UILabel *welcomeTxtLbl;
@property(nonatomic, strong) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnSignin;
@end

