//
//  VerifyPharmacyDetailsViewController.m
//  PfizerHealthConnect
//
//  Created by Abhatia on 23/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#define MyYellow [UIColor colorWithRed:241.0/255.0 green:195.0/255.0 blue:79.0/255.0 alpha:1.0 ]

#import "VerifyPharmacyDetailsViewController.h"
#import "MenuList.h"
#import "LeftViewController.h"
#import "PopOverViewController.h"
#import "AddPharmacistVCTableViewCell.h"
#import "LeftViewController.h"
#import "NSDictionary+Dictionary.h"
#import "MBProgressHUD.h"
#import "Webservice.h"
#import "AppDelegate.h"
#import <SDWebImage/SDImageCache.h>
#import "UIImageView+WebCache.h"

@interface VerifyPharmacyDetailsViewController ()<MKMapViewDelegate,UIActionSheetDelegate,CLLocationManagerDelegate>
@property(nonatomic,assign)BOOL framesAreSet;
@property (nonatomic,strong) LeftViewController *leftVC;
@property(nonatomic,assign)BOOL isFromNewPharmacist;
@property(nonatomic,assign)BOOL isVerifyLandLineNumbers;
@property(nonatomic,assign)BOOL isVerifyMobileNumbers;
@property(nonatomic,assign)BOOL isPharmacist;
@end

@implementation VerifyPharmacyDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self setUpTopButtons];
    //self.buildingPicImageView.backgroundColor = [UIColor orangeColor];
    self.locationManager = [[CLLocationManager alloc]init];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    //[MenuList networkChecking];
    // register for keyboard notifications
    [self setUpPharmacyDetails];
    self.mobileNumbersDict = [[NSMutableDictionary alloc] init];
    self.finalLocationDict = [[NSMutableDictionary alloc] init];
    self.passedPharmacistDict = [[NSMutableDictionary alloc] init];
    self.currentLocationDict = [[NSMutableDictionary alloc] init];
    self.selectedLocationDict = [[NSMutableDictionary alloc] init];
    self.mapView.userInteractionEnabled = YES;
    UILongPressGestureRecognizer *mapViewGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longpressToGetLocation:)];
    [self.mapView addGestureRecognizer:mapViewGesture];
    self.leftVC = (LeftViewController *)self.delegate;
    self.buildingPicImageView.backgroundColor = [appDelegate colorWithHexString:YellowColor];
    self.XYDetailsView.backgroundColor = self.view.backgroundColor;
    self.submitBtn.layer.cornerRadius = 4.0;
    self.verifyPharmacySubmitBtn.layer.cornerRadius = 4.0;
    self.verifyPharmacistSubmitBtn.layer.cornerRadius = 4.0;
    self.topView.backgroundColor = [UIColor clearColor];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.pharmacistsTbl.backgroundColor = [UIColor clearColor];
    self.pharmacistsTbl.separatorStyle = UITableViewCellSeparatorStyleNone;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.pharmacistView addGestureRecognizer:tap];
    [self.pharmacyView addGestureRecognizer:tap];
    UINib *cellNib = [UINib nibWithNibName:@"AddPharmacistVCTableViewCell" bundle:nil];
    [self.pharmacistsTbl registerNib:cellNib forCellReuseIdentifier:@"AddPharmacistVCTableViewCellIdentifier"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    if ([self.passedPharmacyString isEqualToString:@"PHARMACY"]) {
        [self.pharmacyTopButton setBackgroundColor:MyYellow];
        [self.pharmacistTopButton setBackgroundColor:[UIColor clearColor]];
        self.pharmacyTopButton.selected = YES;
        self.pharmacistTopButton.selected = NO;
        //self.pharmacistView.hidden = YES;
        self.XYDetailsView.hidden = YES;
        self.pharmacyView.hidden = NO;
        self.pharmacistView.hidden = YES;
    }else if ([self.passedPharmacyString isEqualToString:@"PHARMACIST"]){
        [self.pharmacistTopButton setBackgroundColor:MyYellow];
        [self.pharmacyTopButton setBackgroundColor:[UIColor clearColor]];
        self.pharmacyTopButton.selected = NO;
        self.pharmacistTopButton.selected = YES;
        self.XYDetailsView.hidden = NO;
        self.pharmacyView.hidden = YES;
        self.pharmacistView.hidden = YES;
        //if(!self.XYDetailsView.superview){
            [self.view addSubview:self.XYDetailsView];
            [NSLayoutConstraint activateConstraints: @[
                                                       [self.XYDetailsView.topAnchor constraintEqualToAnchor:self.horizontalLineView.bottomAnchor],
                                                       [self.XYDetailsView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                       [self.XYDetailsView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                       [self.XYDetailsView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                       ]];
        //}
        
        self.XYDetailsView.translatesAutoresizingMaskIntoConstraints = FALSE;
        [self reloadPharmacistsList];
    }
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!self.framesAreSet) {
        self.framesAreSet = YES;
    }else{
        CGRect tmpFrame = self.scoreView.frame;
        self.scoreView.layer.cornerRadius = tmpFrame.size.height/2;
    }
}

#pragma mark - Custom Methods

-(void)dismissKeyboard{
    [self.view endEditing:YES];
}
-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = @"Loading...";
}

#pragma mark - Initial SetUp Methods

-(void)setUpPharmacistDetails{
    self.isPharmacist = YES;
    self.firstNameTextField.text = [self.passedPharmacistDict stringForKey:@"FirstName"];
    self.lastNameTextField.text = [self.passedPharmacistDict stringForKey:@"LastName"];
    self.mobileNo1TextField.text = [self.passedPharmacistDict stringForKey:@"MobileNumber1"];
    self.mobileNo2TextField.text = [self.passedPharmacistDict stringForKey:@"MobileNumber2"];
    self.mobileNo3TextField.text = [self.passedPharmacistDict stringForKey:@"MobileNumber3"];
    self.yearOfGragTextField.text = [self.passedPharmacistDict stringForKey:@"YearOfGraduation"];
    self.shiftTimingsFromTextField.text = [self.passedPharmacistDict stringForKey:@"ShiftTimingsFrom"];
    self.shiftTimingsToTextField.text = [self.passedPharmacistDict stringForKey:@"ShiftTimingsTo"];
    self.emailNo1TextField.text = [self.passedPharmacistDict stringForKey:@"EmailID1"];
    self.emailNo2TextField.text = [self.passedPharmacistDict stringForKey:@"EmailID2"];
    self.emailNo3TextField.text = [self.passedPharmacistDict stringForKey:@"EmailID3"];
}

-(void)setUpPharmacyDetails{
    self.isPharmacist = NO;
    self.pharmacyNameTextField.text = [self.passedPharmacyDict stringForKey:@"PharmacyName"];
    self.address1TextField.text = [self.passedPharmacyDict stringForKey:@"Address1"];
    self.address2TextField.text = [self.passedPharmacyDict stringForKey:@"Address2"];
    self.cityTextField.text = [self.passedPharmacyDict stringForKey:@"City"];
    self.countryTextField.text = [self.passedPharmacyDict stringForKey:@"Country"];
    self.ownerNameTextField.text = [self.passedPharmacyDict stringForKey:@"OwnerName"];
    self.noOfPhysiciansTextField.text = [self.passedPharmacyDict stringForKey:@"NoOfPharmacists"];
    self.landlineNumberTextField.text = [self.passedPharmacyDict stringForKey:@"LandLineNumber1"];
    self.landlineNumberTextField2.text = [self.passedPharmacyDict stringForKey:@"LandLineNumber2"];
    self.landlineNumberTextField3.text = [self.passedPharmacyDict stringForKey:@"LandLineNumber3"];
    
    if (![[self.passedPharmacyDict stringForKey:@"BuildingPic"] isEqualToString:@"0"]) {
        if (![[self.passedPharmacyDict stringForKey:@"BuildingPic"] isEqualToString:@""]) {
            //[self.buildingPicImageView sd_setImageWithURL:[NSURL URLWithString:[self.passedPharmacyDict stringForKey:@"BuildingPic"]]];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                UIImage *imagename = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self.passedPharmacyDict stringForKey:@"BuildingPic"]]]];
                // Then to set the image it must be done on the main thread
                dispatch_sync( dispatch_get_main_queue(), ^{
                    if (imagename) {
                        [self.buildingPicImageView setImage: imagename];
                    }else{
                        NSData *data = [[NSData alloc]initWithBase64EncodedString:[self.passedPharmacyDict stringForKey:@"BuildingPic"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                        if (data) {
                            self.buildingPicImageView.image = [UIImage imageWithData:data];
                        }
                    }
                });
            });
        }else{
            self.buildingPicImageView.image = self.buildingPicImage;
        }
    }

    float latitudeString = (float)[[self.passedPharmacyDict valueForKey:LATITUDE_STRING] floatValue];
    float longitudeString = (float)[[self.passedPharmacyDict valueForKey:LONGITUDE_STRING] floatValue];
    if (latitudeString && longitudeString) {
        [self.locationManager stopUpdatingLocation];
        self.mapView.delegate = self;
        float spanX = 0.1;
        float spanY = 0.1;
        [self.mapView removeAnnotations:self.mapView.annotations];
        CLLocation *locationAtual = [[CLLocation alloc] initWithLatitude:latitudeString longitude:longitudeString];
        MKCoordinateSpan span = self.mapView.region.span;
        span.latitudeDelta=spanX;
        span.longitudeDelta=spanY;
        MKCoordinateRegion region = MKCoordinateRegionMake(locationAtual.coordinate, span);
        self.mapView.region = region;
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitudeString, longitudeString);
        point.coordinate = coordinate;
        point.title = @"Where am I?";
        point.subtitle = @"selected loaction!";
        region = MKCoordinateRegionMakeWithDistance(coordinate, 400, 400);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
        [self.mapView removeAnnotations:self.mapView.annotations];
        [self.mapView addAnnotation:point];
    }else{
        self.mapView.delegate = self;
        self.mapView.showsUserLocation = YES;
        [self.locationManager startUpdatingLocation];
    }
}

-(void)addXYDetailsView{
    [self.leftVC showHideNavigationItems];
    [self.pharmacistTopButton setBackgroundColor:MyYellow];
    [self.pharmacyTopButton setBackgroundColor:[UIColor clearColor]];
    self.pharmacyTopButton.selected = NO;
    self.pharmacistTopButton.selected = YES;
    self.XYDetailsView.hidden = NO;
    self.pharmacyView.hidden = YES;
    self.pharmacistView.hidden = YES;
    if(!self.XYDetailsView.superview){
        [self.view addSubview:self.XYDetailsView];
        [NSLayoutConstraint activateConstraints: @[
                                                   [self.XYDetailsView.topAnchor constraintEqualToAnchor:self.horizontalLineView.bottomAnchor],
                                                   [self.XYDetailsView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                   [self.XYDetailsView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                   [self.XYDetailsView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                   ]];
    }
    
    self.XYDetailsView.translatesAutoresizingMaskIntoConstraints = FALSE;
    [self reloadPharmacistsList];
}
-(void)reloadPharmacistsList{
    NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
    NSString *baseURLString = PHARMACY_ALL_PHARMACIST_LIST;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,baseURLString,mobileNumber,appDelegate.pharmacyId];
    [Webservice getWithUrlString:urlString success:^(id data) {
        if (data) {
            self.pharmacistsListArray = (NSMutableArray*)data;
            NSDictionary *dict = [self.pharmacistsListArray firstObject];
            self.pharmacistDeniedInfoCountLbl.text = [dict stringForKey:@"PharmacistDeniedCount"];
            self.pharmacyNameLbl.text = [self.passedPharmacyDict stringForKey:@"PharmacyName"];
            self.totalPharmacistsCountLbl.text = [NSString stringWithFormat:@"/%@",[self.passedPharmacyDict stringForKey:@"NoOfPharmacists"]];
            self.addedPharmacistsCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.pharmacistsListArray.count];
            [self.pharmacistsTbl reloadData];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        //Offline
        /*
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:alertAction];
         [self presentViewController:alert animated:YES completion:nil];
         */
        
        NSLog(@"Error: %@",error.localizedDescription);
        if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
            self.pharmacistsListArray =  [NSMutableArray arrayWithArray:self.passedPharmacyDict[@"PharmacistTicketMaster"]];
            // (NSMutableArray*)self.passedPharmacyDict[@"PharmacistTicketMaster"];
            
            //Add offline save pharamcist to  this list as well....
            /***  for (NSDictionary *dicPharmacist in appDelegate.pharmacistNewMissionArray){
             if(!self.pharmacistsListArray){
             self.pharmacistsListArray = [NSMutableArray new];
             }
             if([ dicPharmacist[@"PharmacyId"] isEqualToString : self.passedPharmacyDict[@"PharmacyId"]]){
             [self.pharmacistsListArray addObject:dicPharmacist];
             NSLog(@"count of list now : %ld",self.pharmacistsListArray.count);
             }
             }
             ***/
            //Add offline save pharamcist to  this list as well....
            
            self.totalPharmacistsCountLbl.text = [NSString stringWithFormat:@"/%@",[self.passedPharmacyDict stringForKey:@"NoOfPharmacists"]];
            self.addedPharmacistsCountLbl.text = [NSString stringWithFormat:@"%ld",self.pharmacistsListArray.count];
            if ([self.pharmacistsList count] > 0) {
                __unused NSDictionary *pharmacistDict = (NSDictionary*)[self.pharmacistsListArray firstObject];
                self.pharmacistDeniedInfoCountLbl.text = [pharmacistDict stringForKey:@"PharmacistDeniedCount"];
            }else{
                self.pharmacistDeniedInfoCountLbl.text = @"0";
            }
            [self.pharmacistsTbl reloadData];
            
        }
        //Offline
        
    }];
}

#pragma mark - Get Postdata Methods

-(NSDictionary *)getpharmacyPostData{
    [self.view endEditing:YES];
    NSData *imgData = self.buildingPicData?self.buildingPicData:self.serviceBuildingPicdata;
    NSDictionary *postDataDictionary = @{
                                         @"PharmacyId":appDelegate.pharmacyId,
                                         @"PharmacyName":[self.passedPharmacyDict objectForKey:@"PharmacyName"],
                                         @"Address1":self.address1TextField.text,
                                         @"Address2":self.address2TextField.text,
                                         @"City":self.cityTextField.text,
                                         @"Country":self.countryTextField.text,
                                         @"LandLineNumber1":self.landlineNumberTextField.text,
                                         @"LandLineNumber2":self.landlineNumberTextField2.text?self.landlineNumberTextField2.text:@"",
                                         @"LandLineNumber3":self.landlineNumberTextField3.text?self.landlineNumberTextField3.text:@"",
                                         @"BuildingPic":[imgData base64EncodedStringWithOptions:NSUTF8StringEncoding]?[imgData base64EncodedStringWithOptions:NSUTF8StringEncoding]:@"",
                                         @"OwnerName":self.ownerNameTextField.text?self.ownerNameTextField.text:@"",
                                         @"NoOfPharmacists":self.noOfPhysiciansTextField.text,
                                         @"Latitude":[self.finalLocationDict stringForKey:LATITUDE_STRING],
                                         @"Longitude":[self.finalLocationDict stringForKey:LONGITUDE_STRING]
                                         //offline
                                         ,
                                         @"PharmacistTicketMaster":[self.passedPharmacyDict objectForKey:@"PharmacistTicketMaster"],
                                         @"CompletionDate":[self.passedPharmacyDict objectForKey:@"CompletionDate"],
                                         @"MissionTakenDate":[self.passedPharmacyDict objectForKey:@"MissionTakenDate"],
                                         @"MissionType":[self.passedPharmacyDict objectForKey:@"MissionType"]
                                         //offline
                                         };
    return postDataDictionary;

}

-(NSDictionary *)getpharmacistPostData:(NSString *)PharmacistTransactionString{
    [self.view endEditing:YES];
    NSDictionary *dict = @{
                           @"PharmacistTransactionID":PharmacistTransactionString,
                           @"PharmacyId":appDelegate.pharmacyId,
                           @"FirstName":self.firstNameTextField.text?:@"",
                           @"LastName":self.lastNameTextField.text?:@"",
                           @"MobileNumber1":self.mobileNo1TextField.text?:@"",
                           @"MobileNumber2":self.mobileNo2TextField.text?:@"",
                           @"MobileNumber3":self.mobileNo3TextField.text?:@"",
                           @"EmailID1":self.emailNo1TextField.text?:@"",
                           @"EmailID2":self.emailNo2TextField.text?:@"",
                           @"EmailID3":self.emailNo3TextField.text?:@"",
                           @"YearOfGraduation":self.yearOfGragTextField.text?self.yearOfGragTextField.text:@"",
                           @"ShiftTimingsFrom":@"",
                           @"ShiftTimingsTo":@"",
                           @"Consent1Id":@"",
                           @"Consent2Id":@"",
                           @"Consent3Id":@""
                           };
    return dict;
}

-(NSDictionary *)getMobileNumberData:(NSString *)pharmacyString pharmacistTicketID:(NSString *)pharmacistTicketIDstring
{
    NSDictionary *dict = @{
                           @"PharmacyId":appDelegate.pharmacyId,
                           @"Type":pharmacyString,
                           @"PharmacistTicketId":pharmacistTicketIDstring
                           };
    return dict;
}

-(void)getMobileNumberService:(NSString *)pharmacyString pharmacistTicketID:(NSString *)pharmacistTicketIDstring{
    // NSData *requestData = [self getMobileNumberData];
    NSString  *mobNo = [[NSUserDefaults standardUserDefaults]stringForKey:MOBILE_NUMBER_STRING];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,PHARMACY_MOBILE_NUMBERS,mobNo];
    
    [Webservice postWithURlString:urlString withParameters:[self getMobileNumberData:pharmacyString pharmacistTicketID:pharmacistTicketIDstring] sucess:^(id data) {
        //Success
        if (data) {
            self.mobileNumbersDict = data;
            if (self.isPharmacist) {
                [self verifyMobileNumbersAlertActionSheet];
            }else{
                [self verifyLandlineNumbersAlertActionSheet];
            }
        }
    } failure:^(NSError *error) {
        //error
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:error.description preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}


#pragma mark - UIButton Action Methods

- (IBAction)pharmacyDetailsSubmitAction:(id)sender {
    if ([self.passedPharmacyDict objectForKey:LATITUDE_STRING]){
        [self.finalLocationDict setObject:[self.passedPharmacyDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
    }if ([self.currentLocationDict objectForKey:LATITUDE_STRING]) {
        [self.finalLocationDict setObject:[self.currentLocationDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
    }if ([self.selectedLocationDict objectForKey:LATITUDE_STRING]){
        [self.finalLocationDict setObject:[self.selectedLocationDict objectForKey:LATITUDE_STRING] forKey:LATITUDE_STRING];
    }
    if([self.passedPharmacyDict objectForKey:LONGITUDE_STRING]){
        [self.finalLocationDict setObject:[self.passedPharmacyDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
    }
    if ([self.currentLocationDict objectForKey:LONGITUDE_STRING]) {
        [self.finalLocationDict setObject:[self.currentLocationDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
    }if([self.selectedLocationDict objectForKey:LONGITUDE_STRING]){
        [self.finalLocationDict setObject:[self.selectedLocationDict objectForKey:LONGITUDE_STRING] forKey:LONGITUDE_STRING];
    }
    
    if (![[self.passedPharmacyDict stringForKey:@"BuildingPic"] isEqualToString:@""]) {
        self.serviceBuildingPicdata = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[self.passedPharmacyDict stringForKey:@"BuildingPic"]]];
    }else{
       self.buildingPicData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.buildingPicImage, 0.1)];
    }
    if ([self isFinalValidationsOkToPostData]) {
        [self showProgressView];
        NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
        NSString *baseURLString = PHARMACY_ADD_PHARMACY_VERIFY;
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
        //offline
        NSDictionary *dicPharmacistData = [self getpharmacyPostData];
        //offline
        [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
            //Success
            [self.mbProgressView hideAnimated:YES];
            if (data) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setBool:YES forKey:@"PharmacySave"];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:PHARMACY_VERIFICATION_COMPLETE_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
                self.passedPharmacyDict = (NSMutableDictionary *)data;
                [self setUpPharmacyDetails];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            //Offline
            /*
             UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
             UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
             [alert addAction:alertAction];
             [self presentViewController:alert animated:YES completion:nil];
             */
            NSLog(@"Error: %@",error.localizedDescription);
            if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
                AppDelegate *delG =  appDelegate;
                if(!delG.pharmacyVerifyNewMissionArray){
                    delG.pharmacyVerifyNewMissionArray = [NSMutableArray new];
                }
                int i=0;
                for(i=0; i<delG.pharmacyVerifyNewMissionArray.count;++i){ //PharmacyId
                    NSDictionary *dic = (NSDictionary *)delG.pharmacyVerifyNewMissionArray[i];
                    if([dic[@"PharmacyId"] isEqualToString:self.passedPharmacyDict[@"PharmacyId"]]){
                        [delG.pharmacyVerifyNewMissionArray replaceObjectAtIndex:i withObject:dicPharmacistData];
                        break;
                    }
                    
                }
                if(i == delG.pharmacyVerifyNewMissionArray.count){
                    [delG.pharmacyVerifyNewMissionArray addObject:dicPharmacistData];
                }
                //replace in current list as well
                for(i=0; i<delG.pharmacistVerifyMissionCurrentPharmacistList.count;++i){ //PharmacyId
                    NSDictionary *dic = (NSDictionary *)delG.pharmacistVerifyMissionCurrentPharmacistList[i];
                    if([dic[@"PharmacyId"] isEqualToString:self.passedPharmacyDict[@"PharmacyId"]]){
                        [delG.pharmacistVerifyMissionCurrentPharmacistList replaceObjectAtIndex:i withObject:dicPharmacistData];
                        break;
                    }
                    
                }
                self.passedPharmacyDict = [NSMutableDictionary dictionaryWithDictionary:dicPharmacistData];
                [self setUpPharmacyDetails];
                NSLog(@"pharmacy verify Saved : %ld",delG.pharmacyVerifyNewMissionArray.count);
                [self addXYDetailsView];
            }
            //Offline
        }];
    }else{
        
    }
}

- (IBAction)submitBtnAction:(id)sender {
    if (self.isFromNewPharmacist) {
        if ([self isFinalValidationsEmptyOkToPostData]) {
            [self showProgressView];
            NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
            NSString *baseURLString = PHARMACY_ADD_PHARMACIST_VERIFY_MISSION;
            NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
            [Webservice postWithURlString:urlString withParameters:[self getpharmacistPostData:@""] sucess:^(id data) {
                //Success
                [self.mbProgressView hideAnimated:YES];
                if (data) {
                    if (self.isFromNewPharmacist) {
                        if ([self.delegate respondsToSelector:@selector(PHRAddConsent:)]) {
                            [self.delegate PHRAddConsent:(NSMutableDictionary *)data];
                        }
                    }else{
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:@"Pharmacist details saved successfully." preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                        [alert addAction:alertAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        [self addXYDetailsView];
                    }
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            } failure:^(NSError *error) {
                //error
                [self.mbProgressView hideAnimated:YES];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_MISSING_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else{
        if ([self isFinalValidationsOkToPostData]) {
            [self showProgressView];
            NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
            NSString *baseURLString = PHARMACY_ADD_PHARMACIST_VERIFY_MISSION;
            NSString *urlString = [NSString stringWithFormat:@"%@%@%@/",KBASE_URL,baseURLString,mobileNumber];
            //offline
            NSDictionary *dicPharmacistData = [self getpharmacistPostData:[self.passedPharmacistDict objectForKey:@"PharmacistTransactionID"]];
            [Webservice postWithURlString:urlString withParameters:dicPharmacistData sucess:^(id data) {
                //Success
                [self.mbProgressView hideAnimated:YES];
                if (data) {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setBool:YES forKey:@"PharmacistsSave"];
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:PHARMACIST_VERIFICATION_COMPLETE_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                        [alert addAction:alertAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        [self addXYDetailsView];

                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:alertAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            } failure:^(NSError *error) {
                //error
                [self.mbProgressView hideAnimated:YES];
                //Offline
                /*
                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                 [alert addAction:alertAction];
                 [self presentViewController:alert animated:YES completion:nil];
                 */
                NSLog(@"Error: %@",error.localizedDescription);
                if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
                    AppDelegate *delG =  appDelegate;
                    if(!delG.pharmacistverifyMissionArray){
                        delG.pharmacistverifyMissionArray = [NSMutableArray new];
                    }
                    //check for duplicacy here before adding.
                    BOOL isNew = YES;
                    //finding based on email id as pharmacist id still not generated.
                    for(NSDictionary *dic in delG.pharmacistverifyMissionArray){
                        if([dic[@"PharmacyId"] isEqualToString:dicPharmacistData[@"PharmacyId"]] &&
                           [dic[@"PharmacistTransactionID"] intValue] == [dicPharmacistData[@"PharmacistTransactionID"] intValue]){
                            NSUInteger index = [delG.pharmacistverifyMissionArray indexOfObject:dic];
                            [delG.pharmacistverifyMissionArray replaceObjectAtIndex:index withObject:dicPharmacistData];
                            isNew = NO;
                            break;
                        }
                    }
                    //replace existing..
                    if(isNew){
                        [delG.pharmacistverifyMissionArray addObject:dicPharmacistData];
                    }
                    
                    //Add this pharmacist in the current list of pharmacy.
                    NSMutableDictionary *dicToBeReplaced;
                    NSMutableArray *arrArrToBeReplaced;
                    NSUInteger index = -1;
                    for(NSDictionary *dic in delG.pharmacistVerifyMissionCurrentPharmacistList){
                        if([dic[@"PharmacyId"] isEqualToString:self.passedPharmacyDict[@"PharmacyId"]]){
                            index = [delG.pharmacistVerifyMissionCurrentPharmacistList indexOfObject:dic];
                            dicToBeReplaced = [NSMutableDictionary dictionaryWithDictionary:dic];
                            if(dic[@"PharmacistTicketMaster"]){
                                arrArrToBeReplaced = [NSMutableArray arrayWithArray:dic[@"PharmacistTicketMaster"]];
                                for(NSDictionary *dic in arrArrToBeReplaced){
                                    int num1 = [dic[@"PharmacistTransactionID"] intValue];
                                    int num2 = [dicPharmacistData[@"PharmacistTransactionID"] intValue];
                                    if([dic[@"PharmacyId"] isEqualToString:dicPharmacistData[@"PharmacyId"]] &&
                                       (num1 == num2)){
                                        NSUInteger index = [arrArrToBeReplaced indexOfObject:dic];
                                        [arrArrToBeReplaced replaceObjectAtIndex:index withObject:dicPharmacistData];
                                        break;
                                    }
                                }
                                //replace existing..
                                
                                
                            }else{
                                arrArrToBeReplaced = [NSMutableArray arrayWithObject:dicPharmacistData];
                            }
                            dicToBeReplaced[@"PharmacistTicketMaster"] = arrArrToBeReplaced;
                            break;
                        }
                    }
                    if(index != -1){
                        [delG.pharmacistVerifyMissionCurrentPharmacistList replaceObjectAtIndex:index withObject:dicToBeReplaced];
                        self.passedPharmacyDict = dicToBeReplaced;
                    }
                    NSLog(@"pharmacist verify Saved");
                    [self addXYDetailsView];
                    
                }
                //Offline
            }];
        }else{
            
        }
    }
}

- (IBAction)addNewPharmacistAction:(id)sender {
    self.isFromNewPharmacist = YES;
    NSInteger numberOfPharmacistsAddedCount = [[NSString stringWithFormat:@"%@",self.addedPharmacistsCountLbl.text] integerValue];
    NSString *string = self.totalPharmacistsCountLbl.text;
    NSInteger numberOfPharmacistsCount = [[string stringByReplacingOccurrencesOfString:@"/" withString:@""] integerValue];
    if (numberOfPharmacistsAddedCount < numberOfPharmacistsCount) {
        [self.leftVC addNavigationItem];
        self.pharmacyView.hidden = YES;
        self.XYDetailsView.hidden = YES;
        self.pharmacistView.hidden = NO;
        //if(!self.pharmacistView.superview){
         [self emptyAllFieldsInPharmacist];
            [self.view addSubview:self.pharmacistView];
            [NSLayoutConstraint activateConstraints: @[
                                                       [self.pharmacistView.topAnchor constraintEqualToAnchor:/*self.horizontalLineView.bottomAnchor*/self.view.topAnchor],
                                                       [self.pharmacistView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                       [self.pharmacistView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                       [self.pharmacistView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                       ]];
        //}
        self.pharmacistView.translatesAutoresizingMaskIntoConstraints = FALSE;
    }else{
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:OOPS_MESSAGE
                                    message:CHANGE_THE_NUMBER_OF_PHARMACISTS_MESSAGE
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        LeftViewController *leftVC = (LeftViewController *)self.delegate;
        [leftVC presentViewController:alert animated:YES completion:nil];
    }
}

-(void)emptyAllFieldsInPharmacist{
    self.verifyLandlineNumber.hidden = YES;
    self.verifyFirstName.hidden = YES;
    self.verifyLastName.hidden = YES;
    self.verifyMobNo1.hidden = YES;
    self.verifyMobNo2.hidden = YES;
    self.verifyMobNo3.hidden = YES;
    self.verifyEmailID1.hidden = YES;
    self.verifyEmailID2.hidden = YES;
    self.verifyEmailID3.hidden = YES;
    self.verifyShiftTimings.hidden = YES;
    self.verifyFromShiftTimings.hidden = YES;
    self.verifyToShiftTimings.hidden = YES;
    self.verifyYrOfGrad.hidden = YES;
    self.firstNameTextField.text = @"";
    self.lastNameTextField.text = @"";
    self.mobileNo1TextField.text = @"";
    self.mobileNo2TextField.text = @"";
    self.mobileNo3TextField.text = @"";
    self.emailNo1TextField.text = @"";
    self.emailNo2TextField.text = @"";
    self.emailNo3TextField.text = @"";
    self.yearOfGragTextField.text = @"";
    self.shiftTimingsFromTextField.text = @"";
    self.shiftTimingsToTextField.text = @"";
}

-(void)unHideAllFieldsInPharmacist{
    self.verifyLandlineNumber.hidden = NO;
    self.verifyFirstName.hidden = NO;
    self.verifyLastName.hidden = NO;
    self.verifyMobNo1.hidden = NO;
    self.verifyMobNo2.hidden = NO;
    self.verifyMobNo3.hidden = NO;
    self.verifyEmailID1.hidden = NO;
    self.verifyEmailID2.hidden = NO;
    self.verifyEmailID3.hidden = NO;
    self.verifyShiftTimings.hidden = NO;
    self.verifyFromShiftTimings.hidden = NO;
    self.verifyToShiftTimings.hidden = NO;
    self.verifyYrOfGrad.hidden = NO;
    self.verifyFirstName.selected = NO;
    self.verifyLastName.selected = NO;
    self.verifyMobNo1.selected = NO;
    self.verifyMobNo2.selected = NO;
    self.verifyMobNo3.selected = NO;
    self.verifyEmailID1.selected = NO;
    self.verifyEmailID2.selected = NO;
    self.verifyEmailID3.selected = NO;
    self.verifyYrOfGrad.selected = NO;
    self.verifyFromShiftTimings.selected = NO;
    self.verifyToShiftTimings.selected = NO;

//    [self.verifyFirstName setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyLastName setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyMobNo1 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyMobNo2 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyMobNo3 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyEmailID1 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyEmailID2 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyMobNo3 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyYrOfGrad setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyFromShiftTimings setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyToShiftTimings setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//    [self.verifyFirstName setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyLastName setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyMobNo1 setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyMobNo2 setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyMobNo3 setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyEmailID1 setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyEmailID2 setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyEmailID3 setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyYrOfGrad setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyFromShiftTimings setTitle:@"Verify " forState:UIControlStateSelected];
//    [self.verifyToShiftTimings setTitle:@"Verify " forState:UIControlStateSelected];
}

-(void)selectedStateButton{
    self.verifyFirstName.selected = YES;
    self.verifyLastName.selected = YES;
    self.verifyMobNo1.selected = YES;
    self.verifyMobNo2.selected = YES;
    self.verifyMobNo3.selected = YES;
    self.verifyEmailID1.selected = YES;
    self.verifyEmailID2.selected = YES;
    self.verifyEmailID3.selected = YES;
    self.verifyYrOfGrad.selected = YES;
    self.verifyFromShiftTimings.selected = YES;
    self.verifyToShiftTimings.selected = YES;
//    [self.verifyFirstName setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
//        [self.verifyLastName setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
//        [self.verifyMobNo1 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
//        [self.verifyMobNo2 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//        [self.verifyMobNo3 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//        [self.verifyEmailID1 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//        [self.verifyEmailID2 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//        [self.verifyMobNo3 setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//        [self.verifyYrOfGrad setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//        [self.verifyFromShiftTimings setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//        [self.verifyToShiftTimings setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateSelected];
//        [sender setTitle:@"Verify " forState:UIControlStateNormal];
//        [self.verifyLastName setTitle:@"Verify " forState:UIControlStateNormal];
//        [self.verifyMobNo1 setTitle:@"Verify " forState:UIControlStateSelected];
//        [self.verifyMobNo2 setTitle:@"Verify " forState:UIControlStateSelected];
//        [self.verifyMobNo3 setTitle:@"Verify " forState:UIControlStateSelected];
//        [self.verifyEmailID1 setTitle:@"Verify " forState:UIControlStateSelected];
//        [self.verifyEmailID2 setTitle:@"Verify " forState:UIControlStateSelected];
//        [self.verifyEmailID3 setTitle:@"Verify " forState:UIControlStateSelected];
//        [self.verifyYrOfGrad setTitle:@"Verify " forState:UIControlStateSelected];
//        [self.verifyFromShiftTimings setTitle:@"Verify " forState:UIControlStateSelected];
//        [self.verifyToShiftTimings setTitle:@"Verify " forState:UIControlStateSelected];
}

- (IBAction)finalSubmitAction:(id)sender {
    NSInteger numberOfDeniedPharmacistsCount = [[NSString stringWithFormat:@"%@",self.pharmacistDeniedInfoCountLbl.text] integerValue];
    NSInteger numberOfPharmacistsAddedCount = [[NSString stringWithFormat:@"%@",self.addedPharmacistsCountLbl.text] integerValue];
    NSString *string = self.totalPharmacistsCountLbl.text;
    NSInteger numberOfPharmacistsCount = [[string stringByReplacingOccurrencesOfString:@"/" withString:@""] integerValue];
    if (numberOfPharmacistsAddedCount + numberOfDeniedPharmacistsCount == numberOfPharmacistsCount) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL pharmacistVerified = [defaults boolForKey:@"PharmacistsSave"];
        BOOL pharmacyVerified = [defaults boolForKey:@"PharmacySave"];
        NSArray *arr =appDelegate.finalVerifyPharmacyArray;
        if((pharmacyVerified == YES && pharmacistVerified == YES)/*offline*/|| (appDelegate.pharmacistverifyMissionArray && appDelegate.pharmacistverifyMissionArray.count) || (appDelegate.pharmacistverifyMissionArray && appDelegate.pharmacistverifyMissionArray.count)/*offline*/){
            NSString *mobileNumber = [[NSUserDefaults standardUserDefaults] objectForKey:MOBILE_NUMBER_STRING];
            NSString *baseURLString = PHARMACY_FINAL_SUBMIT_VERIFY;
            NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,baseURLString,mobileNumber,appDelegate.pharmacyId];
            [Webservice getWithUrlString:urlString success:^(id data) {
                if (data) {
                    NSDictionary *dict = (NSDictionary *)data;
                    if ([[dict stringForKey:@"Message"] isEqualToString:@"SUCCESS"]) {
                        [defaults removeObjectForKey:@"PharmacistsSave"];
                        [defaults removeObjectForKey:@"PharmacySave"];
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:SUCCESS_STRING message:@"Details updated successfully."preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                        [alert addAction:alertAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        if ([self.delegate respondsToSelector:@selector(PHRSubmitSucessfull)]) {
                            [self.delegate PHRSubmitSucessfull];
                        }
                    }
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:alertAction];
                    LeftViewController *leftVC = (LeftViewController *)self.delegate;
                    [leftVC presentViewController:alert animated:YES completion:nil];
                }
            } failure:^(NSError *error) {
                //Offline
                /*
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                LeftViewController *leftVC = (LeftViewController *)self.delegate;
                [leftVC presentViewController:alert animated:YES completion:nil];
                */
                NSLog(@"Error: %@",error.localizedDescription);
                if([error.localizedDescription rangeOfString:@"offline"].location != NSNotFound){
                    if(!appDelegate.finalVerifyPharmacyArray){
                        appDelegate.finalVerifyPharmacyArray = [NSMutableArray new];
                    }
                    int i;
                    for (int i=0; i<appDelegate.finalVerifyPharmacyArray.count; ++i) {
                        NSString *strID = appDelegate.finalVerifyPharmacyArray[i];
                        if([strID isEqualToString:appDelegate.pharmacyId]){
                            break;
                        }
                    }
                    if(i==appDelegate.finalVerifyPharmacyArray.count){
                        [appDelegate.finalVerifyPharmacyArray addObject:[NSString stringWithString:appDelegate.pharmacyId]];
                    }
                    else{
                        [appDelegate.finalVerifyPharmacyArray replaceObjectAtIndex:i withObject:appDelegate.pharmacyId];
                    }
                    //remvove from current list as well.
                    for(NSDictionary *dic in appDelegate.pharmacistVerifyMissionCurrentPharmacistList){
                        if([dic[@"PharmacyId"] isEqualToString:appDelegate.pharmacyId]){
                            [appDelegate.pharmacistVerifyMissionCurrentPharmacistList removeObject:dic];
                            break;
                        }
                    }
                    //remvove from current list as well.
                    if ([self.delegate respondsToSelector:@selector(PHRSubmitSucessfull)]) {
                        [self.delegate PHRSubmitSucessfull];
                    }
                    NSLog(@"final submit verify pharmacy saved : %ld",appDelegate.finalVerifyPharmacyArray.count);
                }
                //Offline

                
            }];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:VERIFY_PHARMACY_PHARMACIST_DETAILS_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        LeftViewController *leftVC = (LeftViewController *)self.delegate;
        [leftVC presentViewController:alert animated:YES completion:nil];
    }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:ENTER_ALL_PHARMACISTS_DETAILS_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        LeftViewController *leftVC = (LeftViewController *)self.delegate;
        [leftVC presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - UITableView Delegate & DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.pharmacistsListArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier =@"AddPharmacistVCTableViewCellIdentifier";
    AddPharmacistVCTableViewCell *cell =
    (AddPharmacistVCTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = self.view.backgroundColor;
    NSDictionary *pharmacistDict = [self.pharmacistsListArray objectAtIndex:indexPath.row];
    cell.pharmacistNameLbl.text = [NSString stringWithFormat:@"%@ %@",[pharmacistDict stringForKey:@"FirstName"],[pharmacistDict stringForKey:@"LastName"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.passedPharmacistDict = [self.pharmacistsListArray objectAtIndex:indexPath.row];
    appDelegate.pharmacistTransactionID = [self.passedPharmacistDict stringForKey:@"PharmacistTicketID"];
    [self setUpPharmacistDetails];
    self.isFromNewPharmacist = NO;
    [self.leftVC addNavigationItem];
    self.pharmacyView.hidden = YES;
    self.XYDetailsView.hidden = YES;
    self.pharmacistView.hidden = NO;
   //if(!self.pharmacistView.superview){
    [self unHideAllFieldsInPharmacist];
        [self.view addSubview:self.pharmacistView];
        [NSLayoutConstraint activateConstraints: @[
                                                   [self.pharmacistView.topAnchor constraintEqualToAnchor:/*self.horizontalLineView.bottomAnchor*/self.view.topAnchor],
                                                   [self.pharmacistView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                   [self.pharmacistView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                   [self.pharmacistView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                   ]];
   // }
    self.pharmacistView.translatesAutoresizingMaskIntoConstraints = FALSE;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard Hide & Show Methods

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    UIScrollView *scrollView = nil;
    if(self.pharmacyTopButton.selected){
        return;
    }else if(self.pharmacistTopButton.selected){
        scrollView = self.pharmacistScrollView;
    }
    
    static NSString *str = @"first";
    
    if(str){
        self.initialContentSizePharmacistScrollView = self.pharmacistScrollView.contentSize;
    }
    str = nil;
    
    // keyboard frame is in window coordinates
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect keyboardInfoFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    // get the height of the keyboard by taking into account the orientation of the device too
    CGRect windowFrame = [self.view.window convertRect:self.view.frame fromView:self.view];
    CGRect keyboardFrame = CGRectIntersection (windowFrame, keyboardInfoFrame);
    CGRect coveredFrame = [self.view.window convertRect:keyboardFrame toView:self.view];
    // add the keyboard height to the content insets so that the scrollview can be scrolled
    UIEdgeInsets contentInsets = UIEdgeInsetsMake (0.0, 0.0, coveredFrame.size.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    // make sure the scrollview content size width and height are greater than 0
    [scrollView setContentSize:CGSizeMake (scrollView.frame.size.width, scrollView.contentSize.height+200)];
    // scroll to the text view
    [scrollView scrollRectToVisible:self.activeField.superview.frame animated:YES];
}


- (IBAction)pharmacyOrPharmacistSelected:(id)sender;{
    if(self.activeField){
        [self.activeField resignFirstResponder];
    }
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case 10: //Pharmacy
            [self PharmacySelected];
            break;
        case 20: //Pharmacist
            [self PharmacistSelected];
            break;
        default:
            break;
    }
}
-(void)PharmacySelected{
    self.isPharmacist = NO;
    [self.leftVC addNavigationItem];
    [self.pharmacyTopButton setBackgroundColor:MyYellow];
    [self.pharmacistTopButton setBackgroundColor:[UIColor clearColor]];
    self.pharmacyTopButton.selected = YES;
    self.pharmacistTopButton.selected = NO;
    self.pharmacistView.hidden = YES;
    self.XYDetailsView.hidden = YES;
    self.pharmacyView.hidden = NO;
    [self setUpPharmacyDetails];
}
-(void)PharmacistSelected{
    self.isPharmacist = YES;
    [self.leftVC showHideNavigationItems];
    [self.pharmacistTopButton setBackgroundColor:MyYellow];
    [self.pharmacyTopButton setBackgroundColor:[UIColor clearColor]];
    self.pharmacyTopButton.selected = NO;
    self.pharmacistTopButton.selected = YES;
    self.pharmacistView.hidden = YES;
    self.XYDetailsView.hidden = NO;
    self.pharmacyView.hidden = YES;
    self.pharmacistView.translatesAutoresizingMaskIntoConstraints = FALSE;
    if(!self.XYDetailsView.superview){
        [self.view addSubview:self.XYDetailsView];
        [NSLayoutConstraint activateConstraints: @[
                                                   [self.XYDetailsView.topAnchor constraintEqualToAnchor:self.horizontalLineView.bottomAnchor],
                                                   [self.XYDetailsView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
                                                   [self.XYDetailsView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
                                                   [self.XYDetailsView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
                                                   ]];
    }
    self.XYDetailsView.translatesAutoresizingMaskIntoConstraints = FALSE;
    [self reloadPharmacistsList];
}


#pragma mark - Camera Methods

- (IBAction)cameraButtonPressed:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:CLICK_PHOTO_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self callImagePicker:UIImagePickerControllerSourceTypeCamera];
    }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:UPLOAD_IMAGE_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { [self callImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }];
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.buildingPicImageView;
    popPresenter.sourceRect = self.buildingPicImageView.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)clickPicture{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:NULL];
    }
    else{
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:OOPS_MESSAGE
                                    message:CAMERA_NOT_AVAILABLE_MESSAGE
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        LeftViewController *leftVC = (LeftViewController *)self.delegate;
        [leftVC presentViewController:alert animated:YES completion:nil];
    }
}

-(void)callImagePicker:(UIImagePickerControllerSourceType)type{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = type;
    LeftViewController *leftVC = (LeftViewController *)self.delegate;
    [leftVC presentViewController:imagePicker animated:YES completion:NULL];
}

#pragma mark - UIImagePickerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    UIImage *image_answer = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
    self.buildingPicImageView.image = image_answer;
    self.buildingPicImage = image_answer;
    self.passedPharmacyDict = [[NSMutableDictionary alloc] initWithDictionary:self.passedPharmacyDict];
    if (![[self.passedPharmacyDict stringForKey:@"BuildingPic"] isEqualToString:@""]) {
        [self.passedPharmacyDict removeObjectForKey:@"BuildingPic"];
    }
    self.passedPharmacyString = @"PHARMACY";
    // self.photoTextField.text = @"You have successfully uploaded the image.";
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{    self.passedPharmacyString = @"PHARMACY";
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    UIScrollView *scrollView = nil;
    if(self.pharmacyTopButton.selected){
        return;
    }else if(self.pharmacistTopButton.selected){
        scrollView = self.pharmacistScrollView;
    }
    // scroll back..
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.pharmacistScrollView.contentInset = contentInsets;
    self.pharmacistScrollView.scrollIndicatorInsets = contentInsets;
    [self.pharmacistScrollView setContentSize:self.initialContentSizePharmacistScrollView];
}
-(IBAction)verifyButtonSelected:(id)sender{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    if(button.selected){
        [button setTitle:@"Verified" forState:UIControlStateSelected];
        [button setImage:[UIImage imageNamed:@"checboxselected"] forState:UIControlStateSelected];
    }else{
        [button setTitle:@"Verify  " forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
    
    if (button.tag == 100) {
        [self getMobileNumberService:@"PHARMACY" pharmacistTicketID:@""];

    }
    if (button.tag == 200) {
        [self getMobileNumberService:@"PHARMACIST" pharmacistTicketID:appDelegate.pharmacistTransactionID];
        
    }
//    if (self.verifyPharmacyName.selected || self.verifyAddress1.selected) {
//        self.isVerifyLandLineNumbers = NO;
//    }
//
//    if (self.verifyLandlineNumber.selected) {
//        if (self.verifyLandlineNumber.tag == 100 || self.verifyLandlineNumber2.tag == 200 || self.verifyLandlineNumber3.tag == 300) {
//            [self getMobileNumberService:@"PHARMACY" pharmacistTicketID:@""];
//        }
//    }
    

    
//    if (self.isVerifyMobileNumbers == NO) {
//        if (self.verifyMobNo1.selected) {
//            self.isVerifyMobileNumbers = YES;
//            [self getMobileNumberService:@"PHARMACIST" pharmacistTicketID:appDelegate.pharmacistTransactionID];
//        }else{
//            self.isVerifyMobileNumbers = NO;
//        }
//    }else{
//        self.isVerifyMobileNumbers = YES;
//    }
}


-(void)verifyLandlineNumbersAlertActionSheet{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Landline Numbers List entered by Data Collector"delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Cancel"
                                                    otherButtonTitles:[NSString stringWithFormat:@"Landline Number1:  %@",[self.mobileNumbersDict stringForKey:@"LandlineNumber1"]],[NSString stringWithFormat:@"Landline Number2:  %@",[self.mobileNumbersDict stringForKey:@"LandlineNumber2"]],[NSString stringWithFormat:@"Landline Number3:  %@",[self.mobileNumbersDict stringForKey:@"LandlineNumber3"]], nil];
    actionSheet.tag = 200;
    [actionSheet showFromRect:[(UIButton *)self.verifyLandlineNumber frame] inView:self.view animated:YES];
}
-(void)verifyMobileNumbersAlertActionSheet{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Mobile Numbers List entered by Data Collector"delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Cancel"
                                                    otherButtonTitles:[NSString stringWithFormat:@"Mobile Number1:  %@",[self.mobileNumbersDict stringForKey:@"MobileNumber1"]],[NSString stringWithFormat:@"Mobile Number2:  %@",[self.mobileNumbersDict stringForKey:@"MobileNumber2"]],[NSString stringWithFormat:@"Mobile Number3:  %@",[self.mobileNumbersDict stringForKey:@"MobileNumber3"]], nil];
    actionSheet.tag = 100;
    [actionSheet showFromRect:[(UIButton *)self.verifyMobNo1 frame] inView:self.view animated:YES];
}

-(BOOL)isFinalValidationsOkToPostData{
    if(![super isFinalValidationsOkToPostData]){
        return NO;
    }else{ // ok.
        if(self.pharmacistTopButton.selected){ //pharmacist
            if (![self isMandatoryButtonsSelectedForPharmacist]){
                UIAlertController *alert =   [UIAlertController
                                              alertControllerWithTitle:OOPS_MESSAGE
                                              message:VERIFY_ALL_FIELDS_MESSAGE
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    //do something when click button
                }];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
                return NO;
            }
        }else {//Pharmacy
            if (![self isMandatoryButtonsSelectedForPharmacy]){
                UIAlertController *alert =   [UIAlertController
                                              alertControllerWithTitle:OOPS_MESSAGE
                                              message:VERIFY_ALL_FIELDS_MESSAGE
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    //do something when click button
                }];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
                return NO;
            }
            
        }
        
    }
    return YES;
}
-(BOOL)isMandatoryButtonsSelectedForPharmacy{
    if(!self.verifyPharmacyName.selected || !self.verifyAddress1.selected ||!self.verifyAddress2.selected || !self.verifyCity.selected || !self.verifyCountry.selected || !self.verifyLandlineNumber.selected||!self.verifyMap.selected){
        return NO;
    }
    return YES;
}
-(BOOL)isMandatoryButtonsSelectedForPharmacist{
    if(!self.verifyLastName.selected||!self.verifyFirstName.selected  || !self.verifyMobNo1.selected || !self.verifyEmailID1.selected ){
        return NO;
    }
    return YES;
}

-(BOOL)isMandatoryFieldsEmptyForPharmacist{
    if ([self.firstNameTextField.text  isEqualToString:@""] ||[self.lastNameTextField.text  isEqualToString:@""] || [self.mobileNo1TextField.text  isEqualToString:@""]||[self.emailNo1TextField.text  isEqualToString:@""]){
        return YES;
    }
    return NO;
}


-(BOOL)isFinalValidationsEmptyOkToPostData{
    if(self.pharmacistTopButton.selected){ //pharmacist
        if ([self isMandatoryFieldsEmptyForPharmacist]){
            UIAlertController *alert =   [UIAlertController
                                          alertControllerWithTitle:OOPS_MESSAGE
                                          message:SOMETHING_MISSING_MESSAGE
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:ONLY_YEAR_FORMATTER_STRING];
        NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
        NSInteger currentYear = [strDate integerValue];
        NSInteger yearOfGraduationYear = [[NSString stringWithFormat:@"%@",self.yearOfGragTextField.text] integerValue];
        if (yearOfGraduationYear <= currentYear) {
            NSLog(@"Correct Data");
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:YEAR_OF_GRADUATION_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
        
        
        if (![self checkValidEmail])
        {
            UIAlertController *alert =   [UIAlertController
                                          alertControllerWithTitle:OOPS_MESSAGE
                                          message:VALID_EMAIL_ID_MESSAGE
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
    }else{//Pharmacy
        if ([self isMandatoryFieldsEmptyForPharmacy]){
            UIAlertController *alert =   [UIAlertController
                                          alertControllerWithTitle:OOPS_MESSAGE
                                          message:SOMETHING_MISSING_MESSAGE
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            return NO;
        }
    }
    return YES;
}

-(BOOL)isMandatoryFieldsEmptyForPharmacy{
    if ([self.pharmacyNameTextField.text  isEqualToString:@""] || [self.address1TextField.text  isEqualToString:@""]||[self.address2TextField.text  isEqualToString:@""]||[self.cityTextField.text  isEqualToString:@""]||[self.countryTextField.text  isEqualToString:@""]||[self.landlineNumberTextField.text  isEqualToString:@""] || ![self.finalLocationDict stringForKey:LATITUDE_STRING] || ![self.finalLocationDict stringForKey:LONGITUDE_STRING]){
        return YES;
    }
    return NO;
}

- (BOOL)checkValidEmail {
    BOOL validEmail = NO;
    if (self.emailNo1TextField.text != nil) {
        NSString *expression = EMAIL_VALIDATION_STRING;
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSTextCheckingResult* regexResult = [regex firstMatchInString:self.emailNo1TextField.text options:0 range:NSMakeRange(0, [self.emailNo1TextField.text length])];
        validEmail = regexResult != nil;
    }
    return validEmail;
}


#pragma mark-MapView Methods

#pragma mark - CLLocationManager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    float spanX = 0.1;
    float spanY = 0.1;
    CLLocation *location = [locations lastObject];
    MKCoordinateSpan span = self.mapView.region.span;
    span.latitudeDelta=spanX;
    span.longitudeDelta=spanY;
    MKCoordinateRegion region = MKCoordinateRegionMake(location.coordinate, span);
    self.mapView.region = region;
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I am here!";
    region = MKCoordinateRegionMakeWithDistance(location.coordinate, 400, 400);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotation:point];
    NSString *latitude = [NSString stringWithFormat:@"%.7f", location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%.7f", location.coordinate.longitude];
    self.currentLocationDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitude,LATITUDE_STRING,longitude,LONGITUDE_STRING, nil];
    [self.locationManager stopUpdatingLocation];
}


- (void)longpressToGetLocation:(UILongPressGestureRecognizer *)gestureRecognizer{
    [self.view endEditing:YES];
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    self.mapView.showsUserLocation = NO;
    [self.mapView removeAnnotations:self.mapView.annotations];
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D location =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    float spanX = 0.1;
    float spanY = 0.1;
    CLLocation *locationAtual = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
    MKCoordinateSpan span = self.mapView.region.span;
    span.latitudeDelta=spanX;
    span.longitudeDelta=spanY;
    MKCoordinateRegion region = MKCoordinateRegionMake(locationAtual.coordinate, span);
    self.mapView.region = region;
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = location;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    region = MKCoordinateRegionMakeWithDistance(locationAtual.coordinate, 400, 400);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    [self.mapView addAnnotation:point];
    NSString *latitude = [NSString stringWithFormat:@"%.7f", location.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%.7f", location.longitude];
    self.selectedLocationDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:latitude,@"Latitude",longitude,@"Longitude", nil];
}

#pragma mark - UIActionSheet method implementation

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100) {
        if (buttonIndex == 1) {
            self.mobileNo1TextField.text = [self.mobileNumbersDict stringForKey:@"MobileNumber1"];
        }else if(buttonIndex == 2){
            self.mobileNo2TextField.text = [self.mobileNumbersDict stringForKey:@"MobileNumber2"];
        }else if (buttonIndex == 3){
            self.mobileNo3TextField.text = [self.mobileNumbersDict stringForKey:@"MobileNumber3"];
        }
    }else if (actionSheet.tag == 200){
        if (buttonIndex == 1) {
            self.landlineNumberTextField.text = [self.mobileNumbersDict stringForKey:@"LandlineNumber1"];
        }else if(buttonIndex == 2){
            self.landlineNumberTextField2.text = [self.mobileNumbersDict stringForKey:@"LandlineNumber2"];
        }else if (buttonIndex == 3){
            self.landlineNumberTextField3.text = [self.mobileNumbersDict stringForKey:@"LandlineNumber3"];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
