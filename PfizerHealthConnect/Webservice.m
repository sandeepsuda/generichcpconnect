//
//  Webservice.m
//  PharmConnect
//
//  Created by Sandeep Suda on 24/08/16.
//  Copyright © 2016 Kumar, Utpal (Bangalore). All rights reserved.
//

#import "Webservice.h"

@implementation Webservice

+  (void)getWithUrlString:(NSString *)strURL success:(HttpSuccess)success failure:(HttpFailure)failure {
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:strURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPShouldUsePipelining:YES];
    [[session dataTaskWithRequest:request
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                    if (error == nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            id responseData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                            if(success) {
                                success(responseData);
                            }
                        });
                    }else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (failure) {
                                failure(error);
                            }
                        });
                    }
    }] resume];
}

+  (void)postWithURlString:(NSString*)strURL withParameters:(id)params sucess:(HttpSuccess)success failure:(HttpFailure)failure {
    NSError *err;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
   // NSString *string = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
   // NSLog(@"json Obj: %@",string);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:strURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data,
                                                              NSURLResponse *response,
                                                              NSError *error) {
        if (error == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
            id responseData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
            if(success) {
            success(responseData);
            }
        });
        }else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (failure) {
                    failure(error);
                }
            });
        }
    }] resume];
}


@end
