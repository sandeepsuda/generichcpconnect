//
//  LeaderBoardTableViewCell.h
//  IPadApp
//
//  Created by RajaSekhar on 04/11/16.
//  Copyright © 2016 RajaSekhar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaderBoardTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UIImageView *userProfileImageView,*badgeImageView;
@property (nonatomic,weak) IBOutlet UILabel *nameLabel,*pharmacyLabel,*physicianLabel,*pharmacyPointsLabel,*physicianPointsLabel,*gainedPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointsGainedLbl;

@end
