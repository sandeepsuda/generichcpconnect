//
//  TrainingMaterialsViewController.m
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 04/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "TrainingMaterialsViewController.h"
#import "AppDelegate.h"

@interface TrainingMaterialsViewController (){
     AppDelegate *appDelegate;
}

@end

@implementation TrainingMaterialsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.navigationController.navigationBar.barTintColor = [appDelegate colorWithHexString:@"3A4F5A"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:17]}];
    //self.webContentView.userInteractionEnabled = NO;
    self.webContentView.scalesPageToFit = YES;
    self.webContentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.webContentView.delegate = self;
    NSString *strPath = [[NSBundle mainBundle]pathForResource:@"PharmConnect-HCPconnectTraining Deck - 23 Oct'16" ofType:@"pdf"];
    [self.webContentView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[strPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]]]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
