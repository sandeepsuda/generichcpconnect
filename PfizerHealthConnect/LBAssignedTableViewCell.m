//
//  LBAssignedTableViewCell.m
//  PfizerHealthConnect
//
//  Created by RajaSekhar on 22/12/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import "LBAssignedTableViewCell.h"

@implementation LBAssignedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
