//
//  PharmacyDetailsViewController.h
//  PfizerHealthConnect
//
//  Created by Abhatia on 22/11/16.
//  Copyright © 2016 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"

@protocol PharmacyDetailsViewControllerDelegate <NSObject>
-(void)PHRSubmitSucessfull;
-(void)PHRAddConsent:(NSMutableDictionary*)pharmacistDict;
@end

@interface PharmacyDetailsViewController : UIViewController<MKMapViewDelegate,MKAnnotation,CLLocationManagerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak,nonatomic) id<PharmacyDetailsViewControllerDelegate> delegate;
@property (nonatomic,strong) MBProgressHUD *mbProgressView;
@property (weak, nonatomic) IBOutlet UITextField *activeField;
@property(nonatomic)CGSize initialContentSizePharmacyScrollView;
@property(nonatomic)CGSize initialContentSizePharmacistScrollView;
@property(nonatomic,strong) NSString *passedPharmacyString;
@property(nonatomic,strong)NSMutableDictionary *passedPharmacyDict;
@property(nonatomic,strong)NSMutableArray *pharmacistsListArray;
/**Pharmacy**/
@property (nonatomic,strong) NSData *BuildingPicData;
@property (nonatomic,strong) NSData *serviceBuildingPicdata;
@property (nonatomic,strong) UIImage *buildingPicImage;
@property (nonatomic,strong) NSMutableDictionary *selectedLocationDict,*currentLocationDict,*finalLocationDict;
@property (weak, nonatomic) IBOutlet UIButton *pharmacyTopButton;
@property (weak, nonatomic) IBOutlet UIButton *pharmacistTopButton;
@property (weak, nonatomic) IBOutlet UITextField *pharmacyNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *address1TextField;
@property (weak, nonatomic) IBOutlet UITextField *address2TextField;
@property (strong, nonatomic) IBOutlet UIView *XYDetailsView;
@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *ownerNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *landlineNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *landlineNumberTextField2;
@property (weak, nonatomic) IBOutlet UITextField *landlineNumberTextField3;
@property (weak, nonatomic) IBOutlet UITextField *noOfPhysiciansTextField;
@property (weak, nonatomic) IBOutlet UIImageView *buildingPicImageView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property(weak,nonatomic)IBOutlet UIView *pharmacyView;
@property(strong,nonatomic)IBOutlet UIView *pharmacistView;
@property(strong,nonatomic)IBOutlet UIView *horizontalLineView;
@property(strong,nonatomic)CLLocationManager *locationManager;
@property (strong,nonatomic)NSArray *countryNamesArray;
@property (weak, nonatomic) IBOutlet UIButton *pharmacySubmitBtn;
@property(weak,nonatomic)IBOutlet UIScrollView *pharmacyScrollView;

- (IBAction)pharmacyOrPharmacistSelected:(id)sender;
- (IBAction)submitBtnAction:(id)sender;
- (IBAction)countryDropDownSelected:(id)sender;
- (IBAction)pharmacyDetailsSubmitAction:(id)sender;
/**XYView**/
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *pharmacistDeniedInfoCountLbl;
@property (weak, nonatomic) IBOutlet UITableView *pharmacistsTbl;
@property (weak, nonatomic) IBOutlet UIView *topViewBtn;
@property (weak, nonatomic) IBOutlet UIView *viewBotton;
@property (weak, nonatomic) IBOutlet UILabel *pharmacyNameLbl;
@property (nonatomic,strong) NSMutableArray *pharmacistsList;
@property (weak, nonatomic) IBOutlet UIView *scoreView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scoreViewHightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *addedPharmacistsCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPharmacistsCountLbl;

-(void)reloadPharmacistsList;
-(void)addXYDetailsView;
/**Pharmacist**/
@property (nonatomic,strong) NSDictionary *selectedPharmacist;
@property (strong,nonatomic)NSArray *shiftTimingsArray;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNo1TextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNo2TextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNo3TextField;
@property (weak, nonatomic) IBOutlet UITextField *emailNo1TextField;
@property (weak, nonatomic) IBOutlet UITextField *emailNo2TextField;
@property (weak, nonatomic) IBOutlet UITextField *emailNo3TextField;
@property (weak, nonatomic) IBOutlet UITextField *yearOfGragTextField;
@property (weak, nonatomic) IBOutlet UITextField *shiftTimingsTextField;
@property (weak, nonatomic) IBOutlet UITextField *shiftTimingsFromTextField;
@property (weak, nonatomic) IBOutlet UITextField *shiftTimingsToTextField;
@property(weak,nonatomic)IBOutlet UIScrollView *pharmacistScrollView;
@property (weak, nonatomic) IBOutlet UIButton *pharmacistSubmitBtn;

- (IBAction)shiftTimingsDropDownSelected:(id)sender;
-(BOOL)isFinalValidationsOkToPostData;
/**Pharmacist**/
@end
