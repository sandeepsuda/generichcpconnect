//
//  HCPExistingUserViewController.m
//  PfizerHealthConnect
//
//  Created by RajaSekhar on 06/01/17.
//  Copyright © 2017 IMS Health. All rights reserved.
//

#import "HCPExistingUserViewController.h"
#import "TrainingPageViewController.h"
#import "LeftViewController.h"
#import "WelcomePageViewController.h"
#import "NSDictionary+Dictionary.h"

@interface HCPExistingUserViewController ()

@end

@implementation HCPExistingUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.loginButton setBackgroundColor:[appDelegate colorWithHexString:@"25B4FF"]];
    self.loginButton.layer.cornerRadius = 4.0;
    if (_SingleSignOnCheck) {
        self.pictureTextField.hidden = NO;
        self.cameraIcon.hidden = NO;
    }else{
        self.pictureTextField.hidden = YES;
        self.cameraIcon.hidden = YES;
        
        [NSLayoutConstraint activateConstraints: @[
                                                   [self.userNameTextField.heightAnchor constraintEqualToConstant:40.0],
                                                   [self.userNameTextField.centerXAnchor constraintEqualToAnchor:self.subView.centerXAnchor],
                                                   [self.userNameTextField.bottomAnchor constraintEqualToAnchor:self.passwordTextField.topAnchor constant:-25.0],
                                                   [self.passwordTextField.centerXAnchor constraintEqualToAnchor:self.subView.centerXAnchor],
                                                   [self.passwordTextField.bottomAnchor constraintEqualToAnchor:self.loginButton.topAnchor constant:-25.0],
                                                   [self.passwordTextField.heightAnchor constraintEqualToConstant:40.0],
                                                   [self.loginButton.centerXAnchor constraintEqualToAnchor:self.subView.centerXAnchor],
                                                   [self.loginButton.heightAnchor constraintEqualToConstant:45.0],
                                                   [self.subView.heightAnchor constraintEqualToConstant:225.0],
                                                   ]];
        
        self.passwordTextField.translatesAutoresizingMaskIntoConstraints = FALSE;
        self.userNameTextField.translatesAutoresizingMaskIntoConstraints = FALSE;
        self.loginButton.translatesAutoresizingMaskIntoConstraints = FALSE;
        self.subView.translatesAutoresizingMaskIntoConstraints = FALSE;
    }
    
    
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
   CGRect tmpFrame = self.closeBtn.frame;
   self.closeBtn.layer.cornerRadius = tmpFrame.size.height/2;
   self.subView.layer.cornerRadius = tmpFrame.size.height/2;
}


#pragma mark - UIButton Action Methods

-(IBAction)cancelButtonAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)loginButtonAction:(id)sender{
    if (![self.userNameTextField.text  isEqualToString:@""]) {
        if (![self.passwordTextField.text isEqualToString:@""]) {
            if (_SingleSignOnCheck) {
                [self singleSignOnServiceCall];
            }else{
                [self loginServiceMethod];
            }
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please Enter Password." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please Enter Email." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


#pragma mark - ServiceCall Methods

-(void)loginServiceMethod{
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@/%@",KBASE_URL,EXISTING_USER,self.userNameTextField.text,self.passwordTextField.text];
    [self showProgressView];
    [Webservice getWithUrlString:urlString success:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            NSDictionary *userStatus = (NSDictionary*)data;
            if ([[userStatus stringForKey:@"Message"]isEqualToString:@"WRONGCREDENTIALS"]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please enter correct credentials." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                [[NSUserDefaults standardUserDefaults] setObject:self.userNameTextField.text forKey:EMAIL_STRING];
                [[NSUserDefaults standardUserDefaults] setObject:[userStatus stringForKey:@"DCMobileNumber"] forKey:MOBILE_NUMBER_STRING];
                [[NSUserDefaults standardUserDefaults] setObject:[userStatus stringForKey:@"Country"] forKey:@"DCCountry"];
                [self dismissViewControllerAnimated:YES completion:nil];
                if ([self.delegate respondsToSelector:@selector(navigatingToPageVC:)]) {
                    [self.delegate navigatingToPageVC:userStatus];
                }
            }
        }
    } failure:^(NSError *error) {
        //error
        [self.mbProgressView hideAnimated:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:alertAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}
-(NSDictionary *)getRegistrationData{
    NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(self.cameraImage, 0.1)];
    NSDictionary *dict = @{
                           @"EmailId":self.userNameTextField.text,
                           @"Passsword":self.passwordTextField.text,
                           @"ImageData":[imgData base64EncodedStringWithOptions:NSUTF8StringEncoding]
                           };
    return dict;
}

-(void)singleSignOnServiceCall{
    [self showProgressView];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",KBASE_URL,SINGLE_SIGN_ON];
    [Webservice postWithURlString:urlString withParameters:[self getRegistrationData] sucess:^(id data) {
        //Success
        [self.mbProgressView hideAnimated:YES];
        if (data) {
            NSDictionary *userStatus = (NSDictionary*)data;
            if ([[userStatus stringForKey:@"DCID"]isEqualToString:@"0"]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please enter correct credentials." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                [[NSUserDefaults standardUserDefaults] setObject:self.userNameTextField.text forKey:EMAIL_STRING];
                [[NSUserDefaults standardUserDefaults] setObject:[userStatus stringForKey:@"DCMobileNumber"] forKey:MOBILE_NUMBER_STRING];
                [[NSUserDefaults standardUserDefaults] setObject:[userStatus stringForKey:@"DCCountry"] forKey:@"DCCountry"];
                [[NSUserDefaults standardUserDefaults] setObject:[userStatus stringForKey:@"FirstName"] forKey:@"FirstName"];
                [[NSUserDefaults standardUserDefaults] setObject:[userStatus stringForKey:@"LastName"] forKey:@"LastName"];
                [self dismissViewControllerAnimated:YES completion:nil];
                if ([self.delegate respondsToSelector:@selector(navigatingToPageVC:)]) {
                    [self.delegate navigatingToPageVC:userStatus];
                }
            }
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:@"Please enter correct credentials." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        } failure:^(NSError *error) {
            //error
            [self.mbProgressView hideAnimated:YES];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:OOPS_MESSAGE message:SOMETHING_WENT_WRONG_MESSAGE preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }];
}

#pragma mark - Custom Methods

-(void)showProgressView{
    self.mbProgressView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.mbProgressView.mode = MBProgressHUDModeIndeterminate;
    self.mbProgressView.label.textColor = [UIColor whiteColor];
    self.mbProgressView.label.text = LOADING_MESSAGE;
}

-(void)tapGesture:(UITapGestureRecognizer *)recognizer{
    [self.view endEditing:YES];
}
#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField == self.pictureTextField) {
        [self cameraButtonPressed:nil];
        return NO;
    }
    
    return YES;
}
- (IBAction)cameraButtonPressed:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:CLICK_PHOTO_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self callImagePicker:UIImagePickerControllerSourceTypeCamera];
    }];
    
    UIAlertAction *photoRoll = [UIAlertAction actionWithTitle:UPLOAD_IMAGE_MESSAGE style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { [self callImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:CANCEL_STRING style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }];
    [alert addAction:camera];
    [alert addAction:photoRoll];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popPresenter.sourceView = self.pictureTextField;
    popPresenter.sourceRect = self.pictureTextField.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)callImagePicker:(UIImagePickerControllerSourceType)type{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = type;
    [self presentViewController:imagePicker animated:YES completion:NULL];
}

#pragma mark - UIImagePickerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    UIImage *image_answer = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
    self.cameraImage = image_answer;
    self.pictureTextField.text = @"You have successfully uploaded the image.";
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
