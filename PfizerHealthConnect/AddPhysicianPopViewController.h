//
//  AddPhysicianPopViewController.h
//  PfizerHealthConnect
//
//  Created by Sandeep Suda on 18/01/17.
//  Copyright © 2017 IMS Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddPhysicianPopViewControllerDelegate <NSObject>

-(void)openCamera:(id)sender;
-(void)openGallery:(id)sender;
-(void)addNewPhysicianForm;
@end

@interface AddPhysicianPopViewController : UIViewController

@property (weak,nonatomic) id <AddPhysicianPopViewControllerDelegate> delegate;

@end
