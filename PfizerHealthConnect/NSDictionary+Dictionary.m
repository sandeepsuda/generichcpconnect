//
//  NSDictionary+Dictionary.m
//  PharmConnect
//
//  Created by Local user on 18/06/16.
//  Copyright © 2016 Kumar, Utpal (Bangalore). All rights reserved.
//

#import "NSDictionary+Dictionary.h"

@implementation NSDictionary (Dictionary)

-(NSString *)stringForKey:(NSString *)key {
    
    if([self objectForKey:key] == [NSNull null])
        return @""; //If key doesnt exist
    
    NSString *string = [self objectForKey:key];
    if (string == nil) {
        string =@"";
    }
    else if([string isKindOfClass:[NSNumber class]]){
        string =[(NSNumber*)string stringValue];
        
    }
    else if ([string isEqualToString:@"<null>"] ) {
        string = @"";
    }else if ([string isEqualToString:@"(null)"] ) {
        string = @"";
    }else if ([string isEqualToString:@"NULL"] ) {
        string = @"";
    }
    
    return string;
}
@end
